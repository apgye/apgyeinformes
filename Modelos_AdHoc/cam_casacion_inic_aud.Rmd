---
title: Casación Penal - Poder Judicial de Entre Ríos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
params:
  desagregado: FALSE 
---

```{r, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
desagregacion_mensual = T

source("../R/informe.R")
source("../R/duracion.R")
source("../R/poblacion.R")
library(ggpubr)
theme_set(theme_pubr())

# Params

desagregado = params$desagregado

circ = "Concordia"

```

```{r}
start_date = "2022-02-01"
end_date = "2023-01-01"
```


```{r}
iniciados <- iniciadas_pen(db_con = DB_PROD(), poblacion = cam_pen,
                           start_date = start_date, end_date = end_date)

inic_casacion <- iniciados$inic_prim %>% 
  filter(circunscripcion %in% circ) %>% 
  select(nro, caratula, tipo_proceso = tproc, fecha_inicio = finicio, circunscripcion) %>% 
  mutate(caratula = str_sub(caratula, 1,20), 
         año = year(fecha_inicio)) %>% 
  mutate(nro = ifelse(is.na(nro), "sd", nro))
  
  #filter(str_detect(tipo_proceso, "CASACION"))# & circunscripcion == "Paraná")  

inic_casacion_arrange <- inic_casacion %>% 
  select(circunscripcion, año, tipo_proceso, fecha_inicio,caratula, nro) %>% 
  arrange(circunscripcion, año, tipo_proceso, fecha_inicio, caratula, nro) 

  
```


```{r}
audienciascam <- DB_PROD() %>% apgyeDSL::apgyeTableData('AUDIPC') %>% 
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep %in% !!cam_pen$organismo) %>% 
  collect() %>% 
  filter(esta == "2" & iep == "campen0000pna") %>% 
  mutate(fea = dmy(fea)) 

audic <- audienciascam %>% 
  select(nro, tipo_proceso = tproc, fecha_audiencia = fea)


```


```{r}
consolidado <- inic_casacion_arrange %>% 
  left_join(audic) %>% 
  mutate(evaldates = fecha_audiencia > fecha_inicio) %>% 
  select(-evaldates)

casaciones <- consolidado %>% 
  filter(str_detect(tipo_proceso, "CASACION")) #%>%  & circunscripcion == "Paraná")  %>% 
  

# write.table(casaciones, "~/apgyeinformes/Modelo_Parametrizado/data/casaciones_iniciadasyaudiencias.csv", 
#             col.names = T, row.names = F, sep = ",")
# 
# write.table(consolidado, "~/apgyeinformes/Modelo_Parametrizado/data/legajostotales_iniciadosyaudiencias.csv", 
#             col.names = T, row.names = F, sep = ",")

```

## Casaciones Iniciadas y con Audiencia en el período `r getDataIntervalStr(start_date, end_date)`

Presentamos a continuación, a pedido de la Oficina Provincial de Coordinación y Control de Gestión de OGAs, listado de *RECURSOS DE CASACION* iniciados en la Cámara de Casación de `r circ` en el período `r getDataIntervalStr(start_date, end_date)`. 

El listado está ordenado por: circunscripcion, año, tipo_proceso y fecha_inicio para mantener la cronología de los registros. 

Agregamos también la fecha de audiencia realizada en aquellos casos donde se informó dicho evento.  

Cuando no se consignan datos debe entenderse que no hay registro informado por el organismo.   

Agregamos también listado completo de legajos iniciados con la misma información detallada precedentemente.   

Finalmente, destacamos que no se dispone de datos de audiencia proyectadas para períodos futuros.       

<!-- Se remiten adjuntos archivos *.csv*.    -->

\blandscape

```{r}
casaciones %>% 
  #mutate(tipo_proceso = str_remove_all(tipo_proceso, "RECURSO DE")) %>% 
  kable(caption = str_c("Casaciones Iniciadas y Audiencias"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c',
        longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  %>%
  #column_spec(3, "4cm") %>% column_spec(11, "2cm") %>%  
  row_spec(0, angle = 90) 

```

\pagebreak

```{r}
consolidado %>% 
  #mutate(tipo_proceso = str_remove_all(tipo_proceso, "RECURSO DE")) %>% 
  kable(caption = str_c("Legajos Totales Iniciados y Audiencias"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c',
        longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  %>%
  #column_spec(3, "4cm") %>% column_spec(11, "2cm") %>%  
  row_spec(0, angle = 90) 

```


\elandscape