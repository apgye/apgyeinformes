---
title: OGA - Familia 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')

source("../R/informe.R")
source("../R/habiles.R")
source("../R/poblacion.R")

start_date = "2022-02-01"
end_date = "2022-06-01"

salones = F

```


```{r,  eval= salones, echo=FALSE, include=FALSE}
source("../R/informe.R")
df <- read_csv("~/apgyeinformes/Modelo_Parametrizado/data/salones_agosto.csv")

df <- df %>%
  mutate(organismos = toupper(str_remove_all(organismos, ' ')))
```

```{r, eval= salones, echo=FALSE, fig.height = 12, fig.width = 18, fig.align = "center"}
df %>%
  #pivot_longer(cols = c(Audiencias_Presenciales, Videoconferencias), names_to = "tipo_audiencia", values_to = "duraciones") %>%
  pivot_longer(cols = c(audiencias), names_to = "tipo_audiencia", values_to = "duraciones") %>%
  ggplot(aes(x = as.factor(salones), y = duraciones, fill = organismos)) +
  geom_bar(stat = 'identity') +
  scale_fill_viridis_d() +
  labs(title = "Ocupación de Salones por Organismo",
       subtitle = 'Los salones se indentifican por número',
       x = 'salones', y = 'horas')
```

## Cantidad de audiencias según sus estdos finales `r getDataIntervalStr(start_date, end_date)`

Destacamos que las diferencias entre los totales de audiencia según sus estados puede estar asociada a la diferencia en días entre que se registran las fechas de audiencia y la fecha en que éstos registros asumen sus estados finales. Asimismo, muchas audiencias que se registran en un período pueden completarse en períodos siguientes. Finalmente, en menor proporción, hemos observado la falta de registración de ciertos estados (ej. 'audiencia fijada') en pos de la registración concentrada de las audiencias realizadas.      

```{r}
start_date = "2022-02-01"
end_date = "2022-06-01"

df = audifam_prim(DB_PROD(), poblacion = jdos_fam, start_date, end_date)

df %>% 
  audifam_xestado(desagregacion_mensual = F) %>% 
   kable(caption = str_c("Audiencias segun sus estados finales",
                        " (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE, booktabs = T) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 11)  %>% 
  row_spec(0, angle = 90) %>%
  landscape()



```




