---
title: Resoluciones en Tribunal de Juicio y Apelación
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')

source("../R/informe.R")
source("../R/habiles.R")
source("../R/poblacion.R")

start_date = "2022-05-01"
end_date = "2023-05-01"
desagregacion_mensual = T

circ = "Gualeguaychú"

tja = tja %>% filter(circunscripcion %in% circ)

```


## Tribunal de Juicio - `r getDataIntervalStr(start_date, end_date)`

```{r, pen_tja_res_intro,   results='asis', eval=FALSE}
#ej1:text <- "this is  \nsome  \ntext"
#cat(text)
cat("En esta tabla usted puede consultar las resoluciones por integración. Se agrega al final de la tabla el dato de personas sobreseidas, condenadas y absueltos según el registro de justiciables efectuado en cada legajo. Finalmente, la fila total muestra la suma de resoluciones.")
```


```{r, tjui,  echo = FALSE, message = FALSE, warning = FALSE, eval= FALSE}
resol_tja <- resoluciones_pen(db_con = DB_PROD(), poblacion = tja,
                                start_date = start_date, end_date = end_date,
                                desagregacion_mensual = desagregacion_mensual)
resol_tja$tjui %>%  
    outputTable(caption = str_c("Resoluciones"," (", getDataIntervalStr(start_date, end_date), ")")) %>%
    add_header_above(c(" " = 4, "Resoluciones" = 7, "Cuenta Personas" = 3)) %>%
    row_spec(0, angle = 90) %>%
    column_spec(3, "12cm") %>%
    column_spec(c(4,11), border_right = T) %>%
    column_spec(4, background = "#BBBBBB") %>%
    kable_styling(font_size = 11) %>%
    landscape()
```


```{r}
resultado <- resumir_audip_tja(db_con = DB_PROD(), tja, start_date = start_date,
                  end_date = end_date, desagregacion_mensual = F)
```

## Audiencias Penales: Resultados resumidos del período `r getDataIntervalStr(start_date, end_date)`

<!-- En esta tabla se puede ver la cantidad total y promedio mensual de audiencias realizadas y la cantidad total y promedio mensual de horas en audiencia. Además, agregamos las cantidades de audiencias discriminadas según los tipos relevados. Si la cantidad de Tipos de Audiencia supera el número de diez (10) se mostrará en una Tabla separada.     -->

```{r, pen_tja_resumen_RealizacionesTipos}

con_tipos <- T
if(length(resultado) > 0){
  columnasTipos <- length(resultado)-8
  if(length(resultado) > 18){
    con_tipos <- F
  }
}

  resultado %>%
    rename_with(~ str_c(., "*"), starts_with("promedio")) %>%
    select(-minutos_en_audiencia) %>%
    kable(caption = str_c("Audiencias Penales Realizadas, Horas en Audiencia y Tipos de Audiencia",
                          " (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE, booktabs = T) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 9)  %>% 
    add_header_above(c(" " = 3, "Realizaciones" = 4, "Tipos de audiencia" = columnasTipos)) %>%
    kable_styling(latex_options = c("repeat_header")) %>% 
    column_spec(2, "4cm") %>% 
    column_spec(c(3,7), border_right = T) %>%
    row_spec(0, angle = 90) %>%
    footnote(general = "Los promedios se calculan como: total / cantidad de meses con información",
           general_title = "*:", footnote_as_chunk = T, title_format = c("bold")) %>%
    landscape()

 
```

## Audiencias fijadas por integración/juez

```{r}
source("~/apgyeinformes/R/load_datamarts.R")

df = datamart_penal$audiencias_fijadas_tribunalJA %>% 
  filter(ffijacion >= start_date, ffijacion < end_date) %>% 
  filter(circunscripcion == "Gualeguaychú")

poblacion_infte = oga %>% filter(circunscripcion == "Gualeguaychú")


jvact_final <- df %>% 
        select(iep, jvact) %>%
        dplyr::mutate(rn = row_number()) %>% separate_rows(jvact) %>%
        left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                    mutate(
                      magistrado = apellido,
                      iep_actuante = iep
                    ) %>%
                    select(idagente, iep_actuante, magistrado), by=c("jvact"="idagente"), copy = T) %>%
        group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
        left_join(poblacion_infte, by=c("iep"="organismo") ) %>%
        ungroup() %>%
        #mutate(magistrado = paste0(magistrado, ' (', circunscripcion, ')')) %>% # traducir código html
        .$magistrado
      
df$integracion <- jvact_final

df %>% 
  group_by(integracion) %>% 
  summarise(cantidad_aud_fijadas = n()) %>% 
  filter(integracion != "NA") %>% 
  arrange(desc(cantidad_aud_fijadas)) %>% 
  kable(caption = str_c("Audiencias Fijadas según Integración",
                        "(", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"),
        align = 'c', longtable = TRUE, booktabs = T) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 12)  
```

## Audiencias y Demora por integración/juez

En la tabla se incluyen todos los casos de audiencias realizadas con demora informados por OGA de la circunscripción.    

```{r}
df_demora = datamart_penal$df_audic_demora_TJ %>% 
  filter(circunscripcion %in% circ)

df_demora = df_demora %>% 
  mutate(tipo_demora = as.factor(tipo_demora)) %>% 
  group_by(jvact_final, tipo_demora, .drop = F) %>% 
  summarise(cantidad = n()) %>% ungroup() %>% 
  rename(integracion = jvact_final) %>% 
  arrange(integracion, cantidad)

df_demora %>% 
   kable(caption = str_c("Audiencias y Demora",
                        "(", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"),
        align = 'c', longtable = TRUE, booktabs = T) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 12)  

```

\pagebreak

## Audiencias No Realizadas

Incluyen Canceladas, Fracasadas y Reprogramadas, con referencia a sus motivos. 

```{r}

oga <- apgyeJusEROrganization::listar_organismos() %>%
  filter(stringr::str_detect(fuero, "Penal")) %>% 
  filter(stringr::str_detect(organismo, "oga|jez")) %>% 
  filter(circunscripcion == "Gualeguaychú")


poblacion_infte <- oga %>% 
  filter(circunscripcion %in% unique(oga$circunscripcion))


df_audic_raw_TJ <- try(DB_PROD() %>%
        apgyeDSL::apgyeTableData('AUDIPTJ') %>%
        apgyeDSL::interval(start_date , end_date) %>%
        filter(iep %in% !!poblacion_infte$organismo) %>%
        mutate(nro = ifelse(is.na(nro), "sin_dato", nro)) %>% 
        mutate(iep = ifelse(grepl("\\/S|\\/s", nro), "ogapen0000ssa", iep)) %>% 
        group_by(iep, data_interval_start, data_interval_end) %>% 
        #process() %>% .$result %>% ungroup() %>%
        collect() %>% #.$result %>% ungroup() %>%
        mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m")) %>%
        left_join(poblacion_infte %>% select(organismo, circunscripcion), by=c("iep"="organismo")) %>%
        select(circunscripcion, año_mes, everything()))


df_audic_raw_TJ <- df_audic_raw_TJ %>% 
  filter(estaud != "2") 

df_audic_raw_TJ <- df_audic_raw_TJ %>% 
  mutate(audiencia_estados = case_when(
    estaud == "3" ~ "Audiencia_Fracasada" ,
    estaud == "4" ~ "Audiencia_Cancelada" ,
    estaud == "5" ~ "Audiencia_Reprogramada" ,
    TRUE ~ "sin_dato")) %>% 
  rename(motivos_demora = tdemora) %>% 
  filter(motivos_demora != "NA") %>%
  mutate(tipo_demora = str_trim(motivos_demora, side = "both")) %>% 
  mutate(tipo_demora = case_when(
    tipo_demora =='1' ~ 'Sobreextensión audiencia anterior', 
    tipo_demora =='2' ~ 'Defensor Oficial', 
    tipo_demora =='3' ~ 'Defensor Particular', 
    tipo_demora =='4' ~ 'Fiscal.', 
    tipo_demora =='5' ~ 'Querellante y/o Act.C.', 
    tipo_demora =='6' ~ 'Ministerio Pupilar.', 
    tipo_demora =='7' ~ 'Imputado', 
    tipo_demora =='8' ~ 'Traslado del Detenido', 
    tipo_demora =='9' ~ 'Juez.', 
    tipo_demora =='10' ~ 'Problemas técnicos.', 
    tipo_demora =='11' ~ 'Otros', 
    tipo_demora =='12' ~ 'Acuerdo de Partes',
    TRUE ~ "sin_dato/no_aplica")) %>% 
  rename(motivos_no_realizacion = tipo_demora)

df_audic_TJ = df_audic_raw_TJ %>% 
  group_by(iep, jvact, motivos_no_realizacion) %>% 
  summarise(cantidad_casos = n())

jvact_final <- df_audic_TJ %>% as.data.frame() %>%
      select(iep, jvact) %>%
      dplyr::mutate(rn = row_number()) %>% separate_rows(jvact) %>%
      left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
                  mutate(
                    magistrado = paste0(apellido),
                    iep_actuante = iep
                  ) %>%
                  select(idagente, iep_actuante, magistrado), by=c("jvact"="idagente"), copy = T) %>%
      group_by(iep, rn) %>% dplyr::summarise(magistrado = paste(magistrado, collapse = " - ")) %>%
      left_join(poblacion_infte, by=c("iep"="organismo") ) %>%
      ungroup() %>% .$magistrado

df_audic_TJ$jvact_final <- jvact_final


df_audic_TJ %>% ungroup() %>% 
  select(integracion = jvact_final, motivos_no_realizacion, cantidad_casos, -iep, -jvact) %>% 
  kable(caption = str_c("Audiencias No Realizadas",
                        "(", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"),
        align = 'c', longtable = TRUE, booktabs = T) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 12)  

```


