---
title: "Listado de Legajos en Mediación"
author: "Área de Planificación Gestión y Estadística"
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output:
  html_document:
    df_print: paged
  pdf_document:
    includes:
      in_header: header.tex
      before_body: before_body.tex
  word_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(message=FALSE, warning=FALSE, echo=FALSE)
options(knitr.kable.NA = 'sd')

source("../R/informe.R")
source("../R/habiles.R")
source("../R/poblacion.R")

pob_mediacion <- oma %>% filter(tipo == "oma")


```


# Informe de Legajos OMA: Iniciados y Acuerdos años 2020-2021


A través del presente se remite adjuntos bases de datos completas de los legajos iniciados y legajos con Acuerdos en OMA desde el año 2020 a la fecha.      

Estas bases permiten identificar los legajos iniciados y legajos con Acuerdo de Partes en cada jurisdicción desde 1 de febrero 2020 al 31 de agosto de 2022.    

Cabe resaltar que la información de los funcionarios de OMA en el período analizado tuvo diversas transformaciones en materia de registración y conteo, originadas en las dificultades que éstos presentan en la gestión de su información debido a compartir herramientas de registro con otros funcionarios. Esta situación fue resaltada en la Auditoría de Datos solicitada por el Dr. Juarez y contestada por el Área de Estadística el 3 de septiembre 2021. Auditoría cuyas mejoras propuestas fueron implementadas y dieron origen al actual sistema de registro y conteo aprobado por la Sala Penal de STJER.  

Estas particularidades que presenta la información de OMA plantea la conveniencia de separar el conteo y la información primaria a los fines de mantener la coherencia en los datos procesados. En tal sentido, se adjuntan al presente información primaria consolidada de los legajos recibidos por el área y procesados para el cómputo de indicadores:      

- *iniciados_año_20_21.csv*: contiene los legajos iniciados durante los años 2020 y 2021, con datos identificatorios del legajo (nro, caratula, tipo de proceso y mediador) y datos complementarios (e.g. "reingreso" TRUE/FALSE indica aquellos registros con multiples ingresos en el período analizado).     
- *acuerdos_año_21*: contiene el datalle de los legaos informados con acuerdo de partes con la correspondiente fecha, entre febrero y octubre 2021.    
- *acuerdos_año_21_nov-dic*: contiene el datalle de los legajos informados con acuerdo de partes con la correspondiente fecha, de noviembre y diciembre año 2021. En este período, por disposición del Director de OMA Dr. Rodrigo Juarez entró en vigencia la clasificación de audiencias por tipo, según se trate de: Audiencia de Mediación Conjunta (aquella celebrada con ambas partes y donde es posible lograr un acuerdo de mediación) o Audiencia Preparatoria (aquella realizada con una parte interviniente en el legajo donde se realizan gestiones previas tendientes a celebrar la Audiencia de Mediación Conjunta). 
- *acuerdos_año_21*: contiene período febrero 2021 a octubre 2021, conforme las definiciones precedentes. 
- *iniciados_acuerdos_2022*: contiene listado consolidado de legajos iniciados y legajos con acuerdos desde abril 2022 a agosto 2022.     

Para toda la información precedente se pone a disposición copia de las bases de datos completas (sin consolidar) del organismo.     

Respecto de la metodología de registración y conteo vigente cabe resaltar que la información suministrada por cada mediador permite responder parcialmente a la consulta 2) con las siguientes observaciones:   

- el datos del punto "i" relativo a "cumplimiento/incumplimiento" dato no esta previsto en el sistema, por lo que no está disponible en la base, 
- el dato del punto "e" relativo a "fiscal interviniente" es un dato **no normalizado** pues los funcionarios informantes (y sus sistemas) no emplean denominación univoca para la identificación del fiscal. Sin perjuicio de ello se pone a disposición la información asociada al Ministerio Público en los campos **fiscal y fiscalia** obtenido de los registros informados.   
- agregamos dos columnas que contienen la fecha y descripción de otros movimientos registrados en el legajo que no son "Acuerdos".   

Respecto de la cantidad de legajos en trámite en poder de cada mediador, el dato no está previsto en el sistema por lo que no se puede informar. Sin perjuicio de ello a los fines de disponer de dicha información se puede solicitar a todos los mediadores de la provincia la presentación de un nuevo listado de estadística vinculado a los **legajos en trámite**. Para hacer efectiva esa nueva presentación de información y conforme lo conversado con usted es preciso:    

- que los mediadores se encuentren trabajando en una "solapa" independiente conforme el uso de OMA Paraná, 
- que se cree un modelo de listado particular con las variables requeridas por la Sala Penal del STJER y registradas por los funcionarios de mediación en sus respectivos LEX. La información resultante solo recogerá información ya registrada. La carga retroactiva debe ser indicada en caso de no disponer de información pasada. 

Sin otro particular saludo a ud. atte.   

\pagebreak

# Sistema de registro e indicadores anteriores de feb-2020 a ocutbre-2021

```{r}
# vieja función 
inic_20 = iniciados_oma_old(db_con = DB_PROD(), poblacion = pob_mediacion,
                           start_date =  "2020-02-01",
                           end_date = "2021-01-01") 
  
inic_21 = iniciados_oma_old(db_con = DB_PROD(), poblacion = pob_mediacion,
                           start_date =  "2021-02-01",
                           end_date = "2022-01-01") 


inic_20_21 <- inic_20$inic_oma_pc %>% 
  bind_rows(inic_21$inic_oma_pc) 

# marco duplicados
inic_20_21$reingreso =  duplicated(inic_20_21$nro_om)

# consolido
inic_20_21  = inic_20_21 %>% 
  select(circunscripcion, mediador = resp , año_mes, everything(), -c(justiciable, vincul, 
          pr_ed_sex, id_submission, iep, data_interval_end, estado, organismo)) %>%
  distinct(circunscripcion, mediador, nro_om, data_interval_start, .keep_all = T) %>% 
  group_by(circunscripcion, mediador, nro_om) %>% 
  filter(data_interval_start == max(data_interval_start)) %>%   # Gualeguaychú GARAY A740/22
  select(-data_interval_start) %>% 
  arrange(circunscripcion, mediador, año_mes)


write.table(inic_20_21, "~/apgyeinformes/Modelos_AdHoc/data/inicados_año_20_21.csv", row.names = F, col.names = T, sep = ";", na = "")

```

## Casos iniciados por mediador en los años 2020-2021

En caso de múltiples ingresos se informa el último registro con sus correspondientes movimientos (se encontraron `r sum(inic_20_21$reingreso)`). 

```{r}
inic_20_21 %>% 
  group_by(circunscripcion, mediador) %>% 
  summarise(cantidad_iniciados = n()) %>% 
  janitor::adorn_totals("row") %>% 
  kable()

```

\pagebreak

## Audiencias Realizadas con Acuerdos de Partes

```{r}

audip_20 <- apgyeProcesamiento::audienciasPenales(db_con = DB_PROD(),
                                                   poblacion = oma, 
                                                   start_date = "2020-02-01", 
                                                   end_date = "2020-11-01", 
                                                   desagregacion_mensual = T) 

audic_oma_prim_2020 = audip_20$preNov20$audic_oma_prim %>% 
  mutate(realizadas_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
           str_detect(tr, "realizad") & t1 == "1") %>% 
  mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m")) %>%
  filter(f2 >= data_interval_start, f2 < data_interval_end) %>% 
  rename(fraud = f2, estaud = t1, ra = t2, vmed = t3, tmed = t4, 
         duracm = n1, duracb = n2) %>%
  #codconver("IGMP", "estaud") %>%
  codconver("IGMP", "ra") %>%
  codconver("IGMP", "vmed") %>%
  codconver("IGMP", "tmed") %>% 
  mutate(resp = ifelse(is.na(resp), "sin dato", resp)) %>% 
  filter(ra == "Con Acuerdo" ) %>% 
  rename(resultado_audiencia = ra) %>% 
  select(circunscripcion, mediador = resp, año_mes, nro_om, caratula, tproc, 
         fiscalia = partp, fiscm, fecha_realizacion_aud =fraud, resultado_audiencia)


audip_20_nov_dic <- apgyeProcesamiento::audienciasPenales(db_con = DB_PROD(),
                                                   poblacion = oma, 
                                                   start_date = "2020-11-01", 
                                                   end_date = "2021-01-01", 
                                                   desagregacion_mensual = T) 


audic_oma_prim_2020_nov_dic = audip_20_nov_dic$postNov20$audic_oma_prim %>% 
  mutate(conjuntas_conAcuerdo = ((f2 >= data_interval_start & 
                                   f2 < data_interval_end) &
                                   str_detect(tr, "realizad") &
                                   t1 == "1" &
                                   t2 == "1")) %>% 
  mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m")) %>% 
  filter((f2 >= data_interval_start & 
           f2 < data_interval_end) &
           str_detect(tr, "realizad") &
           t1 == "1") %>%
  rename(fraud = f2, estaud = t1,N20_ra = t2, N20_vmed = t3, N20_tmed = t4,
         duracm = n1, duracb = n2) %>%
  apgyeProcesamiento::codconver("IGMP", "N20_ra") %>%
  apgyeProcesamiento::codconver("IGMP", "N20_vmed") %>%
  apgyeProcesamiento::codconver("IGMP", "N20_tmed") %>%
  mutate(resp = ifelse(is.na(resp), "sin dato", resp)) %>%
  rename(ra = N20_ra, vmed = N20_vmed, tmed = N20_tmed) %>% 
  filter(ra == "Con Acuerdo" ) %>% 
  rename(resultado_audiencia = ra) %>% 
  select(circunscripcion, mediador = resp, año_mes, nro_om, caratula, tproc, 
         fiscalia = partp, fiscm, fecha_realizacion_aud =fraud, resultado_audiencia)
  
  

audip_21 <- apgyeProcesamiento::audienciasPenales(db_con = DB_PROD(),
                                                   poblacion = oma, 
                                                   start_date = "2021-02-01", 
                                                   end_date = "2022-1-01",  
                                                   desagregacion_mensual = T)


audic_oma_prim_2021 = audip_21$postNov20$audic_oma_prim %>% 
  mutate(conjuntas_conAcuerdo = ((f2 >= data_interval_start & 
                                   f2 < data_interval_end) &
                                   str_detect(tr, "realizad") &
                                   t1 == "1" &
                                   t2 == "1")) %>% 
  mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m")) %>% 
  filter((f2 >= data_interval_start & 
           f2 < data_interval_end) &
           str_detect(tr, "realizad") &
           t1 == "1") %>%
  rename(fraud = f2, estaud = t1,N20_ra = t2, N20_vmed = t3, N20_tmed = t4,
         duracm = n1, duracb = n2) %>%
  apgyeProcesamiento::codconver("IGMP", "N20_ra") %>%
  apgyeProcesamiento::codconver("IGMP", "N20_vmed") %>%
  apgyeProcesamiento::codconver("IGMP", "N20_tmed") %>%
  mutate(resp = ifelse(is.na(resp), "sin dato", resp)) %>%
  rename(ra = N20_ra, vmed = N20_vmed, tmed = N20_tmed) %>% 
  filter(ra == "Con Acuerdo" ) %>% 
  rename(resultado_audiencia = ra) %>% 
  select(circunscripcion, mediador = resp, año_mes, nro_om, caratula, tproc, 
         fiscalia = partp, fiscm, fecha_realizacion_aud =fraud, resultado_audiencia)


write.table(audic_oma_prim_2020, "~/apgyeinformes/Modelos_AdHoc/data/acuerdos_año_20.csv", row.names = F, col.names = T, sep = ";", na = "")

write.table(audic_oma_prim_2020_nov_dic, "~/apgyeinformes/Modelos_AdHoc/data/acuerdos_año_20_nov-dic.csv", row.names = F, col.names = T, sep = ";", na = "")

write.table(audic_oma_prim_2021, "~/apgyeinformes/Modelos_AdHoc/data/acuerdos_año_21.csv", row.names = F, col.names = T, sep = ";", na = "")


```

### Feb2020-Oct2020

```{r}
audip_20$preNov20$audic_resultado_acsac %>% select(1:4) %>% 
  kable(longtable = TRUE) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 10)  %>% 
  kable_styling(latex_options = c("repeat_header")) 

```

### Nov2020-Dic2020

```{r}
audip_20_nov_dic$postNov20$audic_resultado_acsac %>% select(1:3) %>% 
  kable(longtable = TRUE) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 10)  %>% 
  kable_styling(latex_options = c("repeat_header")) 
```
### Feb2021-Oct2021

```{r}

audip_21$postNov20$audic_resultado_acsac %>% select(1:3) %>%  
  kable(longtable = TRUE) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 10)  %>% 
  kable_styling(latex_options = c("repeat_header")) 

```

# Sistema de información de transición nov-2021 a marzo-2022

Información procesada reportada por cada mediador vía formulario online (Google Drive).    
  
```{r}
oma_form =  "https://docs.google.com/spreadsheets/d/1hqIs5nAbvKEbAFGMcNTOmgKef6wLvjfDb7U7vZm-mnk/edit?usp=sharing"
text <- gsheet::gsheet2text(url = oma_form, format = 'tsv')
df <- readr::read_tsv(file = text, col_names = T)

df %>% select(6,3,5,10) %>% 
  rename(audiencias_con_acuerdo = 4) %>% 
  arrange(Circunscripción) %>% 
  kable(longtable = TRUE) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 10)  %>% 
  kable_styling(latex_options = c("repeat_header")) 

```

\pagebreak

# Sistema de registro e indicadores vigentes desde abril-2022

```{r}

start_date = "2022-04-01"
end_date = "2022-09-01"

pob_mediacion <- oma %>% filter(tipo == "oma")


resultado <- iniciados_oma(db_con = DB_PROD(), poblacion = pob_mediacion,
                           start_date = start_date,
                           end_date = end_date) 

primaria  <- resultado$primaria_preprocesada_cfiscal %>%
  select(circunscripcion, apellido, nro_om, caratula, tproc, fiscal = fiscm , partp, fiscm, fecha, mov_grupo, descripcion) %>% 
  tidyr::separate(partp, into = c("fiscalia", "n", "l", "o"), sep="\\$") %>%
  select(-c(n, l, o)) %>% 
  arrange(circunscripcion, apellido, caratula, desc(fecha))


#write.table(primaria, "~/apgyeinformes/Modelos_AdHoc/data/lagajos_OMA.csv", row.names = F, col.names = T, sep = ";", na = "")
inic_acuerdo <- primaria %>% 
  filter(str_detect(descripcion, "^00|^33|^83") & !str_detect(descripcion, "FISCAL"))

inic_otros_mov <- primaria %>% 
  filter(!str_detect(descripcion, "^00|^33|^83")) %>% 
  group_by(circunscripcion, apellido, nro_om) %>% 
  filter(fecha == max(fecha)) %>% 
  select(circunscripcion, apellido, nro_om, fecha_otros_mov = fecha, descripcion_otros_mov = descripcion)

         
iniciados <- inic_acuerdo %>% 
  filter(str_detect(descripcion, "^00")) %>% 
  distinct(circunscripcion, apellido, nro_om, fecha, .keep_all = T) %>% 
  group_by(circunscripcion, apellido, nro_om) %>% 
  filter(fecha == max(fecha)) %>% # Gualeguaychú GARAY A740/22
  mutate(fecha_inicio = fecha) %>% 
  select(-c(fecha, mov_grupo, descripcion))
 
  
acuerdos <- inic_acuerdo %>% 
  filter(str_detect(descripcion, "^33|^83")) %>% 
  distinct(circunscripcion, apellido, nro_om, fecha, .keep_all = T) %>% 
  group_by(circunscripcion, apellido, nro_om) %>% 
  #summarise(cantidad = n()) %>% View()
  filter(fecha == max(fecha)) %>% # Gualeguaychú GARAY A740/22
  mutate(fecha_acuerdo = fecha) %>% 
  select(-c(fecha, mov_grupo, descripcion))
  
  

# nueva columna con último movimiento completar los vacíos.

consolidado <- iniciados %>% 
  full_join(acuerdos, by = c("apellido", "nro_om", "circunscripcion", 
                             "caratula", "fiscal", "fiscalia", "tproc")) 

inic_sin_acu <- consolidado %>% 
  filter(is.na(fecha_acuerdo)) %>% .$nro_om

inic_otros_mov = inic_otros_mov %>% 
  filter(nro_om %in% inic_sin_acu)

consolidado <- consolidado %>% 
  left_join(inic_otros_mov,  by = c("apellido", "nro_om", "circunscripcion"))


  
```

## Resumen de resultados

## Casos con código INICIO (00) y ACUERDO (33|83) por circunscripción `r getDataIntervalStr(start_date, end_date)`

```{r}
inic_acuerdo %>% 
  group_by(circunscripcion) %>% 
  summarise(cantidad_iniciados = n()) %>% 
  arrange(desc(cantidad_iniciados)) %>% 
  janitor::adorn_totals("row") %>% 
  kable()
```
\pagebreak

## Casos iniciados por mediador en el período `r getDataIntervalStr(start_date, end_date)`

En caso de múltiples ingresos se informa la fecha del último registro.

```{r}
iniciados %>% 
  group_by(circunscripcion, apellido) %>% 
  summarise(cantidad_iniciados = n()) %>% 
  janitor::adorn_totals("row") %>% 
  kable()

```
\pagebreak

## Casos con Acuerdo por mediador en el período `r getDataIntervalStr(start_date, end_date)`

En caso de múltiples registros con Acuerdo se informa la fecha del último registro.

```{r}
acuerdos %>% 
  group_by(circunscripcion, apellido) %>% 
  summarise(cantidad_acuerdos = n()) %>% 
  janitor::adorn_totals("row") %>% 
  kable()

```

```{r}
write.table(consolidado, "~/apgyeinformes/Modelos_AdHoc/data/iniciados_acuerdos_2022.csv", row.names = F, col.names = T, sep = ";", na = "")
```


```{r}
# Auditoría interna
resultadoproc = resultado$mov_mes %>% 
  filter(str_detect(descripcion, "^INIC|-ACUERDO|ACUERDO CON")) %>% 
  group_by(circunscripcion, apellido, descripcion) %>% 
  summarise(cantidad = sum(cant, na.rm = T))

```


```{r}
# Auditoria abril y mayo Paran

primaria_Parana = primaria %>% 
  mutate(año_mes = str_sub(fecha, 1,7)) %>% 
  filter(circunscripcion == "Paraná") %>% 
  group_by(año_mes, apellido, descripcion) %>% 
  summarise(cantidad = n())
  
  
```

