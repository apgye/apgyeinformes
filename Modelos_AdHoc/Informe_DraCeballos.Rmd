---
title: "Informe Sra.Vocal Dra. Norma Viviana Ceballos"
author: ""
date: "2022-06-13"
output: pdf_document
---

```{r setup, include=FALSE}
source("/home/scastillo/apgyeinformes/R/informe.R")
source("/home/scastillo/apgyeinformes/R/poblacion.R")
```

# Actuación Dra. Norma Viviana Ceballos en la Cámara Segunda Civil y Comercial, de Paraná

En la siguiente tabla se presenta la cantidad de primeros votos emitidos por la Vocal Dra. Norma Viviana Ceballos entre el 1 de diciembre de 2019 y mayo de 2022. Adjuntamos al final del documento Anexo 1 listado de procesos y sus respectivas fechas de resolución.   

```{r, message=FALSE, warning=FALSE, echo=FALSE}
resultado <- DB_PROD() %>%
  apgyeTableData("CADR2C") %>%
  apgyeDSL::interval("2019-12-01", "2022-05-01") %>%
  filter(iep %in% !!cam_civil$organismo) %>%
  resolverconvertidos() %>%
  mutate(fres = dmy(fres), fvenc1 = dmy(fvenc1), feg1 = dmy(feg1), fdesp1 = dmy(fdesp1)) %>%
  collect() %>%
  filter(tres != "0") %>%  # validacion medida mej.prov
  filter(!(is.na(nro) & is.na(caratula))) %>% 
  filter(!is.na(feg1)) %>%
  filter(str_detect(ordv, "^821")) %>% 
  mutate(devol_en_termino = feg1 <= fvenc1) %>% 
  select(nro, caratula, tproc, as, fecha_despacho = fdesp1, fecha_devolucion = feg1,
         fecha_vencimiento = fvenc1, fecha_resolucion = fres, devol_en_termino) %>% 
  mutate(primer_voto_en_termino = ifelse(devol_en_termino, "si", "no")) %>% 
  arrange(desc(primer_voto_en_termino))

resumen <- resultado %>% 
  group_by(primer_voto_en_termino) %>% 
  summarise(cantidad = n()) %>% 
  arrange(desc(cantidad))

resumen_pp <- resumen %>% 
  janitor::adorn_percentages("col") %>%  
  mutate_if(is.numeric, list(~ . * 100)) %>% 
  mutate_if(is.numeric, round, digit = 1) %>% 
    select(-primer_voto_en_termino) %>% 
    rename_all(~paste0(., "_%"))

resumen %>% 
  bind_cols(resumen_pp) %>% 
  kable(caption = "Dra. Norma Viviana Ceballos: primeros votos emitidos en relación al plazo legal de resolución", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),full_width = F, font_size = 10)  
```

\pagebreak


# Listado de Procesos y Resoluciones con primer voto Dra. Norma Viviana Ceballos - Camara Segunda  de Paraná-

```{r, message=FALSE, warning=FALSE, echo=FALSE}
resultado %>%   select(-tproc, -devol_en_termino) %>% mutate(caratula = str_sub(caratula, 1, 10)) %>% 
  kable(caption = "Listado de causas con primer voto Dra. Norma Viviana Ceballos", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),full_width = F, font_size = 10) %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```




