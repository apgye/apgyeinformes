---
title: Informe de Operaciones de lectura directa de base de datos
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output:
  pdf_document:
    includes:
      in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
informe_inicio <- Sys.time()
```

```{r instancias}
library(readr)
library(lubridate)
source("../R/informe.R")
source("../R/duracion.R")
source("../R/poblacion.R")
```

```{r parametros}

db_con <- DB_PROD()
# Perídodo
start_date <- "2022-02-01"
end_date <- "2023-10-01"

# presentaciones del 2023

# esto genera las presentaciones_justat
apgyeRStudio::listar_presentaciones(db_con, v = F)
present2023 <- presentaciones_justat %>% 
  filter(grepl("2023", data_interval_start))

# table(present2023$data_interval_start)

operaciones_actuales <- apgyeJusEROrganization::listar_operaciones_por_organismo() %>% 
  filter(is.na(finalizacion))

# operaciones activas por BD
op_act_Act <- operaciones_actuales %>% 
  filter(read_db)

# operaciones no activadas aún por BD
op_act_NoAct <- operaciones_actuales %>% 
  filter(!read_db)

```

### `r length(unique(op_act_Act$operacion))` Operaciones _activas_ de lectura de base de datos

```{r op_activas_1}

op_act_Act %>%
  select(organismo, operacion) %>%
  distinct() %>% 
  group_by(operacion) %>%
  summarise(cant = n(),
            'detalle de organismos' = str_c(unique(organismo), collapse = ", ")) %>%
  arrange(desc(cant)) %>% 
  kable(caption = str_c("Organismos con operaciones Activas"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 8) %>% 
  column_spec(2, '1cm') %>% 
  column_spec(3, '15cm')

```

### `r length(unique(op_act_NoAct$operacion))` Operaciones (o la combinación con el organismo) _aún no activas_ de lectura de base de datos

<!-- Detalle de operaciones: `r str_c(unique(op_act_NoAct$operacion), collapse = ", ")` -->

<!-- Detalle de organismos: `r str_c(unique(op_act_NoAct$organismo), collapse = ", ")` -->

```{r op_activas_2}

op2 <- op_act_NoAct %>%
  filter(!operacion %in% unique(op_act_Act$operacion)) %>% 
  select(organismo, operacion) %>%
  distinct() %>% 
  group_by(operacion) %>%
  summarise(cant = n(),
            'detalle de organismos' = str_c(unique(organismo), collapse = ", ")) %>%
  arrange(desc(cant))

op2 %>% 
  kable(caption = str_c("Organismos con operaciones No Activas"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) %>% 
  # column_spec(2, '1cm') %>% 
  column_spec(3, '13cm')

```

\pagebreak

```{r op_activas_3}

op3 <- op_act_Act %>%
  select(organismo, read_db_desde) %>%
  distinct() %>% 
  group_by(read_db_desde) %>%
  rename(desde = read_db_desde) %>% 
  summarise(cant = n(),
            'detalle de organismos' = str_c(unique(organismo), collapse = ", ")) %>%
  arrange(desc(cant))

op3 %>% 
  kable(caption = str_c("Organismos con operaciones Activas desde"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 8) %>% 
  column_spec(2, '1cm') %>% 
  column_spec(3, '14cm')

```

### Organismos que aún no han activado, que tienen Operaciones que ya funcionan (activas) o están siendo presentadas por otros organismos.

```{r op_activas_4}

op4_org <- op_act_NoAct %>%
  filter(operacion %in% unique(op_act_Act$operacion)) %>% 
  select(organismo, operacion) %>%
  distinct() 

orgs_pend_activar <- unique(op4_org$organismo)

op4 <- op4_org %>% 
  group_by(operacion) %>%
  summarise(cant = n(),
            'detalle de organismos' = str_c(unique(organismo), collapse = ", ")) %>%
  arrange(desc(cant)) 

op4 %>% 
  kable(caption = str_c("Organismos con operaciones Activas pero no habilitados aún (",sum(op4$cant),") para tal fin"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 8) %>% 
  # column_spec(2, '1cm') %>% 
  column_spec(3, '14cm')

```

\pagebreak

Todo lo anterior lleva a resumir que:

- existen `r sum(op3$cant)` organismos presentando directo de BD (sin listados de Lex)
- existen `r sum(op4$cant)` combinaciones pendientes de habilitar, con:
   - `r length(op4$operacion)` operaciones en funcionamiento y
   - `r length(orgs_pend_activar)` organismos pendientes de activación
- las combinaciones son las relaciones de Operaciones y Organismos

```{r tabla_organismos_habilitables}
orgs_agrup <- op4_org %>% 
  left_join(listar_organismos()) %>% 
  select(-operacion) %>% 
  distinct() %>%
  mutate(detalle = str_c(organismo_descripcion, " - ", localidad)) %>% 
  group_by(fuero, tipo, materia) %>% 
  summarise(cant = n(),
            organismos = str_c(detalle, collapse = ", ")) %>% 
  arrange(desc(cant))


# View(orgs_agrup)

orgs_agrup %>% 
  kable(caption = str_c("Organismos habilitables, por fuero y materia (",sum(orgs_agrup$cant),")."), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 8) %>% 
  column_spec(5, '16cm') %>% 
  landscape()
```


***
```{r final}
tiempo <- as.character(round(lubridate::as.duration(x = Sys.time() - informe_inicio), 2))
```
\begin{center}
Superior Tribunal de Justicia de Entre Ríos - Área de Planificación Gestión y Estadística  
\end{center}
El presente informe se ha generado en: `r tiempo`

