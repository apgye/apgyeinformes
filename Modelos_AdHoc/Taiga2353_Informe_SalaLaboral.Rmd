---
title: Informe por Magistrado - Sala Laboral
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output:
  pdf_document:
    includes:
      in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
informe_inicio <- Sys.time()
```

```{r instancias}
library(readr)
library(lubridate)
source("../R/informe.R")
source("../R/duracion.R")
source("../R/poblacion.R")
```

```{r parametros}
db_con <- DB_PROD()
# Período
start_date <- "2019-02-01"
end_date <- "2024-01-01"

# un_magistrado <- c("BAUTISTA", "GIANELLO", "LAROCCA", "MASARA",
#                    "MUNITZ", "PACE", "RUHL")
# un_magistrado_cod <- c("449", "800", "15910", "1442",
#                    "18362", "1499", "3329")

# Quitamos a RUHL
un_magistrado <- c("BAUTISTA", "GIANELLO", "LAROCCA", "MASARA",
                   "MUNITZ", "PACE")
un_magistrado_cod <- c("449", "800", "15910", "1442",
                   "18362", "1499")

```

Informe de desempeño para los siguientes magistrados: __`r str_to_title(un_magistrado)`__.

En todas las tablas se incluyen aquellos casos que existan y cumplen con los requisitos para ser mostrados. Asimismo, en casos donde en alguna tabla no figure un magistrado, quiere decir que no hay dato de ése magistrado para la condición que se quiera mostrar.


En la siguiente tabla se contabilizan las audiencias laborales en primera instancia para el período `r getDataIntervalStr(start_date, end_date)`, ordenado alfabéticamente por Magistrado.

De los datos obtenidos se representan de la siguiente manera:

   - _desde_ el año y mes de la primer audiencia del magistrado en el período,
   - _hasta_ el año y mes de la última audiencia del magistrado en el período,
   - _celebradas_ la cantidad de audiencias realizadas,
   - _reprogramadas_ la cantidad de audiencias que se postergan a otro momento o realización de cuarto intermedio,
   - _suspendidas_ la cantidad de audiencias canceladas.

```{r 1era}
# aud_resumen <- resumir_audil(db_con = DB_PROD(), poblacion = jdos_lab, start_date = start_date,
#               end_date = end_date, desagregacion_mensual = F) 

aud_primaria <- audilab_prim(db_con = DB_PROD(), poblacion = jdos_lab,
                             start_date, end_date)

aud_primariac <- audilab_prim(db_con = DB_PROD(), poblacion = jdos_cco,
                             start_date, end_date)

aud_primaria_format <- aud_primaria %>% 
  bind_rows(aud_primariac) %>% 
  filter(jaud %in% un_magistrado_cod)

# table(aud_primaria_format$jaud)

periodos <- aud_primaria_format %>% 
  group_by(jaud) %>% 
  summarise(desde = format(as.Date(min(fea)), "%Y-%m"), 
            hasta = format(as.Date(max(fea)), "%Y-%m"), 
            celebradas = sum(esta == "realizada"),
            reprogramadas = sum(esta %in% c("no_realizada", "realizada_no_concluida")),
            suspendidas = sum(esta == "cancelada")
            ) %>% 
  mutate(magistrado = case_when(
    jaud == "449" ~ "BAUTISTA",
    jaud == "800" ~ "GIANELLO",
    jaud == "15910" ~ "LAROCCA",
    jaud == "1442" ~ "MASARA",
    jaud == "18362" ~ "MUNITZ",
    jaud == "1499" ~ "PACE",
    jaud == "3329" ~ "RUHL"
  )) %>% 
  select(magistrado, desde, hasta, celebradas, reprogramadas, suspendidas) %>% 
  arrange(magistrado) %>%
  janitor::adorn_totals("col")

periodos %>%
  kable("latex", longtable = T, booktabs = T, 
        caption = str_c("Resultados de Audiencias Laborales por magistrado"," (",getDataIntervalStr(start_date, end_date), ")")) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 8)
  
```

En la siguiente tabla se observan las resoluciones de primera instancia laboral de dichos Magistrados.

   - _desde_ el año y mes de la primer resolución del magistrado en el período,
   - _hasta_ el año y mes de la última resolución del magistrado en el período
   - Para el caso del Dr. Larocca se encuentra en un Juzgado Civil multifuero, por lo que se les computan las resoluciones de materia laboral, excluyendo las civiles y de familia.

```{r 2da}
db_con = DB_PROD()
operacion = "CADR1L"
poblacion = jdos_lab %>% 
  bind_rows(jdos_cco %>% 
              filter(grepl("lab", materia)))

operacion = rlang::enexpr(operacion)

resultado1l <- db_con %>%
  apgyeTableData("CADR1L") %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!poblacion$organismo) 

resultado1l <- resultado1l %>% 
  group_by(iep, data_interval_start, data_interval_end) %>%
  collect() 

resultado1c <- db_con %>%
  apgyeTableData("CADR1C") %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!poblacion$organismo) 

resultado1c <- resultado1c %>% 
  group_by(iep, data_interval_start, data_interval_end) %>%
  collect() 

res_primarias <- resultado1l %>% 
  bind_rows(resultado1c %>% 
              rename(fvenc = fvenc1) %>% 
              select(-fvenc2) %>% 
              filter(mat %in% c("l", "L"))) %>% #se filtran las laborales por ser multifuero
  filter(codigo_juez %in% un_magistrado_cod) %>% 
  filter(!is.na(fres)) %>%
  group_by(codigo_juez) %>%
  summarise(desde = min(fres), 
            hasta = max(fres),
            # resoluciones = n(),
            autos = sum(as %in% c("a", "A")),
            sentencias = sum(as %in% c("s", "S"))
            ) %>%
  mutate(magistrado = case_when(
    codigo_juez == "449" ~ "BAUTISTA",
    codigo_juez == "800" ~ "GIANELLO",
    codigo_juez == "15910" ~ "LAROCCA",
    codigo_juez == "1442" ~ "MASARA",
    codigo_juez == "18362" ~ "MUNITZ",
    codigo_juez == "1499" ~ "PACE",
    codigo_juez == "3329" ~ "RUHL"
  )) %>%
  select(magistrado, desde, hasta, autos, sentencias) %>% 
  arrange(magistrado) %>%
  mutate(desde = format(dmy(desde), "%Y-%m"),
         hasta = format(dmy(hasta), "%Y-%m")) %>%
  janitor::adorn_totals("col")

res_primarias %>%
  kable("latex", longtable = T, booktabs = T, 
        caption = str_c("Resoluciones de primera instancia laboral por magistrado"," (",getDataIntervalStr(start_date, end_date), ")")) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 8)
  
```

En la siguiente tabla se observan las resoluciones de segunda instancia laboral de dichos Magistrados.

   - _desde_ el año y mes de la primer resolución del magistrado en el período,
   - _hasta_ el año y mes de la última resolución del magistrado en el período,

```{r 3era}

db_con = DB_PROD()
operacion = "CADR2L"
poblacion = cam_lab

operacion = rlang::enexpr(operacion)

resultado <- db_con %>%
  apgyeTableData(!! operacion) %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!poblacion$organismo) 

resultado <- resultado %>% 
  group_by(iep, data_interval_start, data_interval_end) %>%
  collect() 

res_primarias <- resultado %>% 
  filter(any(ordv %in% un_magistrado_cod)) %>%
  # separate(col = ordv,
  #          into = c("voto1", "voto2", "voto3"),
  #          sep = ",") %>% View()
  mutate(magistrado = case_when(
    any(ordv == "449") ~ "BAUTISTA",
    any(ordv == "800") ~ "GIANELLO",
    any(ordv == "15910") ~ "LAROCCA",
    any(ordv == "1442") ~ "MASARA",
    any(ordv == "18362") ~ "MUNITZ",
    any(ordv == "1499") ~ "PACE",
    any(ordv == "3329") ~ "RUHL")) %>% 
  filter(!is.na(fres)) %>%
  group_by(magistrado) %>%
  summarise(desde = min(fres), 
            hasta = max(fres),
            # resoluciones = n(),
            autos = sum(as %in% c("a", "A")),
            sentencias = sum(as %in% c("s", "S"))
            ) %>%
  select(magistrado, desde, hasta, autos, sentencias) %>% 
  arrange(magistrado) %>%
  mutate(desde = format(dmy(desde), "%Y-%m"),
         hasta = format(dmy(hasta), "%Y-%m")) %>%
  janitor::adorn_totals("col")

res_primarias %>%
  kable("latex", longtable = T, booktabs = T, 
        caption = str_c("Resoluciones de segunda instancia laboral por magistrado"," (",getDataIntervalStr(start_date, end_date), ")")) %>% 
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 8)
  

```

Tabla de Moras - Pendientes de resolución - en Primera Instancia

En éste apartado se computan los datos primarios con la última declaración de datos del organismo (dato en columna año_mes) y tomando aquellos registros donde figuren los magistrados del presente informe.

```{r tabla_mora_1inst}

db_con = DB_PROD()
operacion = "CADR1L"
poblacion = jdos_lab

operacion = rlang::enexpr(operacion)
  
  #start_date <- as.Date(end_date) - months(1) # comentamos la línea por Taiga 1849 
  #ultimo_dia_mesinf <- ymd(end_date) - 1
  resultado <- db_con %>% 
    apgyeTableData(!!operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% !!poblacion$organismo) %>%
    group_by(iep) %>%
    filter(data_interval_start == max(data_interval_start)) %>% 
    collect()
  
  resultado <- resultado %>% 
      ungroup() %>% 
      filter(is.na(fres)) %>% 
      filter(!(is.na(nro) & is.na(caratula))) %>% # exclusión de registros con campos críticos vacíos
      mutate(as = toupper(as))
  
  #filtrado por magistrado
  resultado <- resultado %>% 
    mutate(magistrado = case_when(
      grepl("449", codigo_juez) ~ "BAUTISTA",
      grepl("800", codigo_juez) ~ "GIANELLO",
      grepl("15910", codigo_juez) ~ "LAROCCA",
      grepl("1442", codigo_juez) ~ "MASARA",
      grepl("18362", codigo_juez) ~ "MUNITZ",
      grepl("1499", codigo_juez) ~ "PACE",
      grepl("3329", codigo_juez) ~ "RUHL")) %>% 
    filter(!is.na(magistrado))

    resultado <- resultado %>%
      mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
             ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
      select(magistrado, organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio, 
             fecha_adespacho = fdesp, fecha_vencimiento = fvenc, año_mes, ultimo_dia_mesinf) %>% 
      distinct(magistrado, organismo, nro, caratula, tproc, as, fecha_vencimiento, .keep_all = TRUE) %>%
      filter(!is.na(fecha_vencimiento)) %>%
      mutate(fecha_inicio = dmy(fecha_inicio), fecha_adespacho = dmy(fecha_adespacho), fecha_vencimiento = dmy(fecha_vencimiento)) %>%
      mutate(vencido = ifelse(fecha_vencimiento <= ultimo_dia_mesinf, "si", "no"))
  
  resultado <- resultado  %>%   
      mutate(caratula = str_sub(word(caratula, 1,1), 1,15)) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], 
                by = "organismo") %>% 
      select(-organismo) %>% 
      rename(organismo = organismo_descripcion) %>% 
      mutate(tproc = str_sub(gsub("[^[:alnum:][:blank:]?&/\\-]", "", tproc))) %>% 
      mutate(tipo_proc = str_sub(tproc, 1,20),
             organismo = abbreviate(organismo, minlength = 13)) %>% 
      select(-tproc, -ultimo_dia_mesinf)
  
  
  resultado <- resultado %>%   
    select(magistrado, circunscripcion, organismo, año_mes, fecha_adespacho, fecha_vencimiento, nro, caratula, tipo_proc, everything()) %>% 
    arrange(circunscripcion, organismo, desc(vencido))

  # fin de función pendientes, en resultado queda la primaria con detalle
  
  if (nrow(resultado) > 0){
    
    res <- resultado
    
    # cambiamos 'circunscripcion, organismo' por 'magistrado' 
    res_pend <- res %>% 
      group_by(magistrado, año_mes) %>% 
      summarise(pendientes = n())
  
    res_pendV <- res %>% 
      filter(vencido == "si") %>% 
      group_by(magistrado, año_mes) %>% 
      summarise(vencidos = n())
    
    res_pend <- res_pend %>%
      left_join(res_pendV) %>%
      mutate(vencidos = replace_na(vencidos, 0)) %>% 
      mutate('% vencidos' = round((vencidos/pendientes)*100,1)) %>% 
      rename('pendientes totales' = pendientes,
             'pendientes vencidos' = vencidos)
    
    res_agrup <- res %>% 
      group_by(magistrado, año_mes, vencido) %>% 
      summarise('detalle causas' = str_c(nro, collapse = ", "))
  
} else {
  msg_NoData %>% 
      kable(caption = "Pendientes de resolución de primera instancia laboral por magistrado", align = 'c', longtable = TRUE )
    
}

  res_pend %>%
    kable("latex", longtable = T, booktabs = T, 
          caption = str_c("Pendientes de resolución de primera instancia laboral por magistrado"," (",getDataIntervalStr(start_date, end_date), ")")) %>% 
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 8)

```

Tabla de Moras - Pendientes de resolución - en Segunda Instancia

En éste apartado se computan los datos primarios con la última declaración de datos del organismo (dato en columna año_mes) y tomando aquellos registros donde figuren los magistrados del presente informe.

```{r tabla_mora_2inst}

db_con = DB_PROD()
operacion = "CADR2L"
poblacion = cam_lab

operacion = rlang::enexpr(operacion)
  
  #start_date <- as.Date(end_date) - months(1) # comentamos la línea por Taiga 1849 
  #ultimo_dia_mesinf <- ymd(end_date) - 1
  resultado <- db_con %>% 
    apgyeTableData(!!operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% !!poblacion$organismo) %>%
    group_by(iep) %>%
    filter(data_interval_start == max(data_interval_start)) %>% 
    collect()
  
  resultado <- resultado %>% 
      ungroup() %>% 
      filter(is.na(fres)) %>% 
      filter(!(is.na(nro) & is.na(caratula))) %>% # exclusión de registros con campos críticos vacíos
      mutate(as = toupper(as))
  
  #filtrado por magistrado
  resultado <- resultado %>% 
    mutate(magistrado = case_when(
      grepl("449", ordv) ~ "BAUTISTA",
      grepl("800", ordv) ~ "GIANELLO",
      grepl("15910", ordv) ~ "LAROCCA",
      grepl("1442", ordv) ~ "MASARA",
      grepl("18362", ordv) ~ "MUNITZ",
      grepl("1499", ordv) ~ "PACE",
      grepl("3329", ordv) ~ "RUHL")) %>% 
    filter(!is.na(magistrado))

  resultado <- resultado %>% 
    mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
           ultimo_dia_mesinf = ymd(data_interval_end) - 1) %>%
    select(magistrado, organismo = iep, nro, caratula, tproc, as, fecha_inicio = finicio, 
               fecha_adespacho = fdesp1, fecha_vencimiento = fvenc, año_mes, ultimo_dia_mesinf) %>% 
    distinct(magistrado, organismo, nro, caratula, tproc, as, fecha_vencimiento, .keep_all = TRUE) %>%
    filter(!is.na(fecha_vencimiento)) %>%
    mutate(fecha_inicio = dmy(fecha_inicio),
           fecha_adespacho = dmy(fecha_adespacho),
           fecha_vencimiento = dmy(fecha_vencimiento)) %>%
    mutate(vencido = ifelse(fecha_vencimiento <= ultimo_dia_mesinf, "si", "no"))
  
  resultado <- resultado  %>%   
      mutate(caratula = str_sub(word(caratula, 1,1), 1,15)) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                "circunscripcion")], 
                by = "organismo") %>% 
      select(-organismo) %>% 
      rename(organismo = organismo_descripcion) %>% 
      mutate(tproc = str_sub(gsub("[^[:alnum:][:blank:]?&/\\-]", "", tproc))) %>% 
      mutate(tipo_proc = str_sub(tproc, 1,20),
             organismo = abbreviate(organismo, minlength = 13)) %>% 
      select(-tproc, -ultimo_dia_mesinf)
  
  
  resultado <- resultado %>%   
    select(magistrado, circunscripcion, organismo, año_mes, fecha_adespacho, fecha_vencimiento, nro, caratula, tipo_proc, everything()) %>% 
    arrange(circunscripcion, organismo, desc(vencido))

  # fin de función pendientes, en resultado queda la primaria con detalle
  
  if (nrow(resultado) > 0){
    
    res <- resultado
    
    # cambiamos 'circunscripcion, organismo' por 'magistrado' 
    res_pend <- res %>% 
      group_by(magistrado, año_mes) %>% 
      summarise(pendientes = n())
  
    res_pendV <- res %>% 
      filter(vencido == "si") %>% 
      group_by(magistrado, año_mes) %>% 
      summarise(vencidos = n())
    
    res_pend <- res_pend %>%
      left_join(res_pendV) %>%
      mutate(vencidos = replace_na(vencidos, 0)) %>% 
      mutate('% vencidos' = round((vencidos/pendientes)*100,1)) %>% 
      rename('pendientes totales' = pendientes,
             'pendientes vencidos' = vencidos)
    
    res_agrup <- res %>% 
      group_by(magistrado, año_mes, vencido) %>% 
      summarise('detalle causas' = str_c(nro, collapse = ", "))
  
} else {
  msg_NoData %>% 
      kable(caption = "Pendientes de resolución de segunda instancia laboral por magistrado", align = 'c', longtable = TRUE )
    
}

  res_pend %>%
    kable("latex", longtable = T, booktabs = T, 
          caption = str_c("Pendientes de resolución de segunda instancia laboral por magistrado"," (",getDataIntervalStr(start_date, end_date), ")")) %>% 
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 8)

```










***
```{r final}
tiempo <- as.character(round(lubridate::as.duration(x = Sys.time() - informe_inicio), 2))
```
\begin{center}
Superior Tribunal de Justicia de Entre Ríos - Área de Planificación Gestión y Estadística  
\end{center}
<!-- El presente informe se ha generado en: `r tiempo` -->

