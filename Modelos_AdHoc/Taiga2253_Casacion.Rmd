---
title: Informe de Cámaras de Casación
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output:
  pdf_document:
    includes:
      in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
informe_inicio <- Sys.time()
```

```{r instancias}
library(readr)
library(lubridate)
source("../R/informe.R")
source("../R/duracion.R")
source("../R/poblacion.R")
# Funciones de consulta mongo
# source('~/apgyeprocesamiento/R/m_query_registros.R')
# # Conexión SIRIRI
# expedientes = mongo(collection ="expedientes", db="expedientes_production", url = conM2)
# personas = mongo(collection ="personas", db="personas", url = conM2)
# table_schema <- read_csv("~/apgyeprocesamiento/data/table_schema.csv")
# orgsvec_casacion <- c("parcamcasacion", "conccpen")

```

```{r parametros}

db_con <- DB_PROD()
# Perídodo
start_date <- "2022-02-01"
end_date <- "2024-01-01"

prim_ord_por_fechas <- T

# iniciadas
inic_campen <- iniciadas_pen(db_con = DB_PROD(), poblacion = cam_pen,
                           start_date = start_date, end_date = end_date)

inic_primarias <- inic_campen$inic_prim

# resoluciones
res_campen <- db_con %>% 
  apgyeDSL::apgyeTableData('CADRC_v2') %>%
  apgyeDSL::interval(start_date , end_date) %>%
  filter(iep %in% !!cam_pen$organismo) %>%
  collect()

# audiencias
aud_campen <- db_con %>% 
  apgyeDSL::apgyeTableData('AUDIPC') %>%
  apgyeDSL::interval(start_date , end_date) %>%
  filter(iep %in% !!cam_pen$organismo) %>%
  collect()


```


```{r punto 1}

inic_campen$inic_xorg %>% 
  iniciadas_pen_complemento(desagregacion_mensual = F) %>%
  kable(caption = str_c("Causas Iniciadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 9)  

```

```{r punto 2}
# orgsvec_casacion <- c("campen0000pna", "campen0000con")
# #orgsvec_casacion <- c("campen0000con")
# 
# # leer desde mongo
# pipeline1 = m_iniciados_penal(orgsvec = orgsvec_casacion)
# registros_siriri <- expedientes$aggregate(pipeline1, options = '{"allowDiskUse":false}')
# 
# dep_details_periodo <- registros_siriri %>% 
#   mutate(finicio = ymd(inicio)) %>% 
#   filter(finicio >= start_date & finicio < end_date) 
# 
# reg_periodo <- nrow(dep_details_periodo)
# 
# # reg_expedientes <- dep_details_periodo %>%
# #   filter(!is.na(nro_exp)) %>%
# #   pull(nro_exp) %>%
# #   unique()
# reg_expedientes_distintos <- length(unique(dep_details_periodo$actora))
# reg_expedientes_sin_nro <- length(unique(dep_details_periodo %>% 
#                                              filter(is.na(nro_exp)) %>% 
#                                              pull(actora)))
# 
# 
# dep_details <- dep_details_periodo %>% 
#   select(inicio, circunscripcion, organismo, nro_exp, dependencias) %>%
#   unnest(cols = dependencias) %>% 
#   filter(!dependencia %in% c(" ", "")) # no viene NA, sino caracter vacío
# 
# dep_details_resultado_circ <- dep_details %>% 
#   group_by(circunscripcion) %>% 
#   summarise(cant = n()) %>% 
#   adorn_totals()
# 
# dep_details_resultado <- dep_details %>% 
#   group_by(circunscripcion, dependencia) %>% 
#   summarise(cant = n()) %>% 
#   arrange(circunscripcion, desc(cant)) %>% 
#   adorn_totals()



```


<!-- ```{r tabla1_punto2} -->
<!-- # dep_details_resultado_circ %>%  -->
<!-- #   kable(caption = str_c("Jurisdicciones de Tribunales mas recurridas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>% -->
<!-- #   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!-- #                    full_width = F, font_size = 9) -->

<!-- ``` -->


<!-- ```{r tabla2_punto2} -->
<!-- # dep_details_resultado %>%  -->
<!-- #   kable(caption = str_c("Detalle de Tribunales mas recurridos"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>% -->
<!-- #   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!-- #                    full_width = F, font_size = 9) %>%  -->
<!-- #   column_spec(2, '13cm')  -->

<!-- ``` -->

En la siguiente tabla de Inadmisibilidad, se toman las resoluciones en el período indicado, independientemente se hayan iniciado o no en dicho período.

```{r punto 3}

res_campen2 <- res_campen %>% 
  filter(!is.na(fres) &
           tres %in% c(1:16)) %>% 
  mutate(tipo_recurso = case_when(
    tres == 2 ~ "Inadmisible",
    TRUE ~ "Restantes"
  )) %>%
  group_by(iep, tipo_recurso) %>% 
  summarise(cant = n()) %>% 
  pivot_wider(names_from = tipo_recurso,
              values_from = cant, 
              values_fill = 0) %>% 
  mutate('porcentaje inadmisibilidad' = str_c(round((Inadmisible*100)/sum(Restantes,Inadmisible), digits=0), " %")) %>% 
  left_join(apgyeJusEROrganization::listar_organismos() %>% 
              select(iep = organismo, organismo_descripcion, circunscripcion),
              by = "iep") %>%
  rename(organismo = organismo_descripcion) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo, everything(), -iep)

# View(prim_campen2)

res_campen2 %>% 
  kable(caption = str_c("Inadmisibilidad"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                   full_width = F, font_size = 9)  

```

En la siguiente tabla de Medianas de duraciones, se toman las sentencias que han sido del tipo admisibles o aquellas equiparables a admisibilidad del período indicado, con éstos números de expedientes se busca la información relativa a Audiencias y Resoluciones hacia atrás en el tiempo para encontrar más registros de dichos expedientes.

Con esa información, se elaboran los cálculos de las medianas de duraciones detalladas, expresadas en períodos de treinta (30) días -meses-.

_Al final del informe se anexan los listados primarios de las duraciones._

```{r admisibilidad}

# se obtienen las sentencias "equiparables a admisibilidad"
nros_S_admis_prim <- res_campen %>% 
  mutate(as = toupper(as)) %>% 
  filter(as == "S") %>% 
  filter(tres %in% c("4","5","6","16")) # Tomamos éstos tipos por su volumen 

# nros de expedientes a buscar 
nros_S_admis <- unique(nros_S_admis_prim$nro)

# buscamos en primarias de resoluciones y de audiencias
desde_res <- format.Date(dmy(min(nros_S_admis_prim$fing)), tryFormats = c("%Y-%m-%d"))
# resoluciones
res_campen_xnro <- db_con %>% 
  apgyeDSL::apgyeTableData('CADRC_v2') %>%
  apgyeDSL::interval(desde_res, end_date) %>%
  filter(iep %in% !!cam_pen$organismo) %>%
  filter(nro %in% !!nros_S_admis) %>%
  collect()

# ver cuantas sent por exp
# View(res_campen_xnro %>%
#        filter(as %in% c("s", "S")) %>% 
#        group_by(nro) %>% 
#        summarise(n()))

# audiencias
aud_campen_xnro <- db_con %>% 
  apgyeDSL::apgyeTableData('AUDIPC') %>%
  apgyeDSL::interval(desde_res, end_date) %>%
  filter(iep %in% !!cam_pen$organismo) %>%
  filter(nro %in% !!nros_S_admis) %>%
  collect()

# ver cuantas aud ffa por exp
# View(aud_campen_xnro %>%
#        group_by(nro) %>% 
#        summarise(n()))

# para el promedio de ingreso-fijación se toma la fijación mas temprana
durac_ing_ffa_prim <- aud_campen_xnro %>% 
  filter(!is.na(fing) & !is.na(ffa)) %>%
  group_by(nro) %>% 
  distinct(fing, ffa, .keep_all = T) %>% 
  filter(ffa == min(ffa)) %>% 
  filter(dmy(fing) <= dmy(ffa)) %>% 
  select(iep, nro, caratula, tdelito, fing, ffa) %>% 
  mutate(durif = (dmy(ffa) - dmy(fing))/30) %>% 
  mutate(durif = as.numeric(durif))

# Taiga 2314 - buscamos dato de ppl
durac_ing_ffa_prim <- durac_ing_ffa_prim %>%
  ungroup() %>% 
  mutate(comb = str_c(iep, nro))

test <- res_campen %>%
  mutate(comb = str_c(iep, nro)) %>% 
  select(comb, ppl) %>% 
  distinct()

durac_ing_ffa_prim <- durac_ing_ffa_prim %>% 
  left_join(test, by="comb")

durac_ing_ffa <- durac_ing_ffa_prim %>% 
  ungroup() %>% 
  group_by(iep) %>% 
  mutate('mediana ingreso - primer fijación' = round(median(durif))) %>% 
  select(iep, 'mediana ingreso - primer fijación') %>% 
  unique() 

# para el promedio de audiencia-sentencia se toma la última aud realizada 
aud_to_join <- aud_campen_xnro %>% 
  filter(!is.na(fea)) %>%
  filter(esta == 2) %>% 
  group_by(nro) %>% 
  filter(fea == max(fea)) %>% # 226 reg
  select(iep, nro, finga = fing, fea)

sent_to_join <- res_campen_xnro %>% 
  filter(!is.na(fres)) %>%
  filter(as %in% c("s", "S")) %>% 
  group_by(nro) %>% 
  filter(fres == max(fres)) %>% # 395 reg
  select(nro, fings = fing, fres, as, iep, caratula, tdelito)
  
durac_fea_sent_prim <- aud_to_join %>% 
  left_join(sent_to_join, by=c("nro", "iep")) %>% 
  mutate(dur_aud_sent = (dmy(fres) - dmy(fea))/30) %>% 
  mutate(dur_aud_sent = as.numeric(dur_aud_sent))

durac_fea_sent <- durac_fea_sent_prim %>% 
  ungroup() %>% 
  group_by(iep) %>% 
  mutate('mediana última audiencia realizada - sentencia' = round(median(dur_aud_sent))) %>% 
  select(iep, 'mediana última audiencia realizada - sentencia') %>% 
  unique() 


# para el promedio de ingreso-sentencia se toma la sentencia mas nueva (la última)
durac_ing_sent_prim <- res_campen_xnro %>% 
  filter(!is.na(fing) & !is.na(fres)) %>%
  group_by(nro) %>% 
  distinct(fing, fres, .keep_all = T) %>% 
  filter(fres == max(fres)) %>% 
  filter(dmy(fing) <= dmy(fres)) %>% 
  select(iep, nro, caratula, fing, fres) %>% 
  mutate(duris = (dmy(fres) - dmy(fing))/30) %>% 
  mutate(duris = as.numeric(duris))

durac_ing_sent <- durac_ing_sent_prim %>% 
  ungroup() %>% 
  group_by(iep) %>% 
  mutate('mediana ingreso - sentencia' = round(median(duris))) %>% 
  select(iep, 'mediana ingreso - sentencia') %>% 
  unique() 

duraciones_ing_sent <- durac_ing_ffa %>% 
  left_join(durac_fea_sent, by="iep") %>% 
  left_join(durac_ing_sent, by="iep") %>% 
  left_join(apgyeJusEROrganization::listar_organismos() %>% 
              select(iep = organismo, organismo_descripcion, circunscripcion),
              by = "iep") %>%
  rename(organismo = organismo_descripcion) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo, everything(), -iep)

duraciones_ing_sent %>%
  select(-'mediana ingreso - sentencia') %>% 
  kable(caption = str_c("Medianas de duraciones en meses (de 30 días corridos)"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                   full_width = F, font_size = 9) %>% 
  column_spec(c(3:4), '3cm')

```

```{r punto 4}
# # aud_audito <- aud_campen %>% 
# #   mutate(misma_fecha = finicio == fing)
# # View(aud_audito %>% filter(!misma_fecha)) # por ésto decido usar FING
# 
# pre_prom_aud <- aud_campen %>% 
#   # filter(esta == 2) %>%
#   filter(!is.na(fing) & !is.na(ffa)) %>% 
#   filter(dmy(fing) <= dmy(ffa)) %>% 
#   select(iep, nro, caratula, fing, ffa) %>% 
#   mutate(duracion = (dmy(ffa) - dmy(fing))/30) %>% 
#   mutate(duracion = as.numeric(duracion))
#   
# prom_aud <- pre_prom_aud %>% 
#   ungroup() %>% 
#   group_by(iep) %>% 
#   mutate('promedio ingreso - fijación' = round(mean(duracion))) %>% 
#   select(iep, 'promedio ingreso - fijación') %>% 
#   unique() 
# 
# pre_prom_res <- res_campen %>% 
#   filter(!is.na(fing) & !is.na(fres) &
#            tres %in% c(1:16)) %>%
#   filter(dmy(fing) <= dmy(fres)) %>% 
#   select(iep, nro, caratula, fing, fres) %>% 
#   mutate(duracion = (dmy(fres) - dmy(fing))/30) %>% 
#   mutate(duracion = as.numeric(duracion))
#   
# prom_res <- pre_prom_res %>% 
#   ungroup() %>% 
#   group_by(iep) %>% 
#   mutate('promedio ingreso - sentencia' = round(mean(duracion))) %>% 
#   select(iep, 'promedio ingreso - sentencia') %>% 
#   unique() 
# 
# promedios_duraciones <- prom_aud %>% 
#   left_join(prom_res, by="iep") %>% 
#   left_join(apgyeJusEROrganization::listar_organismos() %>% 
#               select(iep = organismo, organismo_descripcion, circunscripcion),
#               by = "iep") %>%
#   rename(organismo = organismo_descripcion) %>% 
#   ungroup() %>% 
#   select(circunscripcion, organismo, everything(), -iep)
# 
# 
# promedios_duraciones %>% 
#   kable(caption = str_c("Promedios de duraciones en meses (de 30 días corridos)"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
#                    full_width = F, font_size = 9)
```


```{r punto 4_prim1}
# pre_prom_aud %>%
#   select(-caratula) %>% 
#   mutate(duracion = round(duracion)) %>% 
#   left_join(apgyeJusEROrganization::listar_organismos() %>% 
#               select(iep = organismo, circunscripcion),
#               by = "iep") %>%
#   ungroup() %>% 
#   select(circunscripcion, everything(), -iep) %>%
#   kable(caption = str_c("Listado primario de audiencias para duraciones"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
#                    full_width = F, font_size = 9) 

#%>%
  # row_spec(0, angle = 90) #%>%
  # column_spec(2, '10cm')
```


```{r punto 4_prim2}
# pre_prom_res %>%
#   select(-caratula) %>% 
#   mutate(duracion = round(duracion)) %>%
#   left_join(apgyeJusEROrganization::listar_organismos() %>% 
#               select(iep = organismo, circunscripcion),
#               by = "iep") %>%
#   ungroup() %>% 
#   select(circunscripcion, everything(), -iep) %>%
#   kable(caption = str_c("Listado primario de sentencias para duraciones"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
#                    full_width = F, font_size = 9) #%>%


#   row_spec(0, angle = 90) %>% 
#   column_spec(2, '10cm') 

```

\pagebreak

En la siguiente tabla se obtienen los porcentajes de Concesión y Rechazo de recursos extraordinarios. A pedido del solicitante se deben calcular dichos indicadores con las sentencias del período, aunque se debe aclarar que existen 200, de las 201 concesiones y rechazo, _cargadas como autos, no como sentencias_, razón por la cual se han incluído en éste cálculo cual si fueran sentencias.
Adicionalmente la columna _Sent. Impugnada_ indica la suma de las Concesiones y los Rechazos.


```{r punto 5}

res_campen5 <- res_campen %>% 
  mutate(as = toupper(as)) %>% 
  filter(as == "S" | # sólo con las sentencias pierdo las siguientes 200 resols
           tres %in% c(9:10)) %>% # aca agarro 200 resols de concede y rechaza, cargadas como Auto -para aclarar-
  filter(!is.na(fres) &
           tres %in% c(1:16)) %>% 
  mutate(tipo_recurso = case_when(
    tres == 9 ~ "Concede", #"Concede extraordinario",
    tres == 10 ~ "Rechaza", #"Rechaza extraordinario",
    TRUE ~ "Restantes"
  )) %>% 
  group_by(iep, tipo_recurso) %>% 
  summarise(cant = n()) %>% 
  pivot_wider(names_from = tipo_recurso,
              values_from = cant, 
              values_fill = 0)

res_campen5 <- res_campen5 %>% 
  mutate('porcentaje concesión' = str_c(round((Concede*100)/sum(Restantes,Concede,Rechaza), digits=0), " %")) %>% 
  mutate('porcentaje rechazo' = str_c(round((Rechaza*100)/sum(Restantes,Concede,Rechaza), digits=0), " %")) %>% 
  mutate(Sentencias = sum(Restantes,Concede,Rechaza),
         'Sent. Impugnada' = sum(Concede,Rechaza)) %>% 
  select(-Restantes) %>% 
  left_join(apgyeJusEROrganization::listar_organismos() %>% 
              select(iep = organismo, organismo_descripcion, circunscripcion),
              by = "iep") %>%
  rename(organismo = organismo_descripcion) %>% 
  ungroup() %>% 
  select(circunscripcion, Sentencias, 'Sent. Impugnada', everything(), -iep, -organismo)

# View(res_campen5)

res_campen5 %>% 
  kable(caption = str_c("Porcentaje de Concesión y Rechazo de Impuganción Extraordinaria"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                   full_width = F, font_size = 9) %>% 
  column_spec(c(2:7), '2cm')

```

```{r punto 6}

res_campen6 <- res_campen %>%
  filter(ppl %in% c(0:1)) %>%
  mutate(privados = case_when(
    ppl == 1 ~ "Privados",
    ppl == 0 ~ "No_Privados"
  )) %>% 
  group_by(iep, privados) %>% 
  summarise(cant = n()) %>%
  pivot_wider(names_from = privados,
              values_from = cant, 
              values_fill = 0)

res_campen6 <- res_campen6 %>% 
  mutate('porcentaje privados lib' = str_c(round((Privados*100)/sum(No_Privados,Privados), digits=0), " %")) %>% 
  left_join(apgyeJusEROrganization::listar_organismos() %>% 
              select(iep = organismo, organismo_descripcion, circunscripcion),
              by = "iep") %>%
  rename(organismo = organismo_descripcion) %>% 
  ungroup() %>% 
  select(circunscripcion, organismo, everything(), -iep)

# View(res_campen6)

res_campen6 %>% 
  kable(caption = str_c("Causas con personas privadas de la libertad"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                   full_width = F, font_size = 9)  

```

```{r punto 7}

# res_campen7 <- res_campen %>%
#   mutate(as = toupper(as)) %>% 
#   filter(as %in% c("A","S")) %>%
#   mutate(sent_definitiva = case_when(
#     as == "A" ~ "Autos",
#     as == "S" ~ "Sentencias"
#   )) %>% 
#   group_by(iep, sent_definitiva) %>% 
#   summarise(cant = n()) %>%
#   pivot_wider(names_from = sent_definitiva,
#               values_from = cant, 
#               values_fill = 0)
# 
# res_campen7 <- res_campen7 %>% 
#   mutate('porcentaje sentencias' = str_c(round((Sentencias*100)/sum(Autos,Sentencias), digits=0), " %")) %>% 
#   left_join(apgyeJusEROrganization::listar_organismos() %>% 
#               select(iep = organismo, organismo_descripcion, circunscripcion),
#               by = "iep") %>%
#   rename(organismo = organismo_descripcion) %>% 
#   ungroup() %>% 
#   select(circunscripcion, organismo, everything(), -iep)
# 
# # View(res_campen7)
# 
# res_campen7 %>% 
#   kable(caption = str_c("Sentencias definitivas o equiparables"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
#                    full_width = F, font_size = 9)  

```

\pagebreak

```{r prim_durac_1}
difp <- durac_ing_ffa_prim %>%
  mutate(durif = round(durif)) %>%
  # select(-caratula) %>% # Taiga 2314 - mostrar caratula y tdelito 
  mutate('con priv. de la lib.' = case_when( 
    ppl == 1 ~ "Privados",
    ppl == 0 ~ "No_Privados",
    TRUE ~ "sin_dato"
  )) %>% # taiga 2314 - mostrar si tiene ppl
  left_join(apgyeJusEROrganization::listar_organismos() %>% 
              select(iep = organismo, organismo_descripcion, circunscripcion),
              by = "iep") %>%
  ungroup() %>%
  select(circunscripcion, nro, caratula, tdelito, f_ingreso = fing,
         f_fijación = ffa, duración = durif, 'con priv. de la lib.',
         -iep, -comb, -ppl) %>%
  mutate(años = round(duración/12, 0)) # Taiga 2314 - agregar columna años
  # por T2314, comentario del 14/2, NO acortar carátula
  #mutate(caratula = str_sub(caratula, 1, 10)) # Taiga 2314 - cortar caratula a 10 caracteres 

if (prim_ord_por_fechas){ # Taiga 2314 - comentario de 7/2/24 ordenar por fechas
  # difp <- difp %>%
  #   mutate(f_ingreso = as.Date(f_ingreso, format="%d/%m/%Y"),
  #          f_fijación = as.Date(f_fijación, format="%d/%m/%Y")) %>% 
  #   arrange(circunscripcion, f_ingreso, f_fijación)
  
  difp <- difp %>%
    # T2314 - para mostrar en formato dmy, creo las fechas_order para ordenar pero luego las elimino, dejando las originales
    mutate(f_ingreso_order = as.Date(f_ingreso, format="%d/%m/%Y"),
           f_fijación_order = as.Date(f_fijación, format="%d/%m/%Y")) %>%
    arrange(circunscripcion, f_ingreso_order, f_fijación_order) %>% 
    select(circunscripcion, nro, caratula, tdelito, f_ingreso, f_fijación,
           duración, años, `con priv. de la lib.`, -f_ingreso_order, -f_fijación_order)
  
} else {
  difp <- difp %>% 
    arrange(circunscripcion, desc(duración))
}

  difp <- difp %>% 
    rename(meses = duración) %>%  # Taiga 2314 - renombrar para mejorar lectura
    kable(caption = str_c("Listado primario de audiencias para duraciones de ingreso a primera fijación"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                     full_width = F, font_size = 9) %>% 
    column_spec(c(3:4), '5cm') %>% 
    landscape()
  
  difp

```

\pagebreak

```{r prim_durac_2}
dfsp <- durac_fea_sent_prim %>% 
  mutate(dur_aud_sent = round(dur_aud_sent)) %>% 
  left_join(apgyeJusEROrganization::listar_organismos() %>% 
              select(iep = organismo, organismo_descripcion, circunscripcion),
              by = "iep") %>%
  rename(organismo = organismo_descripcion) %>% 
  ungroup() %>%
  select(circunscripcion, nro, caratula, tdelito, f_audiencia = fea, f_sentencia = fres, duración = dur_aud_sent,
         -iep, -organismo, -finga, -fings, -as) %>% # Taiga 2314 - mostrar caratula y tdelito
  mutate(años = round(duración/12, 0)) #%>% # Taiga 2314 - agregar columna años
  # mutate(caratula = str_sub(caratula, 1, 10)) # Taiga 2314 - ahora NO cortar caratula a 10 caracteres 

if (prim_ord_por_fechas){ # Taiga 2314 - comentario de 7/2/24 ordenar por fechas
  dfsp <- dfsp %>% 
    mutate(f_audiencia_order = as.Date(f_audiencia, format="%d/%m/%Y"),
           f_sentencia_order = as.Date(f_sentencia, format="%d/%m/%Y")) %>% 
    arrange(circunscripcion, f_audiencia_order, f_sentencia_order) %>% 
    select(-f_audiencia_order, -f_sentencia_order)
  
} else {
  dfsp <- dfsp %>% 
    arrange(circunscripcion, desc(duración))
}

  dfsp <- dfsp %>% 
    rename(meses = duración) %>%  # Taiga 2314 - renombrar para mejorar lectura
    kable(caption = str_c("Listado primario para duraciones de efectivización de última audiencia realizada y la última sentencia de cada expediente"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                     full_width = F, font_size = 9) %>% 
    column_spec(c(3:4), '5cm') %>% 
    landscape()
  
  dfsp

```

\pagebreak

```{r prim_durac_3}

# mostrar la siguiente tabla si se muestra el 3er indicador de duraciones

# durac_ing_sent_prim %>% 
#   mutate(duris = round(duris)) %>% 
#   select(-caratula) %>%
#   left_join(apgyeJusEROrganization::listar_organismos() %>% 
#               select(iep = organismo, organismo_descripcion, circunscripcion),
#               by = "iep") %>%
#   rename(organismo = organismo_descripcion) %>% 
#   ungroup() %>%
#   select(circunscripcion, nro, f_ingreso = fing, f_sentencia = fres, duración = duris,
#          -iep, -organismo) %>%
#   arrange(circunscripcion, desc(duración)) %>% 
#   kable(caption = str_c("Listado primario para duraciones de ingreso a última sentencia de cada expediente"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
#   kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
#                    full_width = F, font_size = 9) 

```

\pagebreak

En la siguiente Tabla se detallan los expedientes ingresados a Cámara con situación "en trámite".

Cabe aclarar que todos los detallados carecen de fecha de fijación de audiencia aún.


```{r primaria_sin_resolver}
# taiga 2314 - con los datos de la primer primaria no sería, sino de la res_campen

primaria3era <- res_campen %>% 
  select(iep, nro, everything()) %>% 
  group_by(iep) %>%
  filter(id_submission == max(id_submission)) %>% # nos quedamos con la ultima presentación
  ungroup() %>% 
  filter(is.na(fres)) %>% # filtramos las sin resolver
  left_join(aud_campen %>% 
              select(iep, nro, ffa) %>% 
              filter(is.na(ffa)), # filtramos las sin ffa
            by = c("iep","nro")) %>%
  select(iep, nro, caratula, tdelito, fing, fvenc) %>% 
  left_join(apgyeJusEROrganization::listar_organismos() %>% 
              select(iep = organismo, organismo_descripcion, circunscripcion),
              by = "iep") %>%
  rename(organismo = organismo_descripcion) %>% 
  select(circunscripcion, nro, caratula, tdelito, fing, fvenc, -iep) %>% 
  mutate(vencida = if_else(as.Date(fvenc, format="%d/%m/%Y") < Sys.Date(), "si", "no")) %>% 
  arrange(circunscripcion, nro)
  
# View(primaria3era)  

primaria3era %>% 
  kable(caption = str_c("Listado primario de cauas en trámite"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                     full_width = F, font_size = 9) %>% 
  column_spec(c(3:4), '5cm') %>% 
  landscape()

```

\pagebreak

En la siguiente Tabla se detallan los expedientes ingresados a Cámara con situación "en trámite".

Estos datos no han sido vinculados con los datos de audiencias.


```{r primarias_pendientes}

primaria_pendientes <- res_campen %>% 
  select(iep, nro, everything()) %>% 
  group_by(iep, nro) %>%
  filter(id_submission == max(id_submission)) %>% # nos quedamos con la ultima presentación
  ungroup() %>% 
  filter(is.na(fres) | fres %in% c("", " ")) %>%
  left_join(apgyeJusEROrganization::listar_organismos() %>% 
              select(iep = organismo, organismo_descripcion, circunscripcion),
              by = "iep") %>%
  rename(organismo = organismo_descripcion) %>% 
  select(circunscripcion, nro, caratula, tdelito, fing, fvenc, -iep) %>% 
  arrange(circunscripcion, nro, fing)


primaria_pendientes %>%
  kable(caption = str_c("Listado primario de cauas en trámite sin cruzar con Audiencias"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                     full_width = F, font_size = 9) %>% 
  column_spec(c(2), '2cm') %>% 
  column_spec(c(3:4), '5cm')

```



<!-- \pagebreak -->

<!-- En la siguiente tabla se muestra el detalle de los datos primarios de Causas Iniciadas. -->

<!-- Se ordena alfabéticamente por Circunscripción y posteriormente por la fecha de inicio y número de expediente. -->

<!-- ```{r iniciadas_primarias} -->

<!-- inic_primarias <- inic_primarias %>%  -->
<!--   select(nro, caratula, tdelito, finicio, iep) %>%  -->
<!--   left_join(apgyeJusEROrganization::listar_organismos() %>%  -->
<!--               select(iep = organismo, organismo_descripcion, circunscripcion), -->
<!--               by = "iep") %>% -->
<!--   rename(organismo = organismo_descripcion) %>%  -->
<!--   select(circunscripcion, finicio, nro, caratula, tdelito, -iep, -organismo) %>%  -->
<!--   # mutate(vencida = if_else(as.Date(fvenc, format="%d/%m/%Y") < Sys.Date(), "si", "no")) %>%  -->
<!--   arrange(circunscripcion, finicio, nro)  -->

<!-- # reparamos caracter incorrecto en caratula de expediente 2010/23 -->
<!-- # existe_caso <- inic_primarias$caratula[inic_primarias$finicio == "2023-04-19" & inic_primarias$nro == "2010/23"] == "CALDERARO BRITOS BIANCA PERINA \u0096 SUS LESIONES GRAVES EN ACCIDENTE DE TRANSITO S-RECURSO DE APELACION" -->
<!-- #  -->
<!-- # if (existe_caso){ -->
<!-- #   inic_primarias$caratula[inic_primarias$finicio == "2023-04-19" & inic_primarias$nro == "2010/23"] <- "CALDERARO BRITOS BIANCA PERINA SUS LESIONES GRAVES EN ACCIDENTE DE TRANSITO S-RECURSO DE APELACION" -->
<!-- # } -->

<!-- if(any(str_detect(inic_primarias$caratula, '\u0096'))){ -->

<!--   inic_primarias <- inic_primarias %>%  -->
<!--     mutate(caratula = str_replace_all(caratula, '\u0096', "")) -->

<!-- } -->


<!-- inic_primarias %>% -->
<!--   # filter(!nro == "2010/23") %>% # probando -->
<!--   kable(caption = str_c("Listado primario de cauas iniciadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                      full_width = F, font_size = 9) %>% -->
<!--   column_spec(c(4:5), '5cm') -->
<!--   # landscape() -->


<!-- ``` -->


***
```{r final}
tiempo <- as.character(round(lubridate::as.duration(x = Sys.time() - informe_inicio), 2))
```
\begin{center}
Superior Tribunal de Justicia de Entre Ríos - Área de Planificación Gestión y Estadística  
\end{center}
<!-- El presente informe se ha generado en: `r tiempo` -->


