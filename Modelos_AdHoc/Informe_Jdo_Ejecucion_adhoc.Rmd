---
title: Informe sobre Juzgados Civil y Comercial N10
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=2cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '0')
knitr::opts_chunk$set(fig.height = 8, fig.width = 8)
source("/home/scastillo/apgyeinformes/R/informe.R")
source("/home/scastillo/apgyeinformes/R/poblacion.R")
```

```{r, toolsparams}
# Tools -----------------------------------------------------------------------
start_date = "2018-02-01"
end_date = "2022-01-01"
```


\pagebreak

```{r}
# Causas Iniciadas---------------------------------------------------------------
iniciados_ecq <- function(db_con, poblacion, start_date="2018-02-01", end_date = "2018-07-01", estadistico = c("promedio", "mediana", "conteo")) {

  #tipos_proceso <- apgyeOperationsJusER::Tipo_procesos %>% filter(materia == !!materia) %>% select(tipo_de_procesos,proceso_conocimiento)

  if(lubridate::ymd(start_date) < lubridate::make_date(2018,07,01)) {
    inic <- inic_xcetal1sem18(db_con, poblacion, "CINC1C")
    inic
  } else {
    inic <- db_con %>%
      apgyeDSL::apgyeTableData("CINC1C") %>%
      filter(iep %in% !!poblacion$organismo) %>%
      filter(!grepl("OFICIO|EXHORTO", tproc)) %>%
      mutate(finicio = dmy(finicio)) %>%
      filter(!is.na(finicio)) %>%
      filter(finicio >= start_date, finicio < end_date ) %>%
      filter(finicio >= data_interval_start, finicio < data_interval_end) %>%       
      select(iep, nro, caratula, tproc, finicio) %>%
      collect()
    inic
  }

  # Reimputación Jdo ECQ de Concrodia por presentaciones duplicadas hasta el mes de marzo 2019
  # anómala situación de las secretarías con BD unificadas
  resultado <- list()
  
  if (nrow(inic) > 0){

  # Evaluación sobre Jdo Concordia para impedir presentaciones duplicadas: nros impares secretaria 1, numeros pares secretaria 2. Fijado por Dra. Pasqualini
    if (any(str_detect(poblacion$organismo, "con")) &
        any(inic$finicio >= make_date(2019,02,01) &
            inic$finicio < make_date(2019,03,01)))  {
  
    is.even <- function(x) x %% 2 == 0
    
    inic_base <- inic %>% 
        filter(!(str_detect(iep, "con")))
    
    inic_base_con <- inic %>% 
        filter((str_detect(iep, "con"))) %>% 
        filter(!(finicio >= make_date(2019,02,01) & finicio < make_date(2019,03,01)))
    
    inic_base_con_feb <- inic %>% 
      filter((str_detect(iep, "con"))) %>% 
      filter((finicio >= make_date(2019,02,01) & finicio < make_date(2019,03,01))) %>% 
        mutate(pares = is.even(as.integer(nro))) %>% 
        filter((iep == "jdocco0502con" & pares )| (iep == "jdocco0501con" & !pares)) %>% 
        distinct(nro, .keep_all = T) %>% select(-pares) 
    
    inic <- bind_rows(inic_base, inic_base_con, inic_base_con_feb)
        
    inic
    
    } 
    else { 
      inic
    }
    
    
    inic <- inic %>%
      distinct(iep, nro, .keep_all = TRUE)

    inic <- resolverconvertidos(inic)

    inic <- inic %>%
      agrupartproc(materia = "ecq")

    inic_prim <- inic

    inic <- inic %>%
      mutate(fecha = lubridate::ceiling_date(finicio, "month") - lubridate::days(1)) %>%
      group_by(iep, gtproc_forma, tproc, fecha) %>%
      summarise(cantidad = n()) %>%
      arrange(gtproc_forma, desc(cantidad)) %>%
      do(janitor::adorn_totals(.)) %>%
      ungroup() %>%
      mutate(tproc = ifelse(gtproc_forma == "-", "subtotal", tproc)) %>%
      left_join(poblacion %>%
                    select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>%
      select(circunscripcion, organismo, grupo_proceso = gtproc_forma, tipo_proceso = tproc, everything(), -iep)


    resultado$inic_ecq_pc <- inic_prim %>%
      left_join(poblacion %>% select(iep=organismo, organismo=organismo_descripcion, circunscripcion), by="iep") %>%
      ungroup() %>%
      select(circunscripcion, organismo, grupo_proceso = gtproc_forma, tipo_proceso = tproc, nro, everything(), -iep, caratula) %>%
      arrange(circunscripcion, organismo, finicio)

    resultado$inic <- inic

    if(estadistico == "promedio") {

      resultado$inic <- resultado$inic %>%
        filter(tipo_proceso != "subtotal") %>%
        group_by(circunscripcion, organismo, grupo_proceso) %>%
        summarise(promedio_ingreso_mensual = round(mean(cantidad, na.rm = T), digits = 1)) %>%
        arrange(desc(promedio_ingreso_mensual))

    } else if (estadistico == "mediana") {

      resultado$inic <- resultado$inic %>%
        filter(tipo_proceso != "subtotal") %>%
        group_by(circunscripcion, organismo, grupo_proceso) %>%
        summarise(mediana_ingreso_mensual = round(median(cantidad, na.rm = T), digits = 1)) %>%
        arrange(desc(mediana_ingreso_mensual))

    } else if (estadistico == "conteo"){

      resultado$inic

    }

    resultado
  } else {
    resultado$inic
  }
}

resultado <-  iniciados_ecq(db_con = DB_PROD(), poblacion = jdos_ecq, start_date = start_date, end_date = end_date,
                              estadistico = "conteo")


inic_jdo10 <- resultado$inic_ecq_pc %>% 
  filter(circunscripcion == "Paraná", str_detect(organismo, "10")) %>% 
  distinct() %>% 
  mutate(fecha = as.Date(finicio)) %>% 
  mutate(periodo = tsibble::yearmonth(fecha), 
         organo = "jdo_civil_comercial_n10_ambas_secretarías")

inic_xmes <- inic_jdo10 %>% 
  group_by(periodo, grupo_proceso) %>% 
  summarise(cantidad = n()) 

grafico_iniciados <- inic_xmes %>% 
  ggplot(aes(x = as.Date(periodo), y = cantidad, fill = grupo_proceso)) + 
  geom_bar(stat="identity") +
  tsibble::scale_x_yearmonth(date_breaks = "2 month", date_labels = "%Y-%m") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.2), 
        #axis.text.y = element_text(size=12),
        axis.title.x=element_blank(),
        legend.title = element_blank(), legend.position = "top",
        axis.title.y=element_blank(), legend.text = element_text(size=10)
        ) +
  labs(title = "Causas inicidas - totales por mes -")

grafico_iniciados_sin_extremos <- inic_xmes %>% 
  filter(!periodo %in% c(ymd('2018-12-01'), ymd('2021-12-01'))) %>% 
  ggplot(aes(x = as.Date(periodo), y = cantidad, fill = grupo_proceso)) + 
  geom_bar(stat="identity") +
  tsibble::scale_x_yearmonth(date_breaks = "2 month", date_labels = "%Y-%m") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.2), 
        #axis.text.y = element_text(size=12),
        axis.title.x=element_blank(),
        legend.title = element_blank(), legend.position = "top",
        axis.title.y=element_blank(), legend.text = element_text(size=10)
        #strip.text = element_text(size=10), plot.title = element_text(size=12), 
        #plot.subtitle = element_text(size=10)
        ) +
  labs(title = "Causas inicidas -excluyendo meses con valores extremos-")

inic_xaño_xtproc <- inic_jdo10 %>% 
  mutate(año = year(fecha)) %>% 
  group_by(año, grupo_proceso) %>% 
  summarise(causas_iniciadas = n()) %>% 
  pivot_wider(names_from = año, values_from = causas_iniciadas) %>% 
  arrange(desc(`2021`)) %>% 
  adorn_totals('row') %>% 
  adorn_totals('col')

inic_pje <- inic_xmes %>% 
  group_by(grupo_proceso) %>% 
  summarise(causas_iniciadas = sum(cantidad)) %>% 
  mutate(porcentaje_iniciadas = round((causas_iniciadas/sum(causas_iniciadas))*100, digits = 1)) %>% 
  do(janitor::adorn_totals(.)) %>% 
  mutate(porcentaje_iniciadas = ifelse(grupo_proceso == "Total", 100, porcentaje_iniciadas)) %>% 
  rename('%' = porcentaje_iniciadas)
  
inic_xaño_xtproc <- inic_xaño_xtproc %>% 
  left_join(inic_pje) %>% ungroup() %>% select(-causas_iniciadas) 


```

## Causas iniciadas en el Juzgado Civil y Comercial N°10 de Paraná años 2018 a 2021
 
Cantidad de causas iniciadas por año y grupo de proceso en el organismo. La columna final presenta el porcentaje por grupos considerando el total de iniciados del período.
 
```{r, }
kable(inic_xaño_xtproc, align = 'c', longtable = TRUE)
```

Como vemos, más del 90% de lo que ingresa en el organismo corresponde a procesos *MONITORIOS* y *EJECUTIVOS*.    

Sin perjuicio de lo anterior, cabe destacar que la cantidad de ingresos mensuales varía notablemente, como se aprecia en el siguiente gráfico de causas iniciadas por mes.      

```{r}
grafico_iniciados
```

La distribución temporal de ingresos presenta valores extremos en el mes de diciembre, particularmente en el año 2018 y notablemente en el 2021 (que corresponden a procesos *MONITORIOS*). Aún excluyendo dichos valores anómalos, la cantidad de ingresos presenta importantes variaciones, como se advierte en el siguiente gráfico.    

```{r}
grafico_iniciados_sin_extremos
```

Una posible explicación de los picos de ingreso que se observan en este organismo puede buscarse en la forma de litigación que plantean ***grandes actores*** (personas que inician múltiples causas en breves períodos de tiempo). En la próxima tabla presentamos las causas iniciadas en el juzgado agrupadas por actor o grupos de actores que comparten características institucionales o industria (e.g.instituciones públicas, bancos, financieras, ventas minoristas, etc.). Aunque el agrupamiento es ad-hoc y por lo tanto puede presentar cierto margen de error, permite superar las inconsistencias de los registros del organismo en el LexDoctor, evidenciando un rasgo importante de la carga de trabajo del juzgado en torno a los sujetos que actúan en el. ^[Cabe destacar que la categoría 'OTROS' incluye casos especiales de dos actores con una gran cantidad de causas iniciadas: LUNA ALEJANDRO DAVID con 292 causas iniciadas en relación a la ejecución de sus honorario en Amparos de Salud y CANEPA MARIA EUGENIA con 200 causas iniciadas sobre ejecución de sus honorarios en Apremios promovidos como apodera de la Municip.de Parana.] 

```{r}

inic_jdo10_xactor <- inic_jdo10 %>% 
  mutate(actor = str_split_fixed(caratula, "C/", 2)[,1]) %>% 
  mutate(actor = str_replace_all(actor, "\\.", '')) %>% 
  group_by(periodo, actor) %>% 
  summarise(cantidad = n()) %>% 
  arrange(desc(cantidad))

inic_jdo10_xactor <- inic_jdo10_xactor %>% 
  mutate(actor_agrup = case_when(
    str_detect(actor, "ADMINISTRAC|ATER|ADMINISTRAD|TRIBUTARIA") ~ "ATER", 
    str_detect(actor, "MUNICIPALIDAD DE PARANA|MUNICIPIO DE PARANA") ~ "MUNICIPALIDAD DE PARANA",
    str_detect(actor, "MUNICIP") & ! str_detect(actor, "MUNICIPALIDAD DE PARANA")  ~ "OTRAS MUNICIPALIDADES", 
    str_detect(actor, "BANCO|BANK") ~ "BANCOS", 
    str_detect(actor, "GOBIERNO|ESTADO PROV|ESTADO DE") ~ "GOBIERNO ENTRE RIOS",
    str_detect(actor, "CFN|CONFINA|CREDIAR|CREDIL|CONFIAR|FINANC|CREDIC|CIERTO|SOÑAR") ~ "CONFINA_CREDIAR_CREDIL_CONFIAR_CIERTO_SOÑAR_ot",
    str_detect(actor, "MEGATONE|CETROGAR|^NALDO") ~ "MEGATONE_CETROGAR_NALDO",
     str_detect(actor, "INSTITUTO AUTAR") ~ "IAPV",
    str_detect(actor, "CAJA|PREVISION") ~ "CAJAS",
    str_detect(actor, "MUTUAL|COOPERAT") ~ "MUTUALES_COOPERATIVAS", 
    TRUE ~ 'OTROS')) 

inic_jdo10_xactor_sumario <- inic_jdo10_xactor %>%
  group_by(actor_agrup) %>% 
  summarise(causas_iniciadas = sum(cantidad)) %>% 
  arrange(desc(causas_iniciadas)) %>% 
  mutate(porcentaje_iniciadas = round((causas_iniciadas/sum(causas_iniciadas))*100, digits = 1)) %>% 
  do(janitor::adorn_totals(.)) %>% 
  mutate(porcentaje_iniciadas = ifelse(actor_agrup == "Total", 100, porcentaje_iniciadas)) 


inic_jdo10_xactor_gafico <- inic_jdo10_xactor %>% 
  ggplot(aes(x = as.Date(periodo), y = cantidad, fill = actor_agrup)) + 
  geom_bar(stat="identity") +
  tsibble::scale_x_yearmonth(date_breaks = "6 month", date_labels = "%Y-%m") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.2), 
        #axis.text.y = element_text(size=12),
        axis.title.x=element_blank(),
        legend.title = element_blank(), legend.position = "none",
        axis.title.y=element_blank(), legend.text = element_text(size=10)) +
  labs(title = "Causas inicidas por actor o grupo de actores a lo largo del período estudiado", 
       subtitle = "Para apreciar la distribución temporal de lo que inicia cada actor cada gráfico tiene su propia escala") +
  facet_wrap(~actor_agrup, scales = "free")

```


```{r}
kable(inic_jdo10_xactor_sumario, caption = "Causas Iniciadas en el período por actor o grupo de actores")
```

```{r}
inic_jdo10_xactor_gafico
```

## Resoluciones dictadas en el Juzgado Civil y Comercial N°10 de Paraná años 2018 a 2021

A continuación pasamos a revisar las resoluciones (autos y sentencias) dictadas por el organismo en el período estudiado.  

Como puede observarse en el siguiente gráfico en el bienio 2020-2021 la cantidad de resoluciones dictadas cayó un **-46%** respecto al bienio 2018-2019.^[resoluciones dictadas 2018-2019: 7767, resoluciones 2020-2021: 4177.]    


```{r}
# Causas Resueltas------------------------------------------------------------

res_prim <- function(db_con, poblacion, operacion = "CADR1C", start_date = "2018-02-01", end_date = "2018-09-02") {
    
    operacion = rlang::enexpr(operacion)
      
    resultado <- db_con %>%
      apgyeTableData(!! operacion) %>%
      apgyeDSL::interval(start_date, end_date) %>%
      filter(iep %in% !!poblacion$organismo) %>%
      resolverconvertidos() %>%
      mutate(fres = dmy(fres)) %>%
      collect() %>%
      filter(!(is.na(nro) & is.na(caratula))) %>%  # exclusión de registros con información crítica faltante
      filter(!is.na(fres)) %>%
      filter(fres >= data_interval_start & fres < data_interval_end) %>%
      filter(tres != "0" | is.na(tres)) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                              "circunscripcion")], by = c("iep" = "organismo")) %>%
      select(circunscripcion, organismo = organismo_descripcion, everything())
    
    resultado
    
  }

jdos_ecq <- jdos_ecq %>% 
  filter(str_detect(organismo, "10"))

resultado <- res_prim(db_con = DB_PROD(), poblacion = jdos_ecq,start_date = start_date, end_date = end_date)

resultado <- resultado %>% 
  filter(!is.na(nro)) %>% 
  mutate(fecha = as.Date(fres)) %>% 
  mutate(periodo = tsibble::yearmonth(fecha))
  
resoluciones_xmes <- resultado %>%
  mutate(autos_sentencias = ifelse(toupper(as) == "A", 'autos', 'sentencias')) %>% 
  group_by(periodo, autos_sentencias) %>% 
  summarise(cantidad = n()) 

resoluciones_xbienio <- resultado %>% 
  mutate(año = year(periodo)) %>% 
  mutate(bienio = ifelse(año %in% c(2018, 2019), '18-19', '20-21')) %>% 
  group_by(bienio) %>% 
  summarise(cantidad = n())

grafico_resoluciones <- resoluciones_xmes  %>%
  ggplot(aes(x = as.Date(periodo), y = cantidad, fill = autos_sentencias)) +
  geom_bar(stat="identity") +
  tsibble::scale_x_yearmonth(date_breaks = "2 month", date_labels = "%Y-%m") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.2), 
        #axis.text.y = element_text(size=12),
        axis.title.x=element_blank(),
        legend.title = element_blank(), legend.position = "top",
        axis.title.y=element_blank(), legend.text = element_text(size=10))


duraciones_xtproc <- resultado %>% 
   mutate(tproc = toupper(tproc)) %>%
   mutate(gtproc_forma = case_when(
        # str_detect(tproc, "^ORDINARIO|^ORDINARIO |USUCAP") ~ "ORDINARIOS", #
        # str_detect(tproc, "^SUMARISIMO|^SUMARISIMO ") ~ "SUMARISIMO",  #
        # str_detect(tproc, "^INCIDENTE|^INCIDENTE ") ~ "INCIDENTES",
        # str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR ") ~ "CAUTELARES",
        str_detect(tproc, "MONITORIO") ~ "MONITORIOS",
        str_detect(tproc, "^EJECU|^PROCESO DE EJECU|^APREMIO") ~ "EJECUCIONES", #
        # str_detect(tproc, "^INTERDICT") ~ "INTERDICTOS",
        # str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION") ~ "PROC.COSTIT.",
        # str_detect(tproc, "^PREPARACION ") ~ "PREPARATORIOS",
        # str_detect(tproc, "^SUCESORIO") ~ "SUCESORIOS", #
        # str_detect(tproc, "^BENEFICIO") ~ "BENEF.LITIG.SG.", #
        # str_detect(tproc, "^HOMOLOGACION") ~ "HOMOLOGACIONES", 
        # str_detect(tproc, "^SECUESTRO PREND") ~ "SECUESTRO PRENDARIO",
        # str_detect(tproc, "^CONCURSO|^QUIEBRA|^PEDIDO DE QUIEBRA") ~ "CONCURSOS/QUIEBRAS", 
        # # Anomalías: desclasificados, no procesos y errores
        # str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA") ~ "noprocesos",
        # str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|^SENTENCIA|SOLICITA") ~ "desclasificados",
        # str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL") ~ "error",
        # # Otros Fueros Laboral y Familia
        # str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION") ~ "desclasificados", 
        # str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON") ~ "desclasificados",
        # str_detect(tproc, "AMENAZA|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU ") ~ "desclasificados",
        # str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO") ~ "desclasificados",
        TRUE ~ "OTROS"
      )) %>% 
  mutate(finicio = dmy(finicio)) %>%
  mutate(durac_inicio_sentencia = as.integer(fres - finicio)) %>% 
  filter(durac_inicio_sentencia > 0) 

cantidad_res_xnro <- resultado %>% 
  group_by(nro) %>% 
  summarise(cantidad_ocurrencias = n())

duraciones_xtproc <- duraciones_xtproc %>% 
  left_join(cantidad_res_xnro)

# duraciones_xtproc_EJE_graf <- duraciones_xtproc %>% 
#   filter(str_detect(gtproc_forma, 'EJECUCIONES')) %>%
#   #filter(gtproc_forma == 'EJECUCIONES' & durac_inicio_sentencia > 146) %>% 
#   group_by(nro) %>% 
#   arrange(desc(durac_inicio_sentencia)) %>% 
#   slice(1) %>% 
#   ungroup() %>% 
#   ggplot(aes(x=durac_inicio_sentencia)) + 
#   geom_histogram(bins = 50) +
#   theme(axis.title.x=element_blank(),
#         legend.title = element_blank(), legend.position = "none",
#         axis.title.y=element_blank(), legend.text = element_text(size=10)) +
#   labs(title = "Frecuencia de las duraciones de procesos")


duraciones_xtproc_MON_graf <- duraciones_xtproc %>%
  filter(str_detect(gtproc_forma, 'MONITORIOS')) %>%
  #filter(durac_inicio_sentencia > 60) %>%
  group_by(nro) %>%
  filter(n() > 1) %>% 
  arrange(desc(durac_inicio_sentencia)) %>%
  slice(1) %>%
  ungroup() %>%
  ggplot(aes(x=durac_inicio_sentencia)) +
  geom_histogram(bins = 50) +
  theme(legend.title = element_blank(), legend.position = "none",
        legend.text = element_text(size=10)) +
  labs(title = "Duración de causas con múltiples resoluciones", 
       subtitle = 'Histograma que toma como duración la fecha de inicio de causa y la fecha de la última resolución') +
  xlab("duración en días corridos") +
  ylab("cantidad de causas")

# Procesos resueltos mediante sentencia monotoria:su duración en días corridos 
duraciones_procesosMON_cSM <- duraciones_xtproc %>% 
  filter(str_detect(gtproc_forma, 'MONITORIOS')) %>% 
  filter(toupper(as) == "S", tres == '8') %>% # sentencias monitorias
  mutate(duraciones_segmentadas = case_when(
    between(durac_inicio_sentencia, 0,30) ~ 'hasta 30 días', 
    between(durac_inicio_sentencia, 31,60) ~ 'entre 31 y 60 días', 
    between(durac_inicio_sentencia, 61,90) ~ 'entre 61 y 90 días', 
    between(durac_inicio_sentencia, 91,180) ~ 'entre 91 y 180 días', 
    between(durac_inicio_sentencia, 181,360) ~ 'entre 181 y 360 días',
    between(durac_inicio_sentencia, 361,1080) ~ 'entre 361 y 1080 días', 
    between(durac_inicio_sentencia, 1081, Inf) ~ 'mas de 1081 días')) %>%
  mutate(duraciones_segmentadas = factor(duraciones_segmentadas, 
                         levels = c('hasta 30 días', 
                                    'entre 31 y 60 días', 
                                    'entre 61 y 90 días', 
                                    'entre 91 y 180 días', 
                                    'entre 181 y 360 días',
                                    'entre 361 y 1080 días', 
                                    'mas de 1081 días')), ordered = TRUE) %>% 
  group_by(duraciones_segmentadas) %>% 
  summarise(cantidad_causas = n(), 
            'promedio duración' = round(mean(durac_inicio_sentencia, na.rm = T), digits = 2), 
            'mediana duración' = median(durac_inicio_sentencia, na.rm = T), 
            'desviacion estandar' = round(sd(durac_inicio_sentencia, na.rm = T), digits = 2)) %>%  
            #rango = range(durac_inicio_sentencia, na.rm = T)) %>% 
            # 'duración mínima' = min(durac_inicio_sentencia, na.rm = T), 
            # 'duración máxima' = max(durac_inicio_sentencia)) %>% 
  mutate(porcentaje = round((cantidad_causas/sum(cantidad_causas)) *100, digits = 1)) %>% 
  select('duraciones segmentadas' = duraciones_segmentadas, cantidad_causas, porcentaje, everything()) %>% 
  arrange('duraciones segmentadas')



```


```{r}
grafico_resoluciones
```
\pagebreak 

# Sobre la duración de procesos

En la tabla siguiente incluimos los resultado del análisis de duración de procesos. Considerando la cantidad de información analizada y a fin de lograr una presentación accesible e intuitiva de la misma, hemos segmentado las duraciones en distintos grupos. 

Al mismo tiempo, hemos seleccionado para análisis un grupo de casos representativo que son las *sentencias monitorias* dictadas en procesos MONITORIOS (grupo de mayores ingresos). Segmentamos este universo de casos en grupos con distintas duraciones, calculando las mismas en días corridos desde la fecha de inicio del proceso hasta la fecha de sentencia (según registro de Lex-Doctor).    

Encontramos que:

- La mitad de las sentencias monitorias que dicta el organismo (el 50%) lo hace dentro de los 30 días de iniciado el proceso,    
- un cuarto de las sentencias monitorias las dicta entre los 31 y 60 días, y      
- el cuarto restante las dicta luego de los 60 días.     
- Dentro del último cuarto (25%) con mayor duración, hay un 5% de sentencias monitorias que tienen una duración mayor a 181 días corridos desde la fecha de inicio del expediente, y dentro de este grupo de casos se observa una importante heterogeneidad en las duraciones.     


```{r}

kable(duraciones_procesosMON_cSM, caption = "Causas según duración y características estadísticas de cada grupo", align = 'c', longtable = TRUE) %>%
kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 10)  


```

# Histograma de duraciones en causas resueltas con sentencia monitoria

Dado que la Dra. Acevedo destaca la 'extensa duración' (i.e. *supervivencia*) que tienen las causas luego de dictada la sentencia, en este gráfico contamos la cantidad de causas considerando la duración desde el inicio hasta su última resolución. De esta manera procuramos aproximarnos a los casos que presentan alta supervivencia en su tramitación y generan múltiples resoluciones a lo largo del trámite, estimando también el tiempo que permanecen activas. Para mantener consistencia en los datos presentados anteriormente, hemos trabajado estrictamente con procesos *MONITORIOS* que constituyen el grupo más representativo de trámites del juzgado.    

```{r}
duraciones_xtproc_MON_graf
```

```{r}
# Movimientos-------------------------------------------------------------------
movimientos_db <- tbl(DB_PROD(), "movimientos") %>%
  filter(str_detect(codigo_organismo, "parcyc10")) %>%
  filter(fecha_hora >= start_date, fecha_hora <= end_date) %>%
  collect()

movimientos_xmes <- movimientos_db %>%
  mutate(fecha = as.Date(ymd_hms(fecha_hora))) %>%
  filter(fecha >= start_date, fecha < end_date) %>%
  distinct() %>%
  mutate(tipo_movimiento = case_when(
    tipo_movimiento == "procesal" | is.na(tipo_movimiento) ~ "actos_procesales",
    tipo_movimiento == "procesal_presentacion" ~"presentaciones_abogados")) %>%
  #mutate(periodo = tsibble::yearweek(fecha)) %>%
  mutate(periodo = tsibble::yearmonth(fecha)) %>%
  group_by(periodo, tipo_movimiento) %>%
  summarise(cantidad = n())
 
grafico_movimiento <- movimientos_xmes %>%
  ggplot(aes(x = as.Date(periodo), y = cantidad, fill = tipo_movimiento)) +
  geom_bar(stat="identity") +
  tsibble::scale_x_yearmonth(date_breaks = "3 month", date_labels = "%Y-%m") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.2),
        axis.title.x=element_blank(),
        legend.title = element_blank(), legend.position = "top",
        axis.title.y=element_blank(), legend.text = element_text(size=12),
        strip.text = element_text(size=10), plot.title = element_text(size=12),
        plot.subtitle = element_text(size=10),
        plot.caption = element_text(size=9, color = "darkgrey", hjust = 1)) +
  labs(title = "Movimientos Judiciales en el organismo")
```

## Actividad judicial considerando actos procesales y presentaciones digitales de abogados años 2020-2021

Finalmente agregamos la evolución de actos procesales y presentaciones digitales del Juzgado 10 a lo largo de los años 2020-2021 (período desde el cual se releva este dato sistemáticamente a través de información recogida en SIRIRI).   

```{r}
grafico_movimiento
```

