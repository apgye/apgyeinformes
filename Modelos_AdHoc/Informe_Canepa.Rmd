---
title: Informe Dr. Alejandro Cánepa
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output:
  pdf_document:
    includes:
      in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r}
library(readr)
library(lubridate)
library(gghighlight)
source("../R/informe.R")
source("../R/duracion.R")
source("../R/poblacion.R")

start_date = "2018-02-01"
end_date = "2023-05-01"
desagregacion_mensual = T
poblacion_infte <- oga %>% filter(organismo == "ogapen0000pna")
tja = tja %>% filter(organismo == "tjapen0000pna ")

juez = "3556"

sentencias = tribble()

# tribunal de Juicio----------------------------------------------
resolpt <- DB_PROD_CONNECTION %>% apgyeDSL::apgyeTableData(RESOLPT) %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!poblacion_infte$organismo) %>% 
  mutate(nro = ifelse(is.na(nro), "sin_dato", nro)) %>% 
  mutate(iep = ifelse(iep == "ogapen0000con" & grepl("\\/S|\\/s", nro), "ogapen0000ssa", iep)) %>% 
  group_by(iep, data_interval_start, data_interval_end) %>% 
  process(RESOLPT)
  
res_tjui <- resolpt$res_prim %>% 
  filter(str_detect(vact, juez)) %>% 
  select(nro, caratula, tproc, fres, jact = vact, tipo = tres) %>% 
  mutate(j_a = "juicio")

resumen_sent <- res_tjui %>% 
  mutate(año_mes = format(fres, "%Y-%m")) %>% 
  group_by(año_mes) %>% 
  summarise(cantidad_sentencias = n()) %>% 
  arrange(año_mes)


sentencias <-  res_tjui %>% 
  mutate(año_mes = format(fres, "%Y-%m")) %>% 
  group_by(año_mes) %>% 
  summarise(cantidad_sentencias = n()) %>% 
  mutate(instancia = "tribunal_juicio")

#resumen_sent[1,2]

# abreviados------------------------------------------------------
res_abrev <- DB_PROD() %>% apgyeDSL::apgyeTableData(ABREV) %>%
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep %in% !!oga$organismo) %>% 
  filter(inst == "2") %>% 
  collect() %>% 
  filter(str_detect(jact, juez))# %>% 
  # mutate(instancia = case_when(
  #       # inst == "1" ~ "garantías",
  #     inst == "2" ~ "tribunal_juicio",
  #     TRUE ~ "na")) %>%
  # mutate(tipo_sentencia = case_when(
  #     tsent == "1" ~ "condena_efectiva",
  #     tsent == "2" ~ "condena_efectiva_sustit",
  #     tsent == "3" ~ "condena_condicional",
  #     tsent == "4" ~ "medida_seguridad", #repe con 7
  #     tsent == "5" ~ "absolución",
  #     tsent == "6" ~ "sobreseimientos",
  #     tsent == "7" ~ "medida_seguridad",
  #     tsent == "8" ~ "otras",
  #     TRUE ~ "na")) 

res_abrev <- res_abrev %>% 
  select(nro, caratula, tproc, fres, jact, tipo =tsent) %>% 
  mutate(j_a = "abreviado")
  
resumen_sent <- resumen_sent %>% 
  # agrego abreviados
  bind_rows(res_abrev %>% 
              mutate(año_mes = format(dmy(fres), "%Y-%m")) %>% 
              group_by(año_mes) %>% 
              summarise(cantidad_sentencias = n())) %>% 
  # sumo sentencias juicios comunes y abrevidos
  group_by(año_mes) %>% 
  summarise(cantidad_sentencias = sum(cantidad_sentencias, na.rm = T)) %>% 
  arrange(año_mes)

sentencias <- sentencias %>% 
  bind_rows(res_abrev %>% 
              mutate(año_mes = format(dmy(fres), "%Y-%m")) %>% 
              group_by(año_mes) %>% 
              summarise(cantidad_sentencias = n()) %>% 
              mutate(instancia = "juicios_abreviados"))
  

# resumen_sent[1,2]

# amparos ------------------------------------------
amparos <- DB_PROD() %>% apgyeDSL::apgyeTableData(CADRPPC) %>%
  apgyeDSL::interval(start_date, end_date) %>% 
  filter(iep %in% !!poblacion_infte$organismo) %>% 
  collect() %>% 
  filter(str_detect(mact, juez)) %>% 
  mutate(fres = dmy(fres))


res_amparos <- amparos %>% 
  select(nro, caratula, tproc, fres, jact = mact, tipo = tres) %>% 
  mutate(j_a = "amparo") 
  
resumen_sent <- resumen_sent %>% 
  # agrego abreviados
  bind_rows(res_amparos %>% 
              mutate(año_mes = format(fres, "%Y-%m")) %>% 
              group_by(año_mes) %>% 
              summarise(cantidad_sentencias = n())) %>% 
  # sumo sentencias juicios comunes y abrevidos
  group_by(año_mes) %>% 
  summarise(cantidad_sentencias = sum(cantidad_sentencias, na.rm = T)) %>% 
  arrange(año_mes)

sentencias <- sentencias %>% 
  bind_rows( res_amparos %>% 
              mutate(año_mes = format(fres, "%Y-%m")) %>% 
              group_by(año_mes) %>% 
              summarise(cantidad_sentencias = n()) %>% 
              mutate(instancia = "amparos"))



#resumen_sent[1,2]

# correccional-----------------------------------------

correccional_canepa <- read_csv("~/apgyeinformes/Modelos_AdHoc/data/correccional_canepa.csv") %>% 
  apgyeDSL::interval("2018-08-01", "2020-01-01") 

correccional_canepa <- correccional_canepa %>% as_tibble() %>% 
  filter(str_detect(json_input, "Sentencias_Dictadas")) %>% 
  separate_rows(json_input, sep = ",") %>% 
  mutate(json_input = str_remove_all(json_input, "[:punct:]")) %>% 
  mutate(json_input = str_split_fixed(str_trim(json_input), " ", 2))  

corr_as <- tibble(
 "mes" = correccional_canepa$data_interval_start,
 "tipo" = correccional_canepa$json_input[,1], 
 "cantidad" = correccional_canepa$json_input[,2]
)

corr_as <- corr_as %>% 
  pivot_wider(names_from = "tipo", values_from = "cantidad")


corr_senten <- corr_as %>% 
  mutate(año_mes = format(mes, "%Y-%m"), 
         SentenciasDictadas = as.integer(SentenciasDictadas)) %>% 
  select(año_mes, cantidad_sentencias = SentenciasDictadas)


resumen_sent <- resumen_sent %>% 
  # agrego abreviados
  bind_rows(corr_senten) %>% 
  # sumo sentencias juicios comunes y abrevidos
  group_by(año_mes) %>% 
  summarise(cantidad_sentencias = sum(cantidad_sentencias, na.rm = T)) %>% 
  arrange(año_mes)

sentencias <- sentencias %>% 
  bind_rows(corr_senten %>% 
            mutate(instancia = "correccional_sentencias"))

#resumen_sent[1,2]

```

\pagebreak

# Sentencias Dictadas por Mes Dr.Alejandro Cánepa `r getDataIntervalStr(start_date, end_date)`

```{r}
resumen_sent %>% 
  janitor::adorn_totals("row") %>% 
  kable(caption = str_c("Sentencias Dictadas"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 12) 
  
```

\pagebreak



```{r}
resumir_audip_tja(db_con = DB_PROD(), tja, start_date = start_date,
                  end_date = end_date, desagregacion_mensual = F) %>% 
  filter(str_detect(magistrado_integracion, "CANEPA")) %>% 
     rename_with(~ str_c(., "*"), starts_with("promedio")) %>%
    select(-minutos_en_audiencia) %>%
    select(1:7) %>%
    kable(caption = str_c("Audiencias Penales Realizadas y Horas en Audiencia",
                          " (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE, booktabs = T) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 9)  %>% 
    add_header_above(c(" " = 3, "Realizaciones" = 4)) %>%
    kable_styling(latex_options = c("repeat_header")) %>% 
    column_spec(2, "12cm") %>% 
    column_spec(c(3), border_right = T) %>%
    row_spec(0, angle = 90) %>%
    footnote(general = "Los promedios se calculan como: total / cantidad de meses con información",
           general_title = "*:", footnote_as_chunk = T, title_format = c("bold")) %>%
    landscape()

```


\pagebreak

# Tabla primaria de Sentencias Dictadas por Mes Dr.Alejandro Cánepa `r getDataIntervalStr(start_date, end_date)`

En esta tabla se encuentra el datalle desagregado de las sentencias dictadas considerando la instancia: sentencias dictadas en el juzgado correccional, sentencias de tribunal de juicio (comun), sentencias en juicios abreviados, sentencias en amparos.

```{r}
sentencias %>% arrange(año_mes) %>% 
  kable(caption = "Sentencias", align = 'c', longtable = TRUE, booktabs = T) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 9) 

```





