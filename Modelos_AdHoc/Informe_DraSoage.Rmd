---
title: "Informe Sra.Vocal Dra.Laura M.Soage"
author: ""
date: "`r format(Sys.Date(), '%d de %B de %Y')`"
output:
  pdf_document:
    toc: TRUE
    toc_depth: 3
    number_sections: TRUE
    includes:
        in_header: header.tex
        before_body: before_body.tex
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')

source("/home/scastillo/apgyeinformes/R/informe.R")
source("/home/scastillo/apgyeinformes/R/poblacion.R")

start_date = "2018-02-01"
end_date = "2023-01-01"

cam_lab <- cam_lab %>% 
  filter(circunscripcion == "Concordia")

camlab_dic_primaria <- read_delim("~/apgyeinformes/Modelo_Parametrizado/data/CamLab_Diciembre2022.csv", 
                                   delim = "\t", escape_double = FALSE, 
                                   trim_ws = TRUE) %>% 
  mutate(fres = dmy(fres), fvenc = dmy(fvenc), feg1 = dmy(feg1), fdesp1 = dmy(fdesp1)) %>%
  filter(tres != "0") %>%  # validacion medida mej.prov
  filter(!(is.na(nro) & is.na(caratula))) %>% 
  filter(str_detect(ordv, "^1409|^SO")) %>% 
  mutate(devol_en_termino = feg1 <= fvenc,
         nro = as.character(nro)) %>% 
  select(nro, caratula, tproc, as, fecha_despacho = fdesp1, fecha_devolucion = feg1,
         fecha_vencimiento = fvenc, fecha_resolucion = fres, devol_en_termino) %>% 
  mutate(primer_voto_en_termino = ifelse(devol_en_termino, "si", "no")) %>% 
  arrange(desc(primer_voto_en_termino))
  
result <- DB_PROD() %>%
  apgyeTableData("CADR2L") %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!cam_lab$organismo) %>%
  resolverconvertidos() %>%
  mutate(fres = dmy(fres), fvenc = dmy(fvenc), feg1 = dmy(feg1), fdesp1 = dmy(fdesp1)) %>%
  collect() %>%
  filter(tres != "0") %>%  # validacion medida mej.prov
  filter(!(is.na(nro) & is.na(caratula))) %>% 
  filter(!is.na(feg1)) %>%
  filter(str_detect(ordv, "^1409|^SO")) %>% 
  mutate(devol_en_termino = feg1 <= fvenc) %>% 
  select(nro, caratula, tproc, as, fecha_despacho = fdesp1, fecha_devolucion = feg1,
         fecha_vencimiento = fvenc, fecha_resolucion = fres, devol_en_termino) %>% 
  mutate(primer_voto_en_termino = ifelse(devol_en_termino, "si", "no")) %>% 
  arrange(desc(primer_voto_en_termino))

result <- bind_rows(result, camlab_dic_primaria)

sentencia_en_termino <- result %>% 
  filter(primer_voto_en_termino == "si") %>% 
  mutate(sent_en_termino = fecha_resolucion <= fecha_vencimiento)

sentenciaentermino = round(mean(sentencia_en_termino$sent_en_termino)*100)

resumen <- result %>% 
  group_by(primer_voto_en_termino) %>% 
  summarise(cantidad = n()) %>% 
  arrange(desc(cantidad))

resumen_vencidos <- result %>% 
  filter(primer_voto_en_termino == "no") %>% 
  mutate(dias_luegovenc = as.integer( fecha_devolucion - fecha_vencimiento)) 

promedio_dias_vencidos = round(mean(resumen_vencidos$dias_luegovenc, na.rm = T))

resumen_pp <- resumen %>% 
  janitor::adorn_percentages("col") %>%  
  mutate_if(is.numeric, list(~ . * 100)) %>% 
  mutate_if(is.numeric, round) %>% 
  select(-primer_voto_en_termino) %>% 
  rename_all(~paste0(., "_%")) 


# Histórico
historico_prim <- read_delim("/home/scastillo/apgyeinformes/Modelo_Parametrizado/data/Camara_lab_con_hit.csv", 
                 delim = "\t", escape_double = FALSE, trim_ws = TRUE)

historico_prim <- historico_prim %>% 
  rename(fecha_despacho = fdesp1, 
         fecha_vencimiento = fdesp2,
         fecha_resolucion = fdesp3, 
         primer_voto = as, 
         as = feg2) %>% 
  select(nro, caratula, tproc, fecha_despacho, fecha_vencimiento, fecha_resolucion, as, primer_voto) %>% 
  mutate(fecha_despacho = dmy(fecha_despacho), 
         fecha_vencimiento = dmy(fecha_vencimiento), 
         fecha_resolucion = dmy(fecha_resolucion)) %>% 
  filter(!is.na(fecha_resolucion))

```


\pagebreak

# Primer Voto Dra.Laura M.Soage, Cámara Laboral Concordia, `r apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`

Los datos empleados para este informe fueron relevados a través del Área de Planificación, Gestión y Estadística del STJER mediante el **sistema de estadística pública judicial** vigente desde el año 2018, cuya información puede consultarse a través del siguiente enlace: https://tablero.jusentrerios.gov.ar/. Para completar la serie histórica se presenta, además, un punto específico con los datos del sistema estadístico anterior al año 2018 y las categorías disponibles en el mismo.      

En la próxima tabla se presenta la cantidad de primeros votos emitidos por la Vocal Dra. Laura M. Soage entre el 2018-02-01 y el `r ymd(end_date)-1` en relación al plazo legal establecido para el dictado de resoluciones.   

Se informa que para las resoluciones dictadas luego del plazo de vencimiento se ocupó en promedio `r promedio_dias_vencidos` días corridos para el dictado de la correspondiente sentencia.    

Adjuntamos al final del documento Anexo 1 con listado de procesos y sus respectivas fechas de resolución.    

```{r, message=FALSE, warning=FALSE, echo=FALSE}
resumen %>% 
  bind_cols(resumen_pp) %>% 
  janitor::adorn_totals("row") %>% 
  kable(caption = "Dra.Laura M.Soage: primeros votos emitidos en relación al plazo legal de resolución", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),full_width = F, font_size = 10)  %>% 
  footnote(general = "Votación completa en termino: 614/650 = 94%")
```


```{r}
resoluciones <- apgyeProcesamiento::resoluciones_lab(db_con = DB_PROD(), poblacion = cam_lab, 
                 start_date = start_date, end_date = end_date, 
                 operacion = "CADR2L", desagregacion_mensual = T)


camlab_dic_primaria_v2 <- read_delim("~/apgyeinformes/Modelo_Parametrizado/data/CamLab_Diciembre2022.csv", 
                                   delim = "\t", escape_double = FALSE, 
                                   trim_ws = TRUE) %>% 
  mutate(fres = dmy(fres), fvenc = dmy(fvenc), feg1 = dmy(feg1), fdesp1 = dmy(fdesp1)) %>%
  filter(tres != "0") %>% 
  select(nro, caratula, tproc, as, fecha_despacho = fdesp1, fecha_devolucion = feg1,
         fecha_vencimiento = fvenc, fecha_resolucion = fres) 

CamLab_dic22_proc <- camlab_dic_primaria_v2 %>% 
  filter(fecha_resolucion > as.Date("2022-11-30")) %>% 
  mutate(fecha = floor_date(fecha_resolucion, unit = "year"),
         organismo = "Cam Apelaciones Sala Laboral") %>% 
  filter(fecha == as.Date("2022-01-01")) %>% 
  group_by(organismo, fecha, as) %>% 
  summarise(cantidad = n()) %>% 
  ungroup() %>% mutate(as = ifelse(as == "A", "autos", "sentencias")) %>% 
  pivot_wider(names_from = "as", values_from = "cantidad")

resultado <- resoluciones %>% 
  filter(año_mes != "Total") %>% 
  mutate(fecha = floor_date(ymd(str_c(año_mes, "-01")), unit = "year")) %>% 
  bind_rows(CamLab_dic22_proc) %>% 
  group_by(organismo, fecha) %>% 
  summarise(sentencias = sum(sentencias), 
            autos = sum(autos)) %>%
  arrange(desc(fecha)) 

resultado <- resultado %>% 
  rowwise() %>% 
  mutate(resoluciones = sum(autos, sentencias, na.rm=T)) 

total_resol <- sum(resultado$resoluciones, na.rm = T)

```

\blandscape

# Resoluciones dictadas por la Cámara Laboral de Concordia `r apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`

```{r, fig.width=14, fig.height=12, fig.align='center'}

resultado %>% 
  #filter(fecha != ymd("2022-01-01")) %>% 
  group_by(fecha) %>% summarise(resoluciones = sum(resoluciones, na.rm = T)) %>% 
  ggplot(aes(x=fecha, y=resoluciones)) +
  geom_bar(stat="identity", fill="#66CC99") + 
  annotate("text", x = ymd("2020-01-01"), y = 500, 
            label = paste("Total resoluciones: ", total_resol), color = "black", size = 11) +
  geom_text(aes(label = resoluciones), # vjust = "inward", hjust = "inward",
            vjust = 1, size = 12,  show.legend = FALSE) + 
  # annotate("text", x = floor_date(Sys.Date()-years(1), unit = "year"), y = 150, 
  #          label = paste("parcial ", year(Sys.Date()-years(1))), size = 11,  angle = 90) +
  theme(#axis.text.x = element_text(angle = 90, hjust = 1, size = 12),
          legend.position = "non", legend.title = element_blank(), 
          axis.text.x = element_text(size = 18), 
          axis.text.y = element_text(size = 18), 
          panel.grid.major = element_blank(), panel.grid.minor = element_blank(), 
          panel.border=element_blank(), axis.ticks = element_blank(), 
          axis.title.x=element_blank(), 
          axis.title.y=element_blank(), legend.text = element_text(size=14),
          strip.text = element_text(size=10), plot.title = element_text(size=14), plot.subtitle = element_text(size=12)) +
  xlab("") +  ylab("") 
```

# Cantidad y tipo de resoluciones dictadas por año en la Cámara Laboral de Concordia `r apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`

```{r, fig.width=14, fig.height=12, fig.align='center'}
resultado %>% ungroup() %>%  select(-resoluciones, -organismo) %>% 
    pivot_longer(!fecha, names_to = "tipo_resolucion", values_to = "cantidad") %>% 
    group_by(fecha, tipo_resolucion) %>% 
    summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
    #filter(fecha != ymd("2022-01-01") & fecha != ymd("2012-01-01")) %>% 
    ggplot(aes(fill=tipo_resolucion, y=cantidad, x=fecha)) + 
    geom_bar(position="stack", stat="identity") +
    #scale_fill_brewer() +
    geom_text(aes(label = cantidad), position = position_stack(vjust = 0.5), size = 6, color = "#333333") +
    theme(axis.text.x = element_text(vjust = 0.2, size = 12, color = "black"), 
          legend.position = "top", legend.title = element_blank(), 
          panel.grid.major = element_blank(), panel.grid.minor = element_blank(), 
          panel.border=element_blank(), axis.ticks = element_blank(), 
          axis.title.x=element_blank(), # axis.text.y=element_blank(),
          axis.title.y=element_blank(), legend.text = element_text(size=14),
          strip.text = element_text(size=10), plot.title = element_text(size=14), plot.subtitle = element_text(size=12)) +
    labs(#title = "Cantidad de resoluciones dictadas por año discriminadas por Cámara",
         x = "",
         caption = "APGE-STJER") #+
     # annotate("text", x = floor_date(Sys.Date()-years(1), unit = "year"), y = 420, size = 6,
     #         label = paste("parcial ", year(Sys.Date()-years(1))), angle = 90) 
```

# Sentencias recurridas en la Cámara Laboral de Concordia (resultados en la instancia) `r getDataIntervalStr(start_date, end_date)`

```{r, fig.width=14, fig.height=12, fig.align='center'}

res_xtres <- DB_PROD() %>%
    apgyeTableData("CADR2L") %>%
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% !!cam_lab$organismo) %>%
    mutate(fres = dmy(fres)) %>%  mutate(fdesp = dmy(fdesp1)) %>%
    mutate(finicio = dmy(finicio)) %>% mutate(fvenc = dmy(fvenc)) %>%
    filter(!is.na(fres)) %>%
    #filter(toupper(as) == "S") %>%
    collect() %>%
    filter(fres >= data_interval_start & fres < data_interval_end) %>%
    filter(!tres %in% c("0")) %>%
    ungroup() %>%
    mutate(tipo_resolucion = case_when(
      tres == "1" ~ "Confirmación",
      tres == "2" ~ "Revocación",
      tres == "3" ~ "Revocación Parcial",
      tres == "4" ~ "Nulidad",
      tres == "5" ~ "Concesión RIL",
      tres == "6" ~ "Denegación RIL",
      tres == "7" ~ "Concede Queja",
      tres == "8" ~ "Deniega Queja",
      tres == "9" ~ "Otras",
      tres == "10" ~ "Nulidad Parcial")) %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                              "circunscripcion")],
              by = c("iep" = "organismo")) 

sentencias = res_xtres %>% 
  # filtro las sentencias en 2a instancia 
  filter(toupper(as) == "S") 

resumen_xtres <- res_xtres %>% 
  group_by(circunscripcion, organismo_descripcion, tipo_resolucion) %>%
  summarise(cantidad = n()) %>%
  #filter(!tipo_resolucion %in% c( "Incompetencia", "Excepción")) %>%
  do(janitor::adorn_totals(.)) %>%
  ungroup()

df_sent = res_xtres %>% 
  # filtro las sentencias en 2a instancia + las concesiones/denegacion de RIL
  filter(toupper(as) == "S" | str_detect(tipo_resolucion, "RIL")) %>% 
  filter(!str_detect(tipo_resolucion, "Queja"))
           
data <- df_sent %>% 
  group_by(tipo_resolucion) %>% 
  summarise(cantidad = n()) %>% 
  mutate(sentenciaorecursos = ifelse(str_detect(tipo_resolucion, "RIL"), "sentencias_recurridas","sentencias_totales")) %>% 
  group_by(sentenciaorecursos) %>% 
  summarise(cantidad = sum(cantidad)) %>% 
  t() %>% as_tibble() %>% row_to_names(1) %>% mutate_all(as.integer) %>% 
  rowwise() %>% 
  mutate(sentencias_no_recurridas = sentencias_totales - sentencias_recurridas)

recurribilidad_total <- data %>% t() %>% as.data.frame() %>% setNames(data[,1]) %>% 
  rownames_to_column() %>% rename(recurribilidad = 1, cantidad = 2) %>% 
  filter(recurribilidad != "sentencias_totales")

recurribilidad_total = recurribilidad_total %>% 
  janitor::adorn_percentages("col") %>% 
  mutate(recurribilidad = paste0(recurribilidad, " (", round(cantidad*100, digits = 2), "%)"))   
  
ggpubr::ggdonutchart(recurribilidad_total,  
                     x = "cantidad",
                     label = "recurribilidad", 
                     fill = "recurribilidad",
                     lab.pos = "in", 
                     lab.font = c(8, "black"))  
```

# Sentencias casadas y sentencias firmes de la Cámara Laboral de Concordia `r getDataIntervalStr(start_date, end_date)`

```{r, fig.width=14, fig.height=12, fig.align='center'}

resultado_prim <- DB_PROD() %>%
    apgyeTableData("CADR3L") %>%
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% !!sal_lab$organismo) %>%
    mutate(fres = dmy(fres)) %>%
    filter(!is.na(fres)) %>%
    filter(toupper(as) == "S") %>%
    filter(torig == "1") %>% 
    collect() %>%
    filter(fres >= data_interval_start & fres < data_interval_end) %>%
    filter(tres != "0") %>%
    ungroup() %>%
    mutate(tipo_resolucion = case_when(
      tres == "1" ~ "Declara Mal Concedido RIL",
      tres == "2" ~ "RIL Inadmisible",
      tres == "3" ~ "RIL Procedente",
      tres == "4" ~ "RIL Improcedente",
      tres == "5" ~ "Hace lugar Rec. De Queja",
      tres == "6" ~ "Rechaza Rec.de Queja",
      tres == "7" ~ "Concesión Rec. Extraordinario",
      tres == "8" ~ "Denegación Rec. Extraordinario",
      tres == "9" ~ "Regulación de Honorarios",
      tres == "10" ~ "Otras",
      TRUE ~ "sin dato")) 

resultado_xaño = resultado_prim %>% 
  mutate(año = year(fres)) %>% 
  group_by(año, tipo_resolucion) %>% summarise(cantidad = n())

#write.table(resultado_xaño, "resultado_sentenciasRIL_STJ-CamLabConcordia.csv", col.names = T, row.names = F, sep = ";")

resultado = resultado_prim %>% 
  mutate(camara_origen = "Cámara Laboral de Concordia") %>% 
  filter(str_detect(tipo_resolucion, "RIL")) %>%
  mutate(resultado_recurso = ifelse(str_detect(tipo_resolucion, "Inadmisible|Improcedente|Mal"), "recursos_rechazados", "recursos_admitidos")) %>% 
  group_by(camara_origen, resultado_recurso) %>% 
  summarise(cantidad = n())


df_total = resultado %>% 
  pivot_wider(names_from = "resultado_recurso", values_from = "cantidad") %>% 
  #janitor::adorn_totals("row") %>% 
  rowwise() %>% 
  mutate(total = recursos_admitidos + recursos_rechazados, 
         '% admitidos' = paste0(round((recursos_admitidos/total)*100, digits = 2), " %"), 
         '% rechazados' = paste0(round((recursos_rechazados/total)*100, digits = 2), " %")) 

df_val = df_total %>% 
  select(camara_origen, recursos_admitidos, recursos_rechazados ) %>% 
  pivot_longer(-camara_origen, names_to = "resultado", values_to = "cantidad") 
  
df_val$resultado[df_val$resultado == "recursos_admitidos"] = paste0(df_val$resultado[df_val$resultado == "recursos_admitidos"], " ",
                                                                    df_total$`% admitidos`)
df_val$resultado[df_val$resultado == "recursos_rechazados"] = paste0(df_val$resultado[df_val$resultado == "recursos_rechazados"], " ",
                                                                    df_total$`% rechazados`)

df_casadas_dictadas <- tribble(
  ~resultado, ~cantidad,
  "sentencias_dictadas_enCamara", nrow(sentencias), 
  "sentencias_casadas_enSTJ", df_val$cantidad[str_detect(df_val$resultado, "admitidos")]
)

df_casadas_dictadas_resumen <- df_casadas_dictadas %>% 
  pivot_wider(names_from = "resultado", values_from = "cantidad") %>% 
  mutate(sentencias_firmes = sentencias_dictadas_enCamara - sentencias_casadas_enSTJ) %>% 
  mutate('% casadas' = paste0(round((sentencias_casadas_enSTJ/sentencias_dictadas_enCamara)*100, digits = 2), " %"), 
         '% firmes' = paste0(round((sentencias_firmes/sentencias_dictadas_enCamara)*100, digits = 2), " %")) 

df_prop <- df_casadas_dictadas %>% 
  bind_rows(tribble(
    ~resultado, ~cantidad,
  "sentencias_firmes", df_casadas_dictadas_resumen$sentencias_firmes
  )) %>% 
  filter(!str_detect(resultado, "dictadas")) %>% 
  mutate(resultado = ifelse(str_detect(resultado, "casadas"), paste0(resultado, " ", df_casadas_dictadas_resumen$`% casadas`), 
                            paste0(resultado, " ", df_casadas_dictadas_resumen$`% firmes`)))

df_prop %>% 
  ggpubr::ggdonutchart(  
                     x = "cantidad",
                     label = "resultado", 
                     fill = "resultado",
                     lab.pos = "in", 
                     lab.font = c(8, "black"))  
```

\elandscape

# Resultado de los recursos ante el STJER de la Cámara Laboral de Concordia `r getDataIntervalStr(start_date, end_date)`

```{r}
resultado_xaño %>% 
  pivot_wider( names_from = "año", values_from = "cantidad") %>% 
  janitor::adorn_totals("row") %>% 
  janitor::adorn_totals("col") %>% 
  kable(caption = "Resultado de los Recursos según decisión de la Sala Laboral del STJER", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),full_width = F)  
```

```{r}
start_date = "2011-02-01"
end_date = "2018-01-01"

```

```{r}

result_historico <- historico_prim %>%
  filter(fecha_resolucion >= start_date, fecha_resolucion < end_date) %>% 
  mutate(fecha = floor_date(fecha_resolucion, unit = "year")) %>% 
  mutate(as = ifelse(str_detect(as, "S"), "S", "A")) %>% 
  filter(!is.na(as)) %>% 
  group_by(fecha) %>% 
  summarise(cantidad = n()) %>% 
  arrange(fecha)

# resultado <- resultado %>% 
#   rowwise() %>% 
#   mutate(resoluciones = sum(autos, sentencias, na.rm=T)) 

total_resol <- sum(result_historico$cantidad, na.rm = T)

```

\blandscape

# Datos anteriores al año 2018, Cámara Laboral Concordia  `r getDataIntervalStr(start_date, end_date)`

```{r, fig.width=14, fig.height=12, fig.align='center'}
historico_prim %>% 
  mutate(as = ifelse(str_detect(as, "S"), "sentencia", "auto")) %>% 
  mutate(fecha = floor_date(fecha_resolucion, unit = "year")) %>% 
  rename(tipo_resolucion = as) %>% 
  group_by(fecha, tipo_resolucion) %>% 
  summarise(cantidad = n()) %>% 
  filter(fecha < as.Date("2018-01-01"),
         !is.na(tipo_resolucion)) %>% 
  ggplot(aes(fill=tipo_resolucion, y=cantidad, x=fecha)) + 
    geom_bar(position="stack", stat="identity") +
    #scale_fill_brewer() +
    annotate("text", x = ymd("2014-01-01"), y = 500, 
            label = paste("Total resoluciones: ", total_resol), color = "black", size = 11) +
    geom_text(aes(label = cantidad), position = position_stack(vjust = 0.5), size = 6, color = "#333333") +
    theme(axis.text.x = element_text(vjust = 0.2, size = 12, color = "black"), 
          legend.position = "top", legend.title = element_blank(), 
          panel.grid.major = element_blank(), panel.grid.minor = element_blank(), 
          panel.border=element_blank(), axis.ticks = element_blank(), 
          axis.title.x=element_blank(), # axis.text.y=element_blank(),
          axis.title.y=element_blank(), legend.text = element_text(size=14),
          strip.text = element_text(size=10), plot.title = element_text(size=14), plot.subtitle = element_text(size=12)) +
    labs(#title = "Cantidad de resoluciones dictadas por año discriminadas por Cámara",
         x = "",
         caption = "APGE-STJER") #+
```

\pagebreak

# Datos anteriores primer voto Dra.Laura Soage  `r getDataIntervalStr(start_date, end_date)`

```{r, fig.width=14, fig.height=12, fig.align='center'}

resultado_historico_soage <- historico_prim %>%
  filter(fecha_resolucion >= start_date, fecha_resolucion < end_date) %>% 
  mutate(fecha = floor_date(fecha_resolucion, unit = "year")) %>% 
  mutate(as = ifelse(str_detect(as, "S"), "S", "A")) %>% 
  filter(!is.na(as)) %>% 
  filter(str_detect(primer_voto, "^SOAG")) %>% 
  group_by(fecha) %>% 
  summarise(cantidad = n()) %>% 
  arrange(fecha)

total_resol <- sum(resultado_historico_soage$cantidad, na.rm = T)

resultado_historico_soage %>% 
  ggplot(aes(x=fecha, y=cantidad)) +
  geom_bar(stat="identity", fill="#66CC99") + 
  annotate("text", x = ymd("2014-01-01"), y = 300, 
            label = paste("Resoluciones 1er Voto Dra.Soage: ", total_resol), color = "black", size = 11) +
  geom_text(aes(label = cantidad), # vjust = "inward", hjust = "inward",
            vjust = 1, size = 12,  show.legend = FALSE) + 
  theme(#axis.text.x = element_text(angle = 90, hjust = 1, size = 12),
          legend.position = "non", legend.title = element_blank(), 
          axis.text.x = element_text(size = 18), 
          axis.text.y = element_text(size = 18), 
          panel.grid.major = element_blank(), panel.grid.minor = element_blank(), 
          panel.border=element_blank(), axis.ticks = element_blank(), 
          axis.title.x=element_blank(), 
          axis.title.y=element_blank(), legend.text = element_text(size=14),
          strip.text = element_text(size=10), plot.title = element_text(size=14), plot.subtitle = element_text(size=12)) +
  xlab("") +  ylab("") 

```

\pagebreak

```{r}
start_date = "2018-02-01"
end_date = "2022-12-01"
```

\elandscape

# Anexo: Listado primario de procesos y resoluciones con primer voto Dra.Laura M.Soage  `r getDataIntervalStr(start_date, end_date)`

```{r, message=FALSE, warning=FALSE, echo=FALSE}
result %>%   select(-tproc, -devol_en_termino) %>% mutate(caratula = str_sub(caratula, 1, 10)) %>% 
  kable(caption = "Listado de causas con primer voto Dra.Laura M.Soage", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),full_width = F, font_size = 10) %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```

