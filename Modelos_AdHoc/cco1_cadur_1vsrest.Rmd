---
title: Duracion de Procesos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
params:
  circunscripcion:
    label: "Circunscripción (separar con coma sin espacio o 'Todas')"
    input: text
    value: "Todas"
  start_date: 
    label: "Fecha_inicio_inf"
    input: text
    value: "2018-02-01"
  end_date:
    label: "Fecha_fin_inf"
    input: text
    value: "2021-01-01"
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
# https://www.kaggle.com/studentar/data-cleaning-challenge-outliers-r
# Validación de datos: valores extremos y anomalías
# Regla1: se eliminan las duracones negativas
# Regla2: más de tres desviaciones estándar es un outlier
# Regla3: se eliminan NA
# http://r-statistics.co/Loess-Regression-With-R.html
source("../R/informe.R")
source("../R/habiles.R")
source("../R/poblacion.R")


circ <- unlist(str_split(params$circunscripcion, ","))
start_date <- params$start_date
end_date <- params$end_date


jdos_cco_solo <- jdos_cco %>% 
  filter(!str_detect(materia, "eje|cqb")) %>% 
  filter(circunscripcion %in% circ)


duraciones <- duracion_prim(db_con = DB_PROD(), jdos_cco_solo, "CADR1C", 
                            start_date = "2017-10-01", end_date = end_date)

analisi_durac <- duracion_A(jdos_cco_solo, duraciones)

```


## Duracion de Procesos 

La duración de los procesos es una medición importante de la actividad judicial, con un alto impacto en la valoración social sobre el servicio de justicia. Por ello presentamos a continuación los primeros datos sistemáticos sobre este indicador relevados desde el inicio del Nuevo Sistema Estadístico del STJER (octubre 2017) a la fecha.      

Para una valoración compleja de este indicador remitimos a las observaciones metodológicas de este apartado, y enunciamos aquí aspectos sobresalientes de la misma:    

+ los procesos incluidos en este análisis son procesos resueltos por Juzgados Civiles y Comerciales (excluyendo a órganos especializados -ej.Juzgados de Ejecución-),     
+ los casos estudiados son los *procesos de conocimiento* excluyendo otros grupos (que serán objeto de tratamiento particular),              
+ empleamos la mediana estadística para evitar la distorsión asociada a valores extremos (exclusión que hacemos constar en una tabla en la sección metdologócia), y              
+ las duraciones se miden en días corridos desde el inicio de la causa registrada en los sistemas de gestión de causas de los órganos judiciales hasta la fecha de sentencia definitiva.     

Los diversos interrogantes que pueden surgir de este análisis son de gran importancia para el Área de Planificación, Gestión y Estadística por lo que esperamos sus comentarios. Así también, ponemos a disposición todo el material técnico y datos empleado en nuestro análisis a fin de contribuir a la reproducibilidad de nuestros hallazgos.    

\pagebreak

## Duracion General de Procesos de Conocimiento   

Gráfico que muestra la duración general de procesos de conocimiento en los organismos del fuero. Dicha duración se expresa en la mediana estadística obtenida de las duraciones particulares de los casos comprendidos en cada año.      

```{r}
analisi_durac$g_general
```

<!-- \pagebreak -->

<!-- ## Duracion General de Procesos de Conocimiento según Tipos Específicos       -->

<!-- ### Variación en los últimos dos años (enteros) -->

<!-- Gráfico que muestra los procesos en donde disminuyeron (azul) o aumentaron (rojo) las duraciones de proceso, y la cantidad de días de diferencia.          -->

<!-- ```{r} -->
<!-- analisi_durac$g_duraciones_variacion -->
<!-- ``` -->

\pagebreak

### Tabla de Duraciones anuales y variación interanual últimos dos años    

Tabla que muestra las medianas de duraciones de procesos por año para cada tipo de proceso. 

```{r}
analisi_durac$duracion_ts_tabla_xaño %>% 
  kable(caption = "Causas Iniciadas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  
```


\blandscape

## Grafico de Duraciones por Tipos de Proceso y Años

```{r, fig.width=19, fig.height=16}
analisi_durac$g_duraxtipoxaño
```


```{r, fig.width=19, fig.height=16}
analisi_durac$gts_duracxproc
```

```{r, fig.width=19, fig.height=16}
analisi_durac$gts_duracxproc_facet
```
\elandscape

\pagebreak

## Observaciones metodológicas

Se pone a disposición todo el material de procesamiento estadístico diseñada en R-Statistcal Computing.   

+ Los tipos de procesos seleccionados son los 10 tipos con mayor cantidad de casos relevados en la muestras de causas con sentencia entre las fechas del relevamiento.
+ Los casos incluidos en la tabla comprenden a todos los procesos resueltos por cada organismo en el período considerado, con exclusión de los siguientes procesos: Incidentales, Cautelares, Sucesorios, Procesos Constitucionales, Beneficios de Litigar Sin Gastos, Interdictos, Homologaciones, Preparación de Vías, Segundo Testimonio, Oficios y Exhortos, y tipos no acordes a la legislación vigente (eg.CONCURSO CERRADO, EXPEDIENTE INTERNO, ADMINISTRATIVO, PERSONAL).    
+ Se practicaron reimputación de tipos de procesos por errores de registración de los organismos (e.g. el *ORDINARIO CIVIL* se reimputó como *ORDINARIO*). Estas reimputaciones están a disposición en la rutina de procesamiento estadístico.      
+ Se excluyeron valores extremos en cada tipo de proceso mayores o menores a 3 desviaciones estándar. Estos valores extremos pueden estar asociados a errores de registración. No obstante ello atento el carácter crítico de estos casos y, eventualmente, la necesidad de un análisis detenido de los mismos agregamos la tabla de valores extremos más abajo.     
+ La duración de los procesos resueltos por cada organismo a través del dictado de sentencia definitiva se calcula como la diferencia en días corridos entre la fecha de inicio de la causa y la fecha del dictado de la sentencia según declaración del organismo.    
+ Para las líneas de tendencia empleamos la Regresión Local para vincular duraciones y el contexto interno y externo.    
+ Finalmente, a fin de evitar datos sesgados por la presencia de valores extremos se optó por la mediana estadística para este análisis y se excluyeron procesos por organismo con menos de dos casos resueltos.    

### Casos con valores extremos o anómalos

En esta tabla usted puede encontrar ordenado por organismo los casos con valores extremos o anómalos identificados en el estudio. Se entiende por *anomalía* en este contexto a todo valor de duración mayor o menor a 3 desviaciones estandar.     

```{r}
analisi_durac$duracion_outliers %>% 
  kable(caption = "Casos Extremos o Anómalos", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>% 
  column_spec(5, "5cm") %>% 
  landscape()
  
```

### Base Completa de Casos Considerados

```{r}
analisi_durac$duraciones_tidy_subselec %>% 
  group_by(circunscripcion, organismo, tipo_proceso) %>% 
  summarise(cantidad = n()) %>% 
  tidyr::spread(tipo_proceso, cantidad, fill = 0) %>%
  janitor::adorn_totals("row") %>% 
  janitor::adorn_totals("col") %>% 
  kable(caption = "Base de casos considerados por Proceso y Organismo", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```

<!-- # Juzgado Civil y Comercial N° 7: subgrupo de casos incluidos en el analisis de la circunscripción Paraná -->

<!-- A continuación presentamos los casos del organismo que quedaron comprendidos dentro del análisis de duraciones de procesos de conocimiento aplicado a todos los juzgados. Cabe destacar que los cosos que a continuación se detallan fueron incluidos conforme una pauta objetiva de selección: cada tipo de proceso está incluido en alguno de los 10 grupos de procesos de conocimiento con mayor cantidad de casos en todos los organismos en el período estudiado.  -->

<!-- ```{r} -->

<!-- df = analisi_durac$duraciones_tidy_subselec %>%  -->
<!--   filter(str_detect(organismo, "7")) -->

<!-- duracon_xaño_base <- df %>%  -->
<!--   mutate(nro_fres = str_c(nro, "-", fres)) %>%  -->
<!--   filter(!is_outlier) %>% -->
<!--   mutate(año = year(fres)) %>% -->
<!--   group_by(tipo_proceso, año) %>%  -->
<!--   summarise(mediana_duracion = round(median(duracion, na.rm = T), digits = 1), -->
<!--             cantidad_casos = n(),  -->
<!--             detalle_nro_fres = str_c(nro_fres, collapse = ', '))  -->

<!-- duracion_ts_tabla_xaño <- duracon_xaño_base %>%  -->
<!--   select(-c(cantidad_casos, detalle_nro_fres)) %>%  -->
<!--   tidyr::spread(año, mediana_duracion) %>%  -->
<!--   janitor::clean_names()  -->


<!-- ``` -->


<!-- ```{r} -->
<!-- duracon_xaño_base %>%  -->
<!--   kable(caption = "Sentencias incluidas en los casos considerados supra", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10) %>%  -->
<!--   column_spec(5, '8cm') %>%  -->
<!--   landscape() -->
<!-- ``` -->
<!-- ## Duraciones por año y tipo de procesos del subgrupo de casos del juzgado  -->

<!-- ```{r} -->
<!-- duracion_ts_tabla_xaño %>%  -->
<!--   kable(caption = "Duraciones", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  -->
<!-- ``` -->
## Duraciones de procesos de conocimiento sub-seleccionados en al análisis de la circunscripción Paraná.

```{r}
df %>% filter(!is_outlier) %>%
    mutate(año = year(fres)) %>%
    group_by(año) %>% 
    summarise(mediana_duracion = median(duracion, na.rm = T)) %>%
    ggplot(aes(x = año, y = mediana_duracion, fill = as.factor(año))) + 
    geom_bar(stat="identity", position = position_dodge(width = 0.5)) +
    scale_fill_brewer(palette="Set1") +
    geom_text(aes(label = mediana_duracion), position = position_dodge(0.9),
              vjust = -0.5, size = 3) +
    theme_minimal() +
    theme(axis.text.x = element_text(hjust = 1,size=14,face="bold"), axis.text.y = element_blank(),
          legend.position = "none", axis.title=element_text(size=14,face="bold")) +
    labs(title = "Duración General de Procesos de Conocimiento: Evolución Interanual",  
         subtitle = "Mediana calculada en días corridos de inicio hasta sentencia", y = "Duraciones", x = "", 
         caption = "APGE-STJER") 
```
### Base de Casos Sub-seleccionado por Ranking (top 10)

```{r}
df %>% 
  group_by(circunscripcion, organismo, tipo_proceso) %>% 
  summarise(cantidad = n()) %>% 
  tidyr::spread(tipo_proceso, cantidad, fill = 0) %>%
  janitor::adorn_totals("row") %>% 
  janitor::adorn_totals("col") %>% 
  kable(caption = "Base de casos considerados por Proceso y Organismo", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```

### Base Completa de casos en el período estudiado

```{r}
duraciones %>% 
  filter(str_detect(organismo, "7")) %>% 
  mutate(nro_fres = str_c(nro,"-",fres)) %>% 
  group_by(circunscripcion, organismo, tipo_proceso) %>% 
  summarise(cantidad = n(), 
            mediana = round(median(duracion, na.rm = T), digits = 1), 
            promedio = round(mean(duracion, na.rm = T), digits = 1), 
            minimo = min(duracion, na.rm = T), 
            maximo = max(duracion, na.rm = T), 
            desviacion_st = round(sd(duracion, na.rm = T), digits = 1),
            casos_y_fres = str_c(nro_fres, collapse = "; ")) %>% 
  arrange(desc(cantidad)) %>% 
  kable(caption = "Base Completa de casos del juzgado", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 8)  %>% 
  column_spec(10, "8cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()




```

### Base Completa de casos en el año 2022

```{r}
duraciones %>% 
  filter(str_detect(organismo, "7")) %>% 
  mutate(año = year(fres)) %>% 
  filter(año == 2022) %>%
  mutate(nro_fres = str_c(nro,"-",fres)) %>% 
  group_by(tipo_proceso) %>% 
  summarise(cantidad = n(), 
            mediana = round(median(duracion, na.rm = T), digits = 1), 
            promedio = round(mean(duracion, na.rm = T), digits = 1), 
            casos_y_fres = str_c(nro_fres, collapse = "; ")) %>% 
  arrange(desc(cantidad)) %>% 
  kable(caption = "Base Completa de casos del juzgado", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 8)  %>% 
  
  row_spec(0, angle = 90) %>% 
  landscape()

```

### Excluciones de casos 20853 y 20371 en el informe del 29/04/22

Para analizar duraciones en los procesos de conocimiento se efectuó un ranking de los 10 tipos de procesos con mayor cantidad de casos. En dicha selección los exepdientes 20853 y 20371 con tipo de proceso 'ORDINARIO CUMPLIMIENTO DE CONTRATO' se ubicaron en el ranking 16 por lo que fueron excluidos del universo de casos a analizar. Esta exclusión se justitifica pues el propósito del estudio no era un análisis exhaustivo de las duraciones por organismo sino establecer medidas de referencia para las duraciones de los procesos más comunes en el Fuero Civil en toda la provincia, con una distribución balanceada a lo largo de todos los órganos. 




