---
title: Resoluciones en Tribunal de Juicio y Apelación
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')


source("../R/informe.R")
source("../R/habiles.R")
source("../R/poblacion.R")

start_date = "2020-02-01"
end_date = "2022-02-01"

resol_tja <- resoluciones_pen(db_con = DB_PROD(), poblacion = tja,
                                start_date = start_date, end_date = end_date,
                                desagregacion_mensual = T)

```

\blandscape

## Sentencias en Tribunal de Juicio - `r getDataIntervalStr(start_date, end_date)`


```{r, tjui,  echo = FALSE, message = FALSE, warning = FALSE}
resol_tja$tjui %>%  
  filter(circunscripcion != 'Total') %>% 
  mutate(año = str_sub(año_mes, 1,4)) %>% ungroup() %>% 
  select(circunscripcion, año, sobreseimientos:absoluciones) %>% 
  group_by(circunscripcion, año) %>% 
  summarise_if(is.numeric, sum, na.rm = TRUE) %>% 
  kable()
```


## Resoluciones en Tribunal de Apelación  `r getDataIntervalStr(start_date, end_date)`

```{r, apel, echo = FALSE, message = FALSE, warning = FALSE}

resol_tja$resultado_tapel %>% 
  filter(circunscripcion != 'Total') %>% 
  mutate(año = str_sub(año_mes, 1,4)) %>% ungroup() %>% 
  select(circunscripcion, año, confirmatoria:otra) %>% 
  group_by(circunscripcion, año) %>% 
  summarise_if(is.numeric, sum, na.rm = TRUE) %>% 
  kable()
```


## Resoluciones de Magistrados de Tribunal de Juicio en Procesos Constitucionales  `r getDataIntervalStr(start_date, end_date)`

Para identificar los casos bajo examen se consultó la base de datos del SGP en referencia a los magistrados de Tribunal de Juicio (etiquetados como cargi = "Vocales").      


```{r}
df <- DB_PROD() %>% 
  apgyeTableData('CADRPPC') %>% 
  apgyeDSL::interval(start_date, end_date) %>% 
  mutate(fres = dmy(fres)) %>%
  filter(!is.na(fres)) %>% 
  filter(fres >= data_interval_start & fres < data_interval_end) %>% 
  collect() 
  
df %>% 
  left_join(DB_PROD()%>% tbl('gh_personal_distinct') %>%
                mutate(magistrado = apellido,iep_actuante = iep) %>%
                select(idagente, iep_actuante, categoria, magistrado, jurisdiccion), by=c("mact"="idagente"), copy = T) %>% 
  filter(str_detect(categoria, "VOCAL")) %>% 
  mutate(año = year(fres)) %>% 
  group_by(jurisdiccion, año) %>% 
  summarise(cantidad = n()) %>% 
  kable()

```

\elandscape


