# Taiga 1767
# Obtención de Jueces Penales 2019

source("R/informe.R")
source("R/poblacion.R")

desde <- "2019-02-01"
hasta <- "2020-01-01"

dbprod <- DB_PROD()
personal_2019 <- dbprod %>% 
  apgyeDSL::apgyeTableData('personal_planta_ocupada') %>%
  filter(data_interval_start >= !!desde,
         data_interval_end <= !!hasta) %>%
  left_join(dbprod %>% apgyeDSL::apgyeTableData('personal_personas') %>%
              filter(data_interval_start >= !!desde,
                     data_interval_end <= !!hasta) %>%
              select(idagente, idcategoria, categoria, apellido, nombres, funcion), by=c("idagente")) %>% 
  collect()


personal_2019_filtrado <- personal_2019 %>% 
  distinct(idagente, .keep_all = T) %>% # dejamos un registro por persona
  filter(grepl("JUEZ DE |VOCAL", categoria)) %>%  # dejamos los Jueces
  filter(grepl("Pena|OGA|Garant|Trans|Juicio", organismo) &
           !grepl("Familia|Civil", organismo)) # dejamos los organismos Penales

View(personal_2019_filtrado)

cantidad_magistrados_2019 <- nrow(personal_2019_filtrado)
  
cantidad_magistrados_2019
# [1] 60
   
