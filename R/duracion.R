# Duraciones
duracion_prim <- function(poblacion, operacion = "CADR1C", start_date = "2018-07-01", end_date = "2018-09-02") {
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% !!poblacion$organismo) %>% 
    resolverconvertidos()
  
  
  # Separa Primera y Segunda Instancia
  
  if (str_detect(poblacion$organismo[[1]], "jdo")) {
    
    # Primera Instancia
    resultado <- resultado %>% 
      group_by(iep) %>% 
      process() %>%
      .$result %>%
      ungroup() 
    
    resultado <- resultado %>% 
      select(organismo = iep, durac_inic_sent_xproc) %>% 
      mutate(durac_inic_sent_xproc = as.character(durac_inic_sent_xproc)) %>% 
      tidyr::separate_rows(durac_inic_sent_xproc, sep="#")  %>%
      tidyr::separate(durac_inic_sent_xproc, into = c("tipo_proceso", "duracion", "fres", "nro"), sep="&") %>%
      mutate(tipo_proceso = str_replace(tipo_proceso, "[[:punct:]]", "")) %>% 
      mutate(tipo_proceso = str_replace(tipo_proceso, '"', "")) %>%
      mutate(tipo_proceso = str_trim(tipo_proceso)) %>% 
      mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tipo_proceso)) %>% 
      mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "ORDINARIO CIVIL"), "ORDINARIO", tipo_proceso)) %>% 
      mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "SUMARISIMO CIVIL"), "SUMARISIMO", tipo_proceso)) %>% 
      mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "DESALOJO Y COBRO DE ALQUIL|DESALOJO"), "DESALOJO Y COBRO ALQUILERES", tipo_proceso)) %>% 
      mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "DIVORCIO"), "DIVORCIO", tipo_proceso)) %>% 
      filter(!str_detect(tipo_proceso, "APELACION JUZGADO"))
      
    resultado
    
  } else {
    
    # Segunda Instancia
    resultado <- resultado %>% 
      filter(as %in% c("S", "s")) %>% 
      filter(!is.na(fres)) %>% filter(tres != "0") %>%
      mutate(fres = dmy(fres)) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect() %>% 
      ungroup()  %>% 
      mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>% 
      mutate(tproc = str_replace(tproc, "[[:punct:]]", "")) %>% 
      mutate(tproc = str_trim(tproc))  %>% 
      filter(!str_detect(tproc, "VIOLENCIA|AMPARO"), !is.na(tproc)) %>% 
      filter(fres <= Sys.Date()) %>%
      select(organismo = iep, nro, caratula, tipo_proceso = tproc, finicio, fres) %>%
      mutate(duracion = fres - finicio)  
       
    resultado
    
  }
  
  resultado <- resultado %>% 
    mutate(tipo_proceso = str_sub(gsub("[^[:alnum:][:blank:]?&/\\-]", "", tipo_proceso))) %>% 
    ungroup() %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], by = "organismo") %>% 
    select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo) %>% 
    mutate(fres = ymd(fres), duracion = as.integer(duracion))
  
  resultado
  
}

duracion_A <- function(poblacion, duraciones) {
  
  # Analiza variación entre año base y año target
  
  library(outliers)
  library(viridis)
  library(ggalt)
  library(viridisLite)
  
 
  analisis_durac_GT <- list()
  
  analisis_durac_GT$duraciones_tidy <- duraciones %>% 
    filter(duracion > 0) %>% 
    group_by(tipo_proceso) %>% 
    mutate(outlier_score = scores(duracion)) %>% 
    mutate(is_outlier = outlier_score > 3 | outlier_score < -3) %>% 
    filter(!is.na(is_outlier)) %>% 
    ungroup()
  
  
  if (str_detect(poblacion$organismo, "cco")) {
    
    analisis_durac_GT$duraciones_tidy_subselec <- analisis_durac_GT$duraciones_tidy %>% 
      durac_subselec("civil") 
    analisis_durac_GT$duraciones_tidy_subselec
      
  } else if (str_detect(poblacion$organismo, "fam")) {
    
    analisis_durac_GT$duraciones_tidy_subselec <- analisis_durac_GT$duraciones_tidy %>% 
      durac_subselec("familia") 
    analisis_durac_GT$duraciones_tidy_subselec
    
  } else if (str_detect(poblacion$organismo, "lab"))  {
    
    analisis_durac_GT$duraciones_tidy_subselec <- analisis_durac_GT$duraciones_tidy %>% 
      durac_subselec("laboral") 
    analisis_durac_GT$duraciones_tidy_subselec
    
  }
    
  analisis_durac_GT$duraciones_tidy_subselec <- analisis_durac_GT$duraciones_tidy_subselec %>% 
    rename(tipo_proceso = tproc) %>% 
    add_count(tipo_proceso, name = "temp") %>%
    mutate(temp = dense_rank(desc(temp))) %>%
    filter(temp %in% 1:10) %>%
    select(-temp)
  
  analisis_durac_GT$g_dts_outliers <- analisis_durac_GT$duraciones_tidy_subselec %>% 
    ggplot(aes(x = tipo_proceso, y = duracion)) +
    geom_boxplot() +
    coord_flip() +
    facet_wrap(~is_outlier) +
    labs(title = "Exclusión de valores anómalos en duraciones",  
         subtitle = "Valor anómalo: > o < 3 desviaciones estandar", y = "tipos_proceso", x = "", 
         caption = "APGE-STJER")
  
  
  
  # Tablas-------------------------------------------------------------------
  
  analisis_durac_GT$duraciones_ts_tabla <- analisis_durac_GT$duraciones_tidy_subselec %>% 
    filter(!is_outlier) %>%
    mutate(año = year(fres)) %>%
    group_by(tipo_proceso, año) %>% 
    summarise(mediana_duracion = round(mean(duracion, na.rm = T)), 
              cantidad_casos = n()) 
  
  analisis_durac_GT$duracion_ts_tabla_xaño <- analisis_durac_GT$duraciones_ts_tabla  %>% 
    select(-cantidad_casos) %>% 
    tidyr::spread(año, mediana_duracion) %>% 
    janitor::clean_names() 
  
  analisis_durac_GT$duracion_ts_tabla_xaño$variacion_ultimos_dos_años =
    analisis_durac_GT$duracion_ts_tabla_xaño[[length(names(analisis_durac_GT$duracion_ts_tabla_xaño))]] -
    analisis_durac_GT$duracion_ts_tabla_xaño[[(length(names(analisis_durac_GT$duracion_ts_tabla_xaño))-1)]]
  
  names(analisis_durac_GT$duracion_ts_tabla_xaño) <-  str_remove_all(names(analisis_durac_GT$duracion_ts_tabla_xaño), "x")
  names(analisis_durac_GT$duracion_ts_tabla_xaño) <-  str_replace_all(names(analisis_durac_GT$duracion_ts_tabla_xaño), pattern = "_", replacement = " ")
  
  analisis_durac_GT$duracion_outliers <-  analisis_durac_GT$duraciones_tidy_subselec %>% 
    filter(is_outlier) %>%
    mutate(fres_durac = str_c(nro,":", fres,":",duracion)) %>% 
    group_by(circunscripcion, organismo, tipo_proceso) %>% 
    summarise(cantidad = n(), 'caso (nro.expte.:fecha resolucion:duración)' = str_c(fres_durac, collapse = ",")) %>% 
    arrange(desc(cantidad))
  
  
  # Gráficos---------------------------------------------------------------
  
  analisis_durac_GT$g_general <- analisis_durac_GT$duraciones_tidy_subselec %>% 
    filter(!is_outlier) %>%
    mutate(año = year(fres)) %>%
    group_by(año) %>% 
    summarise(mediana_duracion = median(duracion, na.rm = T)) %>% 
    ggplot(aes(x = año, y = mediana_duracion, fill = as.factor(año))) + 
    geom_bar(stat="identity", position = position_dodge(width = 0.5)) +
    scale_fill_brewer(palette="Set1") +
    geom_text(aes(label = mediana_duracion), position = position_dodge(0.9),
              vjust = -0.5, size = 6) +
    theme_minimal() +
    theme(axis.text.x = element_text(hjust = 1,size=18,face="bold"), 
          legend.position = "none", axis.title=element_text(size=18,face="bold"),
          legend.text = element_text(size=18), 
          axis.text.y = element_text(size = 18)) +
    labs(title = "Duración General de Procesos de Conocimiento: Evolución Interanual",  
         subtitle = "Mediana calculada en días corridos de inicio hasta sentencia", y = "Duraciones", x = "", 
         caption = "APGE-STJER") 
  
  
  analisis_durac_GT$g_duraciones_variacion <- analisis_durac_GT$duraciones_tidy_subselec %>% 
    filter(!is_outlier) %>%
    mutate(año = year(fres)) %>%
    group_by(tipo_proceso, año) %>% 
    summarise(mediana_duracion = round(mean(duracion, na.rm = T))) %>% 
    tidyr::spread(año, mediana_duracion) %>% select(1,3,4) %>% 
    janitor::clean_names() %>% 
    rowwise() %>% mutate(variacion_interanual = x2019-x2018) %>% rowwise() %>% 
    mutate(variacion = ifelse(variacion_interanual < 0, "disminucion", "aumento")) %>%
    arrange(desc(variacion_interanual)) %>% 
    ggplot(aes(x = reorder(tipo_proceso, -variacion_interanual), y = variacion_interanual)) +
    geom_bar(aes(fill = variacion), stat = 'identity') +  # color by class
    geom_text(aes(label = variacion_interanual), position = position_dodge(0.9),
              vjust = 1, hjust = 1, size = 5) +
    coord_flip() +  # horizontal bars
    theme_minimal() +
    geom_text(aes(y = 0, label = tipo_proceso, hjust = as.numeric(variacion_interanual > 0))) +  # label text based on value
    scale_fill_manual("Variación:", values = c("disminucion" = "blue","aumento" = "red")) +
    theme(axis.text.y = element_blank(),axis.text.x = element_text(hjust = 1,size=8),
          legend.position='top') +
    labs(title = "Variación Interanual de la Duración de Procesos",  
         subtitle = "Mediana en días corridos", y = "", x = "", 
         caption = "APGE-STJER") 
  
  analisis_durac_GT$g_duraxtipoxaño <- analisis_durac_GT$duraciones_tidy_subselec %>% 
    filter(!is_outlier) %>%
    mutate(año = year(fres)) %>%
    group_by(tipo_proceso, año) %>%
    summarise(mediana_duracion = round(mean(duracion, na.rm = T))) %>%
    mutate(año = as.factor(año)) %>% 
    # rowwise() %>% mutate(tendencia = x2018-x2019 > 0) %>% rowwise() %>% 
    # mutate(tendencia = ifelse(tendencia, "disminucion", "aumento")) %>%
    ggplot(aes(x = tipo_proceso, y = mediana_duracion, fill = año)) + 
    geom_bar(stat="identity", position = position_dodge(width = 0.5)) +
    geom_text(aes(label = mediana_duracion), position = position_dodge(0.9),
              vjust = -1, size = 5) +
    scale_fill_brewer(palette="Set1") +
    theme(axis.text.x = element_text(angle = 90, size = 12, hjust = 1), axis.text.y = element_blank(), 
          axis.title.x=element_blank(), axis.title.y=element_blank(), 
          legend.text = element_text(size=20), legend.position = "top", legend.title = element_blank()) +
    labs(title = "Duraciones de proceso por Tipo y Año",  
         subtitle = "Mediana calculada en días corridos", y = "Duraciones", x = "", 
         caption = "APGE-STJER") 
  
  
  # Graf Series Temporales
  
  analisis_durac_GT$gts_duracxproc <- analisis_durac_GT$duraciones_tidy_subselec %>% 
    filter(!is_outlier) %>%
    ggplot(aes(x = fres, y = duracion, colour = tipo_proceso)) +
    geom_smooth(method = "auto", se = F) +
    theme_minimal() +
    scale_x_date(date_breaks = "2 month", date_labels = "%b-%y" ) +
    theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 12), 
          strip.text = element_text(size = 18)) +
    scale_color_viridis(discrete = TRUE, option = "D") +
    labs(title = "Evolución de la Duración de Procesos",  
         subtitle = "Línea de tendencia calculada por método de Regresion Local", 
         y = "Duraciones", x = "", caption = "APGE-STJER") 
  
  # facetado
  
  analisis_durac_GT$gts_duracxproc_facet <- analisis_durac_GT$duraciones_tidy_subselec %>%  
    filter(!is_outlier) %>%
    ggplot(aes(x = fres, y = duracion, colour = tipo_proceso)) +
    geom_smooth(method = "auto", se = F) +
    theme_minimal() +
    scale_x_date(date_breaks = "2 month", date_labels = "%b-%y" ) +
    theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 12), 
          strip.text = element_text(size = 18)) +
    scale_color_viridis(discrete = TRUE, option = "D") +
    facet_wrap(~ tipo_proceso, scales = "free_y") +
    theme(strip.background = element_blank(), strip.text.x = element_text(size = 8)) +
    labs(title = "Evolución de la Duración de Procesos",  
         subtitle = "Línea de tendencia calculada por método de Regresion Local", 
         y = "Duraciones", x = "", caption = "APGE-STJER") 
  
  
  
  # dumbell graf
  
  # analisis_durac_GT$duracdumbell <- duraciones_tidy_subselec %>% 
  #   filter(!is_outlier) %>%
  #   mutate(año = year(fres)) %>% 
  #   group_by(tipo_proceso, año) %>% 
  #   summarise(mediana_duracion = round(mean(duracion, na.rm = T))) %>% 
  #   tidyr::spread(año, mediana_duracion) %>% select(1,3,4) %>% 
  #   janitor::clean_names() %>% 
  #   rowwise() %>% mutate(tendencia = x2018-x2019 > 0) %>% rowwise() %>% 
  #   mutate(tendencia = ifelse(tendencia, "Disminución de Duración", "Aumento de Duración")) %>% 
  #   ggplot(aes(y = reorder(tipo_proceso, x2019), x = x2018, xend = x2019)) +  
  #   geom_dumbbell(size = 1.2, size_x = 2, size_xend = 4, colour = "grey", 
  #                 colour_x = "blue", colour_xend = "red") +
  #   theme_minimal() +
  #   labs(title = "Comparación de duración de procesos años interanual",
  #      subtitle = "2018 (azul) - 2019 (rojo)",  x = "Duraciones de Proceso", y = "") #+ 
  #   #facet_wrap(~tendencia)
  
  analisis_durac_GT
  
}


duracion <- function(poblacion, operacion = "CADR1C", start_date = "2018-07-01", end_date = "2018-09-02", desagregacion_mensual = TRUE, 
                     desagregacion_xorg = T) {
  
  resultado <- DB_PROD() %>% 
    apgyeTableData(!! operacion) %>% 
    apgyeDSL::interval(start_date, end_date) %>% 
    filter(iep %in% !!poblacion$organismo) %>% 
    resolverconvertidos()
  
  
  # Separa Primera y Segunda Instancia
  
  if (str_detect(poblacion$organismo[[1]], "jdo")) {
    
    # Primera Instancia
    
    if(desagregacion_mensual) {
      resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
    } else {
      resultado <- resultado %>% group_by(iep) 
    }
    
    resultado <- resultado %>% 
      process() %>%
      .$result %>%
      ungroup() 
    
    
    if(desagregacion_mensual) {
      
      resultado <- resultado %>% 
        mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F)) %>% 
        select(organismo = iep, mes,  durac_inic_sent_xproc) %>% 
        tidyr::separate_rows(durac_inic_sent_xproc, sep="#")  %>%
        tidyr::separate(durac_inic_sent_xproc, into = c("tipo_proceso", "duracion", "fres"), sep="&") %>%
        mutate(tipo_proceso = str_replace(tipo_proceso, "[[:punct:]]", "")) %>% 
        mutate(tipo_proceso = str_replace(tipo_proceso, '"', "")) %>%
        mutate(tipo_proceso = str_trim(tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "ORDINARIO CIVIL"), "ORDINARIO", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "SUMARISIMO CIVIL"), "SUMARISIMO", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "DESALOJO Y COBRO DE ALQUIL|DESALOJO"), "DESALOJO Y COBRO ALQUILERES", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "DIVORCIO"), "DIVORCIO", tipo_proceso)) %>% 
        filter(!str_detect(tipo_proceso, "APELACION JUZGADO"))
      
      
      if(desagregacion_xorg){
        resultado <- resultado %>% 
          group_by(organismo, mes, tipo_proceso) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          arrange(organismo, mes,   desc(mediana_duracion)) %>% 
          ungroup() %>% 
          left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                    "circunscripcion")], by = "organismo") %>% 
          select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
        resultado
      } else {
        resultado <- resultado %>% 
          group_by(mes, tipo_proceso) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          arrange(mes,   desc(mediana_duracion)) %>% 
          ungroup() 
        resultado
      }
      resultado
      
    } else {
      
      resultado <- resultado %>% 
        select(organismo = iep, durac_inic_sent_xproc) %>% 
        tidyr::separate_rows(durac_inic_sent_xproc, sep="#")  %>%
        tidyr::separate(durac_inic_sent_xproc, into = c("tipo_proceso", "duracion", "fres"), sep="&") %>% 
        mutate(tipo_proceso = str_replace(tipo_proceso, "[[:punct:]]", "")) %>% 
        mutate(tipo_proceso = str_replace(tipo_proceso, '"', "")) %>%
        mutate(tipo_proceso = str_trim(tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "ORDINARIO CIVIL"), "ORDINARIO", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "SUMARISIMO CIVIL"), "SUMARISIMO", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "DESALOJO Y COBRO DE ALQUIL|DESALOJO"), "DESALOJO Y COBRO ALQUILERES", tipo_proceso)) %>% 
        mutate(tipo_proceso = ifelse(str_detect(tipo_proceso, "DIVORCIO"), "DIVORCIO", tipo_proceso)) %>% 
        filter(!str_detect(tipo_proceso, "APELACION JUZGADO"))
        
      
      if(desagregacion_xorg) {
        resultado <- resultado %>% 
          group_by(organismo, tipo_proceso) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          arrange(organismo, desc(mediana_duracion)) %>% 
          ungroup() %>% 
          left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                    "circunscripcion")], by = "organismo") %>% 
          select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
        resultado
        
      } else {
        resultado <- resultado %>% 
          group_by(tipo_proceso) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          arrange(desc(mediana_duracion)) 
        resultado
      }
      
      
      resultado
    }
    
  } else {
    
    # Segunda Instancia
    
    if(desagregacion_mensual) {
      resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end) 
    } else {
      resultado <- resultado %>% group_by(iep) 
    }
    
    resultado <- resultado %>% 
      filter(as %in% c("S", "s")) %>% 
      filter(!is.na(fres)) %>% filter(tres != "0") %>%
      mutate(fres = dmy(fres)) %>% 
      mutate(finicio = dmy(finicio)) %>%
      collect() %>% 
      ungroup()  %>% 
      mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>% 
      mutate(tproc = str_replace(tproc, "[[:punct:]]", "")) %>% 
      mutate(tproc = str_trim(tproc))  
      
    if(desagregacion_mensual) {
      resultado <- resultado %>% 
        mutate(mes = lubridate::month(data_interval_start, label=T, abbr = F))  
    }
    
    if(desagregacion_mensual) {
      resultado <- resultado %>% 
        filter(!str_detect(tproc, "VIOLENCIA|AMPARO"), !is.na(tproc)) %>% 
        filter(fres <= Sys.Date()) %>%
        select(organismo = iep, mes,  nro, caratula, tproc, 
               fecha_inicio_camara = finicio,
               fecha_resolucion = fres) %>%
        mutate(duracion = fecha_resolucion - fecha_inicio_camara)  
      # mutate(tproc = ifelse(tproc == "COBRO DE PESOS", 
      #                       "ORDINARIO COBRO DE PESOS", tproc), 
      #        tproc = ifelse(tproc == "ORDINARIO (CIVIL)", 
      #                       "ORDINARIO", tproc)) %>% 
      if(desagregacion_xorg){
        resultado <- resultado %>% 
          group_by(organismo, mes,  tproc) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          ungroup() %>% 
          arrange(desc(mediana_duracion)) %>% 
          rename(tipo_proceso = tproc) %>% 
          left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                    "circunscripcion")], by = "organismo") %>% 
          select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
        
        resultado
      } else {
        resultado <- resultado %>%
          group_by(mes, tproc) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          ungroup() %>% 
          arrange(desc(mediana_duracion)) %>% 
          rename(tipo_proceso = tproc) 
        resultado
      }
      
    } else {
      
      resultado <- resultado %>% 
        filter(!str_detect(tproc, "VIOLENCIA|AMPARO"), !is.na(tproc)) %>% 
        filter(fres <= Sys.Date()) %>%
        select(organismo = iep, nro, caratula, tproc, 
               fecha_inicio_camara = finicio,
               fecha_resolucion = fres) %>%
        mutate(duracion = fecha_resolucion - fecha_inicio_camara) 
      
      if(desagregacion_xorg){
        resultado <- resultado %>%
          group_by(organismo, tproc) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          ungroup() %>% 
          arrange(desc(mediana_duracion)) %>% 
          rename(tipo_proceso = tproc) %>% 
          left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                                    "circunscripcion")], by = "organismo") %>% 
          select(circunscripcion, organismo = organismo_descripcion, everything(), - organismo)
        
        resultado
        
      } else {
        resultado <- resultado %>%
          group_by(tproc) %>% 
          summarise(mediana_duracion = round(median(as.integer(duracion), na.rm = T), digits = 1), 
                    cantidad_casos = n()) %>% 
          ungroup() %>% 
          arrange(desc(mediana_duracion)) %>% 
          rename(tipo_proceso = tproc) 
        resultado
      }
      
      resultado
    }
    resultado
  }
  
  resultado <- resultado %>% 
    mutate(tipo_proceso = str_sub(gsub("[^[:alnum:][:blank:]?&/\\-]", "", tipo_proceso)))
  
  resultado
           
}

durac_subselec <- function(df, materia) {
  
  df <- df %>% 
    #filter(cantidad_casos > 3) %>% 
    rename(tproc = tipo_proceso)
  
  if (materia ==  "civil") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      filter(!str_detect(tproc, "^INCIDENTE|^INCIDENTE "),
        !str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR |^MEDIDA"),
        !str_detect(tproc, "MONITORIO|^PREPARACION DE LA VIA MONIT"),
        !str_detect(tproc, "^EJECU|^PROCESO DE EJECU|^APREMIO"), #
        !str_detect(tproc, "^INTERDICT"),
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        !str_detect(tproc, "^BENEFICIO"), #
        !str_detect(tproc, "^HOMOLOGACION"), 
        !str_detect(tproc, "^SEGUNDO TESTIMONIO"),
        !str_detect(tproc, "^SUCESORIO"), # exlucido por variación en la codificación de resoluciones
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA"),
        !str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL"),
        # Otros Fueros Laboral y Familia
        !str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION"), 
        !str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON"),
        !str_detect(tproc, "AMENAZA|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU "),
        !str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO")) %>% 
      mutate(tproc = ifelse(str_detect(tproc, "DAÑOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>% 
      mutate(tproc = ifelse(str_detect(tproc, "(CIVIL)"), "ORDINARIO", tproc))
    df
  } else if (materia == "familia") {
    df <- df %>%
      mutate(tproc = toupper(tproc)) %>%
      filter(
        !str_detect(tproc, "VIOLENCIA FAMILIAR"), #
        !str_detect(tproc, "VIOLENCIA DE GENERO"), #
        !str_detect(tproc, "HOMOLOGACION"), 
        !str_detect(tproc, "^EJECU|^PROCESO DE EJECU"),
        !str_detect(tproc, "^INCIDENTE|^INCIDENTE "),
        !str_detect(tproc, "^MEDIDA"),
        #!str_detect(tproc, "^RESTRICCIONES"),
        #!str_detect(tproc, "^INTERNACION"), #
        !str_detect(tproc, "^BENEFICIO"), #
        #!str_detect(tproc, "^REGIMEN"), #
        #!str_detect(tproc, "^AUTORIZACION JUDICIAL"),
        #!str_detect(tproc, "^DECLARACION DE INCAPACIDAD"),
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "^SU |^EXHORTO|^OFICIO|DESARCHIVO|^INFORME|^DENUNCIA|^TESTIMONIO"),
        !str_detect(tproc, "CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|SUCESORIO|USUCAPION|^AMENAZA"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL"),
        !str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION"))
    df
  } else if (materia == "laboral") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      filter(
        !str_detect(tproc, "^HOMOLOGACION"), #
        !str_detect(tproc, "^EJEC|^PROCESO DE EJECU|^APREMIO"), #
        !str_detect(tproc, "^INCIDENTE|^INCIDENTE "),
        !str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR "),
        !str_detect(tproc, "^MEDIDAS PREP"),
        !str_detect(tproc, "^BENEFICIO"), #
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA"),
        !str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|SUCESORIO|USUCAPION"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|MONITORI|PERSONAL"),
        !str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON"),
        !str_detect(tproc, "AMENAZA|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU "),
        !str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO|^DESALOJO|^APELACION JUZGADO|^DIVISION DE|^SEGUNDO TEST"))
    df
  } else if(materia == "pazproc") {
    df <- df %>% 
      mutate(tproc = toupper(tproc)) %>%
      filter(
        !str_detect(tproc, "^INCIDENTE|^INCIDENTE "), #
        !str_detect(tproc, "^MEDIDA CAUTELAR|^MEDIDA CAUTELAR "), #
        !str_detect(tproc, "MONITORIO"), #
        !str_detect(tproc, "^EJECU|^PROCESO DE EJECU|^APREMIO"), #
        !str_detect(tproc, "^INTERDICT"),
        !str_detect(tproc, "AMPARO|HABEAS|INCONSTITUCIONALIDAD|ACCION DE PROHIBICION|ACCION DE EJECUCION"),
        !str_detect(tproc, "^PREPARACION "),
        !str_detect(tproc, "^BENEFICIO"), #
        !str_detect(tproc, "^HOMOLOGACION"), 
        # Anomalías: desclasificados, no procesos y errores
        !str_detect(tproc, "OFICIO|EXHORTO|SUMARIO|COMPENSACION|MATRICULA DE COMERCIANTE|DESARCHIVO|^INFORME|^DENUNCIA|^SUPERVIVENCIA|^AUTORIZACION"),
        !str_detect(tproc, "CIVIL Y COMERCIAL|PROCESO LABORAL|PROCESO DE FAMILIA|CONCURSOS Y QUIEBRAS|^SENTENCIA|SOLICITA|PAZ"),
        !str_detect(tproc, "CONCURSO CERRADO|EXPEDIENTE INTERNO|ADMINISTRATIVO|PERSONAL|^LICENCIAS|^ARANCELES"),
        # Otros Fueros Laboral y Familia
        !str_detect(tproc, "LABORAL|TRABAJO|COMISION MEDICA|SUSTITUCION DE DEPOSITO|ENFERMEDAD PROFESIONAL|SALARIO|SINDICAL|CONCILIACION"), 
        !str_detect(tproc, "VIOLENCIA|DIVORCIO|ALIMENTO|RESTRICCIONES CAPAC|REGIMEN COMUNIC|^IMPEDIMENTO DE CON"),
        !str_detect(tproc, "AMENAZA|^LESIONES|^MEDIDA DE PROTEC|GUARDA|INSCRIPCION|INTERNACION|RECONSTRUCCION DE EX|CAPACIDAD|TUTELA|SALUD MENTAL|^SU "),
        !str_detect(tproc, "^ADOPCION|^IMPUGNACION DE RECON|^USO DE DOCUMENTO|^TESTIMONIO"))
    df
  }
  df
}

durac_interanual <- function(poblacion, operacion = "CADR1C", materia = "civil", start_date = "2017-01-01", end_date = "2018-01-01") {

  library(ggplot2)  
  library(reshape2)
  
  # el año base es el ante último y no el  inmediato anterio.  
  inicio_ano_base <- paste0(lubridate::year(end_date) - 2, '-01-01')
  fin_ano_base <- paste0(lubridate::year(end_date) - 1, '-01-01')
  
  # año analizado es el año en curso
  inicio_ano_analizado <- start_date
  fin_ano_analizado <- end_date
  
  durac_ano_base <- duracion(poblacion, 
                        operacion = operacion, 
                        start_date = inicio_ano_base,
                        end_date = fin_ano_base,  
                        desagregacion_mensual = F, 
                        desagregacion_xorg = F) %>% 
    durac_subselec(materia) %>% 
    mutate(año = lubridate::year(inicio_ano_base))
  
  
  durac_ano_analizado <- duracion(poblacion, 
                             operacion = operacion, 
                             start_date = inicio_ano_analizado,
                             end_date = fin_ano_analizado,  
                             desagregacion_mensual = F, 
                             desagregacion_xorg = F) %>% 
    durac_subselec(materia) %>% 
    mutate(año = lubridate::year(inicio_ano_analizado))
  
  
  durac_ano_base <- durac_ano_base %>% 
    semi_join(durac_ano_analizado, by = "tproc")
  durac_ano_analizado <- durac_ano_analizado %>% 
    semi_join(durac_ano_base, by = "tproc")
  
  durac_interanual <- durac_ano_analizado %>% 
    bind_rows(durac_ano_base) %>% 
    select(tipo_proceso = tproc, año, everything())
  
  casos_insuficienstes <- durac_interanual$tipo_proceso[durac_interanual$cantidad_casos < 10]  
  
  durac_interanual <- durac_interanual %>% 
    filter(!tipo_proceso %in% casos_insuficienstes) 
  
  durac_interanual <<- durac_interanual
  
  durac_interanual_tabla <- durac_interanual %>% 
    select(-cantidad_casos) %>% 
    tidyr::spread(año, mediana_duracion) %>% 
    rowwise() %>% 
    rename(año_base = 2, año_analizado = 3) %>% 
    mutate(diferencia_interanual_absoluta =  año_analizado - año_base)  %>% 
    mutate(diferencia_interanual_relativa =  round(diferencia_interanual_absoluta/año_base, digits = 2) * 100) %>% 
    arrange(diferencia_interanual_relativa) %>% 
    mutate(diferencia_interanual_relativa = paste0(diferencia_interanual_relativa, " %")) %>% 
    rename('variacion interanual absoluta' = diferencia_interanual_absoluta,
           'variacion interanual relativa' = diferencia_interanual_relativa)  
    
  names(durac_interanual_tabla)[2] <- paste0("año_", lubridate::year(end_date) - 2)
  names(durac_interanual_tabla)[3] <- paste0("año_", lubridate::year(end_date))
  
  
  durac_interanual_tabla <<- durac_interanual_tabla
  
  durac_interanual <- durac_interanual %>% 
    mutate(casos_disminucion = tipo_proceso %in% 
             durac_interanual_tabla$tipo_proceso[durac_interanual_tabla$'variacion interanual absoluta' < 0]) %>% 
    mutate(casos_disminucion = ifelse(casos_disminucion, "DISMINUYERON DURACION",
                                      "AUMENTARON DURACION"))
  
  grafico <- durac_interanual %>% 
    ggplot(aes(x = tipo_proceso, fill = as.factor(año), 
               y = ifelse(test = año == lubridate::year(end_date) - 2, yes = -mediana_duracion, no = mediana_duracion))) +
    geom_bar(stat = "identity") +
    #geom_text(aes(label=mediana_duracion), vjust=0.5, color="#666666", size=3.5) +
    scale_y_continuous(limits = max(durac_interanual$mediana_duracion) * c(-1,1)) +
    scale_y_continuous(labels = abs)  +
    coord_flip() +
    scale_fill_manual(values = c("grey", "#66CC99"), name = "") +
    labs(title= expression(atop("Comparación Interanual de la Duración de los Procesos")),
         y = "Duración de procesos en dias corridos de inicio a sentencia (Mediana)", x = "") +
    theme(axis.text.x = element_text(size=10, color="black"),
          axis.text.y = element_text(size=10, color="black"),
          legend.text = element_text(size=14),
          axis.title = element_text(size=14),
          plot.title = element_text(size = 18, face = "bold", colour = "black", vjust = -1),
          legend.position ="top", 
          strip.text = element_text(size=14)) +
    coord_flip() +
    facet_grid(. ~ casos_disminucion) 
    
  
  grafico <<- grafico
  
}


durac_inic_apprueba_cco1 <- function() {
  
  ultimo_ano <- apgyeRStudio::primariasOrganismo("jdocco*", "2018-10-01", "2019-01-01")$AUDIC$tabla() %>%
    mutate(fea = dmy(fea), finicio = dmy(finicio), faprueb = dmy(faprueb)) %>% 
    filter(fea >= data_interval_start, fea < data_interval_end) %>% 
    filter(esta == "2", ta == "1") %>%
    collect() %>% ungroup() %>% 
    filter(faprueb > finicio) %>% 
    distinct(iep, nro, caratula, tproc, fea, .keep_all = T) %>% 
    mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>% 
    mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>%
    mutate(tproc = ifelse(str_detect(tproc, "ORDINARIO CIVIL"), "ORDINARIO", tproc)) %>% 
    mutate(tproc = ifelse(str_detect(tproc, "SUMARISIMO CIVIL"), "SUMARISIMO", tproc)) %>% 
    select(iep, nro, caratula, tproc, fecha_inicio_causa = finicio, fecha_apertura_prueba = faprueb) %>% 
    mutate(dias_inicio_aperturaprueba = fecha_apertura_prueba - fecha_inicio_causa) %>% 
    group_by(tproc) %>% 
    summarise(cantidad_casos = n(), 
              promedio_inicio_aperprueba = round(mean(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
              mediana_inicio_aperprueba = round(median(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
              casos_durac_minima = min(dias_inicio_aperturaprueba),
              casos_durac_maxima = max(dias_inicio_aperturaprueba)) %>% 
    arrange(desc(promedio_inicio_aperprueba)) %>% 
    filter(cantidad_casos > 10) %>% 
    mutate(ano_ref = "año_2018")
  
  historico <- apgyeRStudio::primariasOrganismo("jdocco*", "2017-01-01", "2018-01-01")$CADUR1C$tabla() %>%
    filter(!is.na(fapr)) %>% 
    mutate(finicio = dmy(finicio), fapr = dmy(fapr)) %>% 
    collect() %>% ungroup() %>% 
    filter(fapr > finicio) %>% 
    filter(fapr > as.Date("2017-01-01") & fapr < as.Date("2017-12-31")) %>% 
    mutate(faprueb = fapr) %>% 
    mutate(tproc = ifelse(str_detect(tproc, "Y PERJUICIOS"), "ORDINARIO DAÑOS Y PERJUICIOS", tproc)) %>%
    mutate(tproc = ifelse(str_detect(tproc, "ORDINARIO CIVIL"), "ORDINARIO", tproc)) %>% 
    mutate(tproc = ifelse(str_detect(tproc, "SUMARISIMO CIVIL"), "SUMARISIMO", tproc)) %>% 
    select(iep, nro, caratula, tproc, fecha_inicio_causa = finicio, fecha_apertura_prueba = faprueb) %>% 
    mutate(dias_inicio_aperturaprueba = fecha_apertura_prueba - fecha_inicio_causa) %>% 
    group_by(tproc) %>% 
    summarise(cantidad_casos = n(), 
              promedio_inicio_aperprueba = round(mean(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
              mediana_inicio_aperprueba = round(median(dias_inicio_aperturaprueba, na.rm = T), digits = 1), 
              casos_durac_minima = min(dias_inicio_aperturaprueba),
              casos_durac_maxima = max(dias_inicio_aperturaprueba)) %>% 
    arrange(desc(promedio_inicio_aperprueba)) %>% 
    filter(cantidad_casos > 10) %>% 
    mutate(ano_ref = "año_2017")
  
  tabla_durac__intera_inic_aperprueb <<- ultimo_ano %>% 
    bind_rows(historico)
  
  comparativo <<- ultimo_ano %>% 
    bind_rows(historico) %>% 
    select(tproc, ano_ref, mediana_inicio_aperprueba) %>% 
    tidyr::spread(ano_ref, mediana_inicio_aperprueba) %>% 
    na.omit()
  
}