library(mongolite)
library(dplyr)
library(stringr)
library(tidyr)
library(tibble)
library(lubridate)
library(scales)
library(rlang)
library(readr)
library(jsonlite)

expedientes = mongo(collection ="expedientes", db="expedientes_production", url = conM2)

query_ATERxproc <- function(orgsvec, # lista de organismo
                        start_date,
                        end_date,
                        tproc = "SUCESORIO"){
  
  orgsvec = tbl(DB_PROD(), "lookupentidades") %>%
    filter(organismo %in% !!orgsvec) %>% 
    select(codigo_organismo) %>% 
    collect() %>%  .$codigo_organismo %>% 
    jsonlite::toJSON()
  
  
  if (tproc == "SUCESORIO"){
    
    query = paste0('
      [
        {
          "$match": {
            "$and": [
              {
                "lex_id.codigo_organismo": {
                      "$in":',orgsvec,'
                    }
              }, {
                "tipo_proceso.tipo": {
                  "$regex": "',tproc,'", 
                  "$options": "i"
                }
              }
            ]
          }
        }, {
          "$addFields": {
            "iniciostr": {
              "$dateToString": {
                "format": "%Y-%m-%d", 
                "date": "$inicio"
              }
            }
          }
        }, {
          "$match": {
            "iniciostr": {
              "$gte":"',start_date,'", 
              "$lt":"',end_date,'"
            }
          }
        }, {
          "$addFields": {
            "padre1": {
              "$arrayElemAt": [
                "$tipo_proceso.padres", 0
              ]
            }, 
            "padre2": {
              "$arrayElemAt": [
                "$tipo_proceso.padres", 1
              ]
            }
          }
        }, {
          "$project": {
            "circunscripcion": "$datos_organismo.jurisdiccion", 
            "localidad": "$datos_organismo.localidad", 
            "organismo": "$datos_organismo.nombre_organismo", 
            "organismo_codigo": "$lex_id.codigo_organismo", 
            "nro_exp": "$nro.exp1", 
            "actora": "$actora", 
            "demanda": "$demandada", 
            "padre1": "$padre1", 
            "padre2": "$padre2", 
            "tipo_proceso": "$tipo_proceso.tipo", 
            "inicio": "$iniciostr", 
            "justiciables": "$justiciables"
          }
        }, {
          "$unwind": {
            "path": "$justiciables"
          }
        }, {
          "$replaceRoot": {
            "newRoot": {
              "$mergeObjects": [
                "$$ROOT", "$justiciables"
              ]
            }
          }
        }, {
         "$match": {
             "caracter": {
                 "$regex":"CAUSANT", 
                 "$options":"i"
                }
            }
        }, {
          "$project": {
            "justiciables": 0
          }
        }
      ]
    ')
    
  } else if (tproc == 'APREMIO') {
    
    query = paste0('
      [
        {
          "$match": {
            "$and": [
              {
                "lex_id.codigo_organismo": {
                      "$in":',orgsvec,'
                    }
              }, {
                "tipo_proceso.tipo": {
                  "$regex": "',tproc,'", 
                  "$options": "i"
                }
              }
            ]
          }
        }, {
          "$addFields": {
            "iniciostr": {
              "$dateToString": {
                "format": "%Y-%m-%d", 
                "date": "$inicio"
              }
            }
          }
        }, {
          "$match": {
            "iniciostr": {
              "$gte":"',start_date,'", 
              "$lt":"',end_date,'"
            }
          }
        }, {
          "$addFields": {
            "padre1": {
              "$arrayElemAt": [
                "$tipo_proceso.padres", 0
              ]
            }, 
            "padre2": {
              "$arrayElemAt": [
                "$tipo_proceso.padres", 1
              ]
            }
          }
        }, {
          "$project": {
            "circunscripcion": "$datos_organismo.jurisdiccion", 
            "localidad": "$datos_organismo.localidad", 
            "organismo": "$datos_organismo.nombre_organismo", 
            "organismo_codigo": "$lex_id.codigo_organismo", 
            "nro_exp": "$nro.exp1", 
            "actora": "$actora", 
            "demanda": "$demandada", 
            "padre1": "$padre1", 
            "padre2": "$padre2", 
            "tipo_proceso": "$tipo_proceso.tipo", 
            "inicio": "$iniciostr", 
            "justiciables": "$justiciables"
          }
        }, {
          "$unwind": {
            "path": "$justiciables"
          }
        }, {
          "$replaceRoot": {
            "newRoot": {
              "$mergeObjects": [
                "$$ROOT", "$justiciables"
              ]
            }
          }
        }, {
          "$project": {
            "justiciables": 0
          }
        }
      ]
    ')
    
  }
  
   query
   
}
