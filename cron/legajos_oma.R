#!/usr/bin/Rscript
source("~/.Rprofile")
source("~/apgyeinformes/R/informe.R")
source("~/apgyeinformes/R/poblacion.R")
library(kableExtra)
library(pandoc)

# library(dplyr)
# library(stringr)
# library(tidyr)
# library(lubridate)
# library(telegram.bot)
# library(telegram)
# library(apgyeJusEROrganization)
# library(apgyeOperationsJusER)
# library(apgyeProcesamiento)
# library(apgyeDSL)

# https://github.com/bnosac/cronR
# https://stackoverflow.com/questions/47315509/kableextra-only-works-if-theres-another-table-in-the-presentation/47316275#47316275

# En caso de error imprime log
options(echo = TRUE)

# dates
# Informe semanal
start_date = "2022-04-01"
end_date = lubridate::floor_date(Sys.Date(), "month")

# mes puntual
#start_date = "2023-03-01"
#end_date = "2023-04-01"


# task
Sys.setenv(RSTUDIO_PANDOC="/usr/lib/rstudio-server/bin/pandoc")

# produzco informe y guardo
poblacion_er <- apgyeJusEROrganization::listar_organismos() 
oma <- poblacion_er %>% 
  filter(
        tipo == "oma", 
        #circunscripcion == "Gualeguaychú",
  )


resultado <- iniciados_oma(db_con = DB_PROD(), poblacion = oma,
                           start_date = start_date, end_date = end_date) 

primaria  <- resultado$primaria_preprocesada_cfiscal %>%
  select(circunscripcion, apellido, año_mes, mov_grupo, nro_om, caratula, tproc,
         fecha, descripcion, partp) %>% 
  tidyr::separate(partp, into = c("fiscalia", "n", "l", "o"), sep="\\$") %>%
  select(-c(n, l, o)) %>% 
  mutate(fiscalia = str_remove_all(fiscalia, "fis:")) %>% 
  arrange(circunscripcion, apellido, año_mes, nro_om)

write.table(primaria,
            "~/apgyeinformes/cron/cronOutput/legajos.csv", 
            sep = ",",
            row.names = F,
            col.names = T)

# Creating a Bot Instance
# https://ebeneditos.github.io/telegram.bot/#creating-a-telegram-bot
bot <- Bot(token = R_TELEGRAM_BOT_RTelegramBot)

telegram_chats <- tbl(DB_PROD(), "telegram_chats") %>% collect() %>% 
  mutate(chat_id = as.double(chat_id), 
         from_user = as.double(from_user), 
         id = as.double(id))


# 3 Send Messages ------
bot$sendMessage( telegram_chats$id[telegram_chats$last_name == "Amateis"], 
                 text = glue::glue("Se remite a usted información solicitada al APGE.", Sys.Date()))
bot$sendDocument(telegram_chats$id[telegram_chats$last_name == "Amateis"], 
                 document = "~/apgyeinformes/cron/cronOutput/legajos.csv" )
bot$sendMessage( R_TELEGRAM_USER_me, glue::glue("Se remitió listado primario de legajos OMA -",
                                   Sys.Date()))


