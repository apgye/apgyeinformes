---
title: Fuero Penal - Superior Tribunal de Justicia de Entre Ríos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      toc: TRUE
      toc_depth: 3
      number_sections: TRUE
      includes:
        in_header: header.tex
        before_body: before_body.tex
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable
header-includes: 
 - \renewcommand{\contentsname}{Contenido del Documento}
 - \usepackage{longtable}
 - \usepackage{array}
 - \usepackage{multirow}
 - \usepackage{wrapfig}
 - \usepackage{float}
 - \usepackage{colortbl}
 - \usepackage{pdflscape}
 - \usepackage{tabu}
 - \usepackage{threeparttable}
params:
  start_date: 
    label: "Fecha_inicio_inf"
    input: text
    value: "2022-02-01"
  end_date:
    label: "Fecha_fin_inf"
    input: text
    value: "2023-01-01"
  agrupador:
    label: "Agrupador: año_mes(siempre1°),grupo,circunscripcion"
    input: text
    value: "año_mes,grupo"
  Tablas: TRUE 
  Graficos: FALSE
---

```{r, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
desagregacion_mensual = T

source("../R/informe.R")
source("../R/duracion.R")
source("../R/poblacion.R")
library(ggpubr)
theme_set(theme_pubr())

# Params
start_date <- params$start_date
end_date <- params$end_date
agrupador <- unlist(str_split(params$agrupador, ",")) 
tablas <- params$Tablas
graficos <- params$Graficos

# test
#agrupador =  c("año_mes", "grupo")
#start_date = "2019-01-01"
#end_date = "2020-01-01"
```


```{r, utils}
figure3 <- function(total, dfsub1, dfsub2, desagregado = desagregado) {
  
  if(desagregado) {
    
    figure =  ggarrange(total,
                    ggarrange(dfsub1, dfsub2, ncol = 2, labels = c("Paraná", "Uruguay"), 
                              legend = "none", font.label = list(size = 14)),
                    nrow = 2,
                    labels = "Total", font.label = list(size = 14),
                    common.legend = TRUE, legend = "top")
  # figure = annotate_figure(figure, top = text_grob("Causas Iniciadas", 
  #                  color = "black", face = "bold", size = 10))
    figure
    
  } else {
    
    figure = total
    
    
  }
  
  figure
  
}

penaltable <- function(df){
  
  library(gt)
  # https://gt.rstudio.com/articles/intro-creating-gt-tables.html
  
  if("grupo" %in% colnames(df)){ 
    
    df <- df %>% 
      arrange(grupo, año_mes)
  }
  
  df <- df %>% 
    group_by_at(agrupador[2]) %>% 
    gt() %>% 
    summary_rows(
        groups = TRUE,
        columns = where(is.numeric),
        list(
          promedio = ~round(mean(., na.rm = TRUE), digits = 2),
          total = ~round(sum(., na.rm = TRUE)),
          desvíoST = ~round(sd(., na.rm = TRUE), digits = 2)), 
          formatter = fmt_number, 
          drop_trailing_zeros = TRUE,
        use_seps = FALSE,
      )  %>% 
    tab_header(
      title = "Fuero Penal",
      subtitle = glue::glue("desde: {start_date} hasta: {end_date}")
      ) %>% 
    # tab_source_note(
    #   source_note = "Fuente: Datos declarador por cada organismo"
    #   ) %>% 
    cols_align(
      align = "center", columns = everything()
      )
    
  df
}

donuts <- function(df, x, l, f){
  
  df = df %>% ungroup()
  
  ggpubr::ggdonutchart(df,  
                     x = x, 
                     label = l , 
                     fill = f,
                     lab.pos = "in",
                     lab.font = c(8, "black"),) +
  theme(legend.text = element_text(size = 15), 
        legend.title = element_blank())
       
}

fdemorafracaso <- function(data, inst = "1") {
  
  demfrac = list()
  
  df = data
  
  df <- df %>% 
    filter(instancia == inst)
  
  demfrac$df_demora_gral <- df %>% 
    group_by(demora) %>% 
    summarise(cantidad = n()) %>% 
    ungroup() %>% 
    mutate(demora = ifelse(demora == "1", "audiencias con demora", "audiencias puntuales")) %>% 
    getpje(cantidad, demora) %>% 
    arrange(desc(cantidad))
  
 
  demfrac$df_demora_xtipo <- df %>% 
    filter(demora == "1") %>% 
    group_by(tipo_demora, instancia) %>% 
    summarise(cantidad = n()) %>% 
    getpje(cantidad, tipo_demora) %>% 
    arrange(desc(cantidad))

 
  demfrac$df_fracaso <- df %>% 
    filter(estado %in% c("2", "3", "4", "5")) %>% 
    filter(fecha_audiencia > as.Date("2022-10-01")) %>% 
    group_by(tipo_demora, instancia) %>% 
    summarise(cantidad = n()) %>% 
    rename(fracaso_motivos = tipo_demora) %>% 
    getpje(cantidad, fracaso_motivos) %>% 
    arrange(desc(cantidad))
  
  
  demfrac

}


plot_pie_chart <- function(dfcpje, cantidad, cualidad){
  
  # 
  
  dfcpje %>%
    ggplot(aes(x = "", y = {{cantidad}}, fill = reorder({{cualidad}},{{cantidad}}))) +
    geom_bar(width = 1, stat = "identity") +
    coord_polar("y", start = 0) +
    ggrepel::geom_text_repel(aes(label = pje), size=8,
                             color = "black", 
                             position = position_stack(vjust = 0.5),
                             max.overlaps=Inf) +
    labs(x = NULL, y = NULL, fill = NULL) +
    theme_void() +
    theme(legend.text = element_text(size=20)) +
    scale_y_continuous(labels = scales::percent) +
    guides(fill = guide_legend(reverse = TRUE)) 
              
}


getpje <- function(df, cantidad, cualidad){
  
  col_name <- deparse(substitute(cantidad))
  
  pje <- df %>% 
    janitor::adorn_percentages("col") %>% 
    mutate(pje = paste0(round({{cantidad}}*100, digits = 1), "%"))  %>% 
    mutate(var_pje = paste0({{cualidad}}, " (", round({{cantidad}}*100, digits = 1), "%)"))   
  
  pje$cantidad <- df[[col_name]]
  
  pje

}


plot_2v <- function(df, grupo, var1 = NULL, var2 = NULL){
  
  
  g <- deparse(substitute(grupo))
  
  df <- df %>% 
    select(grupo, {{var1}}, {{var2}}) %>% 
    group_by(grupo) %>% 
    summarise_if(is.numeric, sum, na.rm = T) %>% 
    pivot_longer(-grupo, names_to = "variable", values_to = "cantidad")
  

  df %>%
    ggplot(aes(fill=variable, y=cantidad, x= grupo )) +
    geom_bar(position = "dodge", stat="identity") +
    geom_text(aes(label=cantidad, fontface="bold", vjust=1, alpha = 1),
              position=position_dodge(width=0.9), size=5, show.legend = F) +
    facet_wrap(~grupo, scales = "free") +
    theme_void() +
    theme(legend.position = "bottom", legend.title = element_blank(),
          legend.text = element_text(size = 11),
          strip.text.x = element_text(size = 12))

  
}

```


```{r, source}
source("~/apgyeinformes/R/load_datamarts.R")
#source("~/apgyeinformes/R/datamart_penal.R")
source("~/apgyeinformes/R/variables.R")
source("~/apgyeinformes/R/habiles.R")


```


```{r, proc_rsi}

df_s_raw <- bind_rows(
  "1_Jueces_Garantías" = datamart_penal$resoluciones_garantia$ac, 
  "2_Juez_Penal_Menores" = datamart_penal$resoluciones_penalmenores, 
  "3_Vocales_Tribunal_Juicio" = datamart_penal$resoluciones_tribunalja$tjui, 
  "4_Vocales_Tribunal_Apelacion" = datamart_penal$resoluciones_tribunalja$resultado_tapel,
  "5_Vocales_Casacion" = datamart_penal$resoluciones_casacion, 
  "6_Vocales_Sala_STJ" = datamart_penal$resoluciones_salastj$resultado, 
  .id = "grupo"
  ) %>% 
  mutate(fecha = as.Date(paste0(año_mes, "-01"))) %>% 
  # selecciono intervalo de interes
  filter(fecha >= start_date, fecha < end_date) 


df_a_raw <- bind_rows(
  "1_Jueces_Garantías" = datamart_penal$resoluciones_garantia$res, 
  "2_Juez_Penal_Menores" = datamart_penal$resoluciones_penalmenores, 
  "3_Vocales_Tribunal_Juicio" = datamart_penal$resoluciones_tribunalja$tjui, 
  "4_Vocales_Tribunal_Apelacion" = datamart_penal$resoluciones_tribunalja$resultado_tapel,
  "5_Vocales_Casacion" = datamart_penal$resoluciones_casacion, 
  "6_Vocales_Sala_STJ" = datamart_penal$resoluciones_salastj$resultado, 
  .id = "grupo"
  ) %>% 
  mutate(fecha = as.Date(paste0(año_mes, "-01"))) %>% 
  # selecciono intervalo de interes
  filter(fecha >= start_date, fecha < end_date) 
  

df_inic_raw <- bind_rows(
  "1_Jueces_Garantías" = datamart_penal$iniciados_garantia$inic_gtia_pc, 
  "2_Juez_Penal_Menores" = datamart_penal$iniciados_penalmenores$inic_jdopna_pc, 
  "3_Vocales_Tribunal_Juicio" = datamart_penal$iniciados_tribunalja$inic_tja_pc, 
  "4_Vocales_Tribunal_Apelacion" = datamart_penal$iniciados_tribunalja$inic_ap_pc,
  "5_Vocales_Casacion" = datamart_penal$iniciados_casacion$inic_campen_pc, 
  "6_Vocales_Sala_STJ" = datamart_penal$iniciados_salastj$inic_sal_pc, 
  .id = "grupo"
  ) %>% 
  mutate(fecha = coalesce(finicio, fingj, fiap)) %>% 
  # selecciono intervalo de interes
  filter(fecha >= start_date, fecha < end_date) %>% 
  mutate(año_mes = format(as.Date(fecha), "%Y-%m"))  


var_sentencias = c(
  unique(unidades_conteo$referencia), 
  unique(actos %>% filter(conclusivo) %>% .$referencia)
  )

var_autos = c(
  unique(unidades_conteo$referencia), 
  unique(actos %>% filter(!conclusivo) %>% .$referencia)
  )


df_s <- df_s_raw %>% 
  select(any_of(var_sentencias)) %>% 
  mutate(magistrado = ifelse(is.na(magistrado), organismo, magistrado)) %>% 
  # limpio df
  filter(circunscripcion != "Total") %>% select(-organismo) %>% 
  pivot_longer(-any_of(unique(unidades_conteo$referencia)), names_to = "variables", values_to = "cantidad") %>% 
  group_by_at(agrupador) %>% 
  summarise(actos_conclusivos = sum(cantidad, na.rm = T)) 


df_a <- df_a_raw %>% 
  select(any_of(var_autos)) %>% 
  mutate(magistrado = ifelse(is.na(magistrado), organismo, magistrado)) %>% 
  # limpio df
  filter(circunscripcion != "Total") %>% select(-organismo) %>% 
  pivot_longer(-any_of(unique(unidades_conteo$referencia)), names_to = "variables", values_to = "cantidad") %>% 
  group_by_at(agrupador) %>% 
  summarise(autos = sum(cantidad, na.rm = T)) 

df_inic <- df_inic_raw %>% 
  group_by_at(agrupador) %>% 
  summarise(causas_iniciadas = n())


df_s_inic_ratio <- df_s %>% 
  left_join(df_inic) %>% 
  mutate(causas_iniciadas = ifelse(is.na(causas_iniciadas), 0, causas_iniciadas)) %>% 
  mutate(ratio_ac_iniciadas = round(actos_conclusivos/causas_iniciadas, digits = 2),
         pje = paste0(ratio_ac_iniciadas*100, " %"),
         pendiente = causas_iniciadas-actos_conclusivos) %>% 
  group_by_at(agrupador[2]) %>% 
  mutate(pendiente_acumulado = as.character(cumsum(pendiente))) %>% 
  mutate(pje = ifelse(pje == "Inf %", "-", pje))

df_a_inic_ratio <- df_a %>% 
  left_join(df_inic) %>% 
  mutate(causas_iniciadas = ifelse(is.na(causas_iniciadas), 0, causas_iniciadas)) %>% 
  mutate(ratio_autos_iniciadas = round(autos/causas_iniciadas, digits = 2),
         pje = paste0(ratio_autos_iniciadas*100, " %"),
         pendiente = causas_iniciadas-autos) %>% 
  group_by_at(agrupador[2]) %>% 
  mutate(pendiente_acumulado = as.character(cumsum(pendiente))) %>% 
  mutate(pje = ifelse(pje == "Inf %", "-", pje))

```

\pagebreak

# Introducción

En este informe presentamos un resumen estadístico del Fuero Penal del Poder Judicial de Entre Ríos.    

Resumimos los datos vinculadas a causas que se iniciaron en el Fuero Penal, como así también datos de producción referidos a sentencias, resoluciones y audiencias.    

Destacamos que los datos empleados para este informe fueron relevados en el marco de un sistema estadístico judicial creado por el Superior Tribunal de Justicia. A partir del 2018 se elaboraron junto magistrados y funcionarios del Fuero Penal **guías de estadística** que formalizaron el trabajo de codificación, registro y entrega de datos, y se creó un sistema de procesamiento, validación y producción de información que se mantiene hasta la actualidad. Este sistema se define como un **sistema de estadística pública judicial** y puede consultarse a través del siguiente enlace: https://tablero.jusentrerios.gov.ar/.       

Se pone a disposición del lector los datos primarios y procesados empleados para el cálculo de los indicadores que aquí se presentan bajo requerimiento dirigido a **apge@jusentrerios.gov.ar**.      

Finalmente, no queremos dejar pasar la oportunidad de resaltar el compromiso del área con nuestra comunidad y usuarios finales en proveer de información válida y objetiva para el conocimiento de la realidad. Una vez más reforzamos ese compromiso esperando honrar la labor que diariamente realizan quienes integran el Poder Judicial.     

\blandscape

`r if (tablas) '# Tabla de eficacia en la resolución de conflictos'`

**`r if (tablas)  apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`**


```{r, table_ratio_rsi,  eval = (tablas)}
df_s_inic_ratio %>% penaltable()
```


```{r, eval = (tablas), results='asis'}
cat('\\pagebreak')
```


`r if (graficos) '# Gráfico de eficacia en la resolución de conflictos'`

**`r if (graficos)  apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`**


```{r, plot_rsi, eval = (graficos), fig.width=10, fig.height=6,  fig.align='center'}
plot_2v(df_s_inic_ratio, agrupador[2], actos_conclusivos, causas_iniciadas)
```


`r if (tablas) '# Tabla de eficacia en la Programación de Audiencias'`

**`r if (tablas)  apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`**


```{r, proc_rarf}
# Audiencias
df_aud_realizadas_raw <- bind_rows(
  "1_Jueces_Garantías" = datamart_penal$audiencias_conteos_garantia, 
  "2_Juez_Penal_Menores" = datamart_penal$audiencias_conteos_penalmenores,
  # Acá hay un problema: si separa las realizadas en tjuicio y apelacion
  # no puedo calcular indicador porque fijadas no viene discriminado
  # Solo apelaciones
  # datamart_penal$audiencias_xtipos_tribunalja$`APELACION Art. 507`
  "3_Tribunal_Juicio_Apelacion" = datamart_penal$audiencias_conteos_tribunalja, 
  "4_Vocales_Casacion" = datamart_penal$audiencias_conteos_casacion %>%
    select("circunscripcion","organismo","año_mes","aud_fijadas","aud_realizadas"),
  .id = "grupo"
  ) %>% 
  filter(circunscripcion != "Total") %>% 
  mutate(fecha = as.Date(paste0(año_mes, "-01"))) %>% 
  # selecciono intervalo de interes
  filter(fecha >= start_date, fecha < end_date) 

# falta armar dataframe fijadas para 1,2,3, menos para 4
df_aud_fijadas_raw <- bind_rows(
  "1_Jueces_Garantías" = datamart_penal$audiencias_fijadas_garantia %>% 
    filter(ffijacion >= data_interval_start, ffijacion < data_interval_end) %>% 
    select(circunscripcion, ffijacion), 
  "2_Juez_Penal_Menores" = datamart_penal$audiencias_fijadas_penalmenores %>% 
    filter(ffijacion >= data_interval_start, ffijacion < data_interval_end) %>% 
    select(circunscripcion, ffijacion), 
  # Acá hay un problema: si separa las realizadas en tjuicio y apelacion
  # no puedo calcular indicador porque fijadas no viene discriminado
  # Solo apelaciones
  # datamart_penal$audiencias_xtipos_tribunalja$`APELACION Art. 507`
  "3_Tribunal_Juicio_Apelacion" = datamart_penal$audiencias_fijadas_tribunalJA %>% 
    filter(ffijacion >= data_interval_start, ffijacion < data_interval_end) %>% 
    select(circunscripcion, ffijacion), 
  "4_Vocales_Casacion" = datamart_penal$audiencias_fijadas_casacion %>% 
    filter(ffijacion >= data_interval_start, ffijacion < data_interval_end) %>% 
    select(circunscripcion, ffijacion), 
  .id = "grupo"
  ) %>% 
  filter(ffijacion >= start_date, ffijacion < end_date) %>% 
  rename(audiencia_fijada = ffijacion) %>% 
  mutate(año_mes = format(as.Date(audiencia_fijada), "%Y-%m"))


var_audiencias = c(
  unique(unidades_conteo$referencia), 
  unique(actos %>% filter(str_detect(referencia, "audiencia")) %>% .$referencia)
  )


df_aud_realizadas <- df_aud_realizadas_raw %>% 
  mutate(magistrado = ifelse(is.na(magistrado), organismo, magistrado)) %>%
  mutate(audiencias_realizadas = ifelse(is.na(cantidad), aud_realizadas, cantidad)) %>%
  select(any_of(var_audiencias)) %>% 
  # limpio df
  filter(circunscripcion != "Total") %>% select(-organismo) %>% 
  pivot_longer(-any_of(unique(unidades_conteo$referencia)), names_to = "variables", values_to = "cantidad") %>% 
  group_by_at(agrupador) %>% 
  summarise(audiencias_realizadas = sum(cantidad, na.rm = T)) 


df_aud_fijadas <- df_aud_fijadas_raw %>% 
  # limpio df
  pivot_longer(-any_of(unique(unidades_conteo$referencia)), names_to = "variables", values_to = "cantidad") %>% 
  group_by_at(agrupador) %>% 
  summarise(audiencias_fijadas = n())
  
df_ratio_realiz_fijadas <- df_aud_realizadas %>% 
  left_join(df_aud_fijadas) %>% 
  mutate(audiencias_fijadas = ifelse(is.na(audiencias_fijadas), 0, audiencias_fijadas)) %>% 
  mutate(ratio_realizadas_fijadas = round(audiencias_realizadas/audiencias_fijadas, digits = 2),
         pje = paste0(ratio_realizadas_fijadas*100, " %")) %>% 
  group_by_at(agrupador[2]) %>% 
  mutate(pje = ifelse(pje == "Inf %", "-", pje))

```


```{r, table_ratio_refij, eval = (tablas)}
df_ratio_realiz_fijadas %>% penaltable()
```
```{r, eval = (tablas), results='asis'}
cat('\\pagebreak')
```


`r if (graficos) '# Gráfico de eficacia en la Programación de Audiencias'`

**`r if (graficos)  apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`**


```{r, plot_ratio_real_fij, eval = (graficos), fig.width=10, fig.height=6,  fig.align='center'}

plot_2v(df_ratio_realiz_fijadas, agrupador[2], audiencias_realizadas, audiencias_fijadas)

```


`r if (tablas) '# Tabla de productividad Realizacion de Audiencias'`

**`r if (tablas)  apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`**


```{r, proc_real_diash }

# Ojo actualizar habiles.R
meses_start = unique(ymd(paste0(df_aud_realizadas$año_mes, "-01")))
meses_end  = ceiling_date(meses_start, 'month') %m-% days(1)

diashabiles <- vector()

for(i in seq_along(meses_start)){
  diashabiles[i] <- networkDays(meses_start[i], meses_end[i], feriadoseinhabiles)
}

diashabiles <- tibble(año_mes = format(as.Date(meses_start), "%Y-%m"), diashabiles)

df_aud_realizadas <- df_aud_realizadas %>% 
  left_join(diashabiles) %>% 
  mutate(ratio_realizadas_diash = round(audiencias_realizadas/diashabiles, digits = 1)) 
  
```


```{r, table_real_diah, eval = (tablas)}
df_aud_realizadas %>% penaltable()
```

```{r, eval = (tablas), results='asis'}
cat('\\pagebreak')
```


<!-- `r if (graficos) '# Gráfico de productividad Realizacion de Audiencias'` -->

<!-- **`r if (graficos)  apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`** -->


<!-- ```{r, eval = (graficos), fig.width=10, fig.height=6,  fig.align='center'} -->

<!-- plot_2v(df_aud_realizadas, agrupador[2], audiencias_realizadas) -->

<!-- ``` -->

`r if (tablas) '# Tabla sobre Audiencias, Bloques y Horas'`

**`r if (tablas)  apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`**


```{r, proc_audreal_horas}
df_aud_complejidad <- df_aud_realizadas_raw %>% 
  ungroup() %>% 
  mutate(magistrado = ifelse(is.na(magistrado), organismo, magistrado)) %>%
  select(grupo:`horas en audiencia`) %>% 
  #pivot_longer(-any_of(unique(unidades_conteo$referencia)), names_to = "variables", values_to = "cantidad") %>% 
  group_by_at(agrupador) %>% 
  summarise_if(is.numeric, sum, na.rm = T)  
 
  
```


```{r,table_audreal_horas, eval = (tablas)}
df_aud_complejidad %>% penaltable()
```
```{r, eval = (tablas), results='asis'}
cat('\\pagebreak')
```


```{r, proc_demora_frac}

demorafrac <- fdemorafracaso(datamart_penal$df_audic_demora, inst = "1")

```


`r if (tablas) '# Tabla de Gestión de Audiencias en Garantía: Puntualidad y Fracasos'`

**`r if (tablas)  apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`**

```{r, table_demora_gral, eval = (tablas)}
demorafrac$df_demora_gral %>% select(-var_pje) %>% 
  kable(caption = str_c("Puntualidad en Audiencias"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c',
        longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)  
```

```{r, table_demora_xtipo, eval = (tablas)}
demorafrac$df_demora_xtipo %>%  select(-var_pje) %>% 
  kable(caption = str_c("Demora"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c',
        longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)
```

```{r,table_aud_frac, eval = (tablas)}
demorafrac$df_fracaso %>%  select(-var_pje) %>% 
  kable(caption = str_c("Audiencias Fracasadas "," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c',
        longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10)

```

\elandscape

`r if (graficos) '# Gráficos sobre Audiencias: demora y fracasos'`

**`r if (graficos)  apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`**


```{r, plot_aud_dem_frac, eval = (graficos), fig.width=19, fig.height=20}

gdemora = demorafrac$df_demora_xtipo %>% 
  plot_pie_chart(cantidad, tipo_demora)

gfracaso = demorafrac$df_fracaso %>% 
  plot_pie_chart(cantidad, fracaso_motivos)
  

figure =  ggarrange(gdemora, gfracaso, nrow = 2,
                    labels = c("Audiencias Demoradas", "Audiencias Fracasadas"), 
                    hjust= -2, font.label = list(size = 24), 
                    legend = "bottom") 

# figure = annotate_figure(figure, top = text_grob("Eficiencia en la gestión del tiempo", 
#                   color = "black", face = "bold", size = 20))  

figure
```

```{r, eval = (graficos), results='asis'}
cat('\\pagebreak')
```


`r if (tablas) '# Resultados en Instancias Recursivas'`

**`r if (tablas)  apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`**

```{r, proc_aud_recurso }

# OJO san salvador suma a concordia!

df_conf_rev_anul_raw <- datamart_penal$resoluciones_tribunalja$resultado_tapel %>% 
  filter(circunscripcion != "Total") %>% 
  select(circunscripcion, año_mes, magistrado, confirmatoria, revocatoria, nulidad) %>% 
  rowwise() %>% 
  mutate(total = sum(confirmatoria, revocatoria, nulidad, na.rm = T)) 


df_conf_rev_anul <- df_conf_rev_anul_raw %>% 
  #group_by(circunscripcion, año_mes) %>%
  group_by(circunscripcion) %>% 
  summarise(confirmatoria = sum(confirmatoria,na.rm = T), 
            revocatoria = sum(revocatoria, na.rm = T), 
            nulidad = sum(nulidad, na.rm = T), 
            total = sum(total, na.rm = T))

porcentajes <- df_conf_rev_anul %>% 
    ungroup() %>%
    mutate(total = as.character(total)) %>% 
    janitor::adorn_percentages( na.rm = T) %>% 
    mutate_if(is.numeric, list(~ . * 100)) %>% 
    mutate_if(is.numeric, round, digit = 1) %>% 
    rename_all(~paste0(., "_ptje"))
  
df_conf_rev_anul <- df_conf_rev_anul %>% 
  bind_cols(porcentajes %>%
              select("confirmatoria_ptje", "revocatoria_ptje",
                     "nulidad_ptje")) %>% 
  select(circunscripcion, everything()) %>% 
  mutate(
    confirmatoria_ptje = ifelse(is.nan(confirmatoria_ptje), "-", confirmatoria_ptje),
    revocatoria_ptje = ifelse(is.nan(revocatoria_ptje), "-", revocatoria_ptje),
    nulidad_ptje = ifelse(is.nan(nulidad_ptje), "-", nulidad_ptje)
  )

```


```{r,table_aud_recurso, eval = (tablas)}
df_conf_rev_anul %>% 
  kable(caption = str_c("Confirmación, Revocación y Nulidad "," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c',
        longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                 full_width = F, font_size = 10) %>% 
  kable_styling(latex_options = c("repeat_header"))  
```

```{r, eval = (tablas), results='asis'}
cat('\\pagebreak')
```

`r if (graficos) '# Grafico de resultados por Circunscripción'`

**`r if (graficos)  apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`**


```{r, plot_aud_rec_xcirc, eval = (graficos), fig.width=12, fig.height=8}
df_conf_rev_anul %>% 
  select(-c(confirmatoria, revocatoria, nulidad, total)) %>% 
  pivot_longer(!circunscripcion, names_to = "resultado", values_to = "pje") %>% 
  ggplot(aes(fill=resultado, y=pje, x=circunscripcion)) + 
  geom_bar(position="stack", stat="identity") +
  theme(legend.title = element_blank(), 
        legend.position = "bottom",axis.title.x=element_blank(),
        #axis.title.y=element_blank(),
        legend.text = element_text(size=14),
        strip.text =element_blank(), plot.title = element_text(size=14), 
        plot.subtitle = element_text(size=12), 
        plot.caption = element_text(size=9, color = "darkgrey", hjust = 1)) 


```

```{r, eval = (graficos), results='asis'}
cat('\\pagebreak')
```



\begin{center}
Superior Tribunal de Justicia de Entre Ríos - Área de Planificación Gestión y Estadística  
\end{center}

Director: Lic. Sebastián Castillo   
Equipo: Lic. Marcos Londero y Srta. Emilce Leones  
0343-4209405/410 – ints. 396 y 305  
http://www.jusentrerios.gov.ar/area-planificacion-y-estadisticas-stj/  
correos: apge@jusentrerios.gov.ar - estadistica@jusentrerios.gov.ar   
Laprida 250, Paraná, Entre Ríos 


```{r}
# Revisiones 07/02/23---
# actos_conclusivos = sentencias (eficacia resolucion conflicto): OK
# excluir indicador eficacia en atención inmediata. OK
# agregar motivos vinculados a audiencias no realizadas: 
# agregar puntualidad/imputualidad
# productividad
# instancias recursivas
# graficar datos descriptivos

# Revision 10/02---
# computar solo fraud > septiembre 2022
# unir fracasadas, reprogramadas, etc..
# Gráfico demora_Gral
# Audiencias de Ejecución
# Integrar 
# Cantidad de jueces:
# Cantidad empleados: formales
# habitantes por departamento
# Saloes
# OGa: 1 x dpto, menos Islas (suma a Gualeguay) y SSalvad (suma a Concordia)
#      2 x dpto Federacion (habitantes de las ciudades)


```