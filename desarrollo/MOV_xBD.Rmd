---
title: "Procedimiento para actualizar Base Datos"
author: "Área de Planificación Gestión y Estadística"
date: 
output:
  pdf_document: default
  html_document: default
params:
  ControlenBD_movimientosxbd: yes
  Control_listado_informatica: yes
  GuardaenBD_listado_informatica: no
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
source("../R/informe.R")
library(readr)
```

```{r}
cbd <- params$ControlenBD_movimientosxbd
cli <- params$Control_listado_informatica
gli <- params$GuardaenBD_listado_informatica
```

# Procedimiento para la actualización de movimientos de la BD_Justat con la BD_Informática
# Fecha: `r Sys.time()`

## Base de Datos de JUSTAT

### Cantidad de Registros

```{r, eval = cbd}
movimientos_db <- tbl(DB_PROD(), "movimientos")
movimientos_db %>% summarise(cantidad_registros_preexistentes = n()) %>% kable()

```

### Último registro 

```{r, eval = cbd}
movimientos_db %>% filter(row_number() == n()) %>% collect() %>% t() %>% kable() 
  
```

## Base de Datos de Informatica  

### Cantidad de registros de la consulta

```{r, eval = cli}

mov_informatica <-
  read_csv("/home/scastillo/apgyeinformes/Modelo_Parametrizado/data/movimientosxbd.csv", quote = '"') %>% 
  rename(id = '_id') %>% mutate(id = str_remove_all(id, '"')) %>% 
  mutate(fecha = as.Date(ymd_hms(fecha_hora))) %>% 
  mutate(dia_s = wday(fecha, label = TRUE, abbr = FALSE))  %>% 
  mutate(jurisdiccion = ifelse(jurisdiccion == "Crespo", "Paraná", jurisdiccion), 
         jurisdiccion = ifelse(jurisdiccion == "General Ramirez", "Diamante", jurisdiccion), 
         jurisdiccion = ifelse(jurisdiccion == "Rosario del Tala", "Tala", jurisdiccion))  
  #select(!c('_id', 'tipo_proceso')) # Actualizacion que excluyo para efectuar el guardado. 
  #28/09/21 vuelvo a incluir variables para validar 'distinct' considerando id


mov_informatica %>% summarise(cantidad_registros_actualizacion = n()) %>% kable()
```

### Primer Registro

```{r, eval = cli}
mov_informatica[1, ] %>% t() %>% kable()
```

### Último Registro

```{r, eval = cli}
mov_informatica[nrow(mov_informatica), ] %>% t() %>% kable() 
```

`r if (gli) '# Actualización de Base de Datos JUSTAT'`

```{r, eval=gli}
DBI::dbWriteTable(DB_PROD(), "movimientos", mov_informatica,  append = TRUE,  row.names = FALSE)
```

`r if (gli) '## Cantidad de Registros de Base Actualizada'`

```{r, eval=gli}
movimientos_db %>% summarise(cantidad_registros_resultante = n()) %>% kable()
```

`r if (gli) '## Último registro de Base Actualizada'`

```{r, eval=gli}
movimientos_db %>% filter(row_number() == n()) %>% collect() %>% t() %>% kable()
```

