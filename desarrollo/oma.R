start_date <- "2019-12-01"
end_date <- "2020-01-01"

# resultado_proc$inic %>% View()
#   iniciados_paz_proc_comparativo(cfecha = desagregacion_mensual) %>% View()

poblacion <- jdos_paz
View(df)


str_detect(personal_oma$apellido, inic$resp)

inic_test <- inic %>% 
  mutate(año_mes = format(data_interval_start, "%Y-%m")) %>% 
  mutate(resp = toupper(resp)) %>%
  #eliminamos el personal que no es de OMA
  filter(grepl(paste(personal_oma$apellido, collapse="|"), resp)) %>%
  select(circunscripcion, resp, everything()) 

colnames(temp) <- "casos"
temp$casos <- as.character(temp$casos)


probation 
mediacion
stringr::str_detect(temp$casos[[1]], mediacion)
names(temp)


temp <- data.frame(stringi::stri_trans_general(inic_test$resp, "Latin-ASCII"))
personal_oma$apellido <- stringi::stri_trans_general(personal_oma$apellido, "Latin-ASCII")
mediacion <- str_c(personal_oma$apellido[personal_oma$grupo == "Mediación"], collapse = "|")
probation <- str_c(personal_oma$apellido[personal_oma$grupo == "Probation"], collapse = "|")
temp$funcion <- NA
for (i in seq_along(temp$casos)) {
  
  if (stringr::str_detect(temp$casos[[i]], mediacion)){
    temp$funcion[[i]] <- "Mediación"
  } else 
    if (stringr::str_detect(temp$casos[[i]], probation)){
      temp$funcion[[i]] <- "Probation"
    } else {
      temp$funcion[[i]] <- "No reconocido"
  }
}
table(temp$funcion)


inic_test <- inic %>% 
  group_by(circunscripcion, año_mes, funcion) %>% 
  summarise(cantidad = n()) %>% 
  ungroup() %>%
  group_by(circunscripcion, año_mes) %>% 
  pivot_wider(names_from = funcion,
              values_from = cantidad,
              values_fill = list(cantidad = 0)) %>% 
  arrange(circunscripcion, año_mes) %>% 
  ungroup() %>%
  group_by(circunscripcion) %>%
  do(janitor::adorn_totals(.))


# --------- Audiencias

audic_xestado <- resultado %>%
  mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m")) %>%
  mutate(fijadas_periodo = (f1 >= data_interval_start & f1 < data_interval_end & str_detect(tr, "fijad")),
         programadas_periodo = (f2 >= data_interval_start & f2 < data_interval_end & str_detect(tr, "fijad")),
         realizadas_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
           str_detect(tr, "realizad") & t1 == "1", 
         frac_nodomic_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
           str_detect(tr, "realizad") & t1 == "0", 
         frac_incompdte_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
           str_detect(tr, "realizad") & t1 == "2", 
         rac_incompddo_periodo = f2 >= data_interval_start & f2 < data_interval_end & 
           str_detect(tr, "realizad") & t1 == "3") %>% # Concordia Cargó todo como fijada
  group_by(circunscripcion, año_mes, resp) %>%
  summarise(fijadas = sum(fijadas_periodo, na.rm = T),
            programadas = sum(programadas_periodo, na.rm = T),
            realizadas = sum(realizadas_periodo, na.rm = T), 
            fracasada_inexist_domicil = sum(frac_nodomic_periodo, na.rm = T), 
            fracasada_incomparec_denunciante = sum(frac_incompdte_periodo, na.rm = T), 
            fracasada_incomparec_denunciado = sum(rac_incompddo_periodo, na.rm = T)) %>%
  mutate('no realizadas' = fracasada_inexist_domicil + 
           fracasada_incomparec_denunciante +
           fracasada_incomparec_denunciado) %>% 
  select(-c(fijadas, programadas, fracasada_inexist_domicil,
            fracasada_incomparec_denunciante,
            fracasada_incomparec_denunciado)) %>%
  mutate(resp = ifelse(is.na(resp), "sin dato", resp)) %>% 
  janitor::adorn_totals("row")


audic_resultado <- resultado %>%
  filter(str_detect(tr, "realiz")) %>%
  mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m")) %>%
  filter(f2 >= data_interval_start, f2 < data_interval_end) %>% 
  rename(fraud = f2, estaud = t1, ra = t2, vmed = t3, tmed = t4,
         duracm = n1, duracb = n2) %>%
  #codconver("IGMP", "estaud") %>%
  codconver("IGMP", "ra") %>%
  codconver("IGMP", "vmed") %>%
  codconver("IGMP", "tmed") %>% 
  mutate(resp = ifelse(is.na(resp), "sin dato", resp))


audic_resultado_acsac <- audic_resultado %>% 
  group_by(circunscripcion, año_mes, resp, ra) %>%
  summarise(cantidad_audiencias = n()) %>% 
  tidyr::spread(ra, cantidad_audiencias, fill = 0)
  

# agregamos a la tabla audic_xestado la columna audic_resultado_acsac$'con acuerdo'

audic_resultado_acsac$`Con Acuerdo`
names(audic_xestado)
names(audic_resultado_acsac)

audic_xestado_test <- audic_xestado %>% 
  left_join(audic_resultado_acsac, by = c("circunscripcion", "año_mes", "resp")) %>% 
  select(-c('No Aplica', 'Sin Acuerdo', 'sin_dato'))

names(audic_xestado)


audic_resultado_acsac <- audic_resultado %>% 
  group_by(circunscripcion, año_mes, resp, ra) %>%
  summarise(cantidad_audiencias = n()) %>% 
  tidyr::spread(ra, cantidad_audiencias, fill = 0)

# agregamos a la tabla audic_xestado la columna audic_resultado_acsac$'con acuerdo'
audic_xestado_test <- audic_xestado %>%
  filter(!circunscripcion == "Total") %>% 
  left_join(audic_resultado_acsac,
            by = c("circunscripcion", "año_mes", "resp")) %>% 
  select(-c('No Aplica', 'Sin Acuerdo', 'sin_dato')) %>% 
  rename('con acuerdo' = 'Con Acuerdo') %>% 
  mutate(`con acuerdo` = ifelse(is.na(`con acuerdo`), 0, `con acuerdo`)) %>% 
  group_by(circunscripcion) %>%
  do(janitor::adorn_totals(.))

View(audic_xestado_test)
View(audic_xestado)
