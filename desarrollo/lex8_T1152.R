# Organismos con Lex8

# library(dplyr)
# library(stringr)

source("../apgyeinformes/R/informe.R")

orgs <- apgyeJusEROrganization::listar_organismos()

orgs_conOPL8 <- apgyeJusEROrganization::listar_operaciones_por_organismo() %>% 
  filter(is.na(finalizacion)) %>% 
  filter(grepl("23", operacion))
  # hasta aca filtra las CADR23 y CITIPP23 aún vigentes -en curso-

orgs_conOPL8_ieps <- orgs_conOPL8 %>% 
  select(organismo) %>% unique()
  
orgs_conOPL8 <- orgs_conOPL8 %>% 
  left_join(orgs, by = "organismo") %>% 
  select(circunscripcion, localidad, organismo_descripcion) %>%
  # esto para buscar los nombres de los organismos
  distinct() %>% 
  arrange(circunscripcion, localidad, organismo_descripcion)

orgs_conOPL8 %>% View()

# con el resultado del View, copiamos y pegamos en Excel
# luego copiamos y pegamos en el mail como Tabla dando formato

poblacion_total <- apgyeJusEROrganization::listar_organismos() %>% 
  filter(tipo %in% c("jdo", "cam", "sal", "cen")) 

jdos_paz <- poblacion_total %>% 
  filter(str_detect(materia, "paz"), 
         categoria == "1"  
         | organismo == "jdopaz0000ram" 
         | organismo == "jdopaz0000cre" 
         | organismo == "jdopaz0000ove" 
         | organismo == "jdopaz0000sbe"
         | organismo == "jdopaz0000vpa"
         | organismo == "jdopaz0000vel"
         | organismo == "jdopaz0000sjo"
         | organismo == "jdopaz0000sel"
         | organismo == "jdopaz0000bov")

jdos_paz_23 <- poblacion_total %>% 
  filter(grepl("paz", organismo)) %>% 
  filter(str_detect(materia, "paz"), tipo != "cam", categoria != "1" | is.na(categoria))

start_date <- "2019-02-01"
end_date <- "2020-01-01"

inic23 <- iniciados_paz_23c(poblacion = jdos_paz_23,
                            start_date = start_date,
                            end_date = end_date,
                            estadistico = "conteo") %>% 
  .$inic %>% 
  iniciados_paz_comparativo_23(cfecha = T) %>% 
  filter(circunscripcion != "Total") %>% 
  rename(procesos = proceso) %>% 
  select(circunscripcion, organismo, año_mes, procesos)

inic1 <- iniciados_paz_procxgpo(db_con = DB_PROD(),
                                poblacion = jdos_paz,
                                start_date = start_date,
                                end_date = end_date,
                                estadistico = "conteo") %>% 
  .$inic %>%
  iniciados_paz_proc_comparativo(cfecha = TRUE) %>%
  filter(circunscripcion != "Total") %>% 
  rename(procesos = Total) %>% 
  select(circunscripcion, organismo, año_mes, procesos) 

iniciados <- bind_rows(inic1, inic23) %>% 
  group_by(circunscripcion, organismo) %>% 
  summarise(cantidad = sum(procesos)) %>%
  ungroup() %>% 
  arrange(desc(cantidad)) %>% 
  left_join(poblacion_total %>%
              select(organismo_descripcion, categoria), by = c("organismo" = "organismo_descripcion")) %>% 
  mutate(grupoCat = if_else(categoria == 1, "1º", "2ª/3ª")) %>% 
  select(circunscripcion, organismo, categoria, grupoCat, cantidad) %>% 
  arrange(grupoCat, desc(cantidad))
  



