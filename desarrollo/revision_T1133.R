# %>% 
#   filter(row.names(.) %in% c(5:7))

db_con <- DB_PROD()
poblacion <- cen
start_date <- "2020-03-01"
end_date <- "2020-04-01"
desagregacion_mensual <- T

inic <- iniciados_cen(db_con = DB_PROD(), poblacion = cen, 
                      start_date = start_date, end_date = end_date)

inic[[1]] %>% View()

iniciados_cen <- function(db_con, poblacion, start_date="2018-02-01", end_date = "2018-07-01") {
  
  #tipos_proceso <- apgyeOperationsJusER::Tipo_procesos %>% filter(materia == !!materia) %>% select(tipo_de_procesos,proceso_conocimiento)
  
  inic_tp <- db_con %>%
    apgyeDSL::apgyeTableData("CIMED") %>%
    filter(iep %in% !!poblacion$organismo) %>%
    mutate(finicio = dmy(finicio)) %>%
    filter(!is.na(finicio)) %>%
    filter(finicio >= start_date, finicio < end_date ) %>% collect()
  
  
  if (nrow(inic_tp) == 0){
    return(inic_tp)
  } else {
    inic_tp <- inic_tp %>%
      filter(finicio >= data_interval_start, finicio < data_interval_end) %>%       
      mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m")) %>% 
      mutate(tipo_mediacion = case_when(str_detect(tmed, "MEDIACION OFICIAL") ~ "oficial",
                                        str_detect(tmed, "MEDIACION PRIVADA") ~ "privada",
                                        TRUE ~ "sin_clasificar")) %>% mutate(circ = toupper(circ)) %>%
      mutate(tipo_circ = case_when(str_detect(circ, "GUALEGUAYCH") ~ "Gualeguaychú",
                                   str_detect(circ, "URUGUAY") ~ "Uruguay",
                                   str_detect(circ, "CONCORDIA") ~ "Concordia",
                                   str_detect(circ, "PARAN") ~ "Paraná",
                                   str_detect(circ, "GUALEGUAY") ~ "Gualeguay",
                                   str_detect(circ, "TALA") ~ "Tala",
                                   str_detect(circ, "NOGOY") ~ "Nogoyá",
                                   str_detect(circ, "VICTORIA") ~ "Victoria",
                                   str_detect(circ, "CHAJAR") ~ "Chajarí",
                                   str_detect(circ, "COLON|COLÓN") ~ "Colón",
                                   str_detect(circ, "DIAMANTE") ~ "Diamante",
                                   str_detect(circ, "FEDERACION") ~ "Federación",
                                   str_detect(circ, "FEDERAL") ~ "Federal",
                                   str_detect(circ, "FELICIANO") ~ "Feliciano",
                                   str_detect(circ, "PAZ") ~ "La Paz",
                                   str_detect(circ, "VILLAGUAY") ~ "Villaguay",
                                   str_detect(circ, "SALVADOR") ~ "San Salvador",
                                   str_detect(circ, "IBICUY") ~ "Islas del Ibicuy", TRUE ~ "sin_clasificar")) %>%
      distinct(iep, nro, .keep_all = TRUE)
    
    inic_xcirc <- inic_tp %>%
      filter(tipo_circ != "sin_clasificar") %>%
      select(circunscripcion = tipo_circ, tipo_mediacion, everything(), -iep) %>%
      group_by(circunscripcion, año_mes, tipo_mediacion) %>%
      summarise(cantidad = n()) %>%
      ungroup() %>%
      tidyr::spread(tipo_mediacion, cantidad, drop = F, fill = 0) %>%
      janitor::adorn_totals("col") %>%
      rename(mediacion_oficial = oficial, mediacion_privada = privada) %>% 
      arrange(circunscripcion, año_mes) %>% 
      group_by(circunscripcion) %>% 
      do(janitor::adorn_totals(.))
    
    inic_tp <- list(inic_xcirc)
    
    inic_tp
  }
}


# sueltos
inic_tp <- db_con %>%
  apgyeDSL::apgyeTableData("CIMED") %>%
  filter(iep %in% !!poblacion$organismo) %>%
  mutate(finicio = dmy(finicio)) %>%
  filter(!is.na(finicio)) %>%
  filter(finicio >= start_date, finicio < end_date ) %>% collect()
  filter(finicio >= data_interval_start, finicio < data_interval_end) %>%       
  collect() %>% 
  mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"))

  
  # --- cen_cr
  
resoluciones_cen <- function(db_con, poblacion, operacion = "CRMED", start_date = "2019-02-01", end_date = "2019-03-02", desagregacion_mensual = TRUE, cfecha = F) {
    
    un_mes <- ((lubridate::month(end_date) - lubridate::month(start_date)) == 1)
    
    operacion = rlang::enexpr(operacion)
    
    resultado <- db_con %>%
      apgyeTableData(!!operacion) %>%
      apgyeDSL::interval(start_date, end_date) %>%
      filter(iep %in% !!poblacion$organismo)
    
    
    if(desagregacion_mensual) {
      resultado <- resultado %>% group_by(iep, data_interval_start, data_interval_end)
    } else {
      resultado <- resultado %>% group_by(iep)
    }
    
    resultado <- resultado %>%
      collect()
    
    if (nrow(resultado) == 0){
      return(NULL)
    } else {
      
      resultado <- resultado %>%
        process() %>%
        .$result %>%
        ungroup()
      
      if(desagregacion_mensual) {
        resultado <- resultado %>%
          mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
                 fecha =  as.Date(data_interval_end) - 1) %>%
          mutate(mediana_durac_inic_fin = as.character(mediana_durac_inic_fin)) %>%
          select(tipo_circ, año_mes, tres_disc, resultado, mediana_durac_inic_fin, fecha)
      } else {
        resultado <- resultado %>%
          mutate(mediana_durac_inic_fin = as.character(mediana_durac_inic_fin)) %>%
          select(tipo_circ, tres_disc, resultado, mediana_durac_inic_fin)
      }
      
      
      if(desagregacion_mensual) {
        if(un_mes) {
          resultado <- resultado %>%
            rename(circunscripcion = tipo_circ, cantidad_mediaciones = resultado) %>%
            select(-mediana_durac_inic_fin) %>%
            tidyr::spread(tres_disc, cantidad_mediaciones, drop = T, fill = 0) %>%
            janitor::adorn_totals("col") %>%
            janitor::adorn_totals("row")
          
          resultado
          
        } else {
          
          resultado <- resultado %>%
            rename(circunscripcion = tipo_circ, cantidad_mediaciones = resultado) %>%
            select(-mediana_durac_inic_fin) %>%
            tidyr::spread(tres_disc, cantidad_mediaciones, drop = T, fill = 0 ) %>%
            janitor::adorn_totals("col") %>%
            arrange(circunscripcion, año_mes) %>%
            group_by(circunscripcion) %>%
            do(janitor::adorn_totals(.))
          
          resultado
          
        }
        
      } else {
        resultado <- resultado %>%
          rename(circunscripcion = tipo_circ, cantidad_mediaciones = resultado) %>%
          select(-mediana_durac_inic_fin) %>%
          tidyr::spread(tres_disc, cantidad_mediaciones, drop = T, fill = 0) %>%
          janitor::adorn_totals("col") %>%
          janitor::adorn_totals("row")
        
        resultado
        
      }
      
      if(desagregacion_mensual) { if(!cfecha) {resultado <- resultado %>% select(-fecha)} }
    
    }
    
    resultado
    
  }
  
resoluciones_cen(db_con, poblacion, operacion = "CRMED",
                 start_date, end_date, desagregacion_mensual)
