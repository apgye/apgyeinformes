---
  title: Análisis de duraciones 
subtitle: 
  author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
  pdf_document:
  includes:
  in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---
  
```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')


# bandera de análisis de duraciones: a_d
muestra_ad_compacto <- T
a_d_detalle <- F

if (g){
  muestra_torta <- F
  muestra_barras <- T
}

```


```{r cco1_analisis_durac, eval = a_d}

ad_gral_primaria <- DB_PROD() %>% 
  apgyeTableData("CADR1C") %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!jdos_cco$organismo) %>% 
  collect()

ad_inicio_durac <- ad_gral_primaria %>% 
  mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m"),
         finicio = as.Date(finicio, format="%d/%m/%Y"),
         fres = as.Date(fres, format="%d/%m/%Y")) %>%
  filter(!is.na(finicio) & !is.na(fres)) %>% 
  mutate(dias = fres - finicio) %>% 
  mutate(dias = as.numeric(dias))

```

## Duraciones de Procesos en Juzgados Civiles

```{r cco1_analisis_durac_trabajo, eval = a_d}

ad_primaria <- ad_inicio_durac %>%
  filter(fres >= finicio) %>% 
  left_join(apgyeJusEROrganization::listar_organismos() %>% 
              select(iep = organismo, organismo_descripcion, circunscripcion),
              by = "iep") %>%
  rename(organismo = organismo_descripcion)

ad_primaria_sumxorgxmes <- ad_primaria %>% 
  group_by(circunscripcion, organismo, año_mes) %>% 
  summarise(promedio_dias = round(mean(dias, na.rm = T)),
            causas = n(),
            nro_expedientes = str_c(nro, collapse = ", ")) %>% 
  arrange(desc(promedio_dias))

ad_primaria_sumxorg <- ad_primaria %>% 
  group_by(circunscripcion, organismo) %>% 
  summarise(promedio_dias = round(mean(dias, na.rm = T)),
            causas = n(),
            nro_expedientes = str_c(nro, collapse = ", ")) %>% 
  arrange(desc(promedio_dias))

# View(ad_primaria_sumxorg)
if (muestra_ad_compacto){
  
  if (length(unique(str_c(ad_primaria_sumxorgxmes$circunscripcion, "-", ad_primaria_sumxorgxmes$organismo))) == 1){
    ad_primaria_sumxorgxmes_tbl <- ad_primaria_sumxorgxmes %>% 
      select(-causas, -nro_expedientes)
  } else {
    ad_primaria_sumxorgxmes_tbl <- ad_primaria_sumxorgxmes %>% 
      select(-causas, -nro_expedientes) %>% 
      ungroup() %>% 
      select(-año_mes) 
    
    ad_primaria_sumxorgxmes_tbl <- ad_primaria_sumxorgxmes_tbl %>% 
      group_by(circunscripcion, organismo) %>%
      summarise(promedio_dias = round(mean(promedio_dias))) %>%
      arrange(desc(promedio_dias)) %>% 
      mutate(promedio_años = round(promedio_dias/365,1))
  }
    
  ad_primaria_sumxorgxmes_tbl %>% 
    kable(caption = str_c("Promedio de duración en días corridos y años desde Fecha de Inicio hasta Fecha de Resolución"," (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                  full_width = F, font_size = 10) %>% 
    # column_spec(2, "4cm") %>% 
    # column_spec(6, "8cm") %>% 
    row_spec(0, angle = 90) 
    # landscape()
  
} else {
  
  # Tabla principal de resultados con los promedios y los casos 
  ad_primaria_sumxorgxmes %>%
    kable(caption = str_c("Promedio de duración en días corridos y años desde Fecha de Inicio hasta Fecha de Resolución"," (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                  full_width = F, font_size = 10) %>% 
    column_spec(2, "4cm") %>% 
    column_spec(6, "8cm") %>% 
    row_spec(0, angle = 90) 
    # landscape()
}

# Tabla Adicional de resultados con el detalle del organismo
if (a_d_detalle){
  ad_inicio_durac %>%
    left_join(apgyeJusEROrganization::listar_organismos() %>% 
              select(iep = organismo, organismo_descripcion, circunscripcion),
              by = "iep") %>%
    rename(organismo = organismo_descripcion) %>% 
    select(circunscripcion, organismo, año_mes, nro, finicio, fres, dias) %>%
    arrange(circunscripcion, organismo, año_mes, nro) %>%
    kable(caption = str_c("Casos detallados"," (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                  full_width = F, font_size = 10)
    # column_spec(2, "4cm")
    # row_spec(0, angle = 90) 
    # landscape()
  
  
  # fe_FEstado_SinFDesp %>% 
  #    kable(caption = str_c("Casos detallados"," (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  #   kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
  #                 full_width = F, font_size = 10)
  #   # column_spec(2, "4cm")

}


```

\pagebreak

```{r pre_cco1_g_barras, eval=muestra_barras}

ad_primaria_sum_barras <- ad_primaria_sumxorgxmes %>%
      select(-causas, -nro_expedientes) %>% 
      ungroup() %>% 
      select(-año_mes) 
    
ad_primaria_sum_barras <- ad_primaria_sum_barras %>% 
  group_by(circunscripcion) %>%
  summarise(promedio_dias = round(mean(promedio_dias),1)) %>%
  mutate(promedio_años = round(promedio_dias/365,1))

prom_durac_pcial <- round(mean(ad_primaria_sum_barras$promedio_años),1)

```


El siguiente gráfico ordena las duraciones promedio por jurisdicción, de mayor a menor, indicando en la línea vertical roja el promedio provincial de duración en años (Promedio provincial: `r prom_durac_pcial` años).

```{r cco1_g_barras, eval=muestra_barras}


library(ggplot2)

ggplot(ad_primaria_sum_barras) +
  geom_col(aes(y = promedio_años, 
               x = reorder(circunscripcion, promedio_años)),
               fill = "lightsteelblue3")  +
  geom_hline(yintercept = prom_durac_pcial, col = "red") +
  coord_flip()+
  labs(y = "Promedio de años",    # Cambia el nombre del eje x
       x = "Circunscripción")+
  annotate("text", x = 1, y = prom_durac_pcial, label = "Promedio provincial", 
           vjust = -.5, hjust=0, color = "red", size = 4, angle=90) 
  
  

  # barplot(height = ad_primaria_sum_barras$promedio_años, 
  #         names.arg = ad_primaria_sum_barras$circunscripcion, 
  #         ylab = "Promedio en años", 
  #         col="#69b3a2",
  #         legend.text = T,
  #         las=2, #las=2 hace vertical eje x,
  #         cex.names = 0.75
  #         ) 
  # abline(h = prom_durac_pcial, col = "red")
  # 


```

