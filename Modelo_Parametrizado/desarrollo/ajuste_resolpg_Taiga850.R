library(dplyr)
start_date <- "2019-04-01"
end_date <- "2019-07-01"

resolpg <- DB_PROD() %>%
  apgyeDSL::apgyeTableData('RESOLPG_v2') %>%
  apgyeDSL::interval(start_date, end_date) %>%
  collect()

resolpg$tres

