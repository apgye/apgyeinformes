---
title: Causas y Trámites iniciados por Grupos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
resultado_proc <- apgyeProcesamiento::iniciados_paz_procxgpo(db_con = DB_PROD(),
                                         poblacion = jdos_paz,
                                         start_date = start_date,
                                         end_date = end_date, 
                                         estadistico = "conteo")
resultado_tram <- apgyeProcesamiento::iniciados_paz_tramxgpo(db_con = DB_PROD(), 
                                         poblacion = jdos_paz,
                                         start_date = start_date,
                                         end_date = end_date,
                                         estadistico = "conteo")

is_resultado_pazfam <- FALSE

if (any(grepl("Federac|Crespo", jdos_paz$organismo_descripcion))){

  resultado_pazfam <- apgyeProcesamiento::iniciados_paz_fam(db_con = DB_PROD(),
                                          poblacion = jdos_paz,
                                          start_date = start_date,
                                          end_date = end_date,
                                          estadistico = "conteo")

  is_resultado_pazfam <- nrow(resultado_pazfam$inic) > 0

}

un_mes <- lubridate::date(end_date) - lubridate::date(start_date) <= 31

```

## Causas Iniciadas Juzgados de Paz `r apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`

\blandscape

```{r, paz1_ci_st_mes, eval = (ci & ("mes" %in% stag)),  fig.width=19, fig.height=16}
resultado_proc$inic_paz_pc %>% 
  tsci_g(agrupar =  "mes")
```

```{r, paz1_ci_st_circ, eval = (ci & ("circ" %in% stag)), fig.width=19, fig.height=16}
resultado_proc$inic_paz_pc %>% 
  tsci_g(agrupar =  "circ")
```

```{r, paz1_ci_st_org, eval = (ci & ("org" %in% stag)), fig.width=19, fig.height=16}
resultado_proc$inic_paz_pc %>% 
  tsci_g(agrupar =  "org")
```

\elandscape

```{r, paz1_ci, eval= (ci & t)}
res_ci_t_paz <- resultado_proc$inic %>% 
  apgyeProcesamiento::iniciados_paz_proc_comparativo(cfecha = desagregacion_mensual)

if (un_mes & any(res_ci_t_paz$circunscripcion == "Total")){
  res_ci_t_paz <- res_ci_t_paz %>%
    filter(!circunscripcion == "Total")
}

res_ci_t_paz %>% 
  kable(caption = str_c("Causas Iniciadas Civil"," (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                  full_width = F, font_size = 10)  %>%
  column_spec(4, bold = T, background = "#BBBBBB") %>%
  row_spec(0, angle = 90) %>% 
  footnote(general = "Aparecen en la tabla juzgados de 2a/3a categoría debido a su nueva estructura de datos similar a un juzgado de 1a.",
           general_title = "Nota:", footnote_as_chunk = T,
           title_format = c("italic", "underline"), threeparttable = T) %>%
  landscape()
```

\pagebreak

`r if ((ymd(start_date) >= make_date(2019,01,01)) & is_resultado_pazfam & t) '## Causas Iniciadas - competencia en Familia (a partir de jun-2019)'`

```{r, paz1_ci_fam, eval= (ci & t & ymd('start_date') >= make_date(2019,01,01)) & is_resultado_pazfam }
res_ci_t_paz_fam <- resultado_pazfam$inic %>%
  apgyeProcesamiento::iniciados_fam_comparativo(cfecha = desagregacion_mensual)

if (un_mes & any(res_ci_t_paz_fam$circunscripcion == "Total")){
  res_ci_t_paz_fam <- res_ci_t_paz_fam %>%
    filter(!circunscripcion == "Total")
}

res_ci_t_paz_fam %>% 
  kable(caption = str_c("Causas Iniciadas en materia Familia"," (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"),
        align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                    full_width = F, font_size = 10)  %>%
  column_spec(4, bold = T, background = "#BBBBBB") %>%
  row_spec(0, angle = 90)
# %>%
#   landscape()

```

\pagebreak

`r if (ci & t) '## Trámites Voluntarios Iniciados'`

```{r, paz1_ci_tram, eval= (ci & t) }
resul <- resultado_tram$inic %>%
      apgyeProcesamiento::iniciados_paz_tram_comparativo(cfecha = desagregacion_mensual)

if (!is.null(resul)) {

  if (un_mes){
    resul <- resul %>%
      filter(!circunscripcion == "Total")
  }
  
  resul %>%
      kable(caption = str_c("Trámites Iniciados"," (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
      kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                    full_width = F, font_size = 10)  %>%
      column_spec(4, bold = T, background = "#BBBBBB") %>%
      row_spec(0, angle = 90) %>% 
      landscape()
} else {
  cat(msg_NoData)
}

```

