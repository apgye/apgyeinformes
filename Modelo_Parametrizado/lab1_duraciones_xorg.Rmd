---
title: Duracion de Procesos 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
# https://www.kaggle.com/studentar/data-cleaning-challenge-outliers-r
# Validación de datos: valores extremos y anomalías
# Regla1: se eliminan las duracones negativas
# Regla2: más de tres desviaciones estándar es un outlier
# Regla3: se eliminan NA
# http://r-statistics.co/Loess-Regression-With-R.html
source("../R/informe.R")
source("../R/habiles.R")
source("../R/poblacion.R")

jdos_multif  = jdos_lab %>% 
  bind_rows(jdos_cco)


duraciones <-  apgyeProcesamiento::duracion_prim(db_con = DB_PROD(), jdos_multif, "CADR1L", 
                            start_date = start_date, end_date = end_date)

analisi_durac <- apgyeProcesamiento::duracion_A(jdos_multif, duraciones)


tproc_comparables = analisi_durac$duracion_tabla_long %>% 
  group_by(tipo_proceso) %>% 
  summarise(cantidad = n()) %>% 
  filter(cantidad > 10)

duracion_tabla_long <- analisi_durac$duracion_tabla_long %>% 
  filter(tipo_proceso %in% tproc_comparables$tipo_proceso)


durac_inic_prelim <- function(df){
  
  df = df %>% 
    filter(esta == "realizada" & (ta %in% c('conciliacion'))) %>% 
    filter(!is.na(finicio) & !is.na(fea))
  
  df = df %>% 
    mutate(finicio = dmy(finicio)) %>% 
    group_by(iep, ta) %>% 
    summarise(`cantidad de casos` = n(), 
              duracion_inicio_audiencia_conciliacion = round(median(as.integer(fea - finicio)))) %>% 
    left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                              "circunscripcion")],
              by = c('iep' = "organismo")) %>% 
    ungroup() %>% 
    select(circunscripcion, organismo = organismo_descripcion, everything(), -iep, -ta)
  
  df
  
}

```


## Duración de Procesos desde su inicio hasta la realización de audiencia de conciliación - `r apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`

A continuación presentamos la duración general de procesos desde la fecha de inicio hasta la realización de audiencia conciliacion. Dicha duración se cuenta en días corridos. Para esta métrica hemos empleado la *mediana estadística* que suministra una referencia representativa de las duraciones generales que tienen los procesos en cada organismo, y que a diferencia de la *media o promedio* es un medida que es robusta ante casos extremos de duración. Los casos incluidos son todos aquellos informados por cada organismo con audiencia conciliacion realizada en el período bajo estudio. Finalmente, hemos ordenado la tabla de manera descendente considerando las duraciones que presenta cada organismo. 

```{r}
df = audilab_prim(DB_PROD(), jdos_lab, start_date , end_date)

df =  durac_inic_prelim(df) %>% 
  arrange(desc(duracion_inicio_audiencia_conciliacion))

df %>% 
  arrange(desc(duracion_inicio_audiencia_conciliacion)) %>% 
  kable(caption = str_c("Duración de Procesos desde su inicio hasta audiencia conciliacion (dias corridos)"," (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>% 
   column_spec(4, color = "black", background = spec_color2(df$duracion_inicio_audiencia_conciliacion, 
                                                            palette = paletteer_c("ggthemes::Red-Black Diverging", 
                                                                                  n = length(df$duracion_inicio_audiencia_conciliacion), 
                                                                                  direction = -1))) 
  #row_spec(0, angle = 90) %>% 
  #landscape()


```

\pagebreak

## Duracion General de Procesos de Conocimiento en Juzgados Laborales

Presentamos a continuación informe de duración de procesos por juzgado evaluando la cantidad de días corridos transcurridos entre el inicio de la causa y la fecha de sentencia definitiva. Dado que los organismos presentan diferencias en sus nomecladores de *tipos de proceso* la evaluación exhaustiva de las causas, aunque pertinente, plantea dificultades de lectura y oscurece las comparaciones. Por esa razón, nos enfocaremos en una evaluación selectiva de aquellos tipos de procesos de conocimiento con mayor número de muestras. Sin perjuicio de ello, el alcance que aquí estamos proponiendo se puede fácilmente extender consultando la información completa de causas resueltas en https://tablero.jusentrerios.gov.ar/.         

Por lo anterior, para generar una visión representativa de las duraciones de procesos en cada juzgado y, al mismo tiempo, facilitar la comparación entre ellos se fijaron criterios de selección de casos para realizar este análisis. Los criterios son: 

+ los expedientes incluidos son aquellos con sentencia dictada entre las fechas de relevamiento, 
+ los *tipos de procesos* seleccionados son los 10 con mayor cantidad de ocurrencias en el conjunto relevado, 
+ las causas con duraciones anómalas o excepcionales desde el punto de vista estadístico (es decir, alejadas en 3 desviaciones estándar respecto de la media) se informan por separado, y finalmente
+ a fin de evitar datos sesgados por la presencia de valores extremos se optó por la mediana estadística como métrica para describir las duraciones. 

\pagebreak

```{r}

df = duracion_tabla_long %>% select(-c(duracion_minima, desviacion_estandar, cantidad_casos_extremos))

df = rename_with(df, ~ gsub("_", " ", .x, fixed = TRUE))

df %>% 
  group_by(circunscripcion, organismo) %>% 
  # gt() %>% 
  #  tab_header(
  #     title = "TABLA PRINCIPAL: duración de inicio a sentencia en procesos de conocimiento",
  #     subtitle = glue::glue("desde: {start_date} hasta: {end_date}")
  #   ) %>% 
  #   cols_align(
  #     align = "center", columns = everything()
  #   ) 
  outputTable(caption = str_c("TABLA PRINCIPAL: Duraciones de causas en días corridos desde inicio hasta sentencia -mediana estadística-"," (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")")) %>% 
  column_spec(5, color = "white", background = spec_color2(df$mediana, 
                                                           palette = paletteer_c("ggthemes::Red-Black Diverging", 
                                                                                 n = length(df$mediana), 
                                                                                 direction = -1))) %>% 
  column_spec(7, "6cm") %>% 
  row_spec(0, angle = 90) %>%
  landscape()

```


\pagebreak

```{r}
duracion_tabla_long %>% 
  mutate(tipo_proceso = str_replace_all(tipo_proceso, 
                                        "COBRO DE PESOS Y ENTREGA DE CERTIFICACION LABORAL",
                                        "COBRO DE PESOS Y ENT.CERTIF.LAB.")) %>% 
  select(circunscripcion, organismo, tipo_proceso, mediana) %>% 
  pivot_wider(names_from = "tipo_proceso", values_from = "mediana") %>%
  ungroup() %>% 
  kable(caption = str_c("TABLA AUXILIAR: Comparación de duraciones de causas en días corridos desde inicio hasta sentencia -mediana estadística-"," (", apgyeProcesamiento::getDataIntervalStr(start_date, end_date), ")" ), 
        align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>% 
  row_spec(0, angle = 90) %>%
  landscape()

```



<!-- \blandscape -->

<!-- ```{r, fig.width=10, fig.height=7,  fig.align='center'} -->
<!-- casos_federacion <- analisi_durac$duracion_tabla_long %>%  -->
<!--   filter(circunscripcion == "Federación") %>% .$tipo_proceso -->


<!-- comparacion_prim <- analisi_durac$duracion_tabla_long %>%  -->
<!--   filter(tipo_proceso %in% casos_federacion) -->

<!-- comparacion_prim %>%  -->
<!--   select(circunscripcion, organismo, tipo_proceso, mediana) %>%  -->
<!--   #pivot_wider(names_from = "tipo_proceso", values_from = "mediana") -->
<!--   ggplot(aes(x=tipo_proceso, y=mediana)) +  -->
<!--   geom_boxplot() + -->
<!--   geom_point(#position=position_jitter(0.2),  -->
<!--     size = 3) + -->
<!--   ggrepel::geom_text_repel(aes(label=ifelse(circunscripcion == "Federación", circunscripcion,'')),  -->
<!--                            color = "red", -->
<!--                            force=1, -->
<!--                            point.padding=unit(1,'lines'), -->
<!--                            direction='y', -->
<!--                            nudge_x=0.1, -->
<!--                            segment.size=0.2, -->
<!--                            vjust = 1) + -->
<!--   theme(legend.position = "bottom", legend.title = element_text(size = 8), -->
<!--         legend.text = element_text(size = 11), -->
<!--         strip.text.x = element_text(size = 11),  -->
<!--         plot.caption = element_text(size=10, color="black", hjust = 0.5)) + -->
<!--   labs(x = "", y = "mediana duracion días corridos",  -->
<!--        title = "Mediana de Duración de Procesos Federación vs. Fuero Civil") -->

<!-- ``` -->

<!-- \elandscape -->

<!-- ## Observaciones metodológicas -->

<!-- Se pone a disposición todo el material de procesamiento estadístico diseñada en R-Statistcal Computing.    -->

<!-- + Los tipos de procesos seleccionados son los 10 tipos con mayor cantidad de casos relevados en la muestras de causas con sentencia entre las fechas del relevamiento. Luego se filtró en base a los órganos especialmente estudiados en este informe.  -->
<!-- + Los casos incluidos en la tabla comprenden a todos los procesos resueltos por cada organismo en el período considerado, con exclusión de los siguientes procesos: Incidentales, Cautelares,  Procesos Constitucionales, Beneficios de Litigar Sin Gastos, Oficios y Exhortos, y tipos no acordes a la legislación vigente (eg.CONCURSO CERRADO, EXPEDIENTE INTERNO, ADMINISTRATIVO, PERSONAL).     -->
<!-- + Se practicaron reimputación de tipos de procesos por errores de registración de los organismos (e.g. el *ORDINARIO CIVIL* se reimputó como *ORDINARIO*). Estas reimputaciones están a disposición en la rutina de procesamiento estadístico.       -->
<!-- + Se excluyeron valores extremos en cada tipo de proceso mayores o menores a 3 desviaciones estándar. Estos valores extremos pueden estar asociados a errores de registración. No obstante ello atento el carácter crítico de estos casos y, eventualmente, la necesidad de un análisis detenido de los mismos agregamos la tabla de valores extremos más abajo.      -->
<!-- + La duración de los procesos resueltos por cada organismo a través del dictado de sentencia definitiva se calcula como la diferencia en días corridos entre la fecha de inicio de la causa y la fecha del dictado de la sentencia según declaración del organismo.     -->
<!-- + Para las líneas de tendencia empleamos la Regresión Local para vincular duraciones y el contexto interno y externo.     -->
<!-- + Finalmente, a fin de evitar datos sesgados por la presencia de valores extremos se optó por la mediana estadística para este análisis y se excluyeron procesos por organismo con menos de dos casos resueltos.     -->

<!-- ### Casos con valores extremos o anómalos -->

<!-- En esta tabla usted puede encontrar ordenado por organismo los casos con valores extremos o anómalos identificados en el estudio. Se entiende por *anomalía* en este contexto a todo valor de duración mayor o menor a 3 desviaciones estandar.      -->

<!-- ```{r} -->
<!-- analisi_durac$duracion_outliers %>%  -->
<!--   kable(caption = "Casos Extremos o Anómalos", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>%  -->
<!--   column_spec(5, "5cm") %>%  -->
<!--   landscape() -->

<!-- ``` -->

<!-- ### Base Completa de Casos Considerados -->

<!-- ```{r} -->
<!-- analisi_durac$duraciones_tidy_subselec %>%  -->
<!--   group_by(circunscripcion, organismo, tipo_proceso) %>%  -->
<!--   summarise(cantidad = n()) %>%  -->
<!--   tidyr::spread(tipo_proceso, cantidad, fill = 0) %>% -->
<!--   janitor::adorn_totals("row") %>%  -->
<!--   janitor::adorn_totals("col") %>%  -->
<!--   kable(caption = "Base de casos considerados por Proceso y Organismo", align = 'c', longtable = TRUE ) %>% -->
<!--   kable_styling(bootstrap_options = c("striped", "hover", "condensed"), -->
<!--                 full_width = F, font_size = 10)  %>%  -->
<!--   row_spec(0, angle = 90) %>%  -->
<!--   landscape() -->
<!-- ``` -->


