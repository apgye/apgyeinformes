---
title: Causas Pendientes de Resolución
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

## Listado de causas pendientes de resolución según última declaración de datos del organismo `r getDataIntervalStr(start_date, end_date)`

En este listado usted puede encontrar todas las causas pendientes de resolución informadas por el organismo en su última presentación y su estado de vencimiento respecto de los plazos para resolver (ver columna: "vencido"). Cabe aclarar que esta información representa el estado del despacho del organismo al último día hábil del mes, las resoluciones dictadas posteriormente recién son informadas al Área de Estadística cumplido el período mensual.

```{r}

pendientes(db_con = DB_PROD(),
           poblacion = cam_cad, operacion = "CADRCAD",  
           start_date = start_date, end_date = end_date) %>% 
  kable(caption = "Causas Pendientes", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  %>%
  #column_spec(7, "4cm") %>%
  row_spec(0, angle = 90) %>% 
  landscape()

```
