---
title: Causas Pendientes de Resolución
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

## Listado de causas pendientes de resolución `r getDataIntervalStr(start_date, end_date)`

En esta tabla usted puede encontrar el detalle de todas las causas pendientes de resolución (a despacho) informadas por el organismo en su última presentación y el porcentaje correspondiente a las que se encuentran vencidas a la fecha del primer vencimiento.    

```{r consulta_pendpaz23}
res <- apgyeProcesamiento::pendientes(db_con = DB_PROD(), poblacion = jdos_paz_23,
                                      operacion = "CADRA23", 
                                      start_date = start_date,
                                      end_date = end_date)
#!is.null(res)
if (nrow(res) > 0){
  
  res_pend <- res %>% 
    group_by(circunscripcion, organismo, año_mes) %>% 
    summarise(pendientes = n())
  
  res_pendV1 <- res %>% 
    filter(vencido_alvenc1 == "si") %>% 
    group_by(circunscripcion, organismo, año_mes) %>% 
    summarise(vencidos_al1 = n())
  
  res_pendV2 <- res %>% 
    filter(vencido_alvenc2 == "si") %>% 
    group_by(circunscripcion, organismo, año_mes) %>% 
    summarise(vencidos_al2 = n())
  
  res_pend <- res_pend %>%
    left_join(res_pendV1) %>% 
    left_join(res_pendV2) %>%
    mutate(vencidos_al1 = replace_na(vencidos_al1, 0),
           vencidos_al2 = replace_na(vencidos_al2, 0)) %>% 
    mutate('% vencidos al 1ero' = round((vencidos_al1/pendientes)*100,1)) %>% 
    rename('pendientes totales' = pendientes,
           'pendientes vencidos 1ero' = vencidos_al1,
           'pendientes vencidos 2do' = vencidos_al2)
  
  res_agrup <- res %>% 
    group_by(circunscripcion, organismo, año_mes, vencido_alvenc1) %>% 
    summarise('detalle causas' = str_c(nro, collapse = ", "))
  
  
  # res %>%
  #   kable(caption = "Causas Pendientes", align = 'c', longtable = TRUE ) %>%
  #   kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
  #                 full_width = F, font_size = 10)  %>%
  #   #column_spec(7, "4cm") %>%
  #   row_spec(0, angle = 90) %>% 
  #   landscape()
} else {
  msg_NoData %>% 
      kable(caption = "Causas Pendientes", align = 'c', longtable = TRUE )
    
}
```


```{r pendpaz23_resumen, eval=nrow(res) > 0}

res_pend %>%
  kable(caption = "Tabla de Pendientes Totales y Pendientes Vencidas", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) %>% 
  column_spec(3:4, "2cm")

```


```{r pendpaz23_agrupador, eval=nrow(res) > 0}

# res_agrup %>%
#     kable(caption = "Agrupador Causas Pendientes", align = 'c', longtable = TRUE ) %>%
#     kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
#                   full_width = F, font_size = 10) %>%
#     column_spec(4, "8cm")

```

```{r pendpaz23_detalle, eval=nrow(res) > 0}


  res %>%
    kable(caption = "Listado Completo de Causas Pendientes por Organismo", align = 'c', longtable = TRUE ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                  full_width = F, font_size = 8)  %>%
    #column_spec(7, "4cm") %>%
    row_spec(0, angle = 90) %>% 
    landscape()
    
```