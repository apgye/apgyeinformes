textos_y_comentarios <-  tribble(
  ~codigo, ~mk, ~fpub_inic, ~fpub_fin, ~referencia, ~ref_nro, ~texto,
  #----------------------------
  3, 
  "consideraciones_finales.Rmd", 
  "2020-06-01", 
  "2020-06-01", 
  "Informe Presidencia s-impacto y recuperacion indicadores por COVID19", 
  NA,
  " ",
  
  #----------------------------
  2, 
  "consideraciones_finales.Rmd", 
  "2020-03-11", 
  "2020-03-11", 
  "Informe Presidencia s-duraciones, camara familia y jdo multi ssalvador", 
  NA,
  "Materia de Familia en Segunda Instancia: Creación o transformación de órganos.
  El 67% de lo que se resolvió en el 2019 en segunda instancia correspondió a la materia 
  civil y el 30% a familia (para el residual no se dispone de datos). Esta distribución es relativamente
  homogénea en los distintos órganos de la provincia. La creación de cámaras de familia en las 
  circunscripciones con órganos de segunda instancia podría implicar la transferencia de ese volumen.
  La tranformación de órganos donde se dispone de más de una sala (Concordia y Paraná) podría implicar 
  -bajo condiciones constantes- una distribución aproximada civil-familia de 60/30 en Concordia y de 
  30/30/30 en Paraná. Estas aproximaciones deben tener en cuenta  que el crecimiento de la litigiosidad
  en materia de familia aumenta año a año a razón de un 4.5/5 %, y verosimilmente sucede lo mismo en materia
  civil. Además considerado al magistrado de segunda instancia individualmente durante el 2019 cada uno resolvió
  un promedio de 7.5 sentencias por mes (formula: (total sentencias/total vocales de 2 inst)/meses útiles del año) sin 
  contar autos, es decir: cerca de 2 por semana. Por otro lado dicho magistrado interviene en subrogaciones del STJER.",
  #----------------------------
  1, 
  "consideraciones_finales.Rmd", 
  "2020-03-11", 
  "2020-03-11", 
  NA, 
  NA,
  "De las actuaciones del organismo que surgen del detalle precedente se advierten presentaciones 
  extemporáneas de los listados de información correspondientes a los períodos de septiembre, octubre,
  noviembre y diciembre del 2018, y junio del 2019. Respecto de las **inconsistencias** detectadas en la
  información suministrada por el juzgado cabe hacer las siguientes observaciones. El suscripto solicitó 
  una auditoría de datos a los titulares del organismo en fecha 11/11/2019 en virtud de los valores anómalos
  detectados en los procesos judiciales iniciados en marzo y agosto del mismo año. Auditoría que deribó 
  en la confirmación de un error por parte del organismo y la rectificación de la información 
  suministrada: para marzo se informaron erróneametne (en fecha 23/04/2019) 429 causas iniciadas que 
  con la rectificación (en fecha 11/11/2019) descendieron a 19 y para agosto se informaron erróneamente
  (en fecha 24/09/2019) 563 que luego de la rectificación (en fecha 11/11/2019) descendieron a 25. El
  error en la producción de información (considerando las limitaciones del Lex-Doctor) y la consecuente
  rectificación de datos son eventos contamplados por nuestro sistema (que procura maximizar la calidad 
  de la información) por lo tanto no implican incumplimiento de pauta estadística alguna. Puede llegarse
  a una conclusión diferente en lo que respecta a la obligación de control de información que pesa sobre
  cada magistrado y funcionario conforme lo exige el Reglamento de Estadística, artículo 6. Ello así, toda
  vez que el Juzgado de Paz de Viale tuvo oportunidad de conocer los errores en sus declaraciones en los 
  sucesivos Informes para revisión enviados mensualmente y en los posteriores Boletines de Estadística 
  producidos entre abril y octubre 2019 en donde surgía fehacientemente la inconsistencia aludida.Por 
  último se destaca que requerida la corrección por el suscripto en fecha 11/11/2019 la misma fue resuelta
  el mismo día -según consta en las comunicaciones oficiales que se adjuntan-."
  ) %>% 
  mutate(texto = str_remove_all(texto, pattern = "\n")) %>% 
  mutate(texto = str_squish(texto))


