library(purrr)
library(readr)

# Directorio
setwd("~/apgyeinformes/2019/Modelo_Parametrizado/data/transicion")

# Nº1
## Lectura
files <- list.files(include.dirs = T, pattern = "TXT")
files <- files[str_detect(files, "I1")]
files_list <- map(files, read_delim, quote = "", col_types= cols(.default = "c"),
                  delim = "\t", col_names =F )
names(files_list) <- files

##Procesmiento
instruccion1 <- bind_rows(files_list, .id = "id") %>% 
  rename(nombre_listado = id, nro = X1, caratula = X2)

instruccion1_resumen <- instruccion1 %>% 
  mutate(nombre_listado = str_remove_all(nombre_listado, ".TXT")) %>% 
  mutate(nombre_listado = str_remove_all(nombre_listado, "I1_")) %>%
  mutate(nombre_listado = toupper(nombre_listado)) %>% 
  group_by(nombre_listado) %>% 
  summarise(cantidad_legajos = n())

# N2
##Lectura
files2 <- list.files(include.dirs = T, pattern = "TXT")
files2 <- files2[str_detect(files2, "T2")]
files_list2 <- map(files2, leerArchivoPresentado, isDBF = F)
files_list2[[5]] <- read.delim(files2[[5]])
files2 <- str_remove_all(files2, ".TXT")
names(files_list2) <- files2

## Procesemiento
instruccion2 <- bind_rows(files_list2, .id = "id") 
# Control no perdida de ningun caso sum(unlist(map(files_list2, nrow)))

instruccion2 <- instruccion2 %>% 
  mutate(finicio = dmy(finicio), fmovp = dmy(fmovp), fmovexp = dmy(fmovexp), 
         letra_salida = dmy(letra_salida), letra_regresa = dmy(letra_regresa)) %>% 
  mutate(año_letra_regresa = year(letra_regresa))

instruccion2_resumen <- instruccion2 %>% 
  mutate(año_inic = year(finicio)) %>% 
  mutate(año_inic = ifelse(año_inic < 2000 | is.na(año_inic), 
                                    "+2000_sf", año_inic )) %>% 
  group_by(id, grupo, año_inic) %>% 
  summarise(cantidad_legajos = n()) %>% 
  do(janitor::adorn_totals(.))

instruccion2_tramite_resumen <- instruccion2 %>% 
  filter(str_detect(grupo, "trámite|tramite")) %>% 
  group_by(id) %>% 
  summarise(cantidad_legajos = n()) %>% 
  rename(nombre_listado = id)

instruccion2_tramite_xletra <- instruccion2 %>% 
  filter(str_detect(grupo, "trámite|tramite")) %>% 
  # mutate(año_inic = year(finicio)) %>% 
  # mutate(año_inic = ifelse(año_inic < 2000 | is.na(año_inic), 
  #                          "+2000_sf", año_inic )) %>% 
  # mutate(regresado_letra = ifelse(!is.na(año_letra_regresa), "si", "no")) %>% 
  group_by(id, letra) %>% 
  summarise(cantidad_legajos = n()) %>% 
  arrange(id, desc(cantidad_legajos)) %>% 
  ungroup() %>% 
  group_by(id) %>% 
  do(janitor::adorn_totals(.))
  

instruccion2_tramite_sinfechamov <- instruccion2 %>% 
  filter(str_detect(grupo, "trámite|tramite")) %>%
  filter((is.na(fmovp) & is.na(fmovexp))) %>% 
  group_by(id, año_letra_regresa, letra) %>% 
  summarise(cantidad_legajos = n()) %>% 
  rename(nombre_listado = id, movimiento_letra_fecha = año_letra_regresa) %>% ungroup() %>% 
  mutate(movimiento_letra_fecha = ifelse(is.na(movimiento_letra_fecha),
                                         "sin_registro", movimiento_letra_fecha)) %>% 
  
  mutate(letra_abbr = str_sub(letra, 1,30)) %>% select(-letra)
  

instruccion2_tramite_confechamov <- instruccion2 %>% 
  filter(str_detect(grupo, "trámite|tramite")) %>%
  filter(!(is.na(fmovp) | is.na(fmovexp))) %>% 
  mutate(causa_activa = ifelse(fmovp > make_date(2017,01,01) | fmovexp > make_date(2017,01,01), "activa_ult2", "no_activa")) %>% 
  mutate(tproc = word(tproc,1,1)) %>% 
  mutate(caratula_abb = word(caratula,1,1)) %>% 
  mutate(nro_caratula_abb = str_c(nro, caratula_abb, sep= "-")) %>% 
  group_by(id, causa_activa, tproc) %>% 
  summarise(cantidad_legajos = n()) #, 
            #caratula_abb = str_c(nro_caratula_abb, collapse = ", " )) 




