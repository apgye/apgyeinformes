---
title: Audiencias Realizadas
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r, message=FALSE, warning=FALSE}
audienciascam <- audienciasPenales(db_con = DB_PROD(), poblacion = cam_pen, 
             start_date = start_date, 
             end_date = end_date, 
             desagregacion_mensual = desagregacion_mensual)
```

## Audiencias Realizadas segun sus Estados Finales - `r getDataIntervalStr(start_date, end_date)`

```{r}
if(is.null(audienciascam)){
  cat(msg_NoData)
} else {
  
  if(desagregacion_mensual){
    
    audienciascam %>% 
      select(circunscripcion, organismo, año_mes, aud_fijadas, aud_realizadas,
             aud_fracasadas, aud_canceladas, aud_reprogramadas) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.)) %>% 
      outputTable(caption = "Audiencias Realizadas segun sus Estados Finales") %>%
      row_spec(0, angle = 90) 
      # landscape()
    
  } else {
    
     audienciascam %>% 
      select(circunscripcion, organismo, aud_fijadas, aud_realizadas,
             aud_fracasadas, aud_canceladas, aud_reprogramadas) %>% 
      group_by(circunscripcion, organismo) %>% 
      do(janitor::adorn_totals(.)) %>% 
      outputTable(caption = "Audiencias Realizadas segun sus Estados Finales") %>%
      row_spec(0, angle = 90)
      # landscape()
    
  }
}

```

## Audiencias Realizadas y sus Resultados - `r getDataIntervalStr(start_date, end_date)`

```{r}
if(is.null(audienciascam)){
  cat(msg_NoData)
} else {
  if(desagregacion_mensual){
  
  audienciascam %>% 
    select(circunscripcion, organismo, año_mes, rechaza_recurso = aud_realiz_rechaza_rec,
           admite_recucurso = aud_realiz_admite_rec,
           pase_adespacho_para_resolver = aud_realiz_adespacho, otros = aud_realiz_otra) %>% 
    group_by(circunscripcion, organismo) %>% do(janitor::adorn_totals(.)) %>%
    janitor::adorn_totals("col") %>% 
    outputTable(caption = "Audiencias Realizadas y Resultados") %>%
    row_spec(0, angle = 90) 
    # landscape()
  
} else {
  
   audienciascam %>% 
    select(circunscripcion, organismo, rechaza_recurso = aud_realiz_rechaza_rec,
           admite_recucurso = aud_realiz_admite_rec,
           pase_adespacho_para_resolver = aud_realiz_adespacho, otros = aud_realiz_otra) %>% 
    group_by(circunscripcion, organismo) %>% do(janitor::adorn_totals(.)) %>%
    janitor::adorn_totals("col") %>% 
    outputTable(caption = "Audiencias Realizadas y Resultados") %>%
    row_spec(0, angle = 90) 
    # landscape()
  
}
}
```

\pagebreak

## Audiencias realizadas, Duración de la Causa y Duraciones por Audiencia - `r getDataIntervalStr(start_date, end_date)`

```{r}
if(is.null(audienciascam)){
  cat(msg_NoData)
} else {
  if(desagregacion_mensual){
  
    audienciascam %>%
      mutate(organismo = str_c(organismo, "-", str_sub(circunscripcion, 1,5))) %>% 
      mutate(durac_fing_frealizada = round(durac_fing_frealizada, digits = 1), 
             prom_duracm = round(prom_duracm, digits = 1)) %>% 
      select(circunscripcion, organismo, año_mes, aud_realizadas,
             promedio_duracion_causa_de_ingreso_a_audiencia_en_dias_corridos = durac_fing_frealizada,
             promedio_duracion_por_audiencia_en_minutos = prom_duracm, 
             mediana_duracion_por_audiencia_en_minutos = mediana_duracm) %>%
      arrange(circunscripcion, organismo, año_mes) %>%
      outputTable(caption = "Audiencias Realizadas, Duración de la Causa y Duraciones por Audiencia") %>%
      column_spec(c(5,6,7), "2cm") %>%
      row_spec(0, angle = 90)
  
  } else {
  
    audienciascam %>%
      mutate(organismo = str_c(organismo, "-", str_sub(circunscripcion, 1,5))) %>% 
      mutate(durac_fing_frealizada = round(durac_fing_frealizada, digits = 1), 
             prom_duracm = round(prom_duracm, digits = 1)) %>% 
      select(circunscripcion, organismo, aud_realizadas,
             promedio_duracion_causa_de_ingreso_a_audiencia_en_dias_corridos = durac_fing_frealizada,
             promedio_duracion_por_audiencia_en_minutos = prom_duracm, 
             mediana_duracion_por_audiencia_en_minutos = mediana_duracm) %>%
      arrange(circunscripcion, organismo) %>%
      outputTable(caption = "Audiencias Realizadas, Duración de la Causa y Duraciones por Audiencia") %>%
      column_spec(c(5,6,7), "2cm") %>%
      row_spec(0, angle = 90)
  }
}
```



