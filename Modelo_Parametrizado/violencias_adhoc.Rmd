---
title: Informe sobre Casuas en materia de Violencia en el Fuero de Familia
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output:
  pdf_document:
    includes:
      in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r}
library(readr)
library(lubridate)
library(gghighlight)

```

```{r}

resultado <- iniciados_fam(db_con = DB_PROD(), poblacion = jdos_fam,
                           start_date = "2019-02-01", end_date = "2020-04-01", 
                           estadistico = "conteo") 
  
df <- resultado$inic %>% 
  filter(tipo_proceso != "subtotal") %>% 
  mutate(tproceso =  ifelse(str_detect(grupo_proceso, "VIOLENCIA"), "violencias", "otros_procesos" )) %>% 
  mutate(año = year(fecha)) 

df_violencias_xcirc <- df %>% 
  group_by(circunscripcion, año, tproceso) %>% 
  summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
  ungroup() %>% 
  mutate(circunscripcion = ifelse(circunscripcion == "Chajarí", "Federación (Chajarí)", circunscripcion))

df_violencias_xaño <- df %>% 
  group_by(año, tproceso) %>% 
  summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
  bind_cols(df %>% 
              group_by(año, tproceso) %>% 
              summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
              filter(año == 2019) %>% 
              janitor::adorn_percentages("col") %>% 
              mutate(cantidad = round(cantidad * 100, digits = 1)) %>% 
              rename(pcto = cantidad) %>% 
              bind_rows(df %>% 
                          group_by(año, tproceso) %>% 
                          summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
                          filter(año == 2020) %>% 
                          janitor::adorn_percentages("col") %>% 
                          mutate(cantidad = round(cantidad * 100, digits = 1)) %>% 
                          rename(pcto = cantidad)) %>% 
              ungroup() %>% 
              select(pcto))


poblacion_xdpto <- readxl::read_excel("~/apgyeinformes/Modelo_Parametrizado/data/poblacion.xls", skip = 3) %>% 
  select(1,11:17) %>% #once es el año 2019, 
  slice(1:21) %>% 
  filter(!is.na(Departamento), Departamento != "Total") %>% 
  select(Departamento, "2019") %>%  # selecciono el año de población según la fecha del sistema 
  rename(circunscripcion = Departamento, poblacion_2019 = '2019') 


df_violencias_xhab <- df_violencias_xcirc %>% ungroup() %>% 
  filter(tproceso == "violencias", año == 2019) %>% 
  mutate(circunscripcion = ifelse(circunscripcion == "Chajarí", "Federación (Chajarí)", circunscripcion)) %>%  # renombre circ x dpto
  left_join(poblacion_xdpto) %>% 
  mutate(pob_x100m = poblacion_2019 / 100000) %>% 
  mutate(civiolencias_x100mh = round(cantidad / pob_x100m)) %>% 
  bind_rows(tribble(
      ~circunscripcion, ~cantidad , ~civiolencias_x100mh,  
      "Mediana_Provincial", median(.[[4]], na.rm = T),  median(.[[7]], na.rm = T))) 

```


# Procesos de Violencia Familiar y/o contra la Mujer

## Causas Iniciadas año 2019 y Febrero-Marzo 2020

En el siguiente gráfico se pueden ver cantidades y porcentajes de causas iniciadas en materia de Violencia Familiar y/o contra la Mujer (años 2019 y bimestre Febrero-Marzo 2020) en camparación a las causas iniciadas en los demás tipos de procesos que tramitan ante los Juzgados de Familia.    

Nótese que la unidad de conteo es la causa iniciada ante un organismo jurisdiccional (en el caso particular se toma en consideración la fecha de inicio y el tipo de proceso registrado en el sistema de gestión jurisdiccional) y la población bajo estudio está dada por los Juzgados de Familia del Superior Tribunal de Justicia. 


```{r, violencias_xa}
df_violencias_xaño %>% 
  ggplot(aes(x = as.factor(año), y = cantidad, fill = tproceso)) + 
  geom_bar(stat="identity", position = position_dodge(width = 0.5)) +
  geom_text(aes(label = str_c(cantidad, " (", pcto, "%)")), position = position_dodge(0.9), size = 6, vjust = -0.5) +
  scale_fill_manual(values = c("otros_procesos" = "darkgray", "violencias" = "darkred")) +
  theme_minimal() +
  theme(axis.text.x = element_text(color = "black", size = 14), legend.position = "top", legend.title = element_blank(),
        panel.grid.major = element_blank(), panel.grid.minor = element_blank(), axis.text.y = element_blank(), 
        axis.title.x=element_blank(), axis.title.y=element_blank(), legend.text = element_text(size=12),
        strip.text = element_text(size=12), plot.title = element_text(size=14), plot.subtitle = element_text(size=12)) +
  labs(title = "Causas iniciadas en los Juzgados de Familia en materia de Violencia Familiar y/o contra la Mujer",
       subtitle = "Totales y porcentajes en relación a los demás procesos iniciados en el año 2019 y feb-marzo 2020",
       y = "Cantidad", x = "",
       caption = "APGE-STJER") 
```

## Causas Iniciadas año 2019 y Febrero-Marzo 2020 por Circunscripción

```{r, violencias_xcirc}
df_violencias_xcirc %>% 
  ggplot(aes(x = as.factor(año), y = cantidad, fill = tproceso)) + 
  geom_bar(stat="identity", position = position_dodge(width = 0.5)) +
  geom_text(aes(label = cantidad), position = position_dodge(0.9), size = 5, vjust = -0.1) +
  scale_fill_manual(values = c("otros_procesos" = "darkgray", "violencias" = "darkred")) +
  theme_minimal() +
  theme(axis.text.x = element_text(color = "black", size = 12), legend.position = "top", legend.title = element_blank(),
        panel.grid.major = element_blank(), panel.grid.minor = element_blank(), axis.text.y = element_blank(), 
        axis.title.x=element_blank(), axis.title.y=element_blank(), legend.text = element_text(size=12),
        strip.text = element_text(size=12), plot.title = element_text(size=14), plot.subtitle = element_text(size=12)) +
  labs(title = "Causas iniciadas en los Juzgados de Familia en materia de Violencia Familiar y/o contra la Mujer",
       y = "Cantidad", x = "",
       caption = "APGE-STJER") +
  facet_wrap(~circunscripcion) 
```


## Violencias Iniciadas en el Fuero de Familia año 2019 por Circunscripción

<!-- En este gráfico se pueden ver las Violencias iniciadas cada 100 mil habitantes. Esta es una medición que busca contextualizar la cantidad de causas que se inician en una jurisdicción en relación a su población con el fin de hacer comparaciones entre la litigiosidad de las jurisdicciones. En el caso particular la medición permite apreciar la importancia que tiene la problemática bajo estudio en las jurisdicciones de menor población.      -->

<!-- Sin perjuicio de ello, cabe advertir que, por un lado, la cantidad demográfica empleada en es una proyección efectuada sobre el censo 2010 (Fuente: Instituto Nacional de Estadística y Censos "Estimaciones de población por sexo, departamento y año calendario 2010-2025" Nº 38 Serie Análisis Demográfico - 1a ed., accesible en  https://www.entrerios.gov.ar/dgec/proyecciones-y-estimaciones/), y, por el otro, que los casos de violencia estudiados son aquellos radicados ante Juzgados de Familia (no se consideran los casos iniciados ante el Fuero de Paz ni el Fuero Penal).  -->

En este gráfico se ve la cantidad de Causas de Violencia iniciadas en el año 2019 en el Fuero de Familia ordenadas en orden decreciente. Esta representación, basada en las cantidades absolutas podría contextualizarse considerando la distribución demográfica de cada región, pero dado que el dato poblacional oficial proyectado para el año 2019 tiene como base de relevamiento el año 2010 (ver Instituto Nacional de Estadística y Censos "Estimaciones de población por sexo, circunscripción y año calendario 2010-2025" Nº 38 Serie Análisis Demográfico - 1a ed., accesible en  https://www.entrerios.gov.ar/dgec/proyecciones-y-estimaciones/), entendemos que la misma aporta poco valor para comprender la distribución bajo estudio, pudiendo generar conclusiones sesgadas.     

```{r, violencias_orden}
df_violencias_xhab %>% 
  ggplot(aes(x = reorder(circunscripcion, -cantidad), y = cantidad)) + 
  geom_bar(stat="identity", position = position_dodge(width = 0.5)) +
  geom_text(aes(label = cantidad), position = position_dodge(0.9), size = 5, vjust = -0.1) +
  theme_minimal() +
  theme(axis.text.x = element_text(color = "black", angle = 90, size = 12), legend.title = element_blank(),
        panel.grid.major = element_blank(), panel.grid.minor = element_blank(), axis.text.y = element_blank(), 
        axis.title.x=element_blank(), axis.title.y=element_blank(), legend.text = element_text(size=12),
        strip.text = element_text(size=12), plot.title = element_text(size=14), plot.subtitle = element_text(size=12)) +
  labs(title = "Violencias Familiar y/o contra la Mujer por Circunscripción año 2019",
       y = "Cantidad", x = "",
       caption = "APGE-STJER") +
  gghighlight(circunscripcion != "Mediana_Provincial", unhighlighted_params = list(fill = "darkred"))
```


