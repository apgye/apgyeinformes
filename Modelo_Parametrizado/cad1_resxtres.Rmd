---
title: Resolucioens por Tipo 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}

res_list <- resoluciones_cad1_xtres(db_con = DB_PROD(), 
                                    poblacion = cam_cad,
                                    operacion = "CADRCAD",
                                    start_date = start_date,
                                    end_date = end_date)


```

## Resoluciones por Tipo `r getDataIntervalStr(start_date, end_date)`

```{r, cad1_resxtres_t, eval= (s & t)}
res_list$res_xtipo %>%
  filter(as == "S") %>% 
  #filter(as == "A") %>% 
  select(-4, -8, -9, -10, -11, -12, -14, -15) %>%
  group_by(circunscripcion, organismo) %>% 
  do(janitor::adorn_totals(.)) %>% 
  outputTable(caption = "Sentencias por Tipo") %>%
  row_spec(0, angle = 90) %>% 
  landscape()
```

\pagebreak

`r if (s & g) '## Tipos de Sentencias Dictadas'`

```{r, cad1_resxtres_t_s, eval= (s & g)}
res_list$res_xtipo_S
```

\pagebreak

`r if (s & t) '## Resoluciones por Objeto de Demanda '`

```{r, cad1_resxtres_t_od, eval= (s & t)}
res_list$res_xobjetodem %>%
  select(-circunscripcion) %>%
  mutate(organismo = str_replace_all(organismo, "Cámara Contencioso Administrativo", "Cam.Cont-Adm")) %>% 
  outputTable(caption = "Resoluciones por Objeto de Demanda") %>%
  row_spec(0, angle = 90)
```

\pagebreak

`r if (s & g) '## Objetos de Demanda'`

```{r, cad1_resxtres_graf, eval= (s & g)}
res_list$res_xobjetodem_g
```

\pagebreak

`r if (s & t) '## Resoluciones por Demandado y Resultado '`

```{r, cad1_resxtres_t_dr, eval= (s & t)}
res_list$tdemo_xtres %>%
  select(2:6) %>% # dejo solo resultados de rechazo y hace lugar asociados a sentencias, hay que corregir tb
  outputTable(caption = "Demandado y Resultado") %>%
  #column_spec(1, "3cm") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()
```

`r if (s & t) '## Resoluciones por Demandado y Resultado '`

```{r, cad1_resxtres_t_dr_graf, eval= (s & t), fig.width=13, fig.height=10}
tablexgraph <- res_list$tdemo_xtres %>%
  select(2:6) %>% select(-demando) %>% 
  filter(organismo != "Total") %>% 
  group_by(organismo) %>% 
  summarise_all(sum) %>% 
  tidyr::pivot_longer(-organismo, names_to = "resultado", values_to = "cantidad")
  
blank_theme <- theme_minimal()+
  theme(
    axis.title.x = element_blank(),
    axis.title.y = element_blank(),
    panel.border = element_blank(),
    panel.grid=element_blank(),
    axis.ticks = element_blank(),
    plot.title=element_text(size=14, face="bold")
  )

# tablexgraph %>% 
#   filter(str_detect(organismo, "Paraná")) %>% 
#   select(-organismo) %>% 
#   ggplot(aes(x="", y=cantidad, fill= resultado))+
#   geom_bar(width = 1, stat = "identity") +
#   geom_text(aes(label = paste(round(cantidad / sum(cantidad) * 100, 1), "%")),
#             size=5, position = position_stack(vjust = 0.5)) +
#   scale_fill_grey() +  blank_theme +
#   labs(title = "% de resoluciones según tipo de resultado",  subtitle = "Cámara de Paraná", 
#        y = "", x = "",
#        caption = "APGE-STJER") +
#   theme(axis.text.x=element_blank(), 
#         legend.title = element_blank()) +
#   coord_polar("y", start = 0) +
#   facet_grid(organismo ~ .)
# 
# 
# tablexgraph %>% 
#   filter(str_detect(organismo, "Uruguay")) %>% 
#   select(-organismo) %>% 
#   ggplot(aes(x="", y=cantidad, fill= resultado))+
#   geom_bar(width = 1, stat = "identity") +
#   geom_text(aes(label = paste(round(cantidad / sum(cantidad) * 100, 1), "%")), 
#             size=5, position = position_stack(vjust = 0.5)) +
#   scale_fill_grey() +  blank_theme +
#   labs(title = "% de resoluciones según tipo de resultado",  subtitle = "Cámara de Uruguay", y = "", x = "",
#        caption = "APGE-STJER") +
#   theme(axis.text.x=element_blank(), 
#         legend.title = element_blank()) +
#   coord_polar("y", start = 0)


tablexgraph %>% 
  ggplot(aes(x="", y=cantidad, fill= resultado))+
  geom_bar(width = 1, stat = "identity") +
  geom_text(aes(label = cantidad), 
            size=10, position = position_stack(vjust = 0.5)) +
  labs(title = "Cantidad de resoluciones por organo y resultado",  subtitle = "", y = "", x = "",
        caption = "APGE-STJER") +
   theme(axis.text.x=element_blank(), 
         legend.title = element_blank()) +
  facet_wrap(organismo ~ .)

```


\pagebreak

`r if (s & t) '## Resoluciones en Recursos '`

```{r, cad1_resxtres_t_rec, eval= (s & t)}
res_list$recurso %>%
  select(-circunscripcion, -Sin_dato, -Total) %>% 
  outputTable(caption = "Resoluciones en Recursos") %>%
  row_spec(0, angle = 90)
```

\pagebreak

<!-- `r if (s & t) '## Sentencias Dictadas y Recursos'` -->

<!-- ```{r, cad1_resxtres_t_srec, eval= (s & t)} -->
<!-- res_list$sentencias_recurso %>% -->
<!--   outputTable(caption = "Sentencias y Recursos") %>% -->
<!--   row_spec(0, angle = 90) -->
<!-- ``` -->




