---
title: Audiencias Realizadas
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
existepres <- existepresentacion(db_con = DB_PROD(), poblacion = jdo_pep,
                                 start_date, end_date, "IGEP")
auditar = F
agrupar_xcircunscripcion <- T # Taiga 2058
```


## Audiencias Realizadas y sus Duraciones - `r getDataIntervalStr(start_date, end_date)`

```{r, eval = existepres}
aud <- apgyeProcesamiento::audienciasPenales(db_con = DB_PROD(), poblacion = jdo_pep, 
             start_date = start_date, 
             end_date = end_date, 
             desagregacion_mensual = desagregacion_mensual)

ausencia_magistrados <- !is.null(aud$realizadas_sinMagistrado) || nrow(aud$realizadas_sinMagistrado) > 0
idagente_erroneos <- !is.null(aud$idagente_erroneo)

if (agrupar_xcircunscripcion){

  aud_xc <- aud$resultado %>%
    ungroup() %>% 
    filter(circunscripcion != "Total") %>% 
    select(-magistrado, -año_mes) 
  
  aud_sum <- aud_xc %>% 
    group_by(circunscripcion) %>% 
    summarise(across(everything(), ~ sum(., na.rm = TRUE)))
  
  aud_sum %>%
    select(circunscripcion, Total, everything()) %>% 
    outputTable(caption = "Audiencias Realizadas por Tipo por Circunscripción") %>% 
    row_spec(0, angle = 90) %>%
    landscape()
    
} else {
  
  aud$resultado %>%
    outputTable(caption = "Audiencias Realizadas por Tipo") %>% 
    row_spec(0, angle = 90) %>%
    landscape()
  
}

```

```{r, eval= !existepres}
text_tbl <- data.frame(Estado_Presentación = "Pendiente", Observación = "No registra información presentada en los sistemas de estadística.")
kable(text_tbl, "latex", booktabs = T) %>%
  kable_styling(full_width = F) %>%
  column_spec(1, bold = T, color = "red") %>%
  column_spec(2, width = "30em")
    
```

```{r sinMagist, eval = auditar & ausencia_magistrados}

aud$realizadas_sinMagistrado %>%
  group_by(circunscripcion, organismo_descripcion, año_mes) %>% 
  summarise(cant = n()) %>% 
  arrange(circunscripcion, organismo_descripcion, año_mes) %>% 
  outputTable(caption = "Cantidades de expedientes donde las audiencias no registran Magistrado")

aud$realizadas_sinMagistrado %>%
  select(-tproc) %>% 
  arrange(circunscripcion, organismo_descripcion, año_mes, nro) %>% 
  outputTable(caption = "Detalle de expedientes donde las audiencias no registran Magistrado")

```

```{r agent_erroneos, eval = auditar & idagente_erroneos}

aud$idagente_erroneo %>%
  group_by(circunscripcion, organismo_descripcion, año_mes) %>% 
  summarise(cant = n()) %>% 
  arrange(circunscripcion, organismo_descripcion, año_mes) %>% 
  outputTable(caption = "Cantidades de audiencias con idagente erróneo")

aud$idagente_erroneo %>% ungroup() %>% 
  select(circunscripcion, organismo_descripcion, año_mes, nro, idagente, magistrado) %>%
  outputTable(caption = "Detalle de audiencias con idagente erróneo")

```

