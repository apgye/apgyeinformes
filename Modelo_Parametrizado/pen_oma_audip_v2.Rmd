---
title: Audiencias
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = 'sin_dato')
```

```{r}

pob_mediacion <- oma %>% filter(tipo == "oma")

# sistema Dr.Juarez----

if(ymd(start_date) <= ymd("2021-09-01")) {
  
  audip_oma <- apgyeProcesamiento::audienciasPenales(db_con = DB_PROD(),
                                                   poblacion = oma, 
                                                   start_date = start_date, 
                                                   end_date = "2022-01-01", 
                                                   desagregacion_mensual = desagregacion_mensual) 
  
  audic_oma_prim_2021 = audip_oma$postNov20$audic_oma_prim %>% 
    mutate(conjuntas_conAcuerdo = ((f2 >= data_interval_start & 
                                     f2 < data_interval_end) &
                                     str_detect(tr, "realizad") &
                                     t1 == "1" &
                                     t2 == "1")) %>% 
    mutate(año_mes = format(as.Date(data_interval_start), "%Y-%m")) %>% 
    filter((f2 >= data_interval_start & 
             f2 < data_interval_end) &
             str_detect(tr, "realizad") &
             t1 == "1") %>%
    rename(fraud = f2, estaud = t1,N20_ra = t2, N20_vmed = t3, N20_tmed = t4,
           duracm = n1, duracb = n2) %>%
    apgyeProcesamiento::codconver("IGMP", "N20_ra") %>%
    apgyeProcesamiento::codconver("IGMP", "N20_vmed") %>%
    apgyeProcesamiento::codconver("IGMP", "N20_tmed") %>%
    mutate(resp = ifelse(is.na(resp), "sin dato", resp)) %>%
    rename(ra = N20_ra, vmed = N20_vmed, tmed = N20_tmed) %>% 
    filter(ra == "Con Acuerdo" ) %>% 
    rename(resultado_audiencia = ra) %>% 
    tidyr::separate(partp, into = c("fiscalia", "n", "l", "o"), sep="\\$") %>%
    select(-c(n, l, o)) %>% 
    mutate(organismo = "OMA") %>% 
    select(organismo, circunscripcion, mediador = resp, año_mes, legajo_nro = nro_om,
           caratula, tipo_proceso = tproc, fiscal = fiscm,  fiscalia, fecha_inicio_OMA = finicio, fecha_acuerdo = fraud) %>% 
    mutate(mediador = ifelse(mediador == "MUNOZ MARIELA", "MARIELA MUNOZ", mediador)) %>% 
    mutate(mediador = ifelse(mediador == "AHIBE ANALÍA", "ANALIA AHIBE", mediador)) %>%
    mutate(mediador = ifelse(mediador == "AHIBE ANALIA", "ANALIA AHIBE", mediador)) %>% 
    mutate(mediador = stringr::word(mediador, -1))
  
  
 
}

# sistema de transición ggdrive Dr.Amateis-----

if(ymd(start_date) <= ymd("2021-10-01")) {
  
  oma_form =  "https://docs.google.com/spreadsheets/d/1hqIs5nAbvKEbAFGMcNTOmgKef6wLvjfDb7U7vZm-mnk/edit?usp=sharing"
  text <- gsheet::gsheet2text(url = oma_form, format = 'tsv')
  
  resultado_ggdrive <- readr::read_tsv(file = text, col_names = T) %>% 
    select(3,4,5,10) %>% 
    rename(dni = 2, 
             año_mes = 3, 
             cantidad = 4) %>% 
    mutate(año_mes = format(dmy(año_mes), "%Y-%m")) %>% 
    left_join(apgyeProcesamiento::personal_mediacionPenal(DB_PROD()) %>% 
                  mutate(dni = as.integer(dni)) %>% 
                  select(resp = idagente, dni, apellido, jurisdiccion, organismo) %>% 
                  distinct(), by="dni") %>% 
    mutate(organismo = "OMA", cantidad = as.integer(cantidad)) %>% 
    mutate(jurisdiccion = ifelse(jurisdiccion == "Nogoya", "Nogoyá", jurisdiccion)) %>% 
    select(organismo, circunscripcion = jurisdiccion, mediador = apellido, año_mes, cantidad) %>% 
    distinct(organismo, circunscripcion, mediador, año_mes, .keep_all = T)  
 
}



# sistema vigente Dr. Amateis ------

resultado <- iniciados_oma(db_con = DB_PROD(), poblacion = pob_mediacion,
                           start_date = start_date, end_date = end_date) 

primaria  <- resultado$primaria_preprocesada_cfiscal %>%
  select(circunscripcion, apellido, nro_om, caratula, tproc, fiscal = fiscm , 
         partp, fiscm, fecha, mov_grupo, descripcion) %>% 
  filter(str_detect(descripcion, "^00|^33|^83") & !str_detect(descripcion, "FISCAL")) %>% 
  tidyr::separate(partp, into = c("fiscalia", "n", "l", "o"), sep="\\$") %>%
  select(-c(n, l, o)) %>% 
  arrange(circunscripcion, apellido, caratula, desc(fecha))

# df movimientos que no son ni iniciados ni acuerdos
inic_otros_mov <- primaria %>%
  filter(!str_detect(descripcion, "^00|^33|^83")) %>%
  group_by(circunscripcion, apellido, nro_om) %>%
  filter(fecha == max(fecha)) %>%
  select(circunscripcion, mediador = apellido, legajo_nro = nro_om, fecha_otros_mov = fecha, descripcion_otros_mov = descripcion) %>%
  filter(!is.na(legajo_nro))

# df iniciados
iniciados <- primaria %>% 
  filter(str_detect(descripcion, "^00")) %>% 
  distinct(circunscripcion, apellido, nro_om, fecha, .keep_all = T) %>% 
  group_by(circunscripcion, apellido, nro_om) %>% 
  filter(fecha == max(fecha)) %>% # Gualeguaychú GARAY A740/22
  mutate(fecha_inicio_OMA = fecha) %>% 
  select(circunscripcion, mediador = apellido, legajo_nro = nro_om, fecha_inicio_OMA)

# df acuerdos
acuerdos <- primaria %>% 
  filter(str_detect(descripcion, "^33|^83")) %>% 
  distinct(circunscripcion, apellido, nro_om, fecha, .keep_all = T) %>% 
  group_by(circunscripcion, apellido, nro_om) %>% 
  filter(fecha == max(fecha)) %>% # Gualeguaychú GARAY A740/22
  mutate(fecha_acuerdo = fecha) %>% 
  select(-c(fecha, mov_grupo, descripcion)) %>% 
  mutate(organismo = "OMA", 
         año_mes = format(as.Date(fecha_acuerdo), "%Y-%m")) %>% 
  select(organismo, circunscripcion, apellido, año_mes, everything()) %>% 
  rename(mediador = apellido, legajo_nro = nro_om, tipo_proceso = tproc) %>% 
  mutate(mediador = ifelse(is.na(mediador), "sin_dato", mediador))

# integro acuerdos con dato de iniciadaso-----
acuerdos_inic <- acuerdos %>% 
  left_join(iniciados,  by = c("circunscripcion", "mediador", "legajo_nro"))


# consolido primarios y tabla resumen
acuerdos_inic_crosserie = acuerdos %>% 
  bind_rows(if(exists("audic_oma_prim_2021")) audic_oma_prim_2021) %>% 
  arrange(organismo, circunscripcion, mediador, fecha_acuerdo) 
 

acuerdos_prim <- acuerdos_inic_crosserie %>% 
  #dplyr::relocate(fecha_inicio_OMA, .before = fecha_acuerdo) %>% 
  mutate(fiscalia = str_remove_all(fiscalia, "fis:")) %>% 
  mutate(fiscalia = ifelse(fiscalia == '', NA, fiscalia))

if(anexos) {
  write.table(acuerdos_prim, "~/apgyeinformes/Modelo_Parametrizado/data/acuerdos_OMA.csv",
            col.names = T, row.names = F, sep = ",")
}

acuerdo_resumen <- acuerdos_inic_crosserie %>% 
  group_by(organismo, circunscripcion, mediador, año_mes) %>% 
  summarise(cantidad = n(), 
            casos = str_c(legajo_nro, collapse = "; ")) %>% ungroup() 

if(ymd(start_date) <= ymd("2021-10-01")) {
  
  acuerdo_resumen <- acuerdo_resumen %>% 
    bind_rows(if(exists("resultado_ggdrive")) resultado_ggdrive) %>% 
    arrange(circunscripcion, mediador, año_mes)
}


# genero gráficos y tabla resumen----
graf = acuerdos %>% 
  group_by(circunscripcion) %>% summarise(cantidad = n()) %>% 
  janitor::adorn_percentages("col") %>% 
  mutate(pje = paste0(round(cantidad*100, digits = 2), "%")) %>% 
  mutate(circunscripcion = paste0(circunscripcion, " ", pje)) %>% 
  ggpubr::ggdonutchart(x = "cantidad", 
                     label = "pje", 
                     fill = "circunscripcion",
                     lab.pos = "in",
                     # main = "Legajos iniciados en OMA por Circunscripción",
                     lab.font = c(5, "black")) +
  theme(legend.text = element_text(size = 15),
        legend.title = element_blank()) 

 if(desagregacion_mensual) {
   
    acuerdo_resumen <- acuerdo_resumen %>%
      group_by(organismo, circunscripcion) %>% 
      do(janitor::adorn_totals(.))  
    
   } else {
    acuerdo_resumen <- acuerdo_resumen %>% 
      group_by(organismo, circunscripcion, mediador) %>% 
      summarise(cantidad = sum(cantidad, na.rm = T)) %>% ungroup() %>% 
      mutate(año_mes = "acumulado") %>%
      select(circunscripcion, organismo, mediador, año_mes, everything()) %>% 
      group_by(organismo, circunscripcion) %>%
      do(janitor::adorn_totals(.))
    
   }


# total_con_casos y tabla primaria x legajo : circ, mediador, grupo_1 = legajos con 1 mov y grupo_2 legajos con > 1mov, 
# legajo y fecha del movimiento con su descripción

```


## Audiencias Realizadas con Acuerdo - `r getDataIntervalStr(start_date, end_date)`

```{r, pen_oma_ac_g,  eval = (ci & g), fig.width=19, fig.height=17}
graf
```

\pagebreak


```{r, eval = (ad & t)}

if(tc){
  acuerdo_resumen %>%
    kable(caption = str_c("Audiencias con Acuerdo Mediación"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                   full_width = F, font_size = 8) %>% 
    column_spec(6, "15cm") %>% 
    landscape()
}else{
  acuerdo_resumen %>% select(-casos) %>% 
    kable(caption = str_c("Audiencias con Acuerdo Mediación"," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
    kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                   full_width = F, font_size = 10)
}

```

