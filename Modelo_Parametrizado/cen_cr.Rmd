---
title: Audiencias y sus Estados
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
# Por Taiga 2090
#filtro por Jurisd y organismos
circs <- ""
orgs <- ""

resol_cen <- apgyeProcesamiento::resoluciones_cen(db_con = DB_PROD(), poblacion = cen,
                              start_date = start_date, end_date = end_date,
                              desagregacion_mensual = desagregacion_mensual, 
                              circs = circs, orgs = orgs)

# Por Taiga 2090
mostrar_porcentaje_acuerdo <- F
# , sin disponer de éstos datos en los períodos previos al año 2018
```

## Audiencias y sus estados  `r getDataIntervalStr(start_date, end_date)`
<!-- Por Taiga 2090 -->
<!-- En la siguiente tabla encontrará los datos obtenidos para los organismos de Familia de la circunscripción Paraná para el período declarado.  -->

<!-- Asimismo cabe aclarar que éstos indicadores son aquellos resultantes de las Mediaciones pre-judiciales. -->

\blandscape

```{r, cen_cr_st_mes, eval = (cr & ("mes" %in% stag)),  fig.width=19, fig.height=16}
resol_cen %>% 
  ts_med_res_g(agrupar =  "mes")
```

```{r, cen_cr_st_circ, eval = (cr & (("circ" %in% stag) | ("org" %in% stag))), fig.width=19, fig.height=16}
resol_cen %>% 
  ts_med_res_g(agrupar =  "circ")
```

\elandscape

```{r, cen_cr_t, eval= (cr & t)}
  if (is.null(resol_cen)){
    cat(msg_NoData)
  } else {
    
    # por taiga 2090:
    if (mostrar_porcentaje_acuerdo){
      resol_cen <- resol_cen %>% 
        filter(circunscripcion != "Total") %>% 
        mutate('% de acuerdo' = round(con_acuerdo*100/Total, 2))
    }

    resol_cen %>%
      outputTable(caption = "Resoluciones") %>%
      row_spec(0, angle = 90)
  }

```
