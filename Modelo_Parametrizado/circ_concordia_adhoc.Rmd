---
title: Resumen Concordia
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=2cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')

source("../R/informe.R")
source("../R/habiles.R")
source("../R/all_results.R")
source("../R/poblacion.R")

# Parametros---------------------------------------------------------------
circunscripcion = "Concordia"
poblacion_proyect_año = "2023"
start_date = "2023-02-01"
end_date = "2023-05-01"
```

```{r, toolsparams}
# Tools -----------------------------------------------------------------------
serietemp <- function(df, agrupador = agrupador, vartiempo, intervalo = intervalo, geom = c("puntos", "lineas", "barras_stack", "barras_fill", "area"), etiquetas = F, sma = F, titulo = NULL) {
  
  result <- list()

  df <- df %>% 
    ungroup() %>% 
    filter(!is.na({{vartiempo}})) %>% 
    mutate(dia = lubridate::wday({{vartiempo}}, label = TRUE),
           finde = (dia %in% c("dom", "sáb"))) %>% 
    mutate(diario = ymd({{vartiempo}})) %>% 
    mutate(cuatrimestral = tsibble::yearquarter({{vartiempo}})) %>% 
    mutate(mensual = tsibble::yearmonth({{vartiempo}})) %>% 
    mutate(semanal = tsibble::yearweek({{vartiempo}})) %>% 
    mutate(subdiario = as_datetime({{vartiempo}})) %>% 
    mutate(anual = ymd(str_c(year({{vartiempo}}), "-01-01"))) 
  
  
  df <- df %>% 
    group_by(!!rlang::sym(agrupador), !! rlang::sym(intervalo)) %>%
    summarise(cantidad = n()) 
  
  df <-  tsibble::as_tsibble(df, key = !!rlang::sym(agrupador), index = !! rlang::sym(intervalo)) 
  
  df <- df %>% tsibble::fill_gaps() 
  
  result$df_ts <- df
  
  
  # Graficacion
  g <- df %>% 
    ggplot(aes(x = !! rlang::sym(intervalo), y = cantidad, fill = !!rlang::sym(agrupador)))
  
  if(geom == "puntos") {
    g <- g + geom_point(aes(color = !!rlang::sym(agrupador))) 
    if(etiquetas) {
      g <- g +  geom_text(aes(label = cantidad, alpha = 1), vjust = -0.5, hjust= 0.5, show.legend = FALSE) 
    }
  } else if(geom == "lineas"){
    g <- g + geom_line(aes(color = !!rlang::sym(agrupador))) #+  scale_colour_viridis_d()
    if(etiquetas) {
      g <- g +  geom_text(aes(label = cantidad, alpha = 1), vjust = -0.5, hjust= 0.5, show.legend = FALSE) 
    }
  } else if (geom == "barras_stack") {
    g <- g +  geom_bar(stat="identity") + scale_fill_viridis_d()  # en gemo_bar: position="fill" muestra porcentaje!
    if(etiquetas) {
      g <- g +  geom_text(aes(label = cantidad, alpha = 1), position = position_stack(vjust = 0.5), show.legend = FALSE) 
    }
  } else if (geom == "barras_fill") {
    g <- g +  geom_bar(stat="identity", position = "fill") + scale_fill_viridis_d()  # en gemo_bar: position="fill" muestra porcentaje!
    if(etiquetas) {
      g <- g +  geom_text(aes(label = cantidad, alpha = 1), position = position_stack(vjust = 0.5), show.legend = FALSE) 
    }
  } else if (geom == "area") {
    g <- g +  geom_area() + scale_fill_viridis_d()  # en position="fill" muestra porcentaje!
    if(etiquetas) {
      g <- g +  geom_text(aes(label = cantidad, alpha = 1), position = position_stack(vjust = 0.5), show.legend = FALSE) 
    }
  }
  
  if(sma) {
    g <- g + stat_smooth(color = "#FC4E07", fill = "#FC4E07", method = "loess")
  } else {
    g
  }
  
  
  if("diario" %in% colnames(df)){
    
    g <- g + scale_x_date(date_breaks = "7 day", date_labels = "%Y-%m-%d")  
    
  } else if("semanal" %in% colnames(df)){
    
    g <- g + tsibble::scale_x_yearweek(date_breaks = "4 week", date_labels = "%Y-%m-%d" )
    
  } else if("mensual" %in% colnames(df)){
    
    g <- g + tsibble::scale_x_yearmonth(date_breaks = "1 month", date_labels = "%Y-%m" )
    
  } else if("cuatrimestral" %in% colnames(df)){
    
    g <- g + tsibble::scale_x_yearquarter(date_labels = "%Y-%m")
    
  } else if("anual" %in% colnames(df)){
    
    g <- g + scale_x_date(date_breaks = "1 year", date_labels = "%Y")
    
  } else {
    
    g <- g + tsibble::scale_x_yearmonth(date_breaks = "1 month", date_labels = "%Y-%m" )
    
  }
  
  g <- g +
    theme_economist() +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.2), 
          legend.title = element_blank(), 
          axis.title.y=element_blank(), legend.text = element_text(size=20),
          strip.text = element_text(size=20), plot.title = element_text(size=20), 
          plot.subtitle = element_text(size=20), 
          plot.caption = element_text(size=9, color = "darkgrey", hjust = 1)) +
    labs(title = titulo, caption = "APGE-STJER/apge@jusentrerios.gov.ar")
  
  result$grafico <- g
  
  result
  
}
inic_nop <- function(start_date, end_date) {
  
  inicxmes <- function(df, materia) {
    
    df %>% 
      mutate(materia = materia, 
             superfuero = 'Materias no penales')
  }
  
  
  #cco1--------
  resultado <- iniciados_cco(db_con = DB_PROD(), poblacion = jdos_cco, start_date = start_date,
                             end_date = end_date, estadistico = "conteo")
  
  iniciados <- resultado$inic_cco_pc %>% inicxmes(materia = "Civil-Comercial")
  #fam---------
  resultado <- iniciados_fam(db_con = DB_PROD(), poblacion = jdos_fam,start_date = start_date, end_date = end_date, 
                             estadistico = "conteo") 
  
  inic_multif <- iniciados_multifuero_labfam(db_con = DB_PROD(), poblacion = jdos_cco,
                                             start_date = start_date, end_date = end_date, estadistico = "conteo") 
  
  multifam <- inic_multif$inic_multi_famlab_pc %>% filter(fuero == "familia") %>% select(-fuero)
  
  pazfam <- iniciados_paz_fam(db_con = DB_PROD(), poblacion = jdos_paz, start_date = start_date, end_date = end_date,
                              estadistico = "conteo") %>% .$inic_pazfam_pc
  
  resultado$inic_fam_pc <- resultado$inic_fam_pc %>% bind_rows(multifam) %>% bind_rows(pazfam)
  
  iniciados <- iniciados %>% bind_rows(resultado$inic_fam_pc %>% inicxmes(materia = "Familia"))
  #ecq---------
  resultado <- iniciados_ecq(db_con = DB_PROD(), poblacion = jdos_ecq, start_date = start_date, end_date = end_date,
                             estadistico = "conteo")
  iniciados <- iniciados %>% bind_rows( resultado$inic_ecq_pc %>% inicxmes(materia = "Quiebra_Ejecuciones"))
  
  
  #lab-----------
  resultado <- iniciados_lab(db_con = DB_PROD(), poblacion = jdos_lab, start_date = start_date, end_date = end_date,
                             estadistico = "conteo")
  
  res_multif <- iniciados_multifuero_labfam(db_con = DB_PROD(), poblacion = jdos_cco,
                                            start_date = start_date, end_date = end_date, estadistico = "conteo") 
  
  ccolabpc <- res_multif$inic_ccolab_pc %>% select(-fuero)
  
  multilab <- res_multif$inic_multi_famlab_pc %>% filter(fuero == "laboral") %>% select(-fuero)
  
  resultado$inic_lab_pc <- resultado$inic_lab_pc %>% bind_rows(ccolabpc) %>%  bind_rows(multilab)
  
  iniciados <- iniciados %>% bind_rows( resultado$inic_lab_pc %>% inicxmes(materia = "Laboral"))
  
  #paz1----------
  resultado_proc <- apgyeProcesamiento::iniciados_paz_procxgpo(db_con = DB_PROD(), poblacion = jdos_paz,
                                                               start_date = start_date, end_date = end_date, 
                                                               estadistico = "conteo")
  
  iniciados <- iniciados %>% bind_rows( resultado_proc$inic_paz_pc %>% inicxmes(materia = "Paz 1°"))
  
  #paz23---------
  resultado <- iniciados_paz_23c(poblacion = jdos_paz_23,
                                 start_date = start_date,
                                 end_date = end_date, estadistico = "conteo") 
  
  iniciados <- iniciados %>% bind_rows( resultado$inic_paz23_pc %>% rename(finicio = fecha) %>% 
                                          inicxmes(materia = "Paz 2°-3°"))
  
  #resultados-------
  
  iniciados
  
}
res_nop <- function(start_date, end_date, desagregacion_mensual = T) {
  
  res_prim <- function(db_con, poblacion, operacion = "CADR1C", start_date = "2018-07-01", end_date = "2018-09-02") {
    
    operacion = rlang::enexpr(operacion)
      
    resultado <- db_con %>%
      apgyeTableData(!! operacion) %>%
      apgyeDSL::interval(start_date, end_date) %>%
      filter(iep %in% !!poblacion$organismo) %>%
      resolverconvertidos() %>%
      mutate(fres = dmy(fres)) %>%
      collect() %>%
      filter(!(is.na(nro) & is.na(caratula))) %>%  # exclusión de registros con información crítica faltante
      filter(!is.na(fres)) %>%
      filter(fres >= data_interval_start & fres < data_interval_end) %>%
      filter(tres != "0" | is.na(tres)) %>% 
      left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                              "circunscripcion")], by = c("iep" = "organismo")) %>%
      select(circunscripcion, organismo = organismo_descripcion, everything())
    
    resultado
    
  }
  
  resxmes <- function(df, materia) {
    
    df %>% 
      filter(circunscripcion != "Total") %>% 
      rename(fecha_resolucion = fres) %>% 
      select(circunscripcion, organismo, fecha_resolucion, as) %>% # incluir TPROC
      mutate(fecha_resolucion = ymd(fecha_resolucion)) %>% 
      mutate(as = toupper(as)) %>% 
      mutate(as = ifelse(str_detect(as, "S"), "Sentencias", "Autos")) %>% 
      mutate(as = ifelse(is.na(as), "Autos", as)) %>% 
      mutate(materia = materia, 
              superfuero = 'Materias no penales')
  }
  
  #cco1--------
  resultado <- res_prim(db_con = DB_PROD(), 
                                poblacion = jdos_cco,
                                start_date = start_date,
                                end_date = end_date)
  
  resueltos <- resultado %>% resxmes(materia = "Civil-Comercial")
  #fam---------
  resultado <- res_prim(db_con = DB_PROD(), poblacion = jdos_fam,
                                start_date = start_date, end_date = end_date)
  
  resueltos <- resueltos %>% bind_rows(resultado %>% resxmes(materia = "Familia"))
  
  #ecq---------
  resultado <- res_prim(db_con = DB_PROD(), poblacion = jdos_ecq,
                                             start_date = start_date, end_date = end_date)
  
  resueltos <- resueltos %>% bind_rows( resultado %>% resxmes(materia = "Quiebra_Ejecuciones"))
  #lab-----------
  resultado <- res_prim(db_con = DB_PROD(), operacion = "CADR1L", poblacion = jdos_lab, 
                                start_date = start_date, end_date = end_date)
  
  resueltos <- resueltos %>% bind_rows( resultado %>% resxmes(materia = "Laboral"))
  
  #paz1----------
  resultado <-  res_prim(db_con = DB_PROD(), poblacion = jdos_paz, 
                                                    start_date = start_date, end_date = end_date)
  
  resueltos <- resueltos %>% bind_rows( resultado %>% resxmes(materia = "Paz 1°"))
  
  #paz23---------
  resultado <-  res_prim(db_con = DB_PROD(), poblacion = jdos_paz_23,
                                                       start_date = start_date, end_date = end_date)
  
  resueltos <- resueltos %>% bind_rows( resultado %>% resxmes(materia = "Paz 2°-3°"))
  
  
  resueltos
  #violencias_rejucav--------------------------------------------------------
  viol_pob <- poblacion_total %>% 
    filter(str_detect(organismo, "jdofam|jdopaz")) %>% 
    filter(!organismo %in% org_Inactivos)
  
  rejucav <- movimientos_violencias(db_con = DB_REJUCAV(), poblacion = viol_pob,
                                  start_date = start_date, end_date = end_date) %>% 
    .$primarias %>% filter(movimiento == "SENTENCIA") %>% 
    select(circunscripcion, organismo, fecha_resolucion = fecha_movimiento, as = movimiento, movimientos_tipo) %>% 
    mutate(tproc = ifelse(movimientos_tipo == "Mujer", "VIOLENCIA C/MUJER", "VIOLENCIA FAMILIAR")) %>% 
    mutate(as = "Autos", superfuero = 'Materias no penales') %>% 
    left_join(viol_pob %>% select(organismo, categoria)) %>% 
    mutate(materia = case_when(
      categoria == 1 ~  "Paz 1°", 
      categoria == 2 | categoria == 3 ~  "Paz 2°-3°",
      TRUE ~  "Familia")) %>% 
    select(-categoria, -movimientos_tipo)
  
   resueltos <- resueltos %>% bind_rows(rejucav)
}

```

```{r}
# Panorama Provincial de la Circunscripcion----------------------------------
# Participacion Circuncripcion sobre Poblacional-----------------------------
poblacion <- readxl::read_excel("~/apgyeinformes/Modelo_Parametrizado/data/poblacion.xls") %>% slice(4:26) %>% drop_na() 
colnames(poblacion) <- poblacion[1, ]
poblacion <- poblacion %>% 
  select(Departamento, {{poblacion_proyect_año}}) %>% 
  filter(Departamento %in% c("Total", {{circunscripcion}}))

pje_circuncripcion_objetivo =  round(poblacion[2,2]/poblacion[1,2], digits = 2) %>% .[[1]]
pje_circuncripcion_objetivo = pje_circuncripcion_objetivo * 100

# participacion produccion tabla--------------------------------------------
all_results <- tbl(DB_PROD(), "all_results") %>% collect() 

results_circ <- all_results %>% 
  filter(circunscripcion == {{circunscripcion}})

productos_circ_xaño <- results_circ %>% 
  group_by(año, tproducto) %>% 
  summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
  mutate(unidad_analisis = {{circunscripcion}})

results_demas <- all_results %>% 
  filter(circunscripcion != {{circunscripcion}})

productos_meancirc_xaño <- results_demas %>% 
  group_by(circunscripcion, año, tproducto) %>% 
  summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
  group_by(año, tproducto) %>% 
  summarise(cantidad = round(mean(cantidad, na.rm=T))) %>% 
  mutate(unidad_analisis = "Promedio_Pcial" )

productos_total_xaño <- all_results %>% 
  group_by(año, tproducto) %>% 
  summarise(cantidad = sum(cantidad, na.rm = T)) %>% 
  mutate(unidad_analisis = "Total_Pcial")
  
tabla_resumen_producto <- productos_circ_xaño %>% 
  bind_rows(productos_total_xaño) %>% 
  bind_rows(productos_meancirc_xaño) %>% 
  pivot_wider(names_from = "unidad_analisis", values_from = "cantidad") %>% 
  filter(!is.na(año))

tabla_resumen_producto$pje_participacion_sobre_producto_total <- round((tabla_resumen_producto[[{{circunscripcion}}]] /
  tabla_resumen_producto[["Total_Pcial"]]) * 100, digits = 2)

tabla_resumen_producto$producto_circ_cada_1mil_habitantes <- round(tabla_resumen_producto[[{{circunscripcion}}]] / 1000, digits = 2)
  
tabla_resumen_producto <- tabla_resumen_producto %>% 
  arrange(tproducto, año) %>% 
  rename(producto = tproducto)

# Participacion produccion gráfico----------------------------------------

producto_serie <- results_circ %>% 
  filter(tproducto == "sentencias") %>% 
  mutate(periodo = as.Date(str_c(año, "-",mes, "-01"))) %>% 
  group_by(periodo) %>% 
  summarise(cantidad_sentencias = sum(cantidad, na.rm = T)) %>% 
  mutate(unidad_analisis = {{circunscripcion}}) %>% 
  bind_rows(results_demas %>% 
              filter(tproducto == "sentencias") %>% 
              mutate(periodo = as.Date(str_c(año, "-",mes, "-01"))) %>% 
              group_by(periodo) %>% 
              summarise(cantidad_sentencias = sum(cantidad, na.rm = T)) %>% 
              mutate(unidad_analisis = "Demás_Circunscripciones"))

grafico_producto_serie <- producto_serie %>%
  filter(month(periodo) != 1) %>% 
  mutate(cantidad_sentencias = round(cantidad_sentencias/1000, digits = 1)) %>% 
  ggplot(aes(x = as.Date(periodo), y = cantidad_sentencias, 
             fill = factor(unidad_analisis, levels=c("Demás_Circunscripciones", {{circunscripcion}}), ordered=TRUE))) +
  geom_bar(stat="identity") +
  scale_fill_brewer(palette="Dark2") +
  geom_text(aes(label = cantidad_sentencias, alpha = 1), position = position_stack(vjust = 0.5), show.legend = FALSE) +
  tsibble::scale_x_yearmonth(date_breaks = "1 month", date_labels = "%Y-%m") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.2), 
        axis.title.x=element_blank(),
        legend.title = element_blank(), legend.position = "top",
        axis.title.y=element_blank(), legend.text = element_text(size=14),
        strip.text = element_text(size=16), plot.title = element_text(size=14), 
        plot.subtitle = element_text(size=14)) +
  labs(title = str_c("Participación mensual de sentencias dictadas en ", {{circunscripcion}}, " respecto de las demás circunscripciones"), 
       subtitle = "Cantidad de sentencias expresadas en unidades de mil")
       
```

```{r}

año_actual <- year(start_date)
año_1antes <- año_actual - 1
año_2antes <- año_actual - 2
# ingresar nombre circunscripcion como nombre de columna, seleec
tabla_resumen_producto <- tabla_resumen_producto %>% 
  mutate(pje_prod = round((!!sym(circunscripcion))*100/Total_Pcial, digits = 1))
pje_sent_actual <- tabla_resumen_producto %>% 
  filter(año == año_actual &
           producto == "sentencias") %>% pull(pje_prod)
pje_sent_1antes <- tabla_resumen_producto %>% 
  filter(año == año_1antes &
           producto == "sentencias") %>% pull(pje_prod)
pje_sent_2antes <- tabla_resumen_producto %>% 
  filter(año == año_2antes &
           producto == "sentencias") %>% pull(pje_prod)
pje_aud_1antes <- tabla_resumen_producto %>% 
  filter(año == año_1antes &
           producto == "audiencias_realizadas") %>% pull(pje_prod)
pje_aud_2antes <- tabla_resumen_producto %>% 
  filter(año == año_2antes &
           producto == "audiencias_realizadas") %>% pull(pje_prod)

tendencia_pje <- if_else(pje_sent_actual > pje_sent_1antes, "superior", "inferior")

source("~/apgyeinformes/R/load_datamarts.R")
source("~/apgyeinformes/R/variables.R")

# se tomarán los datos del DataMart al 18/05/2023
dm_circ_periodo <- datamart_penal$resoluciones_garantia$ac %>%
  filter(circunscripcion == !!circunscripcion) %>% 
  filter(año_mes >= format(as.Date(start_date), "%Y-%m") &
           año_mes < format(as.Date(end_date), "%Y-%m"))
dm_cond_efect <- sum(dm_circ_periodo$condena_efectiva) + sum(dm_circ_periodo$condena_efectiva_sustit)
dm_cond_cond <- sum(dm_circ_periodo$condena_condicional)
dm_sobr <- sum(dm_circ_periodo$sobreseimientos)
dm_elev_j <- sum(dm_circ_periodo$elevacion_juicio)

# agregar San Salvador cuando circ es Concordia
dmss_circ_periodo <- datamart_penal$resoluciones_garantia$ac %>%
  filter(circunscripcion == "San Salvador") %>% 
  filter(año_mes >= format(as.Date(start_date), "%Y-%m") &
           año_mes < format(as.Date(end_date), "%Y-%m"))
dmss_cond_efect <- sum(dmss_circ_periodo$condena_efectiva) + sum(dmss_circ_periodo$condena_efectiva_sustit)
dmss_cond_cond <- sum(dmss_circ_periodo$condena_condicional)
dmss_sobr <- sum(dmss_circ_periodo$sobreseimientos)
dmss_elev_j <- sum(dmss_circ_periodo$elevacion_juicio)

# actuaciones en Tribunal de Juicio
dmtj_circ_periodo <- datamart_penal$resoluciones_tribunalja$tjui %>%
  filter(circunscripcion == !!circunscripcion) %>% 
  filter(año_mes >= format(as.Date(start_date), "%Y-%m") &
           año_mes < format(as.Date(end_date), "%Y-%m"))
dmtj_cond_efect <- sum(dmtj_circ_periodo$condena_efectiva)
dmtj_cond_efect_sust <- sum(dmtj_circ_periodo$condena_efectiva_sustit)
dmtj_cond_cond <- sum(dmtj_circ_periodo$condena_condicional)
dmtj_sobr <- sum(dmtj_circ_periodo$sobreseimientos)
dmtj_absol <- sum(dmtj_circ_periodo$absoluciones)

# Resoluciones de Casación Penal
dmcas_circ_periodo <- datamart_penal$resoluciones_casacion %>%
  filter(circunscripcion == !!circunscripcion) %>% 
  filter(año_mes >= format(as.Date(start_date), "%Y-%m") &
           año_mes < format(as.Date(end_date), "%Y-%m"))
dmcas_sent <- sum(dmcas_circ_periodo$sentencias)
dmcas_autos <- sum(dmcas_circ_periodo$autos)

# Resoluciones de Ejecución de Penas
dmep_circ_periodo <- datamart_penal$resoluciones_ejeucicionpenal %>%
  filter(circunscripcion == !!circunscripcion) %>% 
  filter(año_mes >= format(as.Date(start_date), "%Y-%m") &
           año_mes < format(as.Date(end_date), "%Y-%m"))
dmep_salidas <- sum(dmep_circ_periodo$`Salidas excepcionales` +
  dmep_circ_periodo$`Salidas por estudio` +
  dmep_circ_periodo$`Salidas Socio-Familiares` +
  dmep_circ_periodo$`Salidas Socio-Laborales`)
dmep_lib_cond <- sum(dmep_circ_periodo$`Libertad condicional`)
dmep_lib_asis <- sum(dmep_circ_periodo$`Libertad asistida`)
dmep_hc <- sum(dmep_circ_periodo$`Recurso de Hábeas Corpus`)

# audiencias Ejecución Penal
dmep_aud <- datamart_penal$audiencias_xtipo_ejecucionpenal$resultado %>%
  filter(circunscripcion == !!circunscripcion) %>% 
  filter(año_mes >= format(as.Date(start_date), "%Y-%m") &
           año_mes < format(as.Date(end_date), "%Y-%m"))
dmep_aud_total <- sum(dmep_aud$Total)

# Totales actos
dmep_actos <- sum(dmep_circ_periodo$Total)

# resoluciones en penal juvenil
dmrpj <- datamart_penal$resoluciones_penalmenores$penjuv %>%
  filter(circunscripcion == !!circunscripcion) %>% 
  filter(año_mes >= format(as.Date(start_date), "%Y-%m") &
           año_mes < format(as.Date(end_date), "%Y-%m"))
dmrpj_total <- sum(dmrpj %>%
                     mutate(sum = rowSums(across(where(is.numeric)))) %>%
                     pull(sum))
dmrpj_oga <- datamart_penal$resoluciones_penalmenores$oga %>%
  filter(circunscripcion == !!circunscripcion) %>% 
  filter(año_mes >= format(as.Date(start_date), "%Y-%m") &
           año_mes < format(as.Date(end_date), "%Y-%m"))

if (nrow(dmrpj_oga) == 0){
  dmrpj_oga_total <- 0
} else {
  dmrpj_oga_total <- sum(dmrpj_oga %>%
                           mutate(sum = rowSums(across(where(is.numeric)))) %>%
                           pull(sum))
}
# al 18/5 da cero (0) de pen juv en oga


```

## Observaciones generales de la Circunscripción   

Para ponderar la importancia que tiene la circunscripción `r {{circunscripcion}} ` podemos observar que la misma concentra el  `r pje_circuncripcion_objetivo`% de la población de Entre Ríos. Comparado con la producción judicial (en este documento representada por las *sentencias dictadas* y  las *audiencias realizadas*), la circunscripción aportó el año `r año_1antes` el `r pje_sent_1antes`% de las sentencias que se dictaron en la provincia, y en el `r año_2antes` el `r pje_sent_2antes`%. Para el transcurso del año `r año_actual` observamos un porcentaje **`r tendencia_pje`** al año `r año_1antes` de **14,4%** (relativo a los meses feb-abr). En cuanto a su participación en materia de audiencias celebradas durante los años `r año_1antes` y `r año_2antes` la jurisdicción aportó el `r pje_aud_1antes`% y el `r pje_aud_2antes`% respectivamente. Para el transcurso del año `r año_actual` observamos un porcentaje del 20%.


## Observaciones Particulares por Fuero en el Año `r año_actual`

Las observaciones aquí señaladas surgen del documento adjunto con el detalle particular de indicadores mensuales para cada organismo: *Informe General Concordia*.   

**Observaciones en materia Civil y Comercial**

No se observan valores anómalos en los plazos para dictado de resoluciones (i.e. los juzgados civiles tiene porcentajes de resoluciones vencidas inferiores al 10% -conforme pauta establecida por la Sala Civil STJER). En segunda instancia los porcentajes de vencimiento son inferiores al **2%**.   

En cuanto a causas Pendientes de resolución y vencidas, el Juzgado N° 2 dos causas pendientes vencidas según última declaración de datos.

**Observaciones en materia Quiebra y Ejecuciones**

No se observan valores anómalos en el dictado de resoluciones, el porcentaje de resoluciones vencidas es del **0%**. El juzgado no tiene causas pendientes de resolución vencidas según su última declaración de datos de abril 2023.     

**Observaciones en materia Familia**

No se observan valores anómalos en el dictado de resoluciones en los juzgados de familia. El Juzgado 1 tiene un **8%** de resoluciones vencidas durante el año 2023.   

Respecto de causas a despacho pendientes de resoluciones, en general no se observan valores anómalos. El juzgado de familia 1 tiene 1 causa pendiente de resolución vencida conforme su última declaración de datos de abril 2023.

**Observaciones en materia Paz**

No se observan valores anómalos en el dictado de resoluciones el juzgado de Paz de Concordia. Este organismo tiene 1 causa pendiente vencida.

Respecto del Juzgado de Paz de 3a Categoría de Estancia Grande el organismo informó en todo el período (feb-2023 a abr-2023) 6 causas ingresada de Violencia y 217 trámites voluntarios iniciados.   

**Observaciones en materia Laboral**

Se observan valores anómalos en el dictado de resoluciones en los juzgados laborales 2 y 4 con un total del **31%**  y **10%** de resoluciones vencidas respectivamente. En segunda instancia se observa un **2%** en resoluciones vencidas.

Respecto de causas a despacho pendientes de resoluciones: el juzgado 2 tiene **14** causas a despacho con plazo vencido (40%), mientras que el juzgado 4 tiene **41** causas con plazo vencido (58.6%), según última declaración de datos de abril 2023. El detalle **causa por causa** puede consultarte en el Informe General (*ver página 60 del Informe General*).


**Observaciones en materia Penal**

En materia de resoluciones en Garantías se dictaron en Concordia: 

- `r dm_cond_efect` condenas efectivas, 
- `r dm_cond_cond` condenas condicionales, 
- `r dm_sobr` sobreseimientos, y 
- `r dm_elev_j` elevaciones a juicio.

Resoluciones vinculadas a San Salvador registraron:

- `r dmss_cond_efect` condenas efectivas, 
- `r dmss_cond_cond` condenas condicionales, 
- `r dmss_sobr` sobreseimientos, y 
- `r dmss_elev_j` elevaciones a juicio.

Respecto de la actuación en juicios del Tribunal de Juicio se observan: 

- `r dmtj_cond_efect` codenas efectivas,
- `r dmtj_cond_efect_sust` condena efectiva sustitutiva, 
- `r dmtj_cond_cond` condenas condicionales,
- `r dmtj_sobr` sobreseimientos, y
- `r dmtj_absol` absoluciones.

Respecto de la actuación de la Cámara de Casación Penal se dictaron `r dmcas_sent` sentencias definitivas y `r dmcas_autos` autos. El porcentaje de resoluciones vencidas en la Cámara de Casación Penal es de **0%**. 

En cuanto a Ejecución Penal se observan las siguientes cantidades de resoluciones por tipo:

- `r dmep_salidas` salidas, 
- `r dmep_lib_cond` libertad condicional,
- `r dmep_lib_asis` libertad asistida y 
- `r dmep_hc` recursos de habeas corpus. 

Además se realizaron `r dmep_aud_total` audiencias en materia de ejecución y `r dmep_actos` actos en general. 

Finalmente, los juzgados de familia dictaron en materia penal juvenil un total de `r dmrpj_total` resoluciones en el `r año_actual`.  

\pagebreak

##  Tabla de Resultados de Producción de la Circunscripción en relación al Valores Provinciales

```{r}
tabla_resumen_producto %>% 
  select(-pje_prod) %>% 
  kable(caption = str_c("Tabla de Resultados de Producción de la Circunscripción en relación a Valores Provinciales"," (",
                         getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) %>% 
  row_spec(0, angle = 90) 
  

```

\blandscape

```{r, fig.height = 10, fig.width = 16, fig.align = "center"}
grafico_producto_serie
```

\elandscape


## Movimientos 

```{r, movimientos_proc, eval = movimientos, message=FALSE, include=FALSE, warning=FALSE, echo=FALSE}
# Consutla BD-------------------------------------------------------------------

movimientos_db <- tbl(DB_PROD(), "movimientos") %>% 
  filter(fecha_hora >= as.Date("2022-02-01"), fecha_hora <= as.Date("2023-12-01")) %>% 
  left_join(tbl(DB_PROD(), "lookupentidades") %>% rename(iep = organismo)) %>% 
  collect()

# Procesamiento-----------------------------------------------------------------
ap_clean <- movimientos_db %>% 
  mutate(fecha = as.Date(ymd_hms(fecha_hora))) %>% 
  filter(fecha >= as.Date("2022-02-01"), fecha < end_date) %>% 
  distinct()

mov_etiquetado <- ap_clean %>% 
  left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion",
                                                            "circunscripcion", "tipo", "materia")], 
            by = c("iep" = "organismo"))

mov_etiquetado <- mov_etiquetado %>% 
  mutate(circunscripcion = ifelse(circunscripcion == "Entre Ríos", "Paraná", circunscripcion)) %>% 
  filter(circunscripcion %in% c("Paraná", "Concordia", "Gualeguaychú", "Uruguay")) %>% 
  mutate(
    # instancia = case_when(
    # str_detect(tipo, "jdo|jez|oga|oma|tja|tra") ~ "primera_instancia", 
    # str_detect(tipo, "cam") ~ "segunda_instancia", 
    # str_detect(tipo, "stj|sal") ~ "superior_instancia", 
    # TRUE ~ "sd"), 
    materia = case_when(
      str_detect(materia, "cco") ~ "Civil_Comercial", 
      str_detect(materia, "cqb|eje" ) ~ "Quiebra_Ejecuciones", 
      str_detect(materia, "cad") ~ "Contencioso_Administrativo", 
      str_detect(materia, "fam") ~ "Familia", 
      str_detect(materia, "lab") ~ "Laboral", 
      str_detect(materia, "paz") ~ "Paz", 
      str_detect(materia, "pen|pep") ~ "Penal", 
      str_detect(materia, "pco") ~ "Constitucional(apelacion)",
      TRUE ~ "sd"))

resumen <- mov_etiquetado %>% 
  mutate(tipo_movimiento = case_when(
    tipo_movimiento == "procesal" | is.na(tipo_movimiento) ~ "actos_procesales",
    tipo_movimiento == "procesal_presentacion" ~"presentaciones_abogados")) %>% 
  group_by(circunscripcion, materia, fecha, tipo_movimiento) %>% 
  summarise(cantidad = n()) %>% 
  mutate(fecha_mov = fecha) %>% 
  mutate(fecha = as.Date(tsibble::yearmonth(fecha)))

graf_mov_xcirc <- resumen %>% 
  filter(!is.na(circunscripcion)) %>% 
  ggplot(aes(x = fecha, y = cantidad, fill = tipo_movimiento)) + 
  geom_bar(stat="identity") +
  scale_fill_viridis_d(begin = 0, end = .75) +
  #theme_economist() +
  scale_x_date(date_labels  = "%d-%b-%y", date_breaks = "2 month" , expand = c(0,0))  +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 18), axis.text.y = element_text(size = 16), 
        axis.title.x=element_blank(), axis.title.y = element_blank(), 
        legend.text = element_text(size=20), legend.position = "top", legend.title = element_blank(), 
        strip.text = element_text(size=20), plot.title = element_text(size=18, margin=margin(0,0,10,0)), 
        plot.subtitle = element_text(size = 14, margin=margin(0,0,60,0)), 
        panel.grid.major.y = element_line(colour = "grey", linetype = "dotted"),
        plot.margin = unit(c(2,2,2,2), "cm"), plot.caption = element_text(colour = "darkgray", size = 16, hjust = 0,  margin=margin(50,0,0,0))) + 
  labs(title = "Actos procesales y presentaciones digitales por Circunscripción") +
  facet_wrap(~circunscripcion)  

graf_mov_xmat <- resumen %>% 
  filter(!is.na(circunscripcion)) %>% 
  filter(circunscripcion == "Concordia") %>% 
  ggplot(aes(x = fecha, y = cantidad, fill = tipo_movimiento)) + 
  geom_bar(stat="identity") +
  scale_fill_viridis_d(begin = 0, end = .75) +
  #theme_economist() +
  scale_x_date(date_labels  = "%d-%b-%y", date_breaks = "2 month" , expand = c(0,0))  +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 18), axis.text.y = element_text(size = 16), 
        axis.title.x=element_blank(), axis.title.y = element_blank(), 
        legend.text = element_text(size=20), legend.position = "top", legend.title = element_blank(), 
        strip.text = element_text(size=20), plot.title = element_text(size=18, margin=margin(0,0,10,0)), 
        plot.subtitle = element_text(size = 14, margin=margin(0,0,60,0)), 
        panel.grid.major.y = element_line(colour = "grey", linetype = "dotted"),
        plot.margin = unit(c(2,2,2,2), "cm"), plot.caption = element_text(colour = "darkgray", size = 16, hjust = 0,  margin=margin(50,0,0,0))) + 
  labs(title = "Actos procesales y presentaciones digitales por Materia en Concordia") +
  facet_wrap(~materia)  
```

\blandscape

```{r, movimientos_g_xcirc, eval = movimientos, message=FALSE, warning=FALSE, echo=FALSE, fig.height = 12, fig.width = 18, fig.align = "center"}
graf_mov_xcirc
```

```{r, movimientos_g_xmat, eval = movimientos, message=FALSE, warning=FALSE, echo=FALSE, fig.height = 12, fig.width = 18, fig.align = "center"}
graf_mov_xmat
```


```{r}
#-----------------------Segunda Parte-----------------------------------------
```

```{r}
intervalo = "mensual"
geom = "lineas"
sma = F
etiquetas = T
```

## Primera Instancia: Resoluciones en materias No-Penales

```{r, resoluciones_nop_proc,  message=FALSE, include=FALSE, warning=FALSE, echo=FALSE}
res_nop_resultado <- res_nop(start_date = as.Date("2022-02-01"), end_date = end_date) %>% 
  filter(circunscripcion == {{circunscripcion}}) %>% 
  mutate(mes = month(fecha_resolucion)) %>% 
  filter(mes != 1)
```

```{r, resoluciones_nop_g, message=FALSE, warning=FALSE, echo=FALSE, fig.height = 12, fig.width = 18, fig.align = "center"}
# inconsistencia detectada con el informe de seguimiento basado en datos procesados y no en primaria consolidada: Ver
res_nop_as <- res_nop_resultado %>% 
  serietemp(vartiempo = fecha_resolucion, agrupador = "as", intervalo = intervalo, geom = geom , sma = sma, etiquetas = etiquetas, 
              titulo = "Resoluciones dictadas en 1ª instancia en Concordia en materias no penales ") %>% .$grafico
res_nop_as

```

```{r, resolucionesxmat_nop_g, message=FALSE, warning=FALSE, echo=FALSE, fig.height = 12, fig.width = 18, fig.align = "center"}
res_nop_mat <- res_nop_resultado %>% 
  serietemp(vartiempo = fecha_resolucion, agrupador = "materia", intervalo = intervalo, geom = geom , sma = sma, etiquetas = etiquetas, 
              titulo = "Resoluciones dictadas en 1ª instancia en Concordia en materias no penales") %>% .$grafico
res_nop_mat

```


## Primera Instancia: Resoluciones en materia Penal

```{r, resoluciones_p_proc, message=FALSE, include=FALSE, warning=FALSE, echo=FALSE}
res_p_resultado <- resoluciones_pen(db_con = DB_PROD(), poblacion = oga, start_date = as.Date("2022-02-01"), end_date = end_date,
                                      desagregacion_mensual = T) %>%  .$gtia_res_pc %>% filter(!is.na(fres)) %>% 
  left_join(oga %>% select(organismo, circunscripcion), by = c("iep" = "organismo")) %>% 
  mutate(ac_res = ifelse(tres %in% c("6","10","11"), "actos_conclusivos", "resoluciones")) %>% 
  rename(fecha_resolucion = fres) %>% 
  filter(circunscripcion == {{circunscripcion}}) 
  
```

```{r, resoluciones_p_g, message=FALSE, warning=FALSE, echo=FALSE, fig.height = 12, fig.width = 18, fig.align = "center" }
res_p_resultado %>% 
   serietemp(vartiempo = fecha_resolucion, agrupador = "ac_res", intervalo = intervalo, geom = geom , sma = sma, etiquetas = etiquetas, 
              titulo = "Resoluciones dictadas en Concordia en Garantías Penal ") %>% .$grafico
```

\elandscape

```{r}
# https://rpubs.com/chidungkt/702513
# library(tidyverse)
# library(waffle) # remotes::install_github("hrbrmstr/waffle")
# library(patchwork)
# library(lubridate)
# library(extrafont)
# #library(ggtext)
# library(Cairo)
# 
# my_font <- "Roboto Condensed"
# 
# def_plot <- grafico_producto_serie / graf_mov_xcirc
# 
# def_plot +
#   plot_annotation(title = "Toronto Shelters: An Infographic Using R",
#                   subtitle = "As of 2019 there were 62 shelters in Toronto.",
#                   caption = "Data: open.toronto.ca", 
#                   theme = theme(plot.title = element_text(color = "#292929", size = 22, face = "bold", family = my_font),
#                                 plot.subtitle = element_text(color = "#292929", size = 14, family = my_font),
#                                 plot.caption = element_text(color = "grey50", face = "bold.italic", family = my_font),
#                                 plot.background = element_rect("#EBEBEB"),
#                                 panel.background = element_rect("#EBEBEB")))
# 
# # Save the plot: 
# ggsave("Shelters.png",
#        width = 21,
#        height = 30,
#        units = "cm",
#        dpi = 500,
#        type = "cairo-png")



```




