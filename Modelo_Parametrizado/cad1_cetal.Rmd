---
title: Causas en Trámite
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
entramite <- function(db_con, poblacion) {
  
  primer_dia_ano_antes <- paste0(lubridate::year(end_date) - 1, '-01-01')
  primer_dia_ano_actual <- paste0(lubridate::year(end_date), '-01-01')
  
  hace_10_años = as.Date(end_date) - lubridate::years(10)
  hace_5_años = as.Date(end_date) - lubridate::years(5)
  hace_2_años = as.Date(end_date) - lubridate::years(2)
  
  
  resultado <- db_con %>% 
    # en trámite al 31/12 año antes a end_date
    apgyeDSL::apgyeTableData("CETAL_XL") %>% 
    apgyeDSL::interval(primer_dia_ano_antes, primer_dia_ano_actual) %>% 
    filter(iep %in% !!poblacion$organismo) %>% 
    filter(!grepl("OFICIO|EXHORTO", tproc)) %>% 
    mutate(fmov = dmy(fmov), finicio = dmy(finicio)) %>%
    ungroup()  %>%
    select(iep, nro, caratula, tproc, finicio)  %>% 
    collect()
  
  resultado
}

resultado <- entramite(db_con = DB_PROD(), poblacion = cam_cad) 
 
# resto los archivados desde 1/1 año actual hasta end_date
  
sentencias <- DB_PROD() %>%
  apgyeTableData("CADRCAD") %>%
  apgyeDSL::interval("2018-06-01", end_date) %>%
  filter(iep %in% !!cam_cad$organismo) %>%
  resolverconvertidos() %>%
  mutate(fdesp = dmy(fdesp), fres = dmy(fres), fvenc = dmy(fvenc)) %>%
  collect() %>%
  mutate(med_mej_prov = ifelse(tres == "0", 1, NA)) %>% 
  mutate(control_intervalo =  fres >= data_interval_start & fres < data_interval_end) %>%
  filter(!is.na(fres)) %>%
  filter(toupper(as) == "S") %>% 
  filter(tres != 0)
  
  
if (nrow(sentencias) != 0) {
  resultado  <- resultado %>% 
  anti_join(sentencias, by = c( "iep", "nro"))
  } else {
    resultado <- resultado
  }

resultado <- resultado %>% 
  rename(organismo = iep) %>% 
  left_join(apgyeJusEROrganization::listar_organismos()[, c("organismo", "organismo_descripcion", 
                                                              "circunscripcion")], by = "organismo") %>% 
  select(-organismo) %>% 
  select(organismo_descripcion, finicio, nro, caratula, tproc) %>% 
  arrange(finicio)

```

## Causas en trámite 

Cantidad de Causas en Trámite según última declaración de datos.

```{r, cad1_cetal_resumen, eval= (ce & t)}
resultado %>% 
  rename(organismo = organismo_descripcion) %>% 
  group_by(organismo) %>% 
  summarise(cantidad = n()) %>% 
  kable(caption = "Causas en trámite", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 9)  
```


Tabla que presenta las causas en trámite en el organismo. Para dicho conteo se utilizó el informe de causas en trámtie al 31/12, al cual se le restaron todas aquellas causas con sentencia desde el 01/06/18. La tabla se ordena por fecha de inicio registrada.


```{r, cad1_cetal_t, eval= (ce & t)}
resultado %>% 
  rename(organismo = organismo_descripcion) %>% 
  filter(str_detect(organismo, "Paraná")) %>% 
  mutate(organismo = abbreviate(organismo, minlength = 12)) %>%
  mutate(caratula = abbreviate(caratula, minlength = 30)) %>% 
  mutate(tproc = abbreviate(tproc, minlength = 20)) %>% 
  kable(caption = "Causas en trámite", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 9)  %>% 
  kable_styling(latex_options = c("repeat_header")) %>% 
  column_spec(4, "10cm") %>% column_spec(5, "8cm") %>% 
  row_spec(0, angle = 90) %>%
  landscape() 

```

