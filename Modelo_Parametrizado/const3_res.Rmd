---
title: Resoluciones 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r const3_res_script}
resultado <- resoluciones_pco(db_con = DB_PROD(), poblacion = stj_pco,
                              start_date = start_date, end_date = end_date,
                              desagregacion_mensual = desagregacion_mensual) 

existe_enero22 <- F # Agregamos los datos de Enero 2022 a la tabla anterior recalculando indicadores
if (existe_enero22){
  resultado2 <- leerArchivoPresentado("~/apgyeinformes/Modelo_Parametrizado/data/const3_enero22.TXT", isDBF = F)
  
  res_enero22_prim <- resultado2 %>% 
    mutate(
      año_mes = "2022-01",
      fres = dmy(`F. RESOL.`),
      venc = dmy(`F. VTO.`)
    ) %>% 
    mutate(
      'causas resueltas' = (fres >= ymd("2022-01-01") & 
                                  fres <= ymd("2022-01-31")),
      "sentencias" = `A/S` %in% c('s','S'),
      "autos" = `A/S` %in% c('a','A'),
      "resoluciones a término" = fres <= venc,
      "resoluciones vencidas" = fres > venc
    ) 
    
  res_enero22 <- res_enero22_prim %>%   
    group_by(año_mes) %>% 
    summarise(
      'causas resueltas' = sum(`causas resueltas`),
      "sentencias" = sum(sentencias),
      "autos" = sum(autos),
      "resoluciones a término" = sum(`resoluciones a término`),
      "resoluciones vencidas" = sum(`resoluciones vencidas`)
    ) %>% 
    mutate(`resol vencidas` = stringr::str_c(round(`resoluciones vencidas`/`causas resueltas`*100), ' %')) %>% 
    mutate(
      circunscripcion = "Entre Ríos",
      organismo = "STJER Procedimientos Constitucionales"
    ) %>% 
    select(circunscripcion, organismo, everything())

  resultado <- resultado %>% 
    filter(circunscripcion != "Total") %>% 
    bind_rows(res_enero22) %>% 
    arrange(año_mes) %>% 
    janitor::adorn_totals("row") %>% 
    mutate(`resol vencidas` = stringr::str_c(round((`resoluciones vencidas`)/`causas resueltas`*100), ' %'))
  
  }

# ¿Mostrar Resoluciones por Objeto de Demanda? Taiga: 1785
muestra_xodem <- F 

# Muestra Causas donde éste vocal haya emitido voto. Taiga 1801
# NULL para cuando no se deba mostrar
muestra_causas_vocal <- NULL #  c("786") 786 - Mizawak

if (muestra_xodem | !is.null(muestra_causas_vocal)){
  resultadopc <- DB_PROD() %>% apgyeDSL::apgyeTableData('CADR3PC') %>%
    apgyeDSL::interval(start_date, end_date) %>%
    collect()
  
  resultadopc <- resultadopc %>% select(-c(id_submission, data_interval_start, data_interval_end))
}


```


```{r results_const3, eval = results}
all_results(resultado, "constitucional")
```


```{r const3_res_script_historico, eval= (cr & ymd(start_date) < as.Date("2019-08-01"))}
# Consulta resoluciones anteriores a agosto-2019 de amparos en Sala Penal

  res_hist <- resoluciones_pen(db_con = DB_PROD(), poblacion = sal_pen, 
                              start_date = start_date, end_date = end_date,
                              desagregacion_mensual = desagregacion_mensual) 
  
  res_pc_historico <- res_hist$resultadopc
  
  # Consolidado 
  resultado <- res_pc_historico %>% 
  filter(circunscripcion != "Total") %>% select(-'resol vencidas') %>% 
  bind_rows(resultado %>% 
              filter(circunscripcion != "Total") %>% select(-'resol vencidas')) %>% 
  select(-c(circunscripcion, 'resoluciones a término', 'resoluciones vencidas')) %>% 
  janitor::adorn_totals("row") 

```

## Resoluciones en materia Constitucional `r getDataIntervalStr(start_date, end_date)` 

En agosto 2019 por Ley 10704 se establece una nueva integración del tribunal.

```{r const3_res_t, eval= (cr & t)}

if (as.Date("2021-01-01") %within% interval(start_date, as.Date(end_date) - months(1))){
  # Mostrar Amparos Resueltos de Ene2021 cuando Start_Date es Feb2021
  # Traer datos estaticos resultante en Informes/Desarrollo/T1223_Amparos_Ene2021.R
  
  # causas_adespacho causas_resueltas res_xsentencia res_xauto res_atermino res_luego1venc
  #               40               43             41         2           34              9
  vencidas <- stringr::str_c(round((9)/43*100), ' %')

  tibble::tibble('circunscripcion' = 'Entre Ríos', 'organismo' = 'STJER Procedimientos Constitucionales',
                  'año_mes' = '2021-01', 'causas resueltas' = 43, 'sentencias' = 41, 'autos' = 2,
                  'resoluciones a término' = 34, 'resoluciones vencidas' = 9, 'resol vencidas' = vencidas) %>% 
    bind_rows(resultado) %>%
    filter(!circunscripcion == "Total") %>% 
    arrange(circunscripcion, organismo, año_mes) %>%
    janitor::adorn_totals("row") %>%
    rename(resol_vencidas = 'resol vencidas',
           resoluciones_vencidas = 'resoluciones vencidas',
           causas_resueltas = 'causas resueltas') %>% 
    mutate(resol_vencidas = stringr::str_c(round((resoluciones_vencidas)/causas_resueltas*100), ' %')) %>% 
    rename('resol vencidas' = resol_vencidas,
           'resoluciones vencidas' = resoluciones_vencidas,
           'causas resueltas' = causas_resueltas) %>%
    outputTable(caption = "Resoluciones") %>%
    row_spec(0, angle = 90) 
  
} else {
  
  resultado %>% 
    outputTable(caption = "Resoluciones") %>%
    row_spec(0, angle = 90) 
}


  
```

\blandscape

```{r const3_res_st_mes, eval = (cr & (("mes" %in% stag) | ("org" %in% stag))),  fig.width=19, fig.height=16}
resultado %>%
  ts_as_g(agrupar = "mes")
```

\elandscape

```{r script_proc, eval = (cr & td), echo = FALSE, warning=FALSE, message=FALSE}
# Complemento Ad_hoc para incorporar detalle sobre duraciones
# amparos_stj <- readr::read_delim("/home/scastillo/apgyeinformes/Modelo_Parametrizado/data/amparos_stj.csv", 
#                           "\t", escape_double = FALSE, trim_ws = TRUE)
amparos_stj <- DB_PROD() %>% apgyeDSL::apgyeTableData('CADR3PC') %>%
    apgyeDSL::interval(start_date, end_date) %>%
    filter(iep %in% !!stj_pco$organismo) %>% 
    filter(iep != "salpen0100pna") %>% 
    collect()  

colnames(amparos_stj) <- c("nro", "caratula", "as", "integ", "fdesp", "fvenc", "fres", "obs") 

amparos_stj <- amparos_stj %>% 
  select(nro, caratula, as,integ, fdesp,fvenc,fres,obs) %>% 
  mutate(fres = dmy(fres), 
         fdesp = dmy(fdesp),
         fvenc = dmy(fvenc),
         finicio = dmy(str_sub(obs, 1,10))) %>% 
  select("nro","caratula","as","integ","finicio","fdesp","fvenc","fres") %>% 
  mutate(duracion = fres - finicio, 
         vencida = fres > fvenc) %>% 
  filter(duracion > 0 & duracion < 365) %>% # se eliminan negativos y duraciones mayores al año considerándoselas errores de registro
  mutate(duracion_xintervalos = case_when(
    between(duracion, 0, 10) ~ "de 0 a 10 días", 
    between(duracion, 11, 30) ~ "de 11 a 30 días", 
    between(duracion, 31, 60) ~ "de 31 a 60 días", 
    TRUE ~ "más de 60 días")) %>% 
  mutate(plazo_res = ifelse(vencida, "vencida", "a_término")) %>% 
  mutate(año_mes = format(fres, "%Y-%m")) %>% 
  filter(toupper(as) == "S") %>% 
  group_by(nro) %>% 
  slice(which.min(fres)) %>%  # elimino causas más de una sentencia informada, quedándome con la primera sentencia.
  arrange(año_mes, fres)
  
amparos_stj_resumen <- amparos_stj %>% 
  group_by(año_mes) %>% 
  summarise(cantidad_causas_resueltas = n(),
            promedio_duracion = round(mean(duracion))) %>% 
  mutate(promedio_duracion = as.numeric(promedio_duracion))
  
```

`r if (cr & td) '## Duracion de Procesos "Con Sentencia" en días Corridos'`

```{r const_res_td_explic, eval = (cr & td),  results='asis'}
#ej1:text <- "this is  \nsome  \ntext"
#cat(text)
cat("A continuación presentamos el promedio mensual de duraciones de causas con sentencia. El presente indicador comienza a implementarse a partir de noviembre 2020, por lo que los casos considerados son aquellos cuya senentecia se dictó a partir del 01/11/2020. Para calcular ese promedio se identificaron todas las causas informadas con sentencia por el organismo. En caso de haber informado una causa con más de una sentencia, se tomó la fecha de la primera sentencia para el cálculo de la duración. La duración se calcula desde la fecha de inicio del legajo hasta la fecha de resoluciones en días corridos. ")
```

```{r tabla, eval = (cr & td)}
amparos_stj_resumen %>% 
  kable(caption = "Causa Resueltas con Sentencia y Promedio de Duraciones", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  
```

```{r tabla_xinterv, eval = (cr & td)}
amparos_stj %>% 
  group_by(duracion_xintervalos) %>% 
  summarise(cantidad_causas = n()) %>% 
  kable(caption = "Cantidad de causa resuelstas por intervalos de duración de procesos", align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10)  
```


```{r res_xodem, eval = muestra_xodem}

colnames(resultadopc) <- try(c("nro","caratula",
                               "as","vact","fdesp","fvenc",
                               "fres","obs", "iep"))

if (existe_enero22){
  colnames(resultado2) <- try(c("nro","caratula",
                               "as","vact","fdesp","fvenc",
                               "fres","obs"))
  resultadopc <- resultado2 %>% 
    bind_rows(resultadopc %>% 
                select("nro","caratula",
                               "as","vact","fdesp","fvenc",
                               "fres","obs"))
}
resultadopc <- resultadopc %>% 
  # rename(magistrado = vact) %>%
  mutate(fres = dmy(fres)) %>%
  mutate(fdesp = as.Date(fdesp, format="%d/%m/%Y")) %>% 
  mutate(fvenc = as.Date(fvenc, format="%d/%m/%Y")) %>% 
  filter(fres >= start_date & fres < end_date) %>% 
  filter(!is.na(fres)) %>%
  distinct()

resultadopc <- resultadopc %>% 
  tidyr::separate(obs, into = c("fi1", "fi", "obs", "odem", "tdem"), sep = "\\$") %>% 
  mutate(fi1 = str_remove_all(fi1, "fi1:")) %>% 
  mutate(fi = str_remove_all(fi, "fi:")) %>% 
  mutate(obs = str_remove_all(obs, "obs:")) %>% 
  mutate(odem = str_remove_all(odem, "odem:")) %>% 
  mutate(tdem = str_remove_all(tdem, "tdem:"))

resultado_proc <- resultadopc %>% 
  mutate(objeto = case_when(
    odem == "1"	~ "SALUD", 
    odem == "2"	~ "HABERES", 
    odem == "3"	~ "JUBILACIONES", 
    odem == "4"	~ "SUBSIDIOS/PENSIONES", 
    odem == "5"	~ "COLEGIOS PROFESIONALES	", 
    odem == "6"	~ "DERECHO AMBIENTAL", 
    odem == "7"	~ "DERECHO A LA EDUCACIÓN", 
    odem == "8"	~ "MORA ADMINISTRATIVA", 
    odem == "9"	~ "ACCESO A INFO. PUBLICA", 
    odem == "10"	~ "COMPETENCIA", 
    odem == "11"	~ "CONCURSO", 
    odem == "12"	~ "ELECCIONES", 
    odem == "13"	~ "EMPLEO PÚBLICO", 
    odem == "14"	~ "SERVICIOS PÚBLICOS", 
    odem == "15"	~ "SERVICIO DE TELEF./INTERNET", 
    odem == "16"	~ "IMPUGNAC.(ORD,DEC y/o RES)", 
    odem == "17"	~ "SANCIONES ADMINISTRATIVAS", 
    odem == "18"	~ "PLANTEO CONTRA PARTICULARES", 
    odem == "19"	~ "PODER DE POLICÍA", 
    odem == "20"	~ "MULTAS", 
    odem == "21"	~ "OTROS", 
    TRUE ~ "sin_dato")) %>% 
  mutate(caratula = str_sub(caratula, 1,10)) %>%
  mutate(mes = lubridate::month(fres)) %>%
  mutate(año = lubridate::year(fres)) %>%
  arrange(año, mes) %>%
  mutate(año_mes = str_c(año, mes, sep = "-")) %>%
  mutate(en_plazo = fvenc >= fres) %>% 
  mutate(en_plazo = ifelse(en_plazo, "si", "no")) %>% 
  mutate(organismo = "STJER-Constitucional") 

resultado_proc <- resultado_proc %>% 
  select(organismo, año_mes, nro, caratula, fecha_despacho = fdesp, 
         fecha_vencimiento = fvenc, fecha_resolucion = fres, auto_sentencia = as, 
         # votos = magistrado,
         objeto, en_plazo)

resultado_proc_resumen_xobjeto <- resultado_proc %>% 
  group_by(objeto) %>% 
  summarise(cantidad = n()) %>% 
  arrange(desc(cantidad)) %>% 
  janitor::adorn_totals("row")

resultado_proc_resumen_xobjeto %>% 
  kable(caption = "Resoluciones dictadas según Objeto de la Demanda", align = 'c', longtable = TRUE) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F) #, font_size = 8)

```

<!-- ## Causas donde haya votado la Dra. Mizawak -->

```{r causas_vocal, eval = !is.null(muestra_causas_vocal)}


resultadopc <- resultadopc %>% 
  mutate(año_mes = format(dmy(`F. RESOL.`), "%Y-%m")) %>% 
  rename(causa = NEXPTE.,
         orden_voto = `VOCAL 1VOTO`) %>% 
  select(causa, CARATULA, orden_voto, "F. RESOL.", `año_mes`) %>% 
  bind_rows(res_enero22_prim %>% 
              select(causa = NEXPTE., CARATULA, orden_voto = `VOCAL 1VOTO`, "F. RESOL.", `año_mes`)) %>% 
  filter(grepl(muestra_causas_vocal, orden_voto))

tabla_1 <- resultadopc %>%
  group_by(año_mes) %>%
  summarise(cantidad = n(),
            causas = str_c(causa, collapse = ", ")) %>%
  janitor::adorn_totals() %>% 
  arrange(año_mes)

tabla_1 %>%
  outputTable(caption = "Causas por mes") %>%
  column_spec(3, width = "35em")

tabla_2 <- resultadopc %>% 
  # select(-iep) %>% 
  mutate(CARATULA = str_sub(CARATULA, 1,15),
         `F. RESOL.` = format(dmy(`F. RESOL.`), "%Y-%m-%d"))

# se eliminan comas que esperan idagentes
tabla_2$orden_voto <- str_replace(tabla_2$orden_voto, ",,$", "")
tabla_2$orden_voto <- str_replace(tabla_2$orden_voto, ",$", "")

tabla_2 <- tabla_2 %>% 
  codconver("CONST3", variable = "orden_voto") %>% 
  group_by(año_mes) %>% 
  select(año_mes, causa, caratula = CARATULA, orden_voto, `fecha resolución` = `F. RESOL.`) %>% 
  arrange(año_mes, causa)
  
tabla_2 %>%
  outputTable(caption = "Causas detalladas") %>% 
  row_spec(0, angle = 90) %>% 
  landscape()

```

