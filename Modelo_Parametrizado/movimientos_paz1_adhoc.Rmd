---
title: Informe sobre Movimientos
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output:
  pdf_document:
    includes:
      in_header: header.tex 
geometry: margin=1cm
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```

```{r, ad_hoc_params}
start_date = "2022-02-01"
end_date = "2022-12-31"

source("../R/informe.R")
source("../R/habiles.R")
source("../R/poblacion.R")

# pob = jdos_ecq  %>% filter(str_detect(organismo_descripcion, "10")) %>% 
#   mutate(organismo_descripcion = "Jdo Civ y Com 10")
pob = jdos_paz
```

```{r, abrir_df}
library(readr)
library(lubridate)
library(gghighlight)

# Procesamiento-----------------------------------------------------------------
movimientos_db <- tbl(DB_PROD(), "movimientos") %>% 
  filter(fecha_hora >= start_date, fecha_hora <= end_date) %>% 
  filter(!(dia_s %in% c("sábado", "domingo"))) %>% 
  left_join(tbl(DB_PROD(), "lookupentidades") %>% rename(iep = organismo)) %>% 
  filter(iep %in% !!pob$organismo) %>% 
  mutate(fecha = as.Date(fecha_hora)) %>% 
  distinct() %>% 
  mutate(tipo_movimiento = case_when(
    tipo_movimiento == "procesal" | is.na(tipo_movimiento) ~ "actos_procesales",
    tipo_movimiento == "procesal_presentacion" ~"presentaciones_abogados")) %>% 
  group_by(iep, fecha, tipo_movimiento) %>% 
  summarise(cantidad = n()) %>% 
  collect() %>% 
  left_join(pob %>% select(organismo, organismo_descripcion, circunscripcion, tipo, materia), by = c("iep" = "organismo")) %>% 
  rename(organismo = organismo_descripcion) 



resumen_cc1_xcirc <- movimientos_db %>% 
  group_by(fecha, circunscripcion, tipo_movimiento) %>% 
  summarise(cantidad = sum(cantidad, na.rm = T)) 

resumen_cc_xorg <- movimientos_db %>% 
  group_by(fecha, circunscripcion,  organismo, tipo_movimiento) %>% 
  summarise(cantidad = sum(cantidad, na.rm = T))  

# Gráfico-----------------------------------------------------------------------

resumen_c1xorg_plot <- resumen_cc_xorg %>% 
  ggplot(aes(x = fecha, y = cantidad, fill = tipo_movimiento)) + 
  geom_bar(stat="identity") +
  scale_fill_viridis_d(begin = 0, end = .75) +
  scale_x_date(date_labels  = "%d-%b-%y", date_breaks = "1 month" , expand = c(0,0))  +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, size = 12), 
        axis.text.y = element_text(size = 16), 
        axis.title.x=element_blank(), 
        axis.title.y = element_blank(), 
        legend.text = element_text(size=18), legend.position = "top",
        legend.title = element_blank(), 
        strip.text = element_text(size=16), 
        plot.title = element_text(size=18), 
        plot.subtitle = element_text(size = 16),
        panel.grid = element_blank(), 
        panel.grid.major.y = element_line(colour = "grey", linetype = "dotted")) +
  labs(title = "Actos procesales y presentaciones diarias en los órganos judiciales",
       subtitle = "Cada barra es un día.") +
  facet_wrap(~organismo, ncol=2)

tabla_l1xcirc_prom <- resumen_cc1_xcirc %>% 
  filter(tipo_movimiento == "actos_procesales") %>% 
  group_by(circunscripcion) %>% 
  summarise(actos_procesales_realizados = round(mean(cantidad), digits = 2)) %>% 
  arrange(desc(actos_procesales_realizados)) %>%
  rename('promedio diario de actos procesales'=actos_procesales_realizados) 


tabla_cc1xorg_prom <- resumen_cc_xorg %>% 
  #filter(tipo_movimiento == "actos_procesales") %>% 
  group_by(circunscripcion, organismo, tipo_movimiento) %>% 
  summarise(cantidad = round(mean(cantidad), digits = 2)) %>% 
  pivot_wider(names_from = tipo_movimiento, values_from = cantidad) %>% 
  arrange(desc(actos_procesales)) %>% 
  rename('promedio diario de actos procesales' = actos_procesales, 
         'promedio diario de presentaciones abogados' = presentaciones_abogados) 


```

# Promedio Diario de Actos Procesales y Presentaciones Profesionales Digitales en cada juzgado `r apgyeProcesamiento::getDataIntervalStr(start_date, end_date)`

```{r}
tabla_cc1xorg_prom %>% 
  kable(caption = str_c("Promedio Diario de Actos y Presentaciones "," (", getDataIntervalStr(start_date, end_date), ")"), align = 'c', longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"),
                full_width = F, font_size = 10) %>% 
  column_spec(3, "2cm") %>% column_spec(4, "2cm") 
```

<!-- \blandscape -->
<!-- # Gráfico de Actos Procesales y Presentaciones Profesionales diarios por circunscripción `r apgyeProcesamiento::getDataIntervalStr(start_date, end_date)` -->

<!-- ```{r, fig.width=19, fig.height=16} -->
<!-- resumen_c1xorg_plot -->
<!-- ``` -->
<!-- \elandscape -->