---
title: Resoluciones en Casación 
subtitle: 
author: Área Planificación, Gestión y Estadística
date: "`r format(Sys.Date(), '%d de %B de %Y') `"
output: 
    pdf_document:
      includes:
            in_header: header.tex 
mainfont: Liberation Sans
documentclass: article
sansfont: Liberation Sans
df_print: kable  
---

```{r include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning=FALSE, message=FALSE )
options(knitr.kable.NA = '')
```


```{r}
spec_color2 <- function(x, alpha = 1, begin = 0, end = 1,
         direction = 1, option = "D",
         na_color = "#BBBBBB", scale_from = NULL,
         palette = viridisLite::viridis(256, alpha, begin, end, direction, option)) {
  n <- length(palette)
  if (is.null(scale_from)) {
    x <- round(scales::rescale(x, c(1, n)))
  } else {
    x <- round(scales::rescale(x, to = c(1, n),
                       from = scale_from))
  }
  
  color_code <- palette[x]
  color_code[is.na(color_code)] <- na_color
  return(color_code)
}


resultado <- DB_PROD() %>% apgyeDSL::apgyeTableData('CADRC_v2') %>%
  apgyeDSL::interval(start_date, end_date) %>%
  filter(iep %in% !!cam_pen$organismo) %>% 
  left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
              mutate(voto_1 = apellido, iep_actuante = iep) %>%
              select(idagente,  voto_1), by=c("voto1"="idagente")) %>% 
  left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
              mutate(voto_2 = apellido, iep_actuante = iep) %>%
              select(idagente, voto_2), by=c("voto2"="idagente")) %>% 
  left_join(DB_PROD() %>% tbl('gh_personal_distinct') %>%
              mutate(voto_3 = apellido, iep_actuante = iep) %>%
              select(idagente, voto_3), by=c("voto3"="idagente")) 
  
  
if(desagregacion_mensual) {

  resultado <- resultado %>%  group_by(iep, data_interval_start, data_interval_end)
  
  } else {

  resultado <- resultado %>% group_by(iep)
  
  }

resultado_prim <- resultado %>%
  collect(CADRC_v2) %>% 
  mutate(finicio = lubridate::dmy(finicio), fing = lubridate::dmy(fing),
         fvenc = lubridate::dmy(fvenc), fres = lubridate::dmy(fres)) %>% 
  # selección de casos con fecha de resolución, etiquetando medidas para mejor proveer, filtrando casos con 
  # información faltante en nor y caratula
  filter(!is.na(fres)) %>% 
  mutate(med_mej_prov = ifelse(tres == "0", 1, NA)) %>%  # validacion medida mej.prov
  filter(!(is.na(nro) & is.na(caratula))) %>%  # exclusión de registros con información crítica faltante
  mutate(es_igual_inic_ing = finicio == fing) %>% 
  mutate(duracion_inic_resol = as.integer(fres - finicio),
         año_mes = format(as.Date(data_interval_start), "%Y-%m")) %>% 
  ungroup() %>% 
  left_join(cam_pen %>% select(organismo, organismo_descripcion, circunscripcion), by=c("iep"="organismo")) 
  

durac_negativa = resultado_prim %>% 
  filter(duracion_inic_resol < 0) %>% 
  select(circunscripcion, año_mes, nro, caratula, finicio, f_ingreso = fing ,f_vencim = fvenc, f_resolucion = fres) %>% 
  mutate(caratula = str_sub(caratula, 1, 12))  

anomalias = resultado_prim %>% 
  filter(!es_igual_inic_ing) %>% 
  select(circunscripcion, año_mes, nro, caratula, finicio, f_ingreso = fing ,f_vencim = fvenc, f_resolucion = fres) %>% 
  mutate(caratula = str_sub(caratula, 1, 12)) %>% 
  arrange(circunscripcion, año_mes)

mmp = resultado_prim %>% 
  filter(med_mej_prov == 1) %>% 
  select(circunscripcion, año_mes, nro, caratula, finicio, f_ingreso = fing ,f_vencim = fvenc, f_resolucion = fres) %>% 
  mutate(caratula = str_sub(caratula, 1, 12))

write.table(resultado_prim, "~/apgyeinformes/Modelos_AdHoc/data/resoluciones_casacion.csv",
            row.names = F, col.names = T, sep = ";", na = "")
            
resultado_prim <- resultado_prim %>% 
  filter(duracion_inic_resol > 0 & es_igual_inic_ing) %>% 
  mutate(caratula = str_sub(caratula, 1, 12)) %>% 
  select(circunscripcion, año_mes, nro, caratula, 
         finicio, f_ingreso = fing ,f_vencim = fvenc, f_resolucion = fres, as,
         voto_1, voto_2, voto_3, duracion_inic_resol, -es_igual_inic_ing)
```

## Causas resueltas y su duración desde su inicio hasta la fecha de resolución `r getDataIntervalStr(start_date, end_date)`

La siguiente tabla muestra las causas resueltas por órgano y la duración en días corridos desde su inicio hasta la fecha de resolución.  

```{r}
resultado_prim %>%
  kable(caption = "Resoluciones dictadas por órgano y su duración en días corridos", longtable = TRUE ) %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 9) %>%
  kable_styling(latex_options = c("repeat_header")) %>% 
  column_spec(13, color = "white", background = spec_color2(resultado_prim$duracion_inic_resol, 
                                        palette = paletteer_c("scico::berlin", n= length(resultado_prim$duracion_inic_resol)))) %>% 
  column_spec(4, "3cm") %>% 
  column_spec(10, "2cm") %>% 
  column_spec(11, "2cm") %>% 
  column_spec(12, "2cm") %>% 
  row_spec(0, angle = 90) %>%
  landscape()
```

\pagebreak

## Validación de datos `r getDataIntervalStr(start_date, end_date)`

En esta sección consignamos anomalías detectadas en materia de datos registrados.   

\pagebreak

```{r}
mmp %>%
  kable(caption = "Medias para mejor proveer dictadas") %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 9)
```


```{r}
durac_negativa %>%
  kable(caption = "Fecha resolución anterior fecha de ingreso") %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 9) 
```

\pagebreak

```{r}
anomalias %>%
  kable(caption = "Fecha de Inicio distinta a fecha Ingreso") %>%
  kable_styling(bootstrap_options = c("striped", "hover", "condensed"), full_width = F, font_size = 9) 
```

\pagebreak


