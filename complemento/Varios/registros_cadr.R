# https://jeroen.github.io/mongolite/query-data.html#select-by-date
# https://www.mongodb.com/languages/mongodb-and-r-example

library(mongolite)
conM = "mongodb://10.101.2.97:27017/?readPreference=primary&ssl=false"
#conM = "mongodb://10.101.2.97:27017/?readPreference=primary&ssl=false?sockettimeoutms=1200000"
expedientes = mongo(collection ="expedientes", db="expedientes_production", url = conM)
#organismos = mongo(collection ="expedientes", db="expedientes_production", url=conM)
library(glue)

# primer pipe-----

pipeline = '
[
    {
        "$match": {
            "$and": [
                {
                    "lex_id.codigo_organismo": {
                        "$in": [
                            "felcyc01", "parcyc01", "parcyc02", "parcyc03", "parcyc04"
                        ]
                    }
                }, {
                    "grupo": {
                        "$eq": "EnTramite"
                    }
                }, {
                    "movimientos.registros": {
                        "$elemMatch": {
                            "tregdscr": {
                                "$regex": "APGE", 
                                "$options": "i"
                            }, 
                            "fec4": {
                                "$gt": "2000-03-01", 
                                "$lte": "2022-04-31"
                            }
                        }
                    }
                }
            ]
        }
    }, {
        "$project": {
            "circunscripcion": "$datos_organismo.jurisdiccion", 
            "localidad": "$datos_organismo.localidad", 
            "organismo": "$datos_organismo.nombre_organismo", 
            "organismo_codigo": "$lex_id.codigo_organismo", 
            "actora": 1, 
            "demandada": 1, 
            "nro_exp": "$nro.exp1", 
            "tipo_proceso": "$tipo_proceso.tipo", 
            "tipo_proceso_padre0": {
                "$arrayElemAt": [
                    "$tipo_proceso.padres", 0
                ]
            }, 
            "tipo_proceso_padre1": {
                "$arrayElemAt": [
                    "$tipo_proceso.padres", 1
                ]
            }, 
            "inicio": 1, 
            "registros": {
                "$filter": {
                    "input": "$movimientos.registros", 
                    "cond": {
                        "$and": [
                            {
                                "$ne": [
                                    "$$this", []
                                ]
                            }
                        ]
                    }
                }
            }, 
            "justiciables": {
                "$concatArrays": [
                    "$justiciables"
                ]
            }
        }
    }, {
        "$unwind": {
            "path": "$registros"
        }
    }, {
        "$unwind": {
            "path": "$registros"
        }
    }, {
        "$match": {
            "registros.tregdscr": {
                "$regex": "APGE", 
                "$options": "i"
            }, 
            "registros.fec4": {
                "$gt": "2022-02-01", 
                "$lte": "2022-04-31"
            }
        }
    }, {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$$ROOT", "$registros"
                ]
            }
        }
    }
]
'

# segundo pipe------
# componer el pipeline con glue en una función con iep, data_interval_start-data_interval_end


pstar = '
[
    {
        "$match": {
            "$and": [
                {
                    "lex_id.codigo_organismo": {
                        "$in": ['
                        
pend = ']
                    }
                }, {
                    "grupo": {
                        "$eq": "EnTramite"
                    }
                }, {
                    "movimientos.registros": {
                        "$elemMatch": {
                            "tregdscr": {
                                "$regex": "APGE", 
                                "$options": "i"
                            }, 
                            "fec4": {
                                "$gt": "2022-03-01", 
                                "$lt": "2022-04-01"
                            }
                        }
                    }
                }
            ]
        }
    }, {
        "$project": {
            "circunscripcion": "$datos_organismo.jurisdiccion", 
            "localidad": "$datos_organismo.localidad", 
            "organismo": "$datos_organismo.nombre_organismo", 
            "organismo_codigo": "$lex_id.codigo_organismo", 
            "actora": 1, 
            "demandada": 1, 
            "nro_exp": "$nro.exp1", 
            "tipo_proceso": "$tipo_proceso.tipo", 
            "tipo_proceso_padre0": {
                "$arrayElemAt": [
                    "$tipo_proceso.padres", 0
                ]
            }, 
            "tipo_proceso_padre1": {
                "$arrayElemAt": [
                    "$tipo_proceso.padres", 1
                ]
            }, 
            "inicio": 1, 
            "registros": {
                "$filter": {
                    "input": "$movimientos.registros", 
                    "cond": {
                        "$and": [
                            {
                                "$ne": [
                                    "$$this", []
                                ]
                            }
                        ]
                    }
                }
            }, 
            "justiciables": {
                "$concatArrays": [
                    "$justiciables"
                ]
            }
        }
    }, {
        "$unwind": {
            "path": "$registros"
        }
    }, {
        "$unwind": {
            "path": "$registros"
        }
    }, {
        "$match": {
            "registros.tregdscr": {
                "$regex": "APGE", 
                "$options": "i"
            }, 
            "registros.fec4": {
                "$gt": "2022-02-01", 
                "$lte": "2022-04-31"
            }
        }
    }, {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$$ROOT", "$registros"
                ]
            }
        }
    }
]
'

org = '"parcyc05"'

pipeline = paste0(
  pstar, 
  porg, 
  pend
  )

# tercer pipe: full parametrizado match stage----
# params: iep, fdeps, fres, nombrereg, activo, 

pipeline_modelo <- '
[
    {
        "$match": {
            "$and": [
                {
                    "lex_id.codigo_organismo": {
                        "$in": [
                            "diavlsmpaz2"
                        ]
                    }
                }, {
                    "grupo": {
                        "$eq": "EnTramite"
                    }
                }, {
                    "movimientos.registros": {
                        "$elemMatch": {
                            "tregdscr": {
                                "$regex": "APGE", 
                                "$options": "i"
                            }, 
                            "fec4": {
                                "$gt": "2022-02-01", 
                                "$lt": "2022-12-01"
                            }, 
                            "num1": {
                                "$eq": "0"
                            }
                        }
                    }
                }
            ]
        }
    }, {
        "$project": {
            "circunscripcion": "$datos_organismo.jurisdiccion", 
            "localidad": "$datos_organismo.localidad", 
            "organismo": "$datos_organismo.nombre_organismo", 
            "organismo_codigo": "$lex_id.codigo_organismo", 
            "actora": 1, 
            "demandada": 1, 
            "nro_exp": "$nro.exp1", 
            "tipo_proceso": "$tipo_proceso.tipo", 
            "tipo_proceso_padre0": {
                "$arrayElemAt": [
                    "$tipo_proceso.padres", 0
                ]
            }, 
            "tipo_proceso_padre1": {
                "$arrayElemAt": [
                    "$tipo_proceso.padres", 1
                ]
            }, 
            "inicio": 1, 
            "registros": {
                "$filter": {
                    "input": "$movimientos.registros", 
                    "cond": {
                        "$and": [
                            {
                                "$ne": [
                                    "$$this", []
                                ]
                            }
                        ]
                    }
                }
            }, 
            "justiciables": {
                "$concatArrays": [
                    "$justiciables"
                ]
            }
        }
    }, {
        "$unwind": {
            "path": "$registros"
        }
    }, {
        "$unwind": {
            "path": "$registros"
        }
    }, {
        "$match": {
            "registros.tregdscr": {
                "$regex": "APGE", 
                "$options": "i"
            }, 
            "registros.fec4": {
                "$gt": "2022-02-01", 
                "$lte": "2022-12-01"
            }
        }
    }, {
        "$replaceRoot": {
            "newRoot": {
                "$mergeObjects": [
                    "$$ROOT", "$registros"
                ]
            }
        }
    }
]
'







pipeline0 = '
[
  {
    "$match": {
      "$and": [
        {
          "lex_id.codigo_organismo": {
            "$in":'
#pipeline1 = '"felcyc01", "parcyc01", "parcyc02", "parcyc03", "parcyc04"'

pipeline2 = '}
        }, {
          "grupo": {
            "$eq": "EnTramite"
          }
        }, {
          "movimientos.registros": {
            "$elemMatch": {
              "tregdscr": {
                "$regex": "APGE", 
                "$options": "i"
              }, 
              "fec4": {
                "$gt": "2022-09-01", 
                "$lt": "2022-11-01"
              }, 
              "num1": {
                "$eq": "1"
              }
            }
          }
        }
      ]
    }
  }, {
    "$project": {
      "circunscripcion": "$datos_organismo.jurisdiccion", 
      "localidad": "$datos_organismo.localidad", 
      "organismo": "$datos_organismo.nombre_organismo", 
      "organismo_codigo": "$lex_id.codigo_organismo", 
      "actora": 1, 
      "demandada": 1, 
      "nro_exp": "$nro.exp1", 
      "tipo_proceso": "$tipo_proceso.tipo", 
      "tipo_proceso_padre0": {
        "$arrayElemAt": [
          "$tipo_proceso.padres", 0
        ]
      }, 
      "tipo_proceso_padre1": {
        "$arrayElemAt": [
          "$tipo_proceso.padres", 1
        ]
      }, 
      "inicio": 1, 
      "registros": {
        "$filter": {
          "input": "$movimientos.registros", 
          "cond": {
            "$and": [
              {
                "$ne": [
                  "$$this", []
                ]
              }
            ]
          }
        }
      }, 
      "justiciables": {
        "$concatArrays": [
          "$justiciables"
        ]
      }
    }
  }, {
    "$unwind": {
      "path": "$registros"
    }
  }, {
    "$unwind": {
      "path": "$registros"
    }
  }, {
    "$match": {
      "registros.tregdscr": {
        "$regex": "APGE", 
        "$options": "i"
      }, 
      "registros.fec4": {
        "$gt": "2022-02-01", 
        "$lte": "2022-04-31"
      }
    }
  }, {
    "$replaceRoot": {
      "newRoot": {
        "$mergeObjects": [
          "$$ROOT", "$registros"
        ]
      }
    }
  }
]
'


#pipeline1 = '"felcyc01", "parcyc01", "parcyc02", "parcyc03", "parcyc04"'
df = tbl(DB_PROD(), "lookupentidades") %>% collect() 
df$codigo_organismo[1:3]
#pipeline1 = jsonlite::toJSON("diavlsmpaz2")
pipeline1 = jsonlite::toJSON(df$codigo_organismo[1:3])
pipeline = paste0(pipeline0,pipeline1,pipeline2)


# ejecucion-----
registros_modelo <- expedientes$aggregate(
  pipeline_modelo #,
  #options = '{"allowDiskUse":true}'
)


registros <- expedientes$aggregate(
  pipeline #,
  #options = '{"allowDiskUse":true}'
)


# 4to pipeline----
# Construccion de array de organismo para mongo desde R poblacion











# glue-----
# `glue()` can also be used in user defined functions
intro <- function(name, profession, country){
  glue("My name is {name}, a {profession}, from {country}")
}
intro("Shelmith", "Senior Data Analyst", "Kenya")
#> My name is Shelmith, a Senior Data Analyst, from Kenya
intro("Cate", "Data Scientist", "Kenya")
#> My name is Cate, a Data Scientist, from Kenya


