import psycopg2
from psycopg2 import Error
import os

# set working directory
os.chdir('/home/scastillo/apgyeinformes/justat2')

#establishing the connection
conn = psycopg2.connect(
   database= "justat2.2", user='postgres', password='Toba201*', host='192.168.254.221')

#Creating a cursor object using the cursor() method
cursor = conn.cursor()

#Executing an MYSQL function using the execute() method
cursor.execute("select version()")
data = cursor.fetchone()
print("Connection established to: ",data)

# New querry
s = ""
s += "SELECT"
s += " table_schema"
s += ", table_name"
s += " FROM information_schema.tables"
s += " WHERE"
s += " ("
s += " table_schema = 'public'"
s += " )"
s += " ORDER BY table_schema, table_name;"

cursor.execute(s)
list_tables = cursor.fetchall()

for t_name_table in list_tables:
    print(t_name_table + "\n")
